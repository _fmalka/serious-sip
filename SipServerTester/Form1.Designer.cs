﻿namespace SIPServerTester
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssIndicator = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssSeparator = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.lvCalls = new System.Windows.Forms.ListView();
            this.colID = new System.Windows.Forms.ColumnHeader();
            this.colFrom = new System.Windows.Forms.ColumnHeader();
            this.colTo = new System.Windows.Forms.ColumnHeader();
            this.colDir = new System.Windows.Forms.ColumnHeader();
            this.colState = new System.Windows.Forms.ColumnHeader();
            this.colDigits = new System.Windows.Forms.ColumnHeader();
            this.colAction = new System.Windows.Forms.ColumnHeader();
            this.colRtpTx = new System.Windows.Forms.ColumnHeader();
            this.colRtpRx = new System.Windows.Forms.ColumnHeader();
            this.colDuration = new System.Windows.Forms.ColumnHeader();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsbAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.tsbSearch = new System.Windows.Forms.ToolStripTextBox();
            this.tsbClear = new System.Windows.Forms.ToolStripButton();
            this.tsbLogFile = new System.Windows.Forms.ToolStripButton();
            this.tsbClearLog = new System.Windows.Forms.ToolStripButton();
            this.tsbLogLevel = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsbShowErrorsOnly = new System.Windows.Forms.ToolStripButton();
            this.LogBox = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.tbOneLog = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCancelled = new System.Windows.Forms.Label();
            this.lblTimeout = new System.Windows.Forms.Label();
            this.lblHangup = new System.Windows.Forms.Label();
            this.lblConnected = new System.Windows.Forms.Label();
            this.lblTrying = new System.Windows.Forms.Label();
            this.lblCalling = new System.Windows.Forms.Label();
            this.lblAnswered = new System.Windows.Forms.Label();
            this.lblAccepted = new System.Windows.Forms.Label();
            this.lblOffered = new System.Windows.Forms.Label();
            this.Dialpad = new System.Windows.Forms.Panel();
            this.btnDigP = new System.Windows.Forms.Button();
            this.btnDig0 = new System.Windows.Forms.Button();
            this.btnDigS = new System.Windows.Forms.Button();
            this.btnDig7 = new System.Windows.Forms.Button();
            this.btnDig8 = new System.Windows.Forms.Button();
            this.btnDig9 = new System.Windows.Forms.Button();
            this.btnDig4 = new System.Windows.Forms.Button();
            this.btnDig5 = new System.Windows.Forms.Button();
            this.btnDig6 = new System.Windows.Forms.Button();
            this.btnDig3 = new System.Windows.Forms.Button();
            this.btnDig2 = new System.Windows.Forms.Button();
            this.btnDig1 = new System.Windows.Forms.Button();
            this.Inputs = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.tbSpeak = new System.Windows.Forms.TextBox();
            this.tbIdentity = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbDigitsStr = new System.Windows.Forms.TextBox();
            this.tbRecordDir = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPlaybackFile = new System.Windows.Forms.TextBox();
            this.tbTransferTo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNewCallTo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tsbStart = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbNewCall = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbDtmfMode = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsbStart1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsbStop = new System.Windows.Forms.ToolStripButton();
            this.tbsAutoAnswer = new System.Windows.Forms.ToolStripButton();
            this.tbsAutoRun = new System.Windows.Forms.ToolStripButton();
            this.tbsUsePVD = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbNewCall1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tbsAcceptCall = new System.Windows.Forms.ToolStripButton();
            this.tbsAnswerCall = new System.Windows.Forms.ToolStripButton();
            this.tbsHoldCall = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsbRetrieveCall = new System.Windows.Forms.ToolStripButton();
            this.tsbBlindTransfer = new System.Windows.Forms.ToolStripButton();
            this.tsbAttendedTransfer = new System.Windows.Forms.ToolStripButton();
            this.tsbDropCall = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbsSendDigits = new System.Windows.Forms.ToolStripButton();
            this.tbsGetDigits = new System.Windows.Forms.ToolStripButton();
            this.tbsPlay = new System.Windows.Forms.ToolStripButton();
            this.tbsRecord = new System.Windows.Forms.ToolStripButton();
            this.tbsSpeak = new System.Windows.Forms.ToolStripButton();
            this.tbsSpeak1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tbsMusic = new System.Windows.Forms.ToolStripButton();
            this.tbsBridge = new System.Windows.Forms.ToolStripButton();
            this.tbsStop = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbLogLevel_ = new System.Windows.Forms.ToolStripDropDownButton();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.Dialpad.SuspendLayout();
            this.Inputs.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssIndicator,
            this.tssLabel1,
            this.tssSeparator,
            this.tssLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 639);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1344, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssIndicator
            // 
            this.tssIndicator.BackColor = System.Drawing.Color.Red;
            this.tssIndicator.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssIndicator.Enabled = false;
            this.tssIndicator.ForeColor = System.Drawing.Color.Red;
            this.tssIndicator.Name = "tssIndicator";
            this.tssIndicator.Size = new System.Drawing.Size(16, 17);
            this.tssIndicator.Text = "   ";
            // 
            // tssLabel1
            // 
            this.tssLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssLabel1.Name = "tssLabel1";
            this.tssLabel1.Size = new System.Drawing.Size(72, 17);
            this.tssLabel1.Text = "Not running";
            // 
            // tssSeparator
            // 
            this.tssSeparator.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssSeparator.Name = "tssSeparator";
            this.tssSeparator.Size = new System.Drawing.Size(16, 17);
            this.tssSeparator.Text = " | ";
            // 
            // tssLabel2
            // 
            this.tssLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssLabel2.Name = "tssLabel2";
            this.tssLabel2.Size = new System.Drawing.Size(41, 17);
            this.tssLabel2.Text = "0 Calls";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1344, 614);
            this.panel1.TabIndex = 6;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 117);
            this.splitContainer.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.lvCalls);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainer.Size = new System.Drawing.Size(1344, 497);
            this.splitContainer.SplitterDistance = 257;
            this.splitContainer.SplitterWidth = 3;
            this.splitContainer.TabIndex = 1;
            this.splitContainer.Text = "splitContainer1";
            // 
            // lvCalls
            // 
            this.lvCalls.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colID,
            this.colFrom,
            this.colTo,
            this.colDir,
            this.colState,
            this.colDigits,
            this.colAction,
            this.colRtpTx,
            this.colRtpRx,
            this.colDuration});
            this.lvCalls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvCalls.FullRowSelect = true;
            this.lvCalls.GridLines = true;
            this.lvCalls.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvCalls.HideSelection = false;
            this.lvCalls.Location = new System.Drawing.Point(0, 0);
            this.lvCalls.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvCalls.Name = "lvCalls";
            this.lvCalls.Size = new System.Drawing.Size(1344, 257);
            this.lvCalls.TabIndex = 1;
            this.lvCalls.UseCompatibleStateImageBehavior = false;
            this.lvCalls.View = System.Windows.Forms.View.Details;
            this.lvCalls.SelectedIndexChanged += new System.EventHandler(this.lvCalls_SelectedIndexChanged);
            // 
            // colID
            // 
            this.colID.Name = "colID";
            this.colID.Text = "Call-ID";
            this.colID.Width = 180;
            // 
            // colFrom
            // 
            this.colFrom.Name = "colFrom";
            this.colFrom.Text = "From";
            this.colFrom.Width = 180;
            // 
            // colTo
            // 
            this.colTo.Name = "colTo";
            this.colTo.Text = "To";
            this.colTo.Width = 180;
            // 
            // colDir
            // 
            this.colDir.Name = "colDir";
            this.colDir.Text = "Dir";
            // 
            // colState
            // 
            this.colState.Name = "colState";
            this.colState.Text = "State";
            this.colState.Width = 180;
            // 
            // colDigits
            // 
            this.colDigits.Name = "colDigits";
            this.colDigits.Text = "Digits";
            this.colDigits.Width = 80;
            // 
            // colAction
            // 
            this.colAction.Name = "colAction";
            this.colAction.Text = "Action";
            this.colAction.Width = 280;
            // 
            // colRtpTx
            // 
            this.colRtpTx.Name = "colRtpTx";
            this.colRtpTx.Text = "Rtp Tx";
            // 
            // colRtpRx
            // 
            this.colRtpRx.Name = "colRtpRx";
            this.colRtpRx.Text = "Rtp Rx";
            // 
            // colDuration
            // 
            this.colDuration.Name = "colDuration";
            this.colDuration.Text = "Duration";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.toolStrip2);
            this.splitContainer1.Panel1.Controls.Add(this.LogBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tbOneLog);
            this.splitContainer1.Size = new System.Drawing.Size(1344, 237);
            this.splitContainer1.SplitterDistance = 837;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.Text = "splitContainer1";
            // 
            // toolStrip2
            // 
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAutoScroll,
            this.tsbSearch,
            this.tsbClear,
            this.tsbLogFile,
            this.tsbClearLog,
            this.tsbLogLevel,
            this.tsbShowErrorsOnly});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(837, 25);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsbAutoScroll
            // 
            this.tsbAutoScroll.Checked = true;
            this.tsbAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("tsbAutoScroll.Image")));
            this.tsbAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAutoScroll.Name = "tsbAutoScroll";
            this.tsbAutoScroll.Size = new System.Drawing.Size(84, 22);
            this.tsbAutoScroll.Text = "Auto scroll";
            this.tsbAutoScroll.Click += new System.EventHandler(this.tsbAutoScroll_Click);
            // 
            // tsbSearch
            // 
            this.tsbSearch.Name = "tsbSearch";
            this.tsbSearch.Size = new System.Drawing.Size(300, 25);
            this.tsbSearch.Text = "Search";
            this.tsbSearch.Enter += new System.EventHandler(this.tsbFilter_Enter);
            this.tsbSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tsbFilter_KeyUp);
            // 
            // tsbClear
            // 
            this.tsbClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbClear.Enabled = false;
            this.tsbClear.Image = ((System.Drawing.Image)(resources.GetObject("tsbClear.Image")));
            this.tsbClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbClear.Name = "tsbClear";
            this.tsbClear.Size = new System.Drawing.Size(38, 22);
            this.tsbClear.Text = "Clear";
            this.tsbClear.Click += new System.EventHandler(this.tsbClear_Click);
            // 
            // tsbLogFile
            // 
            this.tsbLogFile.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbLogFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbLogFile.DoubleClickEnabled = true;
            this.tsbLogFile.Image = ((System.Drawing.Image)(resources.GetObject("tsbLogFile.Image")));
            this.tsbLogFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLogFile.Name = "tsbLogFile";
            this.tsbLogFile.Size = new System.Drawing.Size(50, 22);
            this.tsbLogFile.Text = "Log file";
            this.tsbLogFile.Click += new System.EventHandler(this.tsbLogFile_Click);
            // 
            // tsbClearLog
            // 
            this.tsbClearLog.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbClearLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbClearLog.Image = ((System.Drawing.Image)(resources.GetObject("tsbClearLog.Image")));
            this.tsbClearLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbClearLog.Name = "tsbClearLog";
            this.tsbClearLog.Size = new System.Drawing.Size(58, 22);
            this.tsbClearLog.Text = "Clear log";
            this.tsbClearLog.Click += new System.EventHandler(this.tsbClearLog_Click);
            // 
            // tsbLogLevel
            // 
            this.tsbLogLevel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbLogLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbLogLevel.Image = ((System.Drawing.Image)(resources.GetObject("tsbLogLevel.Image")));
            this.tsbLogLevel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLogLevel.Name = "tsbLogLevel";
            this.tsbLogLevel.Size = new System.Drawing.Size(67, 22);
            this.tsbLogLevel.Text = "Log level";
            this.tsbLogLevel.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsbLogLevel_DropDownItemClicked);
            // 
            // tsbShowErrorsOnly
            // 
            this.tsbShowErrorsOnly.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbShowErrorsOnly.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbShowErrorsOnly.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowErrorsOnly.Image")));
            this.tsbShowErrorsOnly.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowErrorsOnly.Name = "tsbShowErrorsOnly";
            this.tsbShowErrorsOnly.Size = new System.Drawing.Size(99, 22);
            this.tsbShowErrorsOnly.Text = "Show errors only";
            this.tsbShowErrorsOnly.Click += new System.EventHandler(this.tsbShowErrorsOnly_Click);
            // 
            // LogBox
            // 
            this.LogBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.LogBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogBox.FullRowSelect = true;
            this.LogBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LogBox.HideSelection = false;
            this.LogBox.Location = new System.Drawing.Point(0, 0);
            this.LogBox.Margin = new System.Windows.Forms.Padding(4, 203, 4, 3);
            this.LogBox.MultiSelect = false;
            this.LogBox.Name = "LogBox";
            this.LogBox.Size = new System.Drawing.Size(837, 237);
            this.LogBox.TabIndex = 0;
            this.LogBox.UseCompatibleStateImageBehavior = false;
            this.LogBox.View = System.Windows.Forms.View.Details;
            this.LogBox.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LogBox_ItemSelectionChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Name = "columnHeader1";
            this.columnHeader1.Text = "Timestamp";
            this.columnHeader1.Width = 160;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Name = "columnHeader2";
            this.columnHeader2.Text = "Level";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Name = "columnHeader3";
            this.columnHeader3.Text = "Event";
            this.columnHeader3.Width = 360;
            // 
            // tbOneLog
            // 
            this.tbOneLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbOneLog.Location = new System.Drawing.Point(0, 0);
            this.tbOneLog.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbOneLog.Multiline = true;
            this.tbOneLog.Name = "tbOneLog";
            this.tbOneLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbOneLog.Size = new System.Drawing.Size(503, 237);
            this.tbOneLog.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.Dialpad);
            this.panel2.Controls.Add(this.Inputs);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1344, 117);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.lblCancelled);
            this.panel3.Controls.Add(this.lblTimeout);
            this.panel3.Controls.Add(this.lblHangup);
            this.panel3.Controls.Add(this.lblConnected);
            this.panel3.Controls.Add(this.lblTrying);
            this.panel3.Controls.Add(this.lblCalling);
            this.panel3.Controls.Add(this.lblAnswered);
            this.panel3.Controls.Add(this.lblAccepted);
            this.panel3.Controls.Add(this.lblOffered);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(730, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(521, 117);
            this.panel3.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(182, 85);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 15);
            this.label16.TabIndex = 17;
            this.label16.Text = "Cancelled";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 84);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Timeout";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(363, 85);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 15);
            this.label14.TabIndex = 15;
            this.label14.Text = "Hangup";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(363, 50);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 15);
            this.label13.TabIndex = 14;
            this.label13.Text = "Connected";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(182, 50);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 15);
            this.label11.TabIndex = 12;
            this.label11.Text = "Trying";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(363, 17);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 15);
            this.label10.TabIndex = 11;
            this.label10.Text = "Answered";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(182, 17);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 15);
            this.label9.TabIndex = 10;
            this.label9.Text = "Accepted";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 50);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 15);
            this.label8.TabIndex = 9;
            this.label8.Text = "Calling";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 17);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 15);
            this.label7.TabIndex = 8;
            this.label7.Text = "Offered";
            // 
            // lblCancelled
            // 
            this.lblCancelled.AutoSize = true;
            this.lblCancelled.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblCancelled.Location = new System.Drawing.Point(296, 85);
            this.lblCancelled.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCancelled.Name = "lblCancelled";
            this.lblCancelled.Size = new System.Drawing.Size(13, 15);
            this.lblCancelled.TabIndex = 7;
            this.lblCancelled.Text = "0";
            this.lblCancelled.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTimeout
            // 
            this.lblTimeout.AutoSize = true;
            this.lblTimeout.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblTimeout.Location = new System.Drawing.Point(119, 84);
            this.lblTimeout.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTimeout.Name = "lblTimeout";
            this.lblTimeout.Size = new System.Drawing.Size(13, 15);
            this.lblTimeout.TabIndex = 6;
            this.lblTimeout.Text = "0";
            this.lblTimeout.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblHangup
            // 
            this.lblHangup.AutoSize = true;
            this.lblHangup.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblHangup.Location = new System.Drawing.Point(482, 84);
            this.lblHangup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHangup.Name = "lblHangup";
            this.lblHangup.Size = new System.Drawing.Size(13, 15);
            this.lblHangup.TabIndex = 5;
            this.lblHangup.Text = "0";
            this.lblHangup.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblConnected
            // 
            this.lblConnected.AutoSize = true;
            this.lblConnected.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblConnected.Location = new System.Drawing.Point(482, 50);
            this.lblConnected.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConnected.Name = "lblConnected";
            this.lblConnected.Size = new System.Drawing.Size(13, 15);
            this.lblConnected.TabIndex = 4;
            this.lblConnected.Text = "0";
            this.lblConnected.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTrying
            // 
            this.lblTrying.AutoSize = true;
            this.lblTrying.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblTrying.Location = new System.Drawing.Point(296, 50);
            this.lblTrying.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTrying.Name = "lblTrying";
            this.lblTrying.Size = new System.Drawing.Size(13, 15);
            this.lblTrying.TabIndex = 3;
            this.lblTrying.Text = "0";
            this.lblTrying.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCalling
            // 
            this.lblCalling.AutoSize = true;
            this.lblCalling.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblCalling.Location = new System.Drawing.Point(119, 50);
            this.lblCalling.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCalling.Name = "lblCalling";
            this.lblCalling.Size = new System.Drawing.Size(13, 15);
            this.lblCalling.TabIndex = 2;
            this.lblCalling.Text = "0";
            this.lblCalling.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAnswered
            // 
            this.lblAnswered.AutoSize = true;
            this.lblAnswered.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblAnswered.Location = new System.Drawing.Point(482, 17);
            this.lblAnswered.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAnswered.Name = "lblAnswered";
            this.lblAnswered.Size = new System.Drawing.Size(13, 15);
            this.lblAnswered.TabIndex = 1;
            this.lblAnswered.Text = "0";
            this.lblAnswered.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAccepted
            // 
            this.lblAccepted.AutoSize = true;
            this.lblAccepted.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblAccepted.Location = new System.Drawing.Point(296, 17);
            this.lblAccepted.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAccepted.Name = "lblAccepted";
            this.lblAccepted.Size = new System.Drawing.Size(13, 15);
            this.lblAccepted.TabIndex = 0;
            this.lblAccepted.Text = "0";
            this.lblAccepted.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOffered
            // 
            this.lblOffered.AutoSize = true;
            this.lblOffered.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblOffered.Location = new System.Drawing.Point(119, 17);
            this.lblOffered.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOffered.Name = "lblOffered";
            this.lblOffered.Size = new System.Drawing.Size(13, 15);
            this.lblOffered.TabIndex = 0;
            this.lblOffered.Text = "0";
            this.lblOffered.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Dialpad
            // 
            this.Dialpad.Controls.Add(this.btnDigP);
            this.Dialpad.Controls.Add(this.btnDig0);
            this.Dialpad.Controls.Add(this.btnDigS);
            this.Dialpad.Controls.Add(this.btnDig7);
            this.Dialpad.Controls.Add(this.btnDig8);
            this.Dialpad.Controls.Add(this.btnDig9);
            this.Dialpad.Controls.Add(this.btnDig4);
            this.Dialpad.Controls.Add(this.btnDig5);
            this.Dialpad.Controls.Add(this.btnDig6);
            this.Dialpad.Controls.Add(this.btnDig3);
            this.Dialpad.Controls.Add(this.btnDig2);
            this.Dialpad.Controls.Add(this.btnDig1);
            this.Dialpad.Dock = System.Windows.Forms.DockStyle.Right;
            this.Dialpad.Location = new System.Drawing.Point(1251, 0);
            this.Dialpad.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Dialpad.Name = "Dialpad";
            this.Dialpad.Size = new System.Drawing.Size(93, 117);
            this.Dialpad.TabIndex = 1;
            // 
            // btnDigP
            // 
            this.btnDigP.Enabled = false;
            this.btnDigP.Location = new System.Drawing.Point(64, 81);
            this.btnDigP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDigP.Name = "btnDigP";
            this.btnDigP.Size = new System.Drawing.Size(23, 23);
            this.btnDigP.TabIndex = 0;
            this.btnDigP.Text = "#";
            this.btnDigP.UseVisualStyleBackColor = true;
            this.btnDigP.Click += new System.EventHandler(this.btnDigP_Click);
            // 
            // btnDig0
            // 
            this.btnDig0.Enabled = false;
            this.btnDig0.Location = new System.Drawing.Point(35, 81);
            this.btnDig0.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig0.Name = "btnDig0";
            this.btnDig0.Size = new System.Drawing.Size(23, 23);
            this.btnDig0.TabIndex = 0;
            this.btnDig0.Text = "0";
            this.btnDig0.UseVisualStyleBackColor = true;
            this.btnDig0.Click += new System.EventHandler(this.btnDig0_Click);
            // 
            // btnDigS
            // 
            this.btnDigS.Enabled = false;
            this.btnDigS.Location = new System.Drawing.Point(6, 81);
            this.btnDigS.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDigS.Name = "btnDigS";
            this.btnDigS.Size = new System.Drawing.Size(23, 23);
            this.btnDigS.TabIndex = 0;
            this.btnDigS.Text = "*";
            this.btnDigS.UseVisualStyleBackColor = true;
            this.btnDigS.Click += new System.EventHandler(this.btnDigS_Click);
            // 
            // btnDig7
            // 
            this.btnDig7.Enabled = false;
            this.btnDig7.Location = new System.Drawing.Point(6, 58);
            this.btnDig7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig7.Name = "btnDig7";
            this.btnDig7.Size = new System.Drawing.Size(23, 23);
            this.btnDig7.TabIndex = 0;
            this.btnDig7.Text = "7";
            this.btnDig7.UseVisualStyleBackColor = true;
            this.btnDig7.Click += new System.EventHandler(this.btnDig7_Click);
            // 
            // btnDig8
            // 
            this.btnDig8.Enabled = false;
            this.btnDig8.Location = new System.Drawing.Point(35, 58);
            this.btnDig8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig8.Name = "btnDig8";
            this.btnDig8.Size = new System.Drawing.Size(23, 23);
            this.btnDig8.TabIndex = 0;
            this.btnDig8.Text = "8";
            this.btnDig8.UseVisualStyleBackColor = true;
            this.btnDig8.Click += new System.EventHandler(this.btnDig8_Click);
            // 
            // btnDig9
            // 
            this.btnDig9.Enabled = false;
            this.btnDig9.Location = new System.Drawing.Point(64, 58);
            this.btnDig9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig9.Name = "btnDig9";
            this.btnDig9.Size = new System.Drawing.Size(23, 23);
            this.btnDig9.TabIndex = 0;
            this.btnDig9.Text = "9";
            this.btnDig9.UseVisualStyleBackColor = true;
            this.btnDig9.Click += new System.EventHandler(this.btnDig9_Click);
            // 
            // btnDig4
            // 
            this.btnDig4.Enabled = false;
            this.btnDig4.Location = new System.Drawing.Point(6, 35);
            this.btnDig4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig4.Name = "btnDig4";
            this.btnDig4.Size = new System.Drawing.Size(23, 23);
            this.btnDig4.TabIndex = 0;
            this.btnDig4.Text = "4";
            this.btnDig4.UseVisualStyleBackColor = true;
            this.btnDig4.Click += new System.EventHandler(this.btnDig4_Click);
            // 
            // btnDig5
            // 
            this.btnDig5.Enabled = false;
            this.btnDig5.Location = new System.Drawing.Point(35, 35);
            this.btnDig5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig5.Name = "btnDig5";
            this.btnDig5.Size = new System.Drawing.Size(23, 23);
            this.btnDig5.TabIndex = 0;
            this.btnDig5.Text = "5";
            this.btnDig5.UseVisualStyleBackColor = true;
            this.btnDig5.Click += new System.EventHandler(this.btnDig5_Click);
            // 
            // btnDig6
            // 
            this.btnDig6.Enabled = false;
            this.btnDig6.Location = new System.Drawing.Point(64, 35);
            this.btnDig6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig6.Name = "btnDig6";
            this.btnDig6.Size = new System.Drawing.Size(23, 23);
            this.btnDig6.TabIndex = 0;
            this.btnDig6.Text = "6";
            this.btnDig6.UseVisualStyleBackColor = true;
            this.btnDig6.Click += new System.EventHandler(this.btnDig6_Click);
            // 
            // btnDig3
            // 
            this.btnDig3.Enabled = false;
            this.btnDig3.Location = new System.Drawing.Point(64, 12);
            this.btnDig3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig3.Name = "btnDig3";
            this.btnDig3.Size = new System.Drawing.Size(23, 23);
            this.btnDig3.TabIndex = 0;
            this.btnDig3.Text = "3";
            this.btnDig3.UseVisualStyleBackColor = true;
            this.btnDig3.Click += new System.EventHandler(this.btnDig3_Click);
            // 
            // btnDig2
            // 
            this.btnDig2.Enabled = false;
            this.btnDig2.Location = new System.Drawing.Point(35, 12);
            this.btnDig2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig2.Name = "btnDig2";
            this.btnDig2.Size = new System.Drawing.Size(23, 23);
            this.btnDig2.TabIndex = 0;
            this.btnDig2.Text = "2";
            this.btnDig2.UseVisualStyleBackColor = true;
            this.btnDig2.Click += new System.EventHandler(this.btnDig2_Click);
            // 
            // btnDig1
            // 
            this.btnDig1.Enabled = false;
            this.btnDig1.Location = new System.Drawing.Point(6, 12);
            this.btnDig1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDig1.Name = "btnDig1";
            this.btnDig1.Size = new System.Drawing.Size(23, 23);
            this.btnDig1.TabIndex = 0;
            this.btnDig1.Text = "1";
            this.btnDig1.UseVisualStyleBackColor = true;
            this.btnDig1.Click += new System.EventHandler(this.btnDig1_Click);
            // 
            // Inputs
            // 
            this.Inputs.Controls.Add(this.label12);
            this.Inputs.Controls.Add(this.tbSpeak);
            this.Inputs.Controls.Add(this.tbIdentity);
            this.Inputs.Controls.Add(this.label3);
            this.Inputs.Controls.Add(this.label6);
            this.Inputs.Controls.Add(this.tbDigitsStr);
            this.Inputs.Controls.Add(this.tbRecordDir);
            this.Inputs.Controls.Add(this.label5);
            this.Inputs.Controls.Add(this.label4);
            this.Inputs.Controls.Add(this.tbPlaybackFile);
            this.Inputs.Controls.Add(this.tbTransferTo);
            this.Inputs.Controls.Add(this.label2);
            this.Inputs.Controls.Add(this.tbNewCallTo);
            this.Inputs.Controls.Add(this.label1);
            this.Inputs.Dock = System.Windows.Forms.DockStyle.Left;
            this.Inputs.Location = new System.Drawing.Point(0, 0);
            this.Inputs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Inputs.Name = "Inputs";
            this.Inputs.Size = new System.Drawing.Size(730, 117);
            this.Inputs.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(508, 15);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 15);
            this.label12.TabIndex = 2;
            this.label12.Text = "Speech";
            // 
            // tbSpeak
            // 
            this.tbSpeak.Location = new System.Drawing.Point(561, 12);
            this.tbSpeak.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbSpeak.Name = "tbSpeak";
            this.tbSpeak.Size = new System.Drawing.Size(161, 23);
            this.tbSpeak.TabIndex = 3;
            // 
            // tbIdentity
            // 
            this.tbIdentity.Location = new System.Drawing.Point(79, 14);
            this.tbIdentity.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbIdentity.Name = "tbIdentity";
            this.tbIdentity.Size = new System.Drawing.Size(161, 23);
            this.tbIdentity.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Identity";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(250, 84);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Digits string";
            // 
            // tbDigitsStr
            // 
            this.tbDigitsStr.Location = new System.Drawing.Point(329, 81);
            this.tbDigitsStr.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbDigitsStr.Name = "tbDigitsStr";
            this.tbDigitsStr.Size = new System.Drawing.Size(161, 23);
            this.tbDigitsStr.TabIndex = 1;
            // 
            // tbRecordDir
            // 
            this.tbRecordDir.Location = new System.Drawing.Point(329, 47);
            this.tbRecordDir.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbRecordDir.Name = "tbRecordDir";
            this.tbRecordDir.Size = new System.Drawing.Size(161, 23);
            this.tbRecordDir.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(250, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "Record dir";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(250, 16);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Playback file";
            // 
            // tbPlaybackFile
            // 
            this.tbPlaybackFile.Location = new System.Drawing.Point(329, 13);
            this.tbPlaybackFile.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbPlaybackFile.Name = "tbPlaybackFile";
            this.tbPlaybackFile.Size = new System.Drawing.Size(161, 23);
            this.tbPlaybackFile.TabIndex = 1;
            // 
            // tbTransferTo
            // 
            this.tbTransferTo.Location = new System.Drawing.Point(79, 82);
            this.tbTransferTo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbTransferTo.Name = "tbTransferTo";
            this.tbTransferTo.Size = new System.Drawing.Size(161, 23);
            this.tbTransferTo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 84);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Transfer to";
            // 
            // tbNewCallTo
            // 
            this.tbNewCallTo.Location = new System.Drawing.Point(79, 47);
            this.tbNewCallTo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbNewCallTo.Name = "tbNewCallTo";
            this.tbNewCallTo.Size = new System.Drawing.Size(161, 23);
            this.tbNewCallTo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "New call to";
            // 
            // tsbStart
            // 
            this.tsbStart.Image = ((System.Drawing.Image)(resources.GetObject("tsbStart.Image")));
            this.tsbStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStart.Name = "tsbStart";
            this.tsbStart.Size = new System.Drawing.Size(51, 22);
            this.tsbStart.Text = "Start";
            this.tsbStart.Visible = false;
            this.tsbStart.Click += new System.EventHandler(this.tsbStart_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbNewCall
            // 
            this.tsbNewCall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbNewCall.Enabled = false;
            this.tsbNewCall.Image = ((System.Drawing.Image)(resources.GetObject("tsbNewCall.Image")));
            this.tsbNewCall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNewCall.Name = "tsbNewCall";
            this.tsbNewCall.Size = new System.Drawing.Size(56, 22);
            this.tsbNewCall.Text = "New call";
            this.tsbNewCall.Visible = false;
            this.tsbNewCall.Click += new System.EventHandler(this.tsbNewCall_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbDtmfMode,
            this.tsbStart,
            this.tsbStart1,
            this.tsbStop,
            this.toolStripSeparator2,
            this.tbsAutoAnswer,
            this.tbsAutoRun,
            this.tbsUsePVD,
            this.toolStripSeparator4,
            this.tsbNewCall,
            this.tsbNewCall1,
            this.tbsAcceptCall,
            this.tbsAnswerCall,
            this.tbsHoldCall,
            this.tsbRetrieveCall,
            this.tsbBlindTransfer,
            this.tsbAttendedTransfer,
            this.tsbDropCall,
            this.toolStripSeparator1,
            this.tbsSendDigits,
            this.tbsGetDigits,
            this.tbsPlay,
            this.tbsRecord,
            this.tbsSpeak,
            this.tbsSpeak1,
            this.tbsMusic,
            this.tbsBridge,
            this.tbsStop,
            this.toolStripSeparator3,
            this.tsbLogLevel_});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(1344, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbDtmfMode
            // 
            this.tsbDtmfMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDtmfMode.Image = ((System.Drawing.Image)(resources.GetObject("tsbDtmfMode.Image")));
            this.tsbDtmfMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDtmfMode.Name = "tsbDtmfMode";
            this.tsbDtmfMode.Size = new System.Drawing.Size(81, 22);
            this.tsbDtmfMode.Text = "Dtmf mode";
            this.tsbDtmfMode.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsbDtmfMode_DropDownItemClicked);
            // 
            // tsbStart1
            // 
            this.tsbStart1.Image = ((System.Drawing.Image)(resources.GetObject("tsbStart1.Image")));
            this.tsbStart1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStart1.Name = "tsbStart1";
            this.tsbStart1.Size = new System.Drawing.Size(60, 22);
            this.tsbStart1.Text = "Start";
            this.tsbStart1.ToolTipText = "Start";
            this.tsbStart1.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsbStart1_DropDownItemClicked);
            // 
            // tsbStop
            // 
            this.tsbStop.Enabled = false;
            this.tsbStop.Image = ((System.Drawing.Image)(resources.GetObject("tsbStop.Image")));
            this.tsbStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStop.Name = "tsbStop";
            this.tsbStop.Size = new System.Drawing.Size(51, 22);
            this.tsbStop.Text = "Stop";
            this.tsbStop.Click += new System.EventHandler(this.tsbStop_Click);
            // 
            // tbsAutoAnswer
            // 
            this.tbsAutoAnswer.Image = ((System.Drawing.Image)(resources.GetObject("tbsAutoAnswer.Image")));
            this.tbsAutoAnswer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsAutoAnswer.Name = "tbsAutoAnswer";
            this.tbsAutoAnswer.Size = new System.Drawing.Size(93, 22);
            this.tbsAutoAnswer.Text = "Auto answer";
            this.tbsAutoAnswer.Click += new System.EventHandler(this.tbsAuto_Click);
            // 
            // tbsAutoRun
            // 
            this.tbsAutoRun.Checked = true;
            this.tbsAutoRun.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tbsAutoRun.Image = ((System.Drawing.Image)(resources.GetObject("tbsAutoRun.Image")));
            this.tbsAutoRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsAutoRun.Name = "tbsAutoRun";
            this.tbsAutoRun.Size = new System.Drawing.Size(74, 22);
            this.tbsAutoRun.Text = "Auto run";
            this.tbsAutoRun.Click += new System.EventHandler(this.tbsAutoRun_Click);
            // 
            // tbsUsePVD
            // 
            this.tbsUsePVD.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsUsePVD.Image = ((System.Drawing.Image)(resources.GetObject("tbsUsePVD.Image")));
            this.tbsUsePVD.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsUsePVD.Name = "tbsUsePVD";
            this.tbsUsePVD.Size = new System.Drawing.Size(55, 22);
            this.tbsUsePVD.Text = "Use PVD";
            this.tbsUsePVD.Click += new System.EventHandler(this.tbsUsePVD_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbNewCall1
            // 
            this.tsbNewCall1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbNewCall1.Enabled = false;
            this.tsbNewCall1.Image = ((System.Drawing.Image)(resources.GetObject("tsbNewCall1.Image")));
            this.tsbNewCall1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNewCall1.Name = "tsbNewCall1";
            this.tsbNewCall1.Size = new System.Drawing.Size(65, 22);
            this.tsbNewCall1.Text = "New call";
            this.tsbNewCall1.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsbNewCall1_DropDownItemClicked);
            // 
            // tbsAcceptCall
            // 
            this.tbsAcceptCall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsAcceptCall.Enabled = false;
            this.tbsAcceptCall.Image = ((System.Drawing.Image)(resources.GetObject("tbsAcceptCall.Image")));
            this.tbsAcceptCall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsAcceptCall.Name = "tbsAcceptCall";
            this.tbsAcceptCall.Size = new System.Drawing.Size(69, 22);
            this.tbsAcceptCall.Text = "Accept call";
            this.tbsAcceptCall.Click += new System.EventHandler(this.tbsAcceptCall_Click);
            // 
            // tbsAnswerCall
            // 
            this.tbsAnswerCall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsAnswerCall.Enabled = false;
            this.tbsAnswerCall.Image = ((System.Drawing.Image)(resources.GetObject("tbsAnswerCall.Image")));
            this.tbsAnswerCall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsAnswerCall.Name = "tbsAnswerCall";
            this.tbsAnswerCall.Size = new System.Drawing.Size(71, 22);
            this.tbsAnswerCall.Text = "Answer call";
            this.tbsAnswerCall.Click += new System.EventHandler(this.tbsAnswerCall_Click);
            // 
            // tbsHoldCall
            // 
            this.tbsHoldCall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsHoldCall.Enabled = false;
            this.tbsHoldCall.Image = ((System.Drawing.Image)(resources.GetObject("tbsHoldCall.Image")));
            this.tbsHoldCall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsHoldCall.Name = "tbsHoldCall";
            this.tbsHoldCall.Size = new System.Drawing.Size(67, 22);
            this.tbsHoldCall.Text = "Hold call";
            this.tbsHoldCall.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tbsHoldCall_DropDownItemClicked);
            // 
            // tsbRetrieveCall
            // 
            this.tsbRetrieveCall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbRetrieveCall.Enabled = false;
            this.tsbRetrieveCall.Image = ((System.Drawing.Image)(resources.GetObject("tsbRetrieveCall.Image")));
            this.tsbRetrieveCall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRetrieveCall.Name = "tsbRetrieveCall";
            this.tsbRetrieveCall.Size = new System.Drawing.Size(74, 22);
            this.tsbRetrieveCall.Text = "Retreive call";
            this.tsbRetrieveCall.Click += new System.EventHandler(this.tsbRetrieveCall_Click);
            // 
            // tsbBlindTransfer
            // 
            this.tsbBlindTransfer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbBlindTransfer.Enabled = false;
            this.tsbBlindTransfer.Image = ((System.Drawing.Image)(resources.GetObject("tsbBlindTransfer.Image")));
            this.tsbBlindTransfer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBlindTransfer.Name = "tsbBlindTransfer";
            this.tsbBlindTransfer.Size = new System.Drawing.Size(81, 22);
            this.tsbBlindTransfer.Text = "Blind transfer";
            this.tsbBlindTransfer.Click += new System.EventHandler(this.tsbBlindTransfer_Click);
            // 
            // tsbAttendedTransfer
            // 
            this.tsbAttendedTransfer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbAttendedTransfer.Enabled = false;
            this.tsbAttendedTransfer.Image = ((System.Drawing.Image)(resources.GetObject("tsbAttendedTransfer.Image")));
            this.tsbAttendedTransfer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAttendedTransfer.Name = "tsbAttendedTransfer";
            this.tsbAttendedTransfer.Size = new System.Drawing.Size(103, 22);
            this.tsbAttendedTransfer.Text = "Attended transfer";
            this.tsbAttendedTransfer.Click += new System.EventHandler(this.tsbAttendedTransfer_Click);
            // 
            // tsbDropCall
            // 
            this.tsbDropCall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDropCall.Enabled = false;
            this.tsbDropCall.Image = ((System.Drawing.Image)(resources.GetObject("tsbDropCall.Image")));
            this.tsbDropCall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDropCall.Name = "tsbDropCall";
            this.tsbDropCall.Size = new System.Drawing.Size(58, 22);
            this.tsbDropCall.Text = "Drop call";
            this.tsbDropCall.Click += new System.EventHandler(this.tsbDropCall_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbsSendDigits
            // 
            this.tbsSendDigits.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsSendDigits.Enabled = false;
            this.tbsSendDigits.Image = ((System.Drawing.Image)(resources.GetObject("tbsSendDigits.Image")));
            this.tbsSendDigits.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsSendDigits.Name = "tbsSendDigits";
            this.tbsSendDigits.Size = new System.Drawing.Size(69, 22);
            this.tbsSendDigits.Text = "Send digits";
            this.tbsSendDigits.Click += new System.EventHandler(this.tbsSendDigits_Click);
            // 
            // tbsGetDigits
            // 
            this.tbsGetDigits.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsGetDigits.Enabled = false;
            this.tbsGetDigits.Image = ((System.Drawing.Image)(resources.GetObject("tbsGetDigits.Image")));
            this.tbsGetDigits.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsGetDigits.Name = "tbsGetDigits";
            this.tbsGetDigits.Size = new System.Drawing.Size(61, 22);
            this.tbsGetDigits.Text = "Get digits";
            this.tbsGetDigits.Click += new System.EventHandler(this.tbsGetDigits_Click);
            // 
            // tbsPlay
            // 
            this.tbsPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsPlay.Enabled = false;
            this.tbsPlay.Image = ((System.Drawing.Image)(resources.GetObject("tbsPlay.Image")));
            this.tbsPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsPlay.Name = "tbsPlay";
            this.tbsPlay.Size = new System.Drawing.Size(33, 22);
            this.tbsPlay.Text = "Play";
            this.tbsPlay.Click += new System.EventHandler(this.tbsPlay_Click);
            // 
            // tbsRecord
            // 
            this.tbsRecord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsRecord.Enabled = false;
            this.tbsRecord.Image = ((System.Drawing.Image)(resources.GetObject("tbsRecord.Image")));
            this.tbsRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsRecord.Name = "tbsRecord";
            this.tbsRecord.Size = new System.Drawing.Size(48, 22);
            this.tbsRecord.Text = "Record";
            this.tbsRecord.Click += new System.EventHandler(this.tbsRecord_Click);
            // 
            // tbsSpeak
            // 
            this.tbsSpeak.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsSpeak.Enabled = false;
            this.tbsSpeak.Image = ((System.Drawing.Image)(resources.GetObject("tbsSpeak.Image")));
            this.tbsSpeak.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsSpeak.Name = "tbsSpeak";
            this.tbsSpeak.Size = new System.Drawing.Size(42, 22);
            this.tbsSpeak.Text = "Speak";
            this.tbsSpeak.Visible = false;
            this.tbsSpeak.Click += new System.EventHandler(this.tbsSpeak_Click);
            // 
            // tbsSpeak1
            // 
            this.tbsSpeak1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsSpeak1.Enabled = false;
            this.tbsSpeak1.Image = ((System.Drawing.Image)(resources.GetObject("tbsSpeak1.Image")));
            this.tbsSpeak1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsSpeak1.Name = "tbsSpeak1";
            this.tbsSpeak1.Size = new System.Drawing.Size(51, 22);
            this.tbsSpeak1.Text = "Speak";
            this.tbsSpeak1.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tbsSpeak1_DropDownItemClicked);
            // 
            // tbsMusic
            // 
            this.tbsMusic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsMusic.Enabled = false;
            this.tbsMusic.Image = ((System.Drawing.Image)(resources.GetObject("tbsMusic.Image")));
            this.tbsMusic.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsMusic.Name = "tbsMusic";
            this.tbsMusic.Size = new System.Drawing.Size(43, 22);
            this.tbsMusic.Text = "Music";
            this.tbsMusic.Click += new System.EventHandler(this.tbsMusic_Click);
            // 
            // tbsBridge
            // 
            this.tbsBridge.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsBridge.Enabled = false;
            this.tbsBridge.Image = ((System.Drawing.Image)(resources.GetObject("tbsBridge.Image")));
            this.tbsBridge.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsBridge.Name = "tbsBridge";
            this.tbsBridge.Size = new System.Drawing.Size(45, 19);
            this.tbsBridge.Text = "Bridge";
            this.tbsBridge.Click += new System.EventHandler(this.tbsBridge_Click);
            // 
            // tbsStop
            // 
            this.tbsStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbsStop.Enabled = false;
            this.tbsStop.Image = ((System.Drawing.Image)(resources.GetObject("tbsStop.Image")));
            this.tbsStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbsStop.Name = "tbsStop";
            this.tbsStop.Size = new System.Drawing.Size(35, 19);
            this.tbsStop.Text = "Stop";
            this.tbsStop.Click += new System.EventHandler(this.tbsStop_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbLogLevel_
            // 
            this.tsbLogLevel_.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbLogLevel_.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbLogLevel_.Image = ((System.Drawing.Image)(resources.GetObject("tsbLogLevel_.Image")));
            this.tsbLogLevel_.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLogLevel_.Name = "tsbLogLevel_";
            this.tsbLogLevel_.Size = new System.Drawing.Size(67, 19);
            this.tsbLogLevel_.Text = "Log level";
            this.tsbLogLevel_.Visible = false;
            this.tsbLogLevel_.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsbLogLevel_DropDownItemClicked);
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.Location = new System.Drawing.Point(6, 53);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(23, 23);
            this.button7.TabIndex = 0;
            this.button7.Text = "1";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Location = new System.Drawing.Point(35, 53);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(23, 23);
            this.button8.TabIndex = 0;
            this.button8.Text = "2";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Enabled = false;
            this.button9.Location = new System.Drawing.Point(64, 53);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(23, 23);
            this.button9.TabIndex = 0;
            this.button9.Text = "3";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(0, 0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 661);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.Text = "Sip Server Tester";
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.Dialpad.ResumeLayout(false);
            this.Inputs.ResumeLayout(false);
            this.Inputs.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssLabel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripButton tsbStart;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbNewCall;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbStop;
        private System.Windows.Forms.ToolStripButton tsbBlindTransfer;
        private System.Windows.Forms.ToolStripButton tsbDropCall;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tbsSendDigits;
        private System.Windows.Forms.ToolStripButton tbsPlay;
        private System.Windows.Forms.ToolStripButton tbsRecord;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel Dialpad;
        private System.Windows.Forms.Panel Inputs;
        private System.Windows.Forms.Button btnDig7;
        private System.Windows.Forms.Button btnDig8;
        private System.Windows.Forms.Button btnDig9;
        private System.Windows.Forms.Button btnDig4;
        private System.Windows.Forms.Button btnDig5;
        private System.Windows.Forms.Button btnDig3;
        private System.Windows.Forms.Button btnDig2;
        private System.Windows.Forms.Button btnDig1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button btnDigP;
        private System.Windows.Forms.Button btnDig0;
        private System.Windows.Forms.Button btnDigS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripDropDownButton tsbLogLevel_;
        private System.Windows.Forms.TextBox tbNewCallTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbDigitsStr;
        private System.Windows.Forms.TextBox tbRecordDir;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbPlaybackFile;
        private System.Windows.Forms.TextBox tbTransferTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView LogBox;
        private System.Windows.Forms.TextBox tbOneLog;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TextBox tbIdentity;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvCalls;
        private System.Windows.Forms.ColumnHeader colID;
        private System.Windows.Forms.ColumnHeader colFrom;
        private System.Windows.Forms.ColumnHeader colTo;
        private System.Windows.Forms.ColumnHeader colDir;
        private System.Windows.Forms.ColumnHeader colState;
        private System.Windows.Forms.ColumnHeader colDigits;
        private System.Windows.Forms.ColumnHeader colAction;
        private System.Windows.Forms.Button btnDig6;
        private System.Windows.Forms.ToolStripButton tsbRetrieveCall;
        private System.Windows.Forms.ToolStripButton tsbAttendedTransfer;
        private System.Windows.Forms.ToolStripButton tbsGetDigits;
        private System.Windows.Forms.ToolStripButton tbsAutoAnswer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tbsAcceptCall;
        private System.Windows.Forms.ToolStripButton tbsAnswerCall;
        private System.Windows.Forms.ToolStripButton tbsAutoRun;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsbAutoScroll;
        private System.Windows.Forms.ToolStripDropDownButton tsbLogLevel;
        private System.Windows.Forms.ToolStripTextBox tsbSearch;
        private System.Windows.Forms.ToolStripButton tsbClear;
        private System.Windows.Forms.ToolStripButton tsbClearLog;
        private System.Windows.Forms.ToolStripStatusLabel tssIndicator;
        private System.Windows.Forms.ToolStripDropDownButton tsbDtmfMode;
        private System.Windows.Forms.ToolStripButton tsbShowErrorsOnly;
        private System.Windows.Forms.ColumnHeader colDuration;
        private System.Windows.Forms.ToolStripStatusLabel tssSeparator;
        private System.Windows.Forms.ToolStripStatusLabel tssLabel2;
        private System.Windows.Forms.Label lblConnected;
        private System.Windows.Forms.Label lblTrying;
        private System.Windows.Forms.Label lblCalling;
        private System.Windows.Forms.Label lblAnswered;
        private System.Windows.Forms.Label lblAccepted;
        private System.Windows.Forms.Label lblOffered;
        private System.Windows.Forms.Label lblTimeout;
        private System.Windows.Forms.Label lblHangup;
        private System.Windows.Forms.Label lblCancelled;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripButton tbsUsePVD;
        private System.Windows.Forms.ToolStripButton tbsStop;
        private System.Windows.Forms.ToolStripButton tbsSpeak;
        private System.Windows.Forms.ColumnHeader colRtpTx;
        private System.Windows.Forms.ColumnHeader colRtpRx;
        private System.Windows.Forms.ToolStripButton tbsMusic;
        private System.Windows.Forms.ToolStripButton tsbLogFile;
        private System.Windows.Forms.ToolStripDropDownButton tbsHoldCall;
        private System.Windows.Forms.ToolStripDropDownButton tsbNewCall1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbSpeak;
        private System.Windows.Forms.ToolStripDropDownButton tsbStart1;
        private System.Windows.Forms.ToolStripDropDownButton tbsSpeak1;
        private System.Windows.Forms.ToolStripButton tbsBridge;
    }
}

