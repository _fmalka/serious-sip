﻿using System.Collections.Generic;
using System.Reflection;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace SeriousSIP
{
    public class Startup
    {
        public IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static List<Client> Clients = new List<Client>
        {
                new Client
                {
                    ClientId = "admin@modulo.co.il",
                    AllowedGrantTypes = new List<string> { GrantType.ClientCredentials },
                    ClientSecrets =
                    {
                        new Secret("Passw0rd".Sha256())
                    },
                    AllowedScopes = { "keypad", "write", "read" },
                    AllowedCorsOrigins = new List<string>
                    {
                        "http://localhost:6050",
                    },
                    AccessTokenLifetime = 86400
                }
        };

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("IdentityServerDatabase");
            var migrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            services.AddIdentityServer()
                    .AddDeveloperSigningCredential()
                    .AddInMemoryClients(Program.Config.GetSection("IdentityServer:Clients"))
                    //.AddInMemoryClients(Clients)
                    .AddInMemoryIdentityResources(Program.Config.GetSection("IdentityServer:IdentityResources"))
                    .AddInMemoryApiResources(Program.Config.GetSection("IdentityServer:ApiResources"))
                    .AddInMemoryApiScopes(Program.Config.GetSection("IdentityServer:ApiScopes"));

            services.AddSingleton<ICorsPolicyService>((container) => {
                var logger = container.GetRequiredService<ILogger<DefaultCorsPolicyService>>();
                return new DefaultCorsPolicyService(logger)
                {
                    AllowedOrigins = { "http://localhost:6050" }
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });

            app.UseIdentityServer();
        }
    }
}
