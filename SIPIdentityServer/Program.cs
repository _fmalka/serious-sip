﻿using System;
using Topshelf;
using Microsoft.Extensions.Configuration;
using Serilog.Extensions.Logging;
using System.IO;
using Serilog;

namespace SeriousSIP
{
    class Program
    {
        public const string ApiVersion = "v1";
        public const string ApiPrefix = "/api/";
        public const string ApiName = "KeyPad";

        public static IConfiguration Config { get; private set; } = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", false, true)
                        .Build();

        public static SerilogLoggerFactory LoggerFactory { get; private set; }

        static void Main(string[] args)
        {
            IConfigurationRoot configuration;
            if (File.Exists($"{Program.Config["logConfigurationFile"]}"))
            {
                configuration = new ConfigurationBuilder()
                                      .AddJsonFile($"{Directory.GetCurrentDirectory()}/{Program.Config["logConfigurationFile"]}")
                                      .AddEnvironmentVariables()
                                      .Build();
            }
            else
            {
                configuration = new ConfigurationBuilder()
                                       .AddEnvironmentVariables()
                                       .Build();
            }
            LoggerFactory = new SerilogLoggerFactory(new LoggerConfiguration()
                                .ReadFrom.Configuration(configuration)
                                .CreateLogger());

            HostFactory.Run(x =>
            {
                x.Service<SIPIdentityService>();
                x.EnableServiceRecovery(r => r.RestartService(TimeSpan.FromSeconds(10)));
                x.SetServiceName(Config.GetValue("instance", "SIPIdentityServer#1"));
                x.StartAutomatically();
            });
        }
    }
}
