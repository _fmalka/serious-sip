﻿using IdentityServer4.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Topshelf;

namespace SeriousSIP
{
    class SIPIdentityService : ServiceControl
    {
        private ILogger _logger = null;

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls(Program.Config["useUrls"]);
                });

        public bool Start(HostControl hostControl)
        {
            _logger = Program.LoggerFactory.CreateLogger(this.GetType().FullName);
            _logger.LogInformation(Program.Config.ToString());
            CreateHostBuilder(null).Build().Run();
            return true;
        }

        public bool Stop(HostControl hostControl)
        {

            return true;
        }
    }
}
