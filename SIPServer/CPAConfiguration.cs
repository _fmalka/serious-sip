﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace SeriousSIP
{
    public class CPAConfiguration
    {
        public double[] DtmfFrequencies { get; private set; } = new[] { 697.0, 770.0, 852.0, 941.0, 1209.0, 1336.0, 1477.0 };
        public double[] FaxFrequencies { get; private set; } = new[] { 1100.0, 2100.0 };
        public double[] RingbackFrequencies { get; private set; } = new[] { 400.0, 440.0 };
        public int SilenceFrames { get; private set; } = 50;
        public int FaxFrames { get; private set; } = 50;
        public int RingbackFrames { get; private set; } = 15;
        public double SilenceLowerSensitivyThreshold { get; private set; } = -0.1;
        public double SilenceHigherSensitivyThreshold { get; private set; } = 0.1;

        public int InterDigitTime { get; private set; } = 250;
        public int CpaPVDThreshold { get; private set; } = 2000;
        public int CpaPAMDThreshold { get; private set; } = 4000;
        
        public static CPAConfiguration LoadFromFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                return JsonSerializer.Deserialize<CPAConfiguration>(File.ReadAllText(fileName));
            }

            return new CPAConfiguration();
        }
    }
}
