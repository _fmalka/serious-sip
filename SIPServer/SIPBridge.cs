﻿using Microsoft.Extensions.Logging;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using Serilog.Context;
using SIPSorcery.Net;
using SIPSorceryMedia.Abstractions.V1;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static SeriousSIP.SIPCall;

namespace SeriousSIP
{

    public class SIPBridge
    {
        public enum SIPLeg { ALeg, BLeg, Any };
        public enum SIPBridgeAction { Idle, Playing, Recording };

        /// <summary>
        /// Keeps the unique conference id.
        /// </summary>
        public string Id { get; private set; } = Guid.NewGuid().ToString();

        /// <summary>
        /// Keeps the bridge action state
        /// </summary>
        public SIPBridgeAction Action { get; private set; } = SIPBridgeAction.Idle;

        /// <summary>
        /// Keeps the bridge A leg
        /// </summary>
        private SIPCall _aLeg;

        /// <summary>
        /// Keeps the bridge B leg
        /// </summary>
        private SIPCall _bLeg;

        /// <summary>
        /// Keeps the logger created by the main application.
        /// </summary>
        private ILogger _logger;

        /// <summary>
        /// Triggers the play termination event
        /// </summary>
        public event SIPCallCPADelegate OnPlayDone;

        /// <summary>
        /// Triggers the record termination event
        /// </summary>
        public event SIPCallCPADelegate OnRecordDone;

        /// <summary>
        /// Keeps the A leg wave file writer for recording
        /// </summary>
        private WaveFileWriter _aLegWaveFileWriter = null;

        /// <summary>
        /// Keeps the tmp file name for recording A leg
        /// </summary>
        private string _aLegTmpFile;

        /// <summary>
        /// Keeps the B leg wave file writer for recording
        /// </summary>
        private WaveFileWriter _bLegWaveFileWriter = null;

        /// <summary>
        /// Keeps the tmp file name for recording B leg
        /// </summary>
        private string _bLegTmpFile;

        /// <summary>
        /// Keeps the recording file name for the bridge
        /// </summary>
        private string _recordFilename;

        /// <summary>
        /// Keeps the bridge default wave format
        /// </summary>
        private WaveFormat _waveFormat = new WaveFormat((int)AudioSamplingRatesEnum.Rate8KHz, 16, 1);

        /// <summary>
        /// When true, relay dtmf from 1 leg to another.
        /// </summary>
        public bool RelayDtmf { get;  private set; } = true;

        /// <summary>
        /// Creates a new bridge with 2 legs
        /// </summary>
        public SIPBridge(SIPCall aLeg, SIPCall bLeg)
            : this(aLeg, bLeg, false)
        {
        }

        /// <summary>
        /// Creates a new bridge with 2 legs
        /// </summary>
        public SIPBridge(SIPCall aLeg, SIPCall bLeg, bool relayDtmf)
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}");
            RelayDtmf = relayDtmf;
            AddCall(aLeg);
            AddCall(bLeg);
        }

        public SIPCall[] Calls { get; private set; }

        /// <summary>
        /// Add a call to the conference
        /// </summary>
        /// <param name="call">Call to add to the conference</param>
        /// <returns>The number of calls in the conference</returns>
        private void AddCall(SIPCall call)
        {
            if (call.RtpSession != null)
            {
                call.RtpSession.AudioExtrasSource.SetSource(SIPSorcery.Media.AudioSourcesEnum.None);
            }

            if (call.RtcPeerConnection != null)
            {
                call.RtcPeerConnection.AudioExtrasSource.SetSource(SIPSorcery.Media.AudioSourcesEnum.None);
            }

            if (_aLeg == null)
            {
                _aLeg = call;
                _aLeg.Bridge(Id);
                call.OnLinearSampleAvailable += OnLinearSampleFromALeg;
                if (RelayDtmf)
                {
                    call.OnDtmfTone += OnDtmfToneFromALeg;
                }
            }
            else
            {
                _bLeg = call;
                _bLeg.Bridge(Id);
                call.OnLinearSampleAvailable += OnLinearSampleFromBLeg;
                if (RelayDtmf)
                {
                    call.OnDtmfTone += OnDtmfToneFromBLeg;
                }
            }

            Calls = new SIPCall[] { _aLeg, _bLeg };
        }

        /// <summary>
        /// Relay Rfc2833 dtmf from A leg to B leg
        /// </summary>
        /// <param name="key">dtmf key</param>
        /// <param name="duration">dtmf duration</param>
        private void OnDtmfToneFromALeg(byte key, int duration)
        {
            if (_aLeg.GetCallAction() == SIPCallAction.Bridged &&
                _bLeg.GetCallAction() == SIPCallAction.Bridged)
            {
                _bLeg.SendDtmf(key);
            }
        }

        /// <summary>
        /// Relay Rfc2833 dtmf from B leg to A leg
        /// </summary>
        /// <param name="key">dtmf key</param>
        /// <param name="duration">dtmf duration</param>
        private void OnDtmfToneFromBLeg(byte key, int duration)
        {
            if (_aLeg.GetCallAction() == SIPCallAction.Bridged &&
                _bLeg.GetCallAction() == SIPCallAction.Bridged)
            {
                _aLeg.SendDtmf(key);
            }
        }

        public void Terminate(bool dropCalls)
        {
            Terminate();

            if (dropCalls)
            {
                _aLeg.Hangup();
                _bLeg.Hangup();
            }
        }

        public void Terminate()
        {
            PlayStop(SIPLeg.Any);

            if (_aLeg != null)
            {
                _aLeg.OnLinearSampleAvailable -= OnLinearSampleFromALeg;
                _aLeg.Unbridge();
            }

            if (_bLeg != null)
            {
                _bLeg.OnLinearSampleAvailable -= OnLinearSampleFromBLeg;
                _bLeg.Unbridge();
            }

            Calls = null;
        }

        private void OnLinearSampleFromALeg(string callId, short[] sample)
        {
            if (!_bLeg.IsCallActive) return;

            if (_aLeg.GetCallAction() == SIPCallAction.Bridged &&
                _bLeg.GetCallAction() == SIPCallAction.Bridged)
            {
                if (_bLeg.IsWebRTC)
                {
                    _bLeg.RtcPeerConnection.AudioExtrasSource.EncodeAndSend(sample, 8000);
                }
                else
                {
                    _bLeg.RtpSession.AudioExtrasSource.EncodeAndSend(sample, 8000);
                }

                WriteSample(_aLegWaveFileWriter, sample);
            }
        }

        private void OnLinearSampleFromBLeg(string callId, short[] sample)
        {
            if (!_aLeg.IsCallActive) return;

            if (_aLeg.GetCallAction() == SIPCallAction.Bridged &&
                _bLeg.GetCallAction() == SIPCallAction.Bridged)
            {
                if (_aLeg.IsWebRTC)
                {
                    _aLeg.RtcPeerConnection.AudioExtrasSource.EncodeAndSend(sample, 8000);
                }
                else
                {
                    _aLeg.RtpSession.AudioExtrasSource.EncodeAndSend(sample, 8000);
                }

                WriteSample(_bLegWaveFileWriter, sample);
            }
        }

        public async Task PlayFile(SIPLeg leg, string fileName, bool clearDigitsBuffer, bool stopOnDigit)
        {
            Action = SIPBridgeAction.Playing;

            if (leg == SIPLeg.ALeg || leg == SIPLeg.Any)
            {
                _aLeg.Unbridge();
                _aLeg.OnPlayDone += () => { 
                    _aLeg.Bridge(Id); 
                    if (_aLeg.GetCallAction() == SIPCallAction.Bridged && _bLeg.GetCallAction() == SIPCallAction.Bridged) OnPlayDone?.Invoke();  
                    return Task.CompletedTask; 
                };
                _ = _aLeg.PlayFile(fileName, clearDigitsBuffer, stopOnDigit);
            }

            if (leg == SIPLeg.BLeg || leg == SIPLeg.Any)
            {
                _bLeg.Unbridge();
                _bLeg.OnPlayDone += () => { 
                    _bLeg.Bridge(Id); 
                    if (_aLeg.GetCallAction() == SIPCallAction.Bridged && _bLeg.GetCallAction() == SIPCallAction.Bridged) OnPlayDone?.Invoke(); 
                    return Task.CompletedTask; 
                };
                _ = _bLeg.PlayFile(fileName, clearDigitsBuffer, stopOnDigit);
            }
        }

        public void PlayStop(SIPLeg leg)
        {
            if (leg == SIPLeg.ALeg || leg == SIPLeg.Any)
            {
                if (_aLeg.GetCallAction() == SIPCallAction.Playing)
                {
                    _aLeg.PlayStop();
                    _aLeg.Bridge(Id);
                } 
                else if (_aLeg.GetCallAction() == SIPCallAction.Bridged)
                {
                    if (_aLegWaveFileWriter != null)
                    {
                        _aLegWaveFileWriter.Close();
                        _aLegWaveFileWriter = null;
                        if (leg == SIPLeg.ALeg)
                            MergeFiles(leg);
                    }
                }
            }

            if (leg == SIPLeg.BLeg || leg == SIPLeg.Any)
            {
                if (_aLeg.GetCallAction() == SIPCallAction.Playing)
                {
                    _bLeg.PlayStop();
                    _bLeg.Bridge(Id);
                }
                else if (_bLeg.GetCallAction() == SIPCallAction.Bridged)
                {
                    if (_bLegWaveFileWriter != null)
                    {
                        _bLegWaveFileWriter.Close();
                        _bLegWaveFileWriter = null;
                        if (leg == SIPLeg.BLeg || leg == SIPLeg.Any)
                        MergeFiles(leg);
                    }
                }
            }

            Action = SIPBridgeAction.Idle;
        }

        public void RecordFile(SIPLeg leg, string fileName)
        {
            _recordFilename = fileName;
            Action = SIPBridgeAction.Recording;

            if (leg == SIPLeg.ALeg || leg == SIPLeg.Any)
            {
                _aLegTmpFile = _aLeg.GetCallID() + ".pcm";
                _aLegWaveFileWriter = new WaveFileWriter(_aLegTmpFile, _waveFormat);
            }

            if (leg == SIPLeg.BLeg || leg == SIPLeg.Any)
            {
                _bLegTmpFile = _bLeg.GetCallID() + ".pcm";
                _bLegWaveFileWriter = new WaveFileWriter(_bLegTmpFile, _waveFormat);
            }
        }

        /// <summary>
        /// Write a pcm sample to wave file
        /// </summary>
        /// <param name="sample">The byte array representing the pcm sample</param>
        private void WriteSample(WaveFileWriter waveFileWriter, short[] linearSample)
        {
            
            using (LogContext.PushProperty("CallContext", Id))
            {
                if (waveFileWriter != null && waveFileWriter.CanWrite)
                {
                    try
                    {
                        _logger.LogTrace("Writing rtp sample to file...");
                        for (int index = 0; index < linearSample.Length; index++)
                        {
                            byte[] pcmSample = new byte[] { (byte)(linearSample[index] & 0xFF), (byte)(linearSample[index] >> 8) };
                            waveFileWriter.Write(pcmSample, 0, 2); ;
                        }
                        _logger.LogTrace("Rtp sample writen.");
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e.Message, e);
                    }
                }
            }

        }

        /// <summary>
        /// Merge recordings into one file.
        /// </summary>
        private void MergeFiles(SIPLeg sipLeg)
        {
            using (LogContext.PushProperty("CallContext", Id))
            {
                if (sipLeg == SIPLeg.Any)
                {
                    _logger.LogDebug("merging files...");
                    using (var reader1 = new AudioFileReader(_aLegTmpFile))
                    using (var reader2 = new AudioFileReader(_bLegTmpFile))
                    {
                        var mixer = new MixingSampleProvider(new[] { reader1, reader2 });
                        //WaveFileWriter fw = new WaveFileWriter(_recordFilename, new WaveFormat((int)AudioSamplingRatesEnum.Rate8KHz, 16, 1));
                        WaveFileWriter.CreateWaveFile16(_recordFilename, mixer);
                        File.Delete(_aLegTmpFile);
                        File.Delete(_bLegTmpFile);
                        OnRecordDone?.Invoke();
                        _logger.LogDebug("all done.");
                    }
                } 
                else if (sipLeg == SIPLeg.ALeg)
                {
                    File.Move(_aLegTmpFile, _recordFilename);
                    OnRecordDone?.Invoke();
                }
                else if (sipLeg == SIPLeg.BLeg)
                {
                    File.Move(_bLegTmpFile, _recordFilename);
                    OnRecordDone?.Invoke();
                }
            }
        }

        private void OnLinearSampleAvailable(string callId, short[] sample)
        {
            try
            {
                //int id = 0;
                //int c = getNextCircularCount(callId);
                //_samples[c][id] = sample;
                _aLeg.RtpSession.AudioExtrasSource.EncodeAndSend(sample, 8000);

                //_logger.LogDebug($"#{c} got sample for pid {id}, len = {sample.Length} bytes.");

                // if got all samples
                //if (_samples[c][0] != null && _samples[c][1] != null)
                //{

                    //for (int i = 0; i < sample.Length; i++)
                    //{
                       // sample[i] = (short)((_samples[c][0][i] + _samples[c][1][i]) / 2);
                        //byte[] pcmSample = new byte[] { (byte)(sample[i] & 0xFF), (byte)(sample[i] >> 8) };
                        //_waveFileWriter.Write(pcmSample, 0, 2); ;
                    //}
                      
                    //    _logger.LogDebug($"#{c} got all pid samples.");


                    //    foreach (SIPCall call in _calls.Values)
                    //    {
                    //        if (call.RtpSession != null)
                    //            call.RtpSession.AudioExtrasSource.EncodeAndSend(sample, 8000);
                    //    }

                    //    _samples[c][0] = null;
                    //    _samples[c][1] = null;
                    //    //    //_logger.LogDebug($"#{c} all samples cleaned.");

                //}
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
            } 
        }
    }
}
