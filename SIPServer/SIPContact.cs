﻿using SIPSorcery.SIP;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace SeriousSIP
{
    public enum SIPContactType
    {
        Extension,
        Trunk,
        Mobile,
        External
    }

    public enum SIPContactPresenceState
    {
        Offline,
        Online
    }

    public enum SIPContactPhoneState
    {
        Available,
        Busy
    }

    public class SIPContact
    {

        public static SIPContact UpdateState(SIPRequest sipRequest, ConcurrentDictionary<string, SIPContact> contacts)
        {
            if (sipRequest.Header.ContentType != "application/pidf+xml")
                return null;

            XmlSerializerNamespaces xmlNameSpace = new XmlSerializerNamespaces();
            xmlNameSpace.Add("pp", "urn:ietf:params:xml:ns:pidf:person");
            xmlNameSpace.Add("ep", "urn:ietf:params:xml:ns:pidf:rpid:rpid-person");
            xmlNameSpace.Add("", "urn:ietf:params:xml:ns:pidf");

            XmlSerializer serializer = new XmlSerializer(typeof(presence), "urn:ietf:params:xml:ns:pidf");
            presence p = (presence)serializer.Deserialize(new StringReader(sipRequest.Body));
            
            
            var matched = contacts.Select(item => item.Value.UserName == p.tuple[0].id ? item.Value : null).ToList();
            foreach (var item in matched)
            {
                if (item != null)
                {

                    if (p.tuple.Length > 0)
                    {
                        if (p.tuple[0].status.basic == basic.closed)
                            item.PresenceState = SIPContactPresenceState.Offline;
                        else if (p.tuple[0].status.basic == basic.open)
                            item.PresenceState = SIPContactPresenceState.Online;
                    }

                    if (p.note.Length > 0)
                    {
                        if (p.note[0].Value == "Ready")
                            item.PhoneState = SIPContactPhoneState.Available;
                        else if (p.tuple[0].status.basic == basic.open)
                            item.PhoneState = SIPContactPhoneState.Busy;
                    }

                    return item;

                }
            }

            return null;
        }

        public SIPContact() { }
        public SIPContact(string tel)
        {
            Id = new Guid().ToString();
            Tel = tel;
        }
        public SIPContact(string displayName, string username, string tel, string email)
        {
            Id = Guid.NewGuid().ToString();
            DisplayName = displayName;
            UserName = username;
            Tel = tel;
            Email = email;
        }

        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public SIPContactType Type { get; set; }
        public SIPContactPresenceState PresenceState { get; set; }
        public SIPContactPhoneState PhoneState { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public ConcurrentDictionary<string, Object> Tags { get; set; } = new ConcurrentDictionary<string, Object>();
        public bool IsOnline() => PresenceState == SIPContactPresenceState.Online;
        public bool IsBusy() => PhoneState == SIPContactPhoneState.Busy;
    }
}
