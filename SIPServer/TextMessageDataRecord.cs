using System;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using JamaaTech.Smpp.Net.Client;
using JamaaTech.Smpp.Net.Lib.Protocol;
using Microsoft.Extensions.Logging;
using SIPSorcery.SIP;

namespace SeriousSIP
{

    /// <summary>
    /// Call detail record for a text message.
    /// </summary>
    public class TextMessageDataRecord
    {
        private ILogger _logger; 
        private static string m_newLine = Environment.NewLine;

        public void WriteTDR()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append($"\"type\":\"text\",");
            sb.Append($"\"callId\":\"{this.CallId}\",");
            sb.Append($"\"createdTime\":\"{this.Created}\",");
            sb.Append($"\"calldirection\":\"{this.CallDirection}\",");
            sb.Append($"\"from\":\"{this.From}\",");
            sb.Append($"\"to\":\"{this.Destination}\",");
            sb.Append($"\"localEndPoint\":\"{this.LocalIPEndPoint}\",");
            sb.Append($"\"remoteEndPoint\":\"{this.RemoteIPEndPoint}\",");
            sb.Append($"\"responseReason\":\"{this.ResponseReasonPhrase}\",");
            sb.Append($"\"responseStatus\":\"{this.ResponseStatus}\",");
            sb.Append($"\"responseTime\":\"{this.ResponseTime}\",");
            sb.Append($"\"rejectReason\":\"{this.RejectReasonPhrase}\",");
            sb.Append($"\"rejectStatus\":\"{this.RejectStatus}\",");
            sb.Append($"\"rejectTime\":\"{this.RejectTime}\",");
            sb.Append($"\"length\":\"{this.Length}\"");
            sb.Append("}");
            _logger.LogTrace(sb.ToString());
        }

        public DateTimeOffset Created { get; set; }

        public SIPCallDirection CallDirection { get; set; }

        public string Destination { get; set; }

        public string From { get; set; }

        public int Length { get; set; }

        public int ResponseStatus { get; set; }

        public string ResponseReasonPhrase { get; set; }

        private DateTimeOffset? m_responseTime;

        public DateTimeOffset? ResponseTime
        {
            get { return m_responseTime; }
            set
            {
                if (value != null)
                {
                    m_responseTime = value.Value.ToUniversalTime();
                }
                else
                {
                    m_responseTime = null;
                }
            }
        }

        private DateTimeOffset? m_rejectTime;

        public DateTimeOffset? RejectTime
        {
            get { return m_rejectTime; }
            set
            {
                if (value != null)
                {
                    m_rejectTime = value.Value.ToUniversalTime();
                }
                else
                {
                    m_rejectTime = null;
                }
            }
        }

        public string RejectReasonPhrase { get; set; }
        public int RejectStatus { get; set; }


        public string CallId { get; set; }
        public IPEndPoint LocalIPEndPoint { get; set; }
        public IPEndPoint RemoteIPEndPoint { get; set; }
        
        public TextMessageDataRecord(
            SIPCallDirection callDirection,
            string destination,
            string from,
            string callId,
            int length,
            IPEndPoint localIPEndPoint,
            IPEndPoint remoteIPEndPoint)
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}"); 
            Created = DateTimeOffset.UtcNow;
            CallDirection = callDirection;
            Destination = destination;
            From = from;
            CallId = callId;
            LocalIPEndPoint = localIPEndPoint;
            RemoteIPEndPoint = remoteIPEndPoint;
            Length = length;
        }


        public void Response(int responseStatusCode, string responseReason, IPEndPoint localEndPoint, IPEndPoint remoteEndPoint)
        {
            try
            {
                ResponseTime = DateTimeOffset.UtcNow;
                ResponseStatus = (int)responseStatusCode;
                ResponseReasonPhrase = responseReason;

                if (localEndPoint != null)
                {
                    LocalIPEndPoint = localEndPoint;
                }

                if (remoteEndPoint != null)
                {
                    RemoteIPEndPoint = remoteEndPoint;
                }

                WriteTDR();
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPTDR Response. " + excp);
            }
        }

        public void TimedOut()
        {
            try
            {
                RejectTime = DateTimeOffset.UtcNow;
                RejectReasonPhrase = "Timed out";
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPTDR TimedOut. " + excp);
            }
        }

        public void Reject(SIPResponseStatusCodesEnum rejectStatus, string rejectReason)
        {
            try
            {
                RejectTime = DateTimeOffset.UtcNow;
                RejectReasonPhrase = rejectReason;
                RejectStatus = (int)rejectStatus;
                
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPTDR Reject. " + excp);
            }
        }
    }
}
