﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Serilog.Context;
using SIPSorcery.SIP;
using SIPSorcery.SIP.App;

namespace SeriousSIP
{
    public class SIPMessage
    {
        /// <summary>
        /// Keeps the underlaying sip transport.
        /// </summary>
        private SIPTransport _sipTransport;

        /// <summary>
        /// Keeps the logger created by the main application.
        /// </summary>
        private ILogger _logger;

        /// <summary>
        /// Keeps the user agent client for messages.
        /// </summary>
        private SIPSimpleUserAgent _simpleUac;

        private SIPCallDirection _direction;

        private SIPRequest _lastSIPRequest;

        /// <summary>
        /// Keeps the message MDR.
        /// </summary>
        public SIPMessageDataRecord MDR { get; set; }


        public SIPSimpleUserAgent GetUac() => _simpleUac;

        public string GetCallID()
        {
            if (_lastSIPRequest != null)
                return _lastSIPRequest.Header.CallId;
            if (_simpleUac != null)
                return _simpleUac.GetCallID();

            return null;
        }

        public SIPURI GetToURI()
        {
            if (_lastSIPRequest != null)
                return _lastSIPRequest.Header.To.ToURI;
            if (_simpleUac != null)
                return _simpleUac.GetToURI();

            return null;
        }

        public SIPURI GetFromURI()
        {
            if (_lastSIPRequest != null)
                return _lastSIPRequest.Header.From.FromURI;
            if (_simpleUac != null)
                return _simpleUac.GetFromURI();

            return null;
        }

        /// <summary>
        /// Returns the identity of the message.
        /// </summary>
        /// <returns></returns>
        public SIPURI GetIdentity()
        {
            if (_direction == SIPCallDirection.In)
                return new SIPURI(_simpleUac.GetUserName(), _simpleUac.GetFromURI().Host,
                    null, _simpleUac.GetFromURI().Scheme, _simpleUac.GetFromURI().Protocol);
            else
            {
                return new SIPURI(_simpleUac.GetUserName(), _simpleUac.GetToURI().Host,
                    null, _simpleUac.GetToURI().Scheme, _simpleUac.GetToURI().Protocol);
            }
        }
        public SIPCallDirection GetCallDirection()
        {
            return _direction;
        }

        public SIPMessage(SIPTransport sipTransport, SIPRequest sipRequest)
        {
           using (LogContext.PushProperty("CallContext", sipRequest.Header.CallId))
            { 
                MDR = new SIPMessageDataRecord(SIPCallDirection.In,
                                            sipRequest.Header.To.ToURI,
                                            sipRequest.Header.From.FromURI,
                                            sipRequest.Header.CallId,
                                            sipRequest.Body.Length,
                                            sipRequest.LocalSIPEndPoint,
                                            sipRequest.RemoteSIPEndPoint);

                
            }
            _sipTransport = sipTransport;
            _lastSIPRequest = sipRequest;
            _direction = SIPCallDirection.In;
        }

        public SIPMessage(SIPTransport sipTransport, string username, string password, string domain)
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}");
            _sipTransport = sipTransport;
            _direction = SIPCallDirection.Out;
            _simpleUac = new SIPSimpleUserAgent(_sipTransport, username, password, domain);

            MDR = new SIPMessageDataRecord(_direction,
                                            null,
                                            null,
                                            _simpleUac.GetCallID(),
                                            0,
                                            null,
                                            null);

            using (LogContext.PushProperty("CallContext", _simpleUac.GetCallID()))
            {
                _logger.LogInformation("New simple message uac created.");
            }
        }

        public async Task Ack()
        {
            using (LogContext.PushProperty("CallContext", _simpleUac.GetCallID()))
            {
                if (_lastSIPRequest == null)
                {
                    _logger.LogError("Cannot ack a null request.");
                    return;
                }

                _logger.LogDebug("Sending 200 OK...");
                SIPResponse messageResponse = SIPResponse.GetResponse(_lastSIPRequest, SIPResponseStatusCodesEnum.Ok, null);
                await _sipTransport.SendResponseAsync(messageResponse);

                MDR.Response(200, SIPResponseStatusCodesEnum.Ok, messageResponse.Header.Reason,
                        messageResponse.LocalSIPEndPoint, messageResponse.RemoteSIPEndPoint);

                _logger.LogDebug("Response sent.");
            }
        }

        public async Task Accept()
        {
            using (LogContext.PushProperty("CallContext", _simpleUac.GetCallID()))
            {
                if (_lastSIPRequest == null)
                {
                    _logger.LogError("Cannot accept a null request.");
                    return;
                }

                _logger.LogDebug("Sending 202 Accepted...");
                SIPResponse messageResponse = SIPResponse.GetResponse(_lastSIPRequest, SIPResponseStatusCodesEnum.Accepted, null);
                await _sipTransport.SendResponseAsync(messageResponse);

                MDR.Response(202, SIPResponseStatusCodesEnum.Accepted, messageResponse.Header.Reason,
                        messageResponse.LocalSIPEndPoint, messageResponse.RemoteSIPEndPoint);

                _logger.LogDebug("Response sent.");
            }
        }

        public async Task Reject(string reason)
        {
            using (LogContext.PushProperty("CallContext", _simpleUac.GetCallID()))
            {
                if (_lastSIPRequest == null)
                {
                    _logger.LogError("Cannot reject a null request.");
                    return;
                }

                _logger.LogDebug("Sending 406 NotAcceptable...");
                SIPResponse messageResponse = SIPResponse.GetResponse(_lastSIPRequest, SIPResponseStatusCodesEnum.NotAcceptable, reason);
                await _sipTransport.SendResponseAsync(messageResponse);

                MDR.Response(406, SIPResponseStatusCodesEnum.NotAcceptable, messageResponse.Header.Reason,
                        messageResponse.LocalSIPEndPoint, messageResponse.RemoteSIPEndPoint);

                _logger.LogDebug("Response sent.");
            }
        }

        public void Send(string to, string message)
        {
            _simpleUac.MessageFailed += (resp, reason) =>
            {
                if (resp != null) 
                    MDR.Response(resp.StatusCode, resp.Status, reason,
                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);
                else
                    MDR.Reject(SIPResponseStatusCodesEnum.NotAcceptable, reason);
            };

            _simpleUac.MessageTimeout += (resp, reason) =>
            {
                if (resp != null)
                    MDR.Response(resp.StatusCode, resp.Status, reason,
                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);
                else
                    MDR.Reject(SIPResponseStatusCodesEnum.RequestTimeout, reason);
            };

            _simpleUac.MessageSent += (resp, reason) =>
            {
                MDR.Response(resp.StatusCode, resp.Status, reason,
                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);
            };

            _simpleUac.SendMessage(to, message);            
        }
    }
}
