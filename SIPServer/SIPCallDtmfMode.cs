﻿namespace SeriousSIP
{
    public enum SIPCallDtmfMode
    {
        Rfc2833,
        Inband,
        DtmfRelay,
        Dtmf
    }
}
