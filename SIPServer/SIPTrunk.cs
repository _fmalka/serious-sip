﻿using Microsoft.Extensions.Logging;
using SIPSorcery.SIP;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace SeriousSIP
{
    public enum SIPTrunkState
    {
        Up,
        Down
    }

    public class SIPTrunk
    {
        private SIPTransport _sipTransport;
        private SIPURI _from;
        private SIPURI _to;
        private SIPToHeader _toHeader;
        private SIPContactHeader _contactHeader;
        private SIPEndPoint _sipEndPoint;
        private int _interval;
        private int _cseq = 0;
        private ILogger _logger;
        private bool _isRunning = false;

        public SIPTrunkState State { get; private set; } = SIPTrunkState.Down;

        /// <summary>
        /// Defines the delegate for trunk monitoring events
        /// </summary>
        public delegate Task SIPTrunkMonitorDelegate(SIPURI uri);

        /// <summary>
        /// Defines the delegate for trunk monitoring events
        /// </summary>
        public delegate Task SIPTrunkMonitorErrorDelegate(SIPURI uri, string reason);

        /// <summary>
        /// Triggers the trunk UP event
        /// </summary>
        public event SIPTrunkMonitorDelegate OnTrunkUp;

        /// <summary>
        /// Triggers the trunk DOWN event
        /// </summary>
        public event SIPTrunkMonitorErrorDelegate OnTrunkDown;

        /// <summary>
        /// The UserAgent header string.
        /// </summary>
        public string UserAgent { get; set; } = SIPConstants.SIP_USERAGENT_STRING;

        public SIPTrunk(SIPTransport sipTransport, SIPURI from, SIPURI to, int interval)
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}");
            _sipTransport = sipTransport;
            _from = from;
            _to = to;
            _interval = interval;
            _toHeader = new SIPToHeader(null, to, null);
            _contactHeader = new SIPContactHeader(null, from);
            _sipEndPoint = new SIPEndPoint(to);
        }

        public void Stop()
        {
            _isRunning = false;
        }

        public void Start()
        {
            _isRunning = true;

            Task.Factory.StartNew(async () =>
            {
                SIPNonInviteTransaction transaction = null;
                while (_isRunning)
                {
                    if (transaction == null)
                    {
                        SIPRequest sipRequest = SIPRequest.GetRequest(SIPMethodsEnum.OPTIONS, _from, _toHeader,
                                                        new SIPFromHeader(null, _from, CallProperties.CreateNewTag()));

                        sipRequest.Header.Contact = new List<SIPContactHeader> { _contactHeader };
                        sipRequest.Header.CSeq = ++_cseq;
                        sipRequest.Header.CallId = Guid.NewGuid().ToString();
                        sipRequest.Header.UserAgent = UserAgent;

                        transaction = new SIPNonInviteTransaction(_sipTransport, sipRequest, _sipEndPoint);

                        transaction.NonInviteTransactionFinalResponseReceived += (lep, rep, tn, rsp) =>
                        {
                            _logger.LogDebug("Server response " + rsp.Status + " received for " + _sipEndPoint.ToString() + ".");

                            if (rsp.Status == SIPResponseStatusCodesEnum.Ok)
                            {
                                if (State == SIPTrunkState.Down)
                                {
                                    State = SIPTrunkState.Up;
                                    OnTrunkUp?.Invoke(_to);
                                }
                            }
                            else
                            {
                                if (State == SIPTrunkState.Up)
                                {
                                    State = SIPTrunkState.Down;
                                    OnTrunkDown?.Invoke(_to, $"{rsp.Status}: {rsp.ReasonPhrase}");
                                }
                            }
                            transaction = null;
                            return Task.FromResult(SocketError.Success);
                        };
                        transaction.NonInviteTransactionTimedOut += (tn) =>
                        {
                            _logger.LogDebug("No response from server for " + _sipEndPoint.ToString() + ".");
                            if (State == SIPTrunkState.Up)
                            {
                                State = SIPTrunkState.Down;
                                OnTrunkDown?.Invoke(_to, "timeout");
                            }
                            transaction = null;
                        };

                        _sipTransport.SendTransaction(transaction);
                    }
                    await Task.Delay(_interval);
                }
            });
        }
    }
}
