﻿using Microsoft.Extensions.Logging;
using NAudio.Wave;
using Serilog.Context;
using SIPSorceryMedia.Abstractions.V1;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SeriousSIP
{
    public class RTPStreamAnalyzer
    {
        /// <summary>
        /// The ninimum sample block size to detect a tone.
        /// </summary>
        private const int _sampleBlockSize = 205;

        /// <summary>
        /// Keeps the sample rate to use to analyze the stream.
        /// </summary>
        private AudioSamplingRatesEnum _sampleRate = AudioSamplingRatesEnum.Rate8KHz;

        /// <summary>
        /// Keeps the position in sample block array.
        /// </summary>
        private int _sampleBlockPos = -1;

        /// <summary>
        /// Keeps the sample array to be analyzed.
        /// </summary>
        private float[] _sampleBlock;

        /// <summary>
        /// Keeps the call ID of the call using this analyzer.
        /// </summary>
        private readonly string _callId;

        /// <summary>
        /// Keeps the logger of the call.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Keeps the standard dtmf tones frequencies.
        /// </summary>
        private double[] _dtmfFrequencies = new[] { 697.0, 770.0, 852.0, 941.0, 1209.0, 1336.0, 1477.0 };

        /// <summary>
        /// Keeps the standard fax tones frequencies.
        /// </summary>
        private double[] _faxFrequencies = new[] { 1100.0, 2100.0 };

        /// <summary>
        /// Keeps the standard ringback tones frequencies.
        /// </summary>
        private double[] _ringbackFrequencies = new[] { 400.0, 440.0 };

        /// <summary>
        /// Keeps the number of frames to determine silence
        /// By default 50 frames -> 1 second
        /// </summary>
        private int _silenceFrames = 50;

        /// <summary>
        /// Keeps the number of frames to determine fax tone
        /// By default 50 frames -> 1 second
        /// </summary>
        private int _faxFrames = 50;

        /// <summary>
        /// Keeps the number of frames to determine ringback tone
        /// By default 15 frames -> 0.3 second
        /// </summary>
        private int _ringbackFrames = 15;

        /// <summary>
        /// Keeps a counter of number of frames to determine silence
        /// </summary>
        private int _silenceFramesCount = 0;

        /// <summary>
        /// Keeps the low sensitivity threshold of silence sample
        /// </summary>
        private double _silenceLowerSensitivyThreshold = -0.1;

        /// <summary>
        /// Keeps the high sensitivity threshold of silence sample
        /// </summary>
        private double _silenceHigherSensitivyThreshold = 0.1;

        /// <summary>
        /// Keeps a counter of number of frames to determine fax tone
        /// </summary>
        private int _faxFramesCount = 0;

        /// <summary>
        /// Keeps a counter of number of frames to determine ringback tone
        /// </summary>
        private int _ringbackFramesCount = 0;

        /// <summary>
        /// Keeps the mapping between digits and their respective frequencies.
        /// </summary>
        private static readonly Dictionary<int, Dictionary<int, string>> _phoneKeyOf = new Dictionary<int, Dictionary<int, string>>
        {
            {1209, new Dictionary<int, string> {{1477, "?"}, {1336, "?"}, {1209, "?"}, {941, "*"}, {852, "7"}, {770, "4"}, {697, "1"}}},
            {1336, new Dictionary<int, string> {{1477, "?"}, {1336, "?"}, {1209, "?"}, {941, "0"}, {852, "8"}, {770, "5"}, {697, "2"}}},
            {1477, new Dictionary<int, string> {{1477, "?"}, {1336, "?"}, {1209, "?"}, {941, "#"}, {852, "9"}, {770, "6"}, {697, "3"}}},
            { 941, new Dictionary<int, string> {{1477, "#"}, {1336, "0"}, {1209, "*"}, {941, "?"}, {852, "?"}, {770, "?"}, {697, "?"}}},
            { 852, new Dictionary<int, string> {{1477, "9"}, {1336, "8"}, {1209, "7"}, {941, "?"}, {852, "?"}, {770, "?"}, {697, "?"}}},
            { 770, new Dictionary<int, string> {{1477, "6"}, {1336, "5"}, {1209, "4"}, {941, "?"}, {852, "?"}, {770, "?"}, {697, "?"}}},
            { 697, new Dictionary<int, string> {{1477, "3"}, {1336, "2"}, {1209, "1"}, {941, "?"}, {852, "?"}, {770, "?"}, {697, "?"}}}
        };

        /// <summary>
        /// Keeps the mapping between digits and their respective frequencies.
        /// </summary>
        private static readonly Dictionary<string, int[]> _phoneKeyFrequencies = new Dictionary<string, int[]>
        {
            {"0", new int[] { 1336, 941 } },
            {"1", new int[] { 1209, 697 } },
            {"2", new int[] { 1336, 697 } },
            {"3", new int[] { 1477, 697 } },
            {"4", new int[] { 1209, 770 } },
            {"5", new int[] { 1336, 770 } },
            {"6", new int[] { 1477, 770 } },
            {"7", new int[] { 1209, 852 } },
            {"8", new int[] { 1336, 852 } },
            {"9", new int[] { 1477, 852 } },
            {"*", new int[] { 1209, 941 } },
            {"#", new int[] { 1477, 941 } },
            {"10", new int[] { 1209, 941 } },
            {"11", new int[] { 1477, 941 } }
        };

        /// <summary>
        /// Keeps an empty sample array for silence detection.
        /// </summary>
        private static readonly float[] _silentSampleBlock = new float[_sampleBlockSize];

        /// <summary>
        /// Enumarates silence ON/OFF states.
        /// </summary>
        public enum SilenceState { On, Off }

        /// <summary>
        /// Keeps an empty sample array for silence detection.
        /// </summary>
        private SilenceState _currentSilenceState = SilenceState.Off;

        /// <summary>
        /// Keeps the timestamp of the last silence ON event.
        /// </summary>
        private long _silenceOnStartTime = 0;

        /// <summary>
        /// Keeps the timestamp of the last silence OFF event.
        /// </summary>
        private long _silenceOffStartTime = 0;

        /// <summary>
        /// Keeps the timestamp of the last inband digit detected.
        /// </summary>
        private long _lastDigitTimestamp = 0;

        /// <summary>
        /// Keeps the minimum time between digits in milliseconds, 250 by default.
        /// </summary>
        private int _interDigitTime = 250;

        /// <summary>
        /// Keeps the timestamp of the start of the call progress analysis procedure.
        /// </summary>
        private long _cpaStartTime = 0;

        /// <summary>
        /// Keeps the time offest in milliseconds to wait until 
        /// call progress analysis procedure is aborted.
        /// </summary>
        private long _cpaTimeout = 0;

        /// <summary>
        /// Keeps the time in milliseconds to recognize PVD.
        /// </summary>
        private int _cpaPVDThreshold = 2000;

        /// <summary>
        /// Keeps the time in milliseconds to recognize PAMD.
        /// </summary>
        private int _cpaPAMDThreshold = 4000;

        /// <summary>
        /// A delegate for inband dtmf detection events triggering.
        /// </summary>
        public delegate void RTPStreamInbandDtmfDelegate(string digit);

        /// <summary>
        /// A delegate for fax detection events triggering.
        /// </summary>
        public delegate void RTPStreamEventDelegate();

        /// <summary>
        /// A delegate for silence detection events triggering.
        /// </summary>
        public delegate void RTPStreamSilenceDelegate(SilenceState state);

        /// <summary>
        /// Allows application to register to the inband dtmf detection event.
        /// </summary>
        public event RTPStreamInbandDtmfDelegate OnInbandDtmf;

        /// <summary>
        /// Allows application to register to the fax tone detection event.
        /// </summary>
        public event RTPStreamEventDelegate OnFaxTone;

        /// <summary>
        /// Allows application to register to the fax tone detection event.
        /// </summary>
        public event RTPStreamEventDelegate OnRingbackTone;

        /// <summary>
        /// Allows application to register to the event.
        /// </summary>
        public event RTPStreamSilenceDelegate OnSilenceStateChange;

        /// <summary>
        /// Triggers the PAMD event
        /// </summary>
        public event RTPStreamEventDelegate OnPAMD;

        /// <summary>
        /// Triggers the PVD event
        /// </summary>
        public event RTPStreamEventDelegate OnPVD;

        /// <summary>
        /// Creates a new stream analyzer for a specific call.
        /// </summary>
        /// <param name="callId">the call ID header of the call</param> 
        public RTPStreamAnalyzer(string callId)
            :this(callId, new CPAConfiguration())
        {
        }

        /// <summary>
        /// Creates a new stream analyzer for a specific call.
        /// </summary>
        /// <param name="callId">the call ID header of the call</param> 
        /// <param name="configuration">the custom configuration to use for the CPA session</param>
        public RTPStreamAnalyzer(string callId, CPAConfiguration configuration)
        {
            _dtmfFrequencies = configuration.DtmfFrequencies;
            _faxFrequencies = configuration.FaxFrequencies;
            _ringbackFrequencies = configuration.RingbackFrequencies;
            _silenceFrames = configuration.SilenceFrames;
            _faxFrames = configuration.FaxFrames;
            _ringbackFrames = configuration.RingbackFrames;
            _silenceHigherSensitivyThreshold = configuration.SilenceHigherSensitivyThreshold;
            _silenceLowerSensitivyThreshold = configuration.SilenceLowerSensitivyThreshold;
            _interDigitTime = configuration.InterDigitTime;
            _cpaPVDThreshold = configuration.CpaPVDThreshold;
            _cpaPAMDThreshold = configuration.CpaPAMDThreshold;
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().Namespace}.{this.GetType().Name}");
            _callId = callId;
        }

        /// <summary>
        /// Changes the default sample rate (default: 8000)
        /// </summary>
        /// <param name="sampleRate">The sampe rate to use to analyze the stream</param>
        public void SetSampleRate(AudioSamplingRatesEnum sampleRate)
        {
            using (LogContext.PushProperty("CallContext", _callId))
            {
                _logger.LogInformation($"Changing sample rate to {sampleRate}");
                _sampleRate = sampleRate;
            }
        }

        /// <summary>
        /// Changes the default inter-digit time (default: 250ms)
        /// </summary>
        /// <param name="interDigitTime">The new inter-digit time in milliseconds</param>
        public void SetInterDigitTime(int interDigitTime)
        {
            using (LogContext.PushProperty("CallContext", _callId))
            {
                _logger.LogInformation($"Changing inter-digit time to {interDigitTime}");
                _interDigitTime = interDigitTime;
            }
        }

        /// <summary>
        /// Retrieve the high and low frequencies of a dtmf tone.
        /// </summary>
        /// <param name="digit">The digit to retrieve frequencies for.</param>
        public int[] GetDtmfFrequencies(string digit)
        {
            int[] result;
            _phoneKeyFrequencies.TryGetValue(digit, out result);
            return result;
        }

        /// <summary>
        /// Gets the silence duration
        /// </summary>
        /// <returns>Seconds elapsed since no noise received.</returns>
        internal int GetSilenceOnDuration()
        {
            if (_silenceOnStartTime == 0)
                return 0;

            return ((int)(DateTimeOffset.Now.ToUnixTimeMilliseconds() - _silenceOnStartTime));
        }

        /// <summary>
        /// Gets the non silence duration
        /// </summary>
        /// <returns>Seconds elapsed since no noise received.</returns>
        internal int GetSilenceOffDuration()
        {
            if (_silenceOffStartTime == 0)
                return 0;

            return ((int)(DateTimeOffset.Now.ToUnixTimeMilliseconds() - _silenceOffStartTime));
        }

        /// <summary>
        /// Resets the silence duration timers
        /// </summary>
        /// <returns>Seconds elapsed since no noise received.</returns>
        internal void ResetSilenceTimers()
        {
            _silenceOnStartTime = 0;
            _silenceOffStartTime = 0;
            _silenceFramesCount = 0;
            _currentSilenceState = SilenceState.Off;
        }

        /// <summary>
        /// Check silence ON, silence OFF duration to determine
        /// PVD or PAMD events triggering.
        /// </summary>
        private void CheckCallProgressAnalysis()
        {
            if ((_cpaStartTime == 0) || (_cpaTimeout == 0))
                return;

            else if (_cpaStartTime + _cpaTimeout > DateTimeOffset.Now.ToUnixTimeMilliseconds())
            {
                var silenceOffDuration = GetSilenceOffDuration();
                var silenceOnDuration = GetSilenceOnDuration();
                _logger.LogDebug($"Do call progress analysis; silence OFF duration: {silenceOffDuration}, silence ON duration: {silenceOnDuration}");
                if (silenceOnDuration > _cpaPVDThreshold)
                {
                    _cpaStartTime = 0;
                    _cpaTimeout = 0;
                    _logger.LogInformation($"Raising PVD because continuous silence during {_cpaPVDThreshold} milliseconds");
                    OnPVD?.Invoke();
                }
                else if (silenceOffDuration > _cpaPAMDThreshold)
                {
                    _cpaStartTime = 0;
                    _cpaTimeout = 0;
                    _logger.LogInformation($"Raising PAMD because continuous speech during {_cpaPAMDThreshold} milliseconds");
                    OnPAMD?.Invoke();
                }
            }
        }

        /// <summary>
        /// Check for silence in the stream
        /// </summary>
        /// <param name="samples">The float array representation of the stream sample</param>
        private void CheckSilenceState(float[] samples)
        {
            if (OnSilenceStateChange != null)
            {
                using (LogContext.PushProperty("CallContext", _callId))
                {
                    _logger.LogTrace($"Checking for silence in samples.");

                    // attenuate to detect silence even if not complete silence
                    // to test
                    for (int index = 0; index < samples.Length; index++)
                    {
                        if (samples[index] > _silenceLowerSensitivyThreshold && 
                            samples[index] < _silenceHigherSensitivyThreshold)
                        {
                            samples[index] = 0;
                        }
                    }

                    if (samples.SequenceEqual(_silentSampleBlock))
                    {
                        _silenceFramesCount++;

                        if (_silenceFramesCount == _silenceFrames)
                        {
                            _silenceFramesCount = 0;
                            if (_currentSilenceState != SilenceState.On)
                            {
                                _logger.LogDebug($"Silence is ON");
                                _currentSilenceState = SilenceState.On;
                                _silenceOffStartTime = 0;
                                _silenceOnStartTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                                OnSilenceStateChange.Invoke(SilenceState.On);
                            }
                        }   
                    }
                    else
                    {
                        if (_currentSilenceState != SilenceState.Off)
                        {
                            _silenceFramesCount = 0;
                            _logger.LogDebug($"Silence is OFF");
                            _currentSilenceState = SilenceState.Off;
                            _silenceOnStartTime = 0;
                            _silenceOffStartTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                            OnSilenceStateChange.Invoke(SilenceState.Off);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check for inband dtmf in the stream
        /// </summary>
        /// <param name="samples">The float array representation of the stream sample</param>
        private void CheckInbandDtmf(float[] samples)
        {
            if (OnInbandDtmf != null)
            {
                using (LogContext.PushProperty("CallContext", _callId))
                {
                    _logger.LogTrace($"Checking for dtmf in samples.");

                    var magnitudes = _dtmfFrequencies.Select(frequency => new
                    {
                        Frequency = frequency,
                        Magnitude = CalculateGoertzel(samples, frequency)
                    }).ToList();

                    var sortedMagnitudes = magnitudes.OrderByDescending(result => result.Magnitude);
                    var highestMagnitudes = sortedMagnitudes.Take(2).ToList();

                    if (highestMagnitudes.All(result => result.Magnitude > 10.0))
                    {
                        string key = _phoneKeyOf[(int)highestMagnitudes[0].Frequency][(int)highestMagnitudes[1].Frequency];
                        if ((_lastDigitTimestamp + _interDigitTime) < DateTimeOffset.Now.ToUnixTimeMilliseconds())
                        {
                            _logger.LogDebug($"Found matching freq. for key: {key}");
                            _lastDigitTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                            OnInbandDtmf?.Invoke(key);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check for fax tone in the stream
        /// </summary>
        /// <param name="samples">The float array representation of the stream sample</param>
        private void CheckFaxTone(float[] samples)
        {
            if ((_cpaStartTime == 0) || (_cpaTimeout == 0))
                return;

            if (OnFaxTone != null)
            {
                using (LogContext.PushProperty("CallContext", _callId))
                {
                    _logger.LogTrace($"Checking for fax tone in samples.");

                    var magnitudes = _faxFrequencies.Select(frequency => new
                    {
                        Frequency = frequency,
                        Magnitude = CalculateGoertzel(samples, frequency)
                    }).ToList();

                    var sortedMagnitudes = magnitudes.OrderByDescending(result => result.Magnitude);
                    var highestMagnitudes = sortedMagnitudes.Take(1).ToList();

                    _logger.LogTrace($"Frequency: {highestMagnitudes[0].Frequency}, magnitude: {highestMagnitudes[0].Magnitude}");

                    if (highestMagnitudes[0].Magnitude > 10.0)
                    //if (highestMagnitudes[0].Magnitude > 5.0)
                    {
                        _faxFramesCount++;

                        if (_faxFramesCount == _faxFrames)
                        {
                            _faxFramesCount = 0;
                            _cpaStartTime = 0;
                            _cpaTimeout = 0;
                            _logger.LogDebug($"Positive fax tone detection");
                            OnFaxTone?.Invoke();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check for ringback in the stream
        /// </summary>
        /// <param name="samples">The float array representation of the stream sample</param>
        private void CheckRingbackTone(float[] samples)
        {
            if ((_cpaStartTime == 0) || (_cpaTimeout == 0))
                return;

            if (OnRingbackTone != null)
            {
                using (LogContext.PushProperty("CallContext", _callId))
                {
                    _logger.LogTrace($"Checking for ringback tone in samples.");

                    var magnitudes = _ringbackFrequencies.Select(frequency => new
                    {
                        Frequency = frequency,
                        Magnitude = CalculateGoertzel(samples, frequency)
                    }).ToList();

                    var sortedMagnitudes = magnitudes.OrderByDescending(result => result.Magnitude);
                    var highestMagnitudes = sortedMagnitudes.Take(1).ToList();

                    _logger.LogTrace($"Frequency: {highestMagnitudes[0].Frequency}, magnitude: {highestMagnitudes[0].Magnitude}");

                    if (highestMagnitudes[0].Magnitude > 10.0)
                    {
                        _ringbackFramesCount++;

                        if (_ringbackFramesCount == _ringbackFrames)
                        {
                            _ringbackFramesCount = 0;
                            _logger.LogDebug($"Positive ringback tone detection");
                            OnRingbackTone?.Invoke();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Decode a pcm sample and check for voice events in the stream.
        /// </summary>
        /// <param name="buffer">A byte array representing the sample</param>
        /// <param name="format">The codec in which the sample is encoded</param>
        public void Analyze_OLD(byte[] buffer, SDPWellKnownMediaFormatsEnum format)
        {
            // do nothing if no one registered to the events
            if ((OnInbandDtmf == null) && (OnSilenceStateChange == null))
                return;

            using (LogContext.PushProperty("CallContext", _callId))
            {
                if (_sampleBlockPos == -1)
                {
                    _logger.LogTrace($"Creating new sample block array.");
                    _sampleBlockPos = 0;
                    _sampleBlock = new float[buffer.Length * 2];
                }

                _logger.LogTrace($"Converting input buffer to float array.");
                for (int index = 0; index < buffer.Length; index++)
                {
                    if (format == SDPWellKnownMediaFormatsEnum.PCMA)
                    {
                        short pcm = NAudio.Codecs.ALawDecoder.ALawToLinearSample(buffer[index]);
                        _sampleBlock[_sampleBlockPos++] = pcm / 32768f;
                    }
                    else
                    {
                        short pcm = NAudio.Codecs.MuLawDecoder.MuLawToLinearSample(buffer[index]);
                        _sampleBlock[_sampleBlockPos++] = pcm / 32768f;
                    }
                }

                if (_sampleBlockPos > 205)
                {
                    CheckSilenceState(_sampleBlock.Take(_sampleBlockSize).ToArray());
                    CheckCallProgressAnalysis();
                    CheckInbandDtmf(_sampleBlock.Take(_sampleBlockSize).ToArray());
                    CheckFaxTone(_sampleBlock.Take(_sampleBlockSize).ToArray());
                    CheckRingbackTone(_sampleBlock.Take(_sampleBlockSize).ToArray());

                    _logger.LogTrace($"Resetting float array.");
                    _sampleBlockPos = -1;
                }
            }
        }

        /// <summary>
        /// Check for voice events in the stream.
        /// </summary>
        /// <param name="linearSample">A short array representing a linear sample</param>
        public void Analyze(short[] linearSample)
        {
            // do nothing if no one registered to the events
            if ((OnInbandDtmf == null) && (OnSilenceStateChange == null))
                return;

            using (LogContext.PushProperty("CallContext", _callId))
            {
                try
                {
                    if (_sampleBlockPos == -1)
                    {
                        _logger.LogTrace($"Creating new sample block array.");
                        _sampleBlockPos = 0;
                        _sampleBlock = new float[linearSample.Length * 2];
                    }

                    _logger.LogTrace($"Converting input buffer to float array.");
                    for (int index = 0; index < linearSample.Length; index++)
                    {
                        _sampleBlock[_sampleBlockPos++] = linearSample[index] / 32768f;
                    }

                    if (_sampleBlockPos > 205)
                    {
                        CheckSilenceState(_sampleBlock.Take(_sampleBlockSize).ToArray());
                        CheckCallProgressAnalysis();
                        CheckInbandDtmf(_sampleBlock.Take(_sampleBlockSize).ToArray());
                        CheckFaxTone(_sampleBlock.Take(_sampleBlockSize).ToArray());
                        CheckRingbackTone(_sampleBlock.Take(_sampleBlockSize).ToArray());

                        _logger.LogTrace($"Resetting float array.");
                        _sampleBlockPos = -1;
                    }
                } catch (Exception e)
                {
                    _sampleBlockPos = -1;
                    _logger.LogError(e.Message, e);
                }
            }
        }

        /// <summary>
        /// Start the call progress analysis procedure
        /// Stops at first positive detection or at timeout
        /// </summary>
        /// <param name="timeout">Timeout in milliseconds</param>
        public void StartCallProgressAnalysis(int timeout) 
        {
            _cpaStartTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            _cpaTimeout = timeout;
        }

        /// <summary>
        /// Calculate the Goertzel coheficient to determine the magnitude of a tone
        /// </summary>
        /// <param name="samples">A float array reprenting the sample to be analyzed</param>
        /// <param name="frequency">A single tone frequency to be detected</param>
        /// <returns></returns>
        private double CalculateGoertzel(float[] samples, double frequency)
        {
            var normalizedFrequency = Math.Round(frequency * samples.Length / (int)_sampleRate);
            var w = (2.0 * Math.PI / samples.Length) * normalizedFrequency;
            var cosine = Math.Cos(w);
            var sine = Math.Sin(w);
            var coeff = 2.0 * cosine;

            var q0 = 0.0;
            var q1 = 0.0;
            var q2 = 0.0;

            foreach (var sample in samples)
            {
                q0 = coeff * q1 - q2 + sample;
                q2 = q1;
                q1 = q0;
            }

            return Math.Sqrt(q1 * q1 + q2 * q2 - q1 * q2 * coeff);
        }
    }
}
