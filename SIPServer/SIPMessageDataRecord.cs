using System;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.Extensions.Logging;
using SIPSorcery.SIP;

namespace SeriousSIP
{
    public delegate void MDRReadyDelegate(SIPMessageDataRecord cdr);                // Used to inform MDR handlers when a MDR has been udpated.

    /// <summary>
    /// Call detail record for a SIP message.
    /// </summary>
    public class SIPMessageDataRecord
    {
        private ILogger _logger; 
        private static string m_newLine = Environment.NewLine;

        public static event MDRReadyDelegate MDRCreated = c => { if (c.WriteOnCreated) c.WriteMDR(); };
        public static event MDRReadyDelegate MDRAccept = c => { if (c.WriteOnAccept) c.WriteMDR(); };
        public static event MDRReadyDelegate MDRResponse = c => { if (c.WriteOnResponse) c.WriteMDR(); };
        public static event MDRReadyDelegate MDRReject = c => { if (c.WriteOnReject) c.WriteMDR(); };

        public void WriteMDR()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append($"\"type\":\"message\",");
            sb.Append($"\"callId\":\"{this.CallId}\",");
            sb.Append($"\"createdTime\":\"{this.Created}\",");
            sb.Append($"\"calldirection\":\"{this.CallDirection}\",");
            sb.Append($"\"from\":\"{this.From}\",");
            sb.Append($"\"to\":\"{this.Destination}\",");
            sb.Append($"\"localEndPoint\":\"{this.LocalSIPEndPoint}\",");
            sb.Append($"\"remoteEndPoint\":\"{this.RemoteSIPEndPoint}\",");
            sb.Append($"\"responseReason\":\"{this.ResponseReasonPhrase}\",");
            sb.Append($"\"responseStatus\":\"{this.ResponseStatus}\",");
            sb.Append($"\"responseTime\":\"{this.ResponseTime}\",");
            sb.Append($"\"acceptReason\":\"{this.AcceptReasonPhrase}\",");
            sb.Append($"\"acceptStatus\":\"{this.AcceptStatus}\",");
            sb.Append($"\"acceptTime\":\"{this.AcceptTime}\",");
            sb.Append($"\"rejectReason\":\"{this.RejectReasonPhrase}\",");
            sb.Append($"\"rejectStatus\":\"{this.RejectStatus}\",");
            sb.Append($"\"rejectTime\":\"{this.RejectTime}\",");
            sb.Append($"\"length\":\"{this.Length}\"");
            sb.Append("}");
            _logger.LogTrace(sb.ToString());
        }

        public bool WriteOnCreated { get; set; }

        public bool WriteOnAccept { get; set; }

        public bool WriteOnResponse { get; set; }

        public bool WriteOnReject { get; set; }

        public string Owner { get; set; }

        public string AdminMemberId { get; set; }

        public DateTimeOffset Created { get; set; }

        public SIPCallDirection CallDirection { get; set; }

        public SIPURI Destination { get; set; }

        public SIPURI From { get; set; }

        public int Length { get; set; }

        public int ResponseStatus { get; set; }

        public string ResponseReasonPhrase { get; set; }

        private DateTimeOffset? m_responseTime;

        public DateTimeOffset? ResponseTime
        {
            get { return m_responseTime; }
            set
            {
                if (value != null)
                {
                    m_responseTime = value.Value.ToUniversalTime();
                }
                else
                {
                    m_responseTime = null;
                }
            }
        }

        public int AcceptStatus { get; set; }

        public string AcceptReasonPhrase { get; set; }

        private DateTimeOffset? m_acceptTime;

        public DateTimeOffset? AcceptTime
        {
            get { return m_acceptTime; }
            set
            {
                if (value != null)
                {
                    m_acceptTime = value.Value.ToUniversalTime();
                }
                else
                {
                    m_acceptTime = null;
                }
            }
        }

        private DateTimeOffset? m_rejectTime;

        public DateTimeOffset? RejectTime
        {
            get { return m_rejectTime; }
            set
            {
                if (value != null)
                {
                    m_rejectTime = value.Value.ToUniversalTime();
                }
                else
                {
                    m_rejectTime = null;
                }
            }
        }

        public string RejectReasonPhrase { get; set; }
        public int RejectStatus { get; set; }


        public string CallId { get; set; }
        public SIPEndPoint LocalSIPEndPoint { get; set; }
        public SIPEndPoint RemoteSIPEndPoint { get; set; }
        public bool InProgress { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsHungup { get; set; }

        public SIPMessageDataRecord(
            SIPCallDirection callDirection,
            SIPURI destination,
            SIPURI from,
            string callId,
            int length,
            SIPEndPoint localSIPEndPoint,
            SIPEndPoint remoteSIPEndPoint)
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}"); 
            Created = DateTimeOffset.UtcNow;
            CallDirection = callDirection;
            Destination = destination;
            From = from;
            CallId = callId;
            LocalSIPEndPoint = localSIPEndPoint;
            RemoteSIPEndPoint = remoteSIPEndPoint;
            InProgress = false;
            IsAnswered = false;
            IsHungup = false;
            IsCancelled = false;

            WriteOnCreated = false;
            WriteOnAccept = false;
            WriteOnResponse = true;
            WriteOnReject = true;

            Length = length;
            MDRCreated(this);
        }

        public void Accept(SIPResponseStatusCodesEnum acceptStatus, string acceptReason, SIPEndPoint localEndPoint, SIPEndPoint remoteEndPoint)
        {
            try
            {
                InProgress = true;
                AcceptTime = DateTimeOffset.UtcNow;
                AcceptStatus = (int)acceptStatus;
                AcceptReasonPhrase = acceptReason;

                if (localEndPoint != null)
                {
                    LocalSIPEndPoint = localEndPoint;
                }

                if (remoteEndPoint != null)
                {
                    RemoteSIPEndPoint = remoteEndPoint;
                }

                MDRAccept(this);
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPMDR Accept. " + excp);
            }
        }

        public void Response(int responseStatusCode, SIPResponseStatusCodesEnum responseStatus, string responseReason, SIPEndPoint localEndPoint, SIPEndPoint remoteEndPoint)
        {
            try
            {
                IsAnswered = true;
                ResponseTime = DateTimeOffset.UtcNow;
                ResponseStatus = (int)responseStatus;
                ResponseReasonPhrase = responseReason;

                if (localEndPoint != null)
                {
                    LocalSIPEndPoint = localEndPoint;
                }

                if (remoteEndPoint != null)
                {
                    RemoteSIPEndPoint = remoteEndPoint;
                }

                MDRResponse(this);
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPMDR Response. " + excp);
            }
        }

        public void TimedOut()
        {
            try
            {
                RejectTime = DateTimeOffset.UtcNow;
                RejectReasonPhrase = "Timed out";
                MDRReject(this);
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPMDR TimedOut. " + excp);
            }
        }

        public void Reject(SIPResponseStatusCodesEnum rejectStatus, string rejectReason)
        {
            try
            {
                RejectTime = DateTimeOffset.UtcNow;
                RejectReasonPhrase = rejectReason;
                RejectStatus = (int)rejectStatus;
                MDRReject(this);
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPMDR Hungup. " + excp);
            }
        }
    }
}
