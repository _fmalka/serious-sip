﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeriousSIP
{
    public class SIPIdentity
    {
        public string Uri { get; set; }
        public bool IsRegistered { get; private set; }
        public bool IsTalking { get; private set; }

        public SIPIdentity(string uri, bool registered, bool inCall)
        {
            Uri = uri;
            IsRegistered = registered;
            IsTalking = inCall;
        }
    }
}
