﻿namespace SeriousSIP
{
    public enum SIPCallAction
    {
        Idle,
        Playing,
        Recording,
        SendingDigits,
        ReceivingDigits,
        Bridged
    }
}
