﻿using Microsoft.Extensions.Logging;
using NAudio.Wave;
using Serilog.Context;
using SIPSorcery.Media;
using SIPSorcery.Net;
using SIPSorcery.SIP;
using SIPSorcery.SIP.App;
using SIPSorceryMedia.Abstractions.V1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using static SeriousSIP.RTPStreamAnalyzer;
using System.Speech.Synthesis;
using System.Speech.AudioFormat;
using System.Linq;
using System.Net;

namespace SeriousSIP
{
    /// <summary>
    /// Extend the default SIP user agent that encompasses both client and server user agents.
    /// Manage and provides an interface to basic operations after the call is established like play/record/getdigits.
    /// </summary
    public class SIPCall : SIPUserAgent
    {
        /// <summary>
        /// Keeps the underlaying sip transport.
        /// </summary>
        private SIPTransport _sipTransport;

        /// <summary>
        /// Keeps the logger created by the main application.
        /// </summary>
        private ILogger _logger;

        /// <summary>
        /// Keeps the rtp session of the call.
        /// </summary>
        public VoIPMediaSession RtpSession { get; private set; }

        /// <summary>
        /// Keeps the local sip endpoint of the call.
        /// </summary>
        public SIPEndPoint LocalEndPoint { get; private set; }

        /// <summary>
        /// Keeps the remote sip endpoint of the call.
        /// </summary>
        public SIPEndPoint RemoteEndPoint { get; private set; }

        /// <summary>
        /// Keeps the rtp session of the call.
        /// </summary>
        public RTCPeerConnection RtcPeerConnection { get; private set; }

        /// <summary>
        /// Keeps the rtp IPv4 address to use for the call.
        /// </summary>
        public IPAddress RtpAddress { get; set; } = IPAddress.Any;

        /// <summary>
        /// Keeps a user context attached to the call
        /// </summary>
        public string UserContext { get; set; } = String.Empty;

        /// <summary>
        /// Keeps records if PlayLoop is running
        /// </summary>
        private bool PlayLoopOn { get; set; } = false;

        /// <summary>
        /// When true, disconnect the call if no RTP is received for 30s.
        /// </summary>
        public bool HangupWhenNoRtp { get; set; } = true;

        /// <summary>
        /// Keeps the default file format for play and record.
        /// </summary>
        private readonly WaveFormat _waveFormat = new WaveFormat((int)AudioSamplingRatesEnum.Rate8KHz, 16, 1);

        /// <summary>
        /// Keeps the file writer used for recording files.
        /// </summary>
        private WaveFileWriter _waveFileWriter = null;

        /// <summary>
        /// Keeps the digits collected during the call.
        /// </summary>
        private string _digitsBuffer = "";

        /// <summary>
        /// Keeps the current action executing on the call.
        /// </summary>
        private SIPCallAction _action = SIPCallAction.Idle;

        /// <summary>
        /// Keeps the start time (connected) of the call.
        /// </summary>
        private long _startTime = 0;

        /// <summary>
        /// Keeps the current action executing on the call.
        /// </summary>
        private SIPCallDirection _direction = SIPCallDirection.None;

        /// <summary>
        /// Keeps the call identity.
        /// </summary>
        private string _identity = null;

        /// <summary>
        /// Keeps an internal cancellation token.
        /// </summary>
        private readonly CancellationTokenSource _exitCts = new CancellationTokenSource();

        /// <summary>
        /// Keeps the last request received during the call.
        /// </summary>
        private SIPRequest _lastSIPRequest = null;

        /// <summary>
        /// Keeps the user agent server when used.
        /// </summary>
        private SIPServerUserAgent _sipServerUserAgent;

        /// <summary>
        /// Keeps the user agent client when used.
        /// </summary>
        private SIPClientUserAgent _sipClientUserAgent;

        /// <summary>
        /// Keeps the analyzer object for inband detection.
        /// </summary>
        private RTPStreamAnalyzer _analyzer = null;

        /// <summary>
        /// Keeps the list of codecs that are allowed to be used.
        /// </summary>
        private static List<AudioCodecsEnum> _codecs = new List<AudioCodecsEnum> { AudioCodecsEnum.PCMU, AudioCodecsEnum.PCMA, AudioCodecsEnum.G722 };

        /// <summary>
        /// Keeps the prefered dtmf mode for the call.
        /// </summary>
        private static SIPCallDtmfMode _dtmfMode = SIPCallDtmfMode.Rfc2833;

        /// <summary>
        /// Defines the delegate for call progress analysis events
        /// </summary>
        public delegate Task SIPCallCPADelegate();

        /// <summary>
        /// Defines the delegate for call progress analysis events
        /// </summary>
        public delegate Task SIPCallErrorDelegate(string message);

        /// <summary>
        /// Defines the delegate for linear samples manipulation (conference)
        /// </summary>
        public delegate void SIPCallLinearSampleDelegate(String callId, short[] sample);

        /// <summary>
        /// Triggers an event with a linear sample to process
        /// </summary>
        public event SIPCallLinearSampleDelegate OnLinearSampleAvailable;

        /// <summary>
        /// Triggers the PAMD event
        /// </summary>
        public event SIPCallCPADelegate OnPositiveAnswerMachineDetection;

        /// <summary>
        /// Triggers the PVD event
        /// </summary>
        public event SIPCallCPADelegate OnPositiveVoiceDetection;

        /// <summary>
        /// Triggers the PFD event
        /// </summary>
        public event SIPCallCPADelegate OnPositiveFaxDetection;

        /// <summary>
        /// Triggers the Ringback tone event
        /// </summary>
        public event SIPCallCPADelegate OnRingbackToneDetection;

        /// <summary>
        /// Triggers the silence state change event
        /// </summary>
        public event RTPStreamSilenceDelegate OnSilenceStateChange;

        /// <summary>
        /// Triggers the get digits termination event
        /// </summary>
        public event SIPCallCPADelegate OnGetDigitsDone;

        /// <summary>
        /// Triggers the send digit termination event
        /// </summary>
        public event SIPCallCPADelegate OnSendDigitDone;

        /// <summary>
        /// Triggers the send digits termination event
        /// </summary>
        public event SIPCallCPADelegate OnSendDigitsDone;

        /// <summary>
        /// Triggers the play termination event
        /// </summary>
        public event SIPCallCPADelegate OnPlayDone;

        /// <summary>
        /// Triggers when play error occurs
        /// </summary>
        public event SIPCallErrorDelegate OnPlayError;

        /// <summary>
        /// Triggers when record error occurs
        /// </summary>
        public event SIPCallErrorDelegate OnRecordError;

        /// <summary>
        /// Triggers the speak termination event
        /// </summary>
        public event SIPCallCPADelegate OnSpeakDone;

        /// <summary>
        /// Triggers the playMusic termination event
        /// </summary>
        public event SIPCallCPADelegate OnPlayMusicDone;

        /// <summary>
        /// Triggers the playFiles termination event
        /// </summary>
        public event SIPCallCPADelegate OnPlayFilesDone;

        /// <summary>
        /// Triggers the playRecord termination event
        /// </summary>
        public event SIPCallCPADelegate OnPlayRecordDone;

        /// <summary>
        /// Triggers the record termination event
        /// </summary>
        public event SIPCallCPADelegate OnRecordDone;

        /// <summary>
        /// Triggers the call offered completion event
        /// </summary>
        public event SIPCallCPADelegate OnCallOffered;

        /// <summary>
        /// Triggers the call accepted completion event
        /// </summary>
        public event SIPCallCPADelegate OnCallAccepted;

        /// <summary>
        /// Triggers the call answered completion event
        /// </summary>
        public event SIPCallCPADelegate OnCallAnswered;

        /// <summary>
        /// Keeps the audio encoder utils class.
        /// </summary>
        AudioEncoder audioEncoder = new AudioEncoder();

        private SIPCallState _state { get; set; }

        /// <summary>
        /// Keeps the audio encoder utils class.
        /// </summary>
        public string BridgeId { get; private set; }

        /// <summary>
        /// Keeps the call CDR.
        /// </summary>
        public SIPCallDataRecord CDR { get; set; }

        /// <summary>
        /// Creates a SIP Call class for an outgoing call using default codecs.
        /// </summary>
        /// <param name="sipTransport">The transport to be used to create the outbound call.</param>
        /// <param name="logger">The logger to be used to log the call events.</param>
        //public SIPCall(SIPTransport sipTransport)
        //    : this(sipTransport, null, null, null, _codecs, _dtmfMode, null)
        //{ }

        /// <summary>
        /// Creates a SIP Call class for an outgoing call using pre-defined set of codecs.
        /// </summary>
        /// <param name="sipTransport">The transport to be used to create the outbound call.</param>
        /// <param name="codecs">List of supported codecs for the call</param>
        /// <param name="logger">The logger to be used to log the call events.</param>
        //public SIPCall(SIPTransport sipTransport, List<AudioCodecsEnum> codecs)
        //    : this(sipTransport, null, null, null, codecs, _dtmfMode, null)
        //{ }

        /// <summary>
        /// Creates a SIP Call class for an incoming call using default codecs.
        /// </summary>
        /// <param name="sipTransport">The transport to be used to create the outbound call.</param>
        /// <param name="sipRequest">The incoming request that triggered the incoming call.</param>
        /// <param name="logger">The logger to be used to log the call events.</param>
        public SIPCall(SIPTransport sipTransport, string username, string password, SIPRequest sipRequest)
            : this(sipTransport, username, password, sipRequest, _codecs, _dtmfMode, null)
        { }

        /// <summary>
        /// Creates a SIP Call class for an incoming call using pre-define set of codecs.
        /// </summary>
        /// <param name="sipTransport">The transport to be used to create the outbound call.</param>
        /// <param name="sipRequest">The incoming request that triggered the incoming call.</param>
        /// <param name="codecs">List of supported codecs for the call</param>
        /// <param name="logger">The logger to be used to log the call events.</param>
        public SIPCall(SIPTransport sipTransport, string username, string password, SIPRequest sipRequest, List<AudioCodecsEnum> codecs)
            : this(sipTransport, username, password, sipRequest, codecs, _dtmfMode, null)
        { }

        /// <summary>
        /// Creates a SIP Call class for an outgoing call using pre-defined set of codecs.
        /// </summary>
        /// <param name="sipTransport">The transport to be used to create the outbound call.</param>
        /// <param name="codecs">List of supported codecs for the call</param>
        /// <param name="logger">The logger to be used to log the call events.</param>
        //public SIPCall(SIPTransport sipTransport, List<AudioCodecsEnum> codecs, SIPCallDtmfMode dtmfMode)
        //    : this(sipTransport, null, null, null, codecs, dtmfMode, null)
        //{ }

        /// <summary>
        /// Creates a SIP Call class for an incoming call using pre-define set of codecs.
        /// </summary>
        /// <param name="sipTransport">The transport to be used to create the outbound call.</param>
        /// <param name="sipRequest">The incoming request that triggered the incoming call.</param>
        /// <param name="codecs">List of supported codecs for the call</param>
        /// <param name="dtmfMode">The prefered mode for dtmf exchange</param>
        /// <param name="logger">The logger to be used to log the call events.</param>
        public SIPCall(SIPTransport sipTransport, string username, string password,
        SIPRequest sipRequest, List<AudioCodecsEnum> codecs, SIPCallDtmfMode dtmfMode, IPAddress rtpAddress)
        : base(sipTransport, username, password)
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}");
            _sipTransport = sipTransport;
            _codecs = codecs;
            _dtmfMode = dtmfMode;

            if (rtpAddress != null)
                RtpAddress = rtpAddress;

            if (RtpAddress == IPAddress.Any)
            {
                RtpAddress = _sipTransport.GetSIPChannels()[0].ListeningIPAddress;
            }

            if (RtpAddress == IPAddress.Any)
            {
                try
                {
                    RtpAddress = Dns.GetHostAddresses(Dns.GetHostName()).First(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                } catch (Exception e)
                {
                    _logger.LogError(e.Message, e);
                }
            }

            if (sipRequest != null)
            {
                using (LogContext.PushProperty("CallContext", sipRequest.Header.CallId))
                {
                    CallDescriptor.CallId = sipRequest.Header.CallId;
                    CallDescriptor.From = sipRequest.Header.From.ToString();

                    CDR = new SIPCallDataRecord(SIPCallDirection.In,
                                                sipRequest.Header.To.ToURI,
                                                sipRequest.Header.From.FromURI,
                                                sipRequest.Header.CallId,
                                                sipRequest.LocalSIPEndPoint,
                                                sipRequest.RemoteSIPEndPoint);

                    OnTransactionStateChange += (transaction) =>
                    {
                        _logger.LogDebug($"Invite transaction state changed: {transaction.TransactionState}");
                        if (transaction.TransactionState == SIPTransactionStatesEnum.Cancelled)
                        {
                            CDR.Cancelled();
                        }
                        else if (transaction.TransactionState == SIPTransactionStatesEnum.Proceeding)
                            CDR.Progress(transaction.UnreliableProvisionalResponse.Status,
                                transaction.UnreliableProvisionalResponse.ReasonPhrase, null, null);
                        else if (transaction.TransactionState == SIPTransactionStatesEnum.Completed)
                        {
                            CDR.Answered(transaction.TransactionFinalResponse.StatusCode,
                                                transaction.TransactionFinalResponse.Status,
                                                transaction.TransactionFinalResponse.ReasonPhrase,
                                                transaction.TransactionFinalResponse.LocalSIPEndPoint,
                                                transaction.TransactionFinalResponse.RemoteSIPEndPoint);
                        }
                        else if (transaction.TransactionState == SIPTransactionStatesEnum.Confirmed)
                        {
                            if ((!CDR.IsAnswered) && (!CDR.IsCancelled)) { 
                                CDR.TimedOut();
                            }
                        }
                    };

                    OnCallHungup += (dialogue) => CDR.Hungup(_lastSIPRequest.Header.Reason);
                }

                _lastSIPRequest = sipRequest;
                _direction = SIPCallDirection.In;
            }
            else
            {
                ClientCallTrying += (uac, resp) => CDR?.Progress(resp.Status, resp.ReasonPhrase,
                                                                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);

                ClientCallRinging += (uac, resp) => CDR?.Progress(resp.Status, resp.ReasonPhrase,
                                                                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);

                ClientCallAnswered += (uac, resp) => CDR?.Answered(resp.StatusCode, resp.Status, resp.ReasonPhrase,
                                                                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);

                ClientCallFailed += (uac, error, resp) => CDR?.Hungup(resp?.ReasonPhrase);

                OnCallHungup += (dialogue) => CDR.Hungup(_lastSIPRequest?.Header.Reason);

                _direction = SIPCallDirection.Out;
                _state = SIPCallState.Dialing;
            }
            if (dtmfMode == SIPCallDtmfMode.Rfc2833)
                OnDtmfTone += UpdateDigitsBuffer;
        }

        /// <summary>
        /// Creates a SIP Call class for an incoming call using pre-define set of codecs.
        /// </summary>
        /// <param name="sipTransport">The transport to be used to create the outbound call.</param>
        /// <param name="localEndPoint">The sip end point receiving the call.</param>
        /// <param name="remoteEndPoint">The sip end point sending the call.</param>
        /// <param name="sipRequest">The incoming request that triggered the incoming call.</param>
        /// <param name="codecs">List of supported codecs for the call</param>
        /// <param name="dtmfMode">The prefered mode for dtmf exchange</param>
        /// <param name="logger">The logger to be used to log the call events.</param>
        public SIPCall(SIPTransport sipTransport, SIPEndPoint localEndPoint, SIPEndPoint remoteEndPoint,
        SIPRequest sipRequest, List<AudioCodecsEnum> codecs, SIPCallDtmfMode dtmfMode, IPAddress rtpAddress)
        : base(sipTransport, remoteEndPoint, true)
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}");
            _sipTransport = sipTransport;
            _codecs = codecs;
            _dtmfMode = dtmfMode;

            if (localEndPoint != null)
            {
                LocalEndPoint = localEndPoint;
            }

            if (remoteEndPoint != null)
            {
                if (remoteEndPoint.Protocol == SIPProtocolsEnum.ws ||
                    remoteEndPoint.Protocol == SIPProtocolsEnum.wss)
                {
                    IsWebRTC = true;
                }
                RemoteEndPoint = remoteEndPoint;
            }

            if (rtpAddress != null)
                RtpAddress = rtpAddress;

            if (RtpAddress == IPAddress.Any)
            {
                RtpAddress = _sipTransport.GetSIPChannels()[0].ListeningIPAddress;
            }

            if (RtpAddress == IPAddress.Any)
            {
                try
                {
                    RtpAddress = Dns.GetHostAddresses(Dns.GetHostName()).First(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message, e);
                }
            }

            if (sipRequest != null)
            {
                using (LogContext.PushProperty("CallContext", sipRequest.Header.CallId))
                {
                    CallDescriptor.CallId = sipRequest.Header.CallId;
                    CallDescriptor.From = sipRequest.Header.From.ToString();

                    CDR = new SIPCallDataRecord(SIPCallDirection.In,
                                                sipRequest.Header.To.ToURI,
                                                sipRequest.Header.From.FromURI,
                                                sipRequest.Header.CallId,
                                                sipRequest.LocalSIPEndPoint,
                                                sipRequest.RemoteSIPEndPoint);

                    OnTransactionStateChange += (transaction) =>
                    {
                        _logger.LogDebug($"Invite transaction state changed: {transaction.TransactionState}");
                        if (transaction.TransactionState == SIPTransactionStatesEnum.Cancelled)
                            CDR.Cancelled();
                        else if (transaction.TransactionState == SIPTransactionStatesEnum.Proceeding)
                            CDR.Progress(transaction.UnreliableProvisionalResponse.Status,
                                transaction.UnreliableProvisionalResponse.ReasonPhrase, null, null);
                        else if (transaction.TransactionState == SIPTransactionStatesEnum.Completed)
                        {
                            CDR.Answered(transaction.TransactionFinalResponse.StatusCode,
                                                transaction.TransactionFinalResponse.Status,
                                                transaction.TransactionFinalResponse.ReasonPhrase,
                                                transaction.TransactionFinalResponse.LocalSIPEndPoint,
                                                transaction.TransactionFinalResponse.RemoteSIPEndPoint);
                        }
                        else if (transaction.TransactionState == SIPTransactionStatesEnum.Confirmed)
                        {
                            if ((!CDR.IsAnswered) && (!CDR.IsCancelled))
                                CDR.TimedOut();
                        }
                    };

                    OnCallHungup += (dialogue) => CDR.Hungup(_lastSIPRequest.Header.Reason);
                }

                _lastSIPRequest = sipRequest;
                _direction = SIPCallDirection.In;
            }
            else
            {
                ClientCallTrying += (uac, resp) => CDR?.Progress(resp.Status, resp.ReasonPhrase,
                                                                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);

                ClientCallRinging += (uac, resp) => CDR?.Progress(resp.Status, resp.ReasonPhrase,
                                                                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);

                ClientCallAnswered += (uac, resp) => CDR?.Answered(resp.StatusCode, resp.Status, resp.ReasonPhrase,
                                                                    resp.LocalSIPEndPoint, resp.RemoteSIPEndPoint);

                ClientCallFailed += (uac, error, resp) => CDR?.Hungup(resp?.ReasonPhrase);

                OnCallHungup += (dialogue) => CDR.Hungup(_lastSIPRequest?.Header.Reason);

                _direction = SIPCallDirection.Out;
            }
            if (dtmfMode == SIPCallDtmfMode.Rfc2833)
                OnDtmfTone += UpdateDigitsBuffer;
        }

        public void Offered()
        {
            if (_state == SIPCallState.Offered)
                return;

            _state = SIPCallState.Offered;
            OnCallOffered?.Invoke();
        }

        public async Task Reject(SIPResponseStatusCodesEnum code, string reason)
        {
            if (GetLastSIPRequest() != null)
            {
                _logger.LogInformation($"Call rejected by app with code {code}.");
                SIPResponse rejectResponse = SIPResponse.GetResponse(GetLastSIPRequest(), code, reason);
                await _sipTransport.SendResponseAsync(rejectResponse);
            }

            Hangup();
        }

        /// <summary>
        /// Updates the digit buffer of the call when the call state is set to receive digits.
        /// </summary>
        /// <param name="key">The byte representation of the digit.</param>
        /// <param name="duration">The duration of the key press as detected.</param>
        private void UpdateDigitsBuffer(byte key, int duration)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogDebug($"New digit detected for buffer. key: {key}, duration: {duration}");
                if ((_action == SIPCallAction.ReceivingDigits) ||
                    (_action == SIPCallAction.Recording) ||
                    (_action == SIPCallAction.Playing))
                {
                    if (key == 10) _digitsBuffer += "*";
                    else if (key == 11) _digitsBuffer += "#";
                    else _digitsBuffer += key;
                    _logger.LogInformation($"Digit {key} saved in digit buffer, digit buffer is now {_digitsBuffer}.");
                }
                else
                {
                    _logger.LogDebug($"Digit {key} ignored because call current action is {_action}.");
                }
            }
        }

        /// <summary>
        /// Returns the current action beeing executed on the call.
        /// </summary>
        public SIPCallAction GetCallAction() => _action;

        /// <summary>
        /// Returns the timestamp of the call answer time.
        /// </summary>
        public long GetStartTimestamp() => _startTime;

        /// <summary>
        /// When set, the rtp stack use windows multimedia timers when playing files.
        /// </summary>
        public bool UseMultiMediaTimers { get; set; } = false;

        /// <summary>
        /// Returns the current duration of the call (connected time) in milliseconds.
        /// </summary>
        public long GetDuration() {
            if (IsCallActive)
            {
                if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - _startTime > 1000)
                    return DateTimeOffset.Now.ToUnixTimeMilliseconds() - _startTime;
            }

            return 0;
        }

        /// <summary>
        /// Returns the current state of the call.
        /// </summary>
        public SIPCallState GetCallState()
        {
            if (this.IsCallActive)
                return SIPCallState.Active;
            else if (this.IsCalling)
                return SIPCallState.Ringing;
            else if (this.IsHangingUp)
                return SIPCallState.HangingUp;
            else if (this.IsOnLocalHold)
                return SIPCallState.LocalHold;
            else if (this.IsOnRemoteHold)
                return SIPCallState.RemoteHold;
            else if (this.IsRinging)
                return SIPCallState.Ringing;
            else if (this.IsRinging)
                return SIPCallState.Ringing;
            else if (_state == SIPCallState.Dialing || 
                _state == SIPCallState.Offered ||
                _state == SIPCallState.Accepted)
                return _state;
            else return SIPCallState.Unknown;
        }

        /// <summary>
        /// Returns the digits buffer of the call.
        /// </summary>
        public string GetDigitsBuffer() => _digitsBuffer;

        /// <summary>
        /// Clear the call digits buffer.
        /// </summary>
        public void ClearDigitsBuffer() => _digitsBuffer = "";

        /// <summary>
        /// Returns the direction of the call.
        /// </summary>
        public SIPCallDirection GetCallDirection() => _direction;

        /// <summary>
        /// Creates an webrtc peer connection.
        /// </summary>
        public RTCPeerConnection CreatePeerConnection()
        {
            _logger.LogInformation($"Creating new rtc peer connection.");
            //RTCConfiguration config = new RTCConfiguration
            //{
            //    iceServers = new List<RTCIceServer> { new RTCIceServer { urls = "stun:stun.l.google.com:19302" } }
            //};
            RtcPeerConnection = new RTCPeerConnection(null);

            var audioSource = AudioSourcesEnum.None;
            AudioExtrasSource audioExtrasSource = new AudioExtrasSource(new AudioEncoder(), new AudioSourceOptions { AudioSource = audioSource });
            audioExtrasSource.RestrictFormats(formats => _codecs.Contains(formats.Codec));
            RtcPeerConnection.AudioExtrasSource = audioExtrasSource;

            RtcPeerConnection.onconnectionstatechange += (state) =>
            {
                _logger.LogDebug($"Peer connection state change to {state}.");

                if (state == RTCPeerConnectionState.failed)
                {
                    RtcPeerConnection.Close("ice disconnection");
                }
                else if (state == RTCPeerConnectionState.connected)
                {
                    audioExtrasSource.StartAudio();
                    RtcPeerConnection.OnRtpPacketReceived += (ep, type, rtp) => this.OnRtpPacketReceived(type, rtp);
                }
                else if (state == RTCPeerConnectionState.closed)
                {
                    RtcPeerConnection.OnRtpPacketReceived -= (ep, type, rtp) => this.OnRtpPacketReceived(type, rtp);
                }
            };

            RtcPeerConnection.GetRtpChannel().OnStunMessageReceived += (msg, ep, isRelay) => _logger.LogDebug($"STUN {msg.Header.MessageType} received from {ep}.");
            RtcPeerConnection.OnReceiveReport += (re, media, rr) => _logger.LogDebug($"RTCP Receive for {media} from {re}\n{rr.GetDebugSummary()}");
            RtcPeerConnection.OnSendReport += (media, sr) => _logger.LogDebug($"RTCP Send for {media}\n{sr.GetDebugSummary()}");

            MediaStreamTrack audioTrack = new MediaStreamTrack(audioExtrasSource.GetAudioSourceFormats(), MediaStreamStatusEnum.SendRecv);
            RtcPeerConnection.addTrack(audioTrack);

            RtcPeerConnection.OnAudioFormatsNegotiated += (audioFormats) => audioExtrasSource.SetAudioSourceFormat(audioFormats.First());

            audioExtrasSource.OnAudioSourceEncodedSample += RtcPeerConnection.SendAudio;

            return RtcPeerConnection;
        }

        /// <summary>
        /// Creates an rtp session associated with the call using default codecs.
        /// </summary>
        public VoIPMediaSession CreateRtpSession()
        {
            _logger.LogInformation($"Creating new rtp session using default codecs.");
            return CreateRtpSession(_codecs);
        }

        /// <summary>
        /// Creates an rtp session associated with the call using default codecs.
        /// </summary>
        /// <param name="useMultiMediatimers">When true, use windows multimedia timers for rtp</param>
        public VoIPMediaSession CreateRtpSession(bool useMultiMediatimers)
        {
            _logger.LogInformation($"Creating new rtp session using default codecs.");
            return CreateRtpSession(_codecs);
        }

        /// <summary>
        /// Creates an rtp session associated with the call using incoming requests codecs.
        /// </summary>
        /// <param name="sipRequest">The incoming sip request with the offered SDP</param>
        public VoIPMediaSession CreateRtpSession(SIPRequest sipRequest)
        {
            SDP sdp = SDP.ParseSDPDescription(_lastSIPRequest.Body);
            var codecs = new List<AudioCodecsEnum>();

            _logger.LogInformation($"Creating new rtp session, trying to match offered codec.");

            foreach (SDPAudioVideoMediaFormat preferedCodec in sdp.Media[0].MediaFormats.Values)
            {
                if (preferedCodec.Kind == SDPMediaTypesEnum.audio)
                {
                    AudioCodecsEnum codec = (AudioCodecsEnum)Enum.Parse(typeof(AudioCodecsEnum), preferedCodec.ToAudioFormat().FormatName);
                    if (_codecs.Contains(codec))
                    {
                        codecs.Add(codec);
                        // pick only the first codec matched.
                        _logger.LogInformation($"Codec {codec} matches offered codec.");
                        break;
                    }
                }
            }

            return CreateRtpSession(codecs);
        }

        /// <summary>
        /// Creates an rtp session associated with the call using defined codec list.
        /// </summary>
        /// <param name="codecs">List of codecs</param>
        public VoIPMediaSession CreateRtpSession(List<AudioCodecsEnum> codecs)
        {
            var audioSource = AudioSourcesEnum.None;
            AudioExtrasSource audioExtrasSource = new AudioExtrasSource(new AudioEncoder(), new AudioSourceOptions { AudioSource = audioSource });
            audioExtrasSource.RestrictFormats(formats => codecs.Contains(formats.Codec));

            if (_dtmfMode != SIPCallDtmfMode.Rfc2833)
            {
                RtpSession = new VoIPMediaSession(new MediaEndPoints { AudioSource = audioExtrasSource }, RtpAddress, true);
            }
            else
            {
                RtpSession = new VoIPMediaSession(new MediaEndPoints { AudioSource = audioExtrasSource }, RtpAddress, false);
            }

            RtpSession.AcceptRtpFromAny = true;
            RtpSession.AudioExtrasSource.UseMultiMediaTimers = UseMultiMediaTimers;

            _logger.LogInformation($"Rtp audio session created.");

            // Wire up the event handler for RTP packets received from the remote party.
            // removed for testing
            RtpSession.OnRtpPacketReceived += (ep, type, rtp) => this.OnRtpPacketReceived(type, rtp);
            RtpSession.OnTimeout += (mediaType) =>
            {
                if (this?.Dialogue != null)
                {
                    _logger.LogWarning($"Rtp timeout on call with {this.Dialogue.RemoteTarget}, hanging up.");
                }
                else
                {
                    _logger.LogWarning($"Rtp timeout on incomplete call, closing RTP session.");
                }

                if (HangupWhenNoRtp)
                    this.Hangup();
            };
            RtpSession.OnStarted += () =>
            {
                _logger.LogInformation($"Rtp audio session source set to {AudioSourcesEnum.Silence}.");
                RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
            };

            return RtpSession;
        }

        /// <summary>
        /// Write a pcm sample to wave file
        /// </summary>
        /// <param name="sample">The byte array representing the pcm sample</param>
        private void WriteSample(short[] linearSample)
        {
            if (_action != SIPCallAction.Recording)
                return;

            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (_waveFileWriter != null)
                {
                    try
                    {
                        _logger.LogTrace("Writing rtp sample to file...");
                        for (int index = 0; index < linearSample.Length; index++)
                        {
                            byte[] pcmSample = new byte[] { (byte)(linearSample[index] & 0xFF), (byte)(linearSample[index] >> 8) };
                            _waveFileWriter.Write(pcmSample, 0, 2); ;
                        }
                        _logger.LogTrace("Rtp sample writen.");
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e.Message, e);
                    }
                }
            }

        }

        /// <summary>
        /// Returns the Call-ID header.
        /// </summary>
        /// <returns></returns>
        public string GetCallID()
        {
            if (CallDescriptor != null)
                return CallDescriptor.CallId;
            else if (Dialogue != null)
                return Dialogue.CallId;
            else if (_lastSIPRequest != null)
                return _lastSIPRequest.Header.CallId;
            else
            {
                _logger.LogWarning("Could not find call id.");
                return null;
            }
        }

        /// <summary>
        /// Returns the To header.
        /// </summary>
        /// <returns></returns>
        public SIPURI GetToURI()
        {
            if (_lastSIPRequest != null)
                return _lastSIPRequest.Header.To.ToURI;
            else if (Dialogue != null)
                return Dialogue.RemoteTarget;
            else if (CallDescriptor != null)
                return SIPURI.ParseSIPURI(CallDescriptor.To);
            else
            {
                _logger.LogWarning("Could not find To header.");
                return null;
            }
        }

        /// <summary>
        /// Returns the identity of the call.
        /// </summary>
        /// <returns></returns>
        public string GetIdentity()
        {
            if (_identity != null)
                return _identity;

            if (_direction == SIPCallDirection.In)
            {
                if (_lastSIPRequest != null)
                {
                    _identity = _lastSIPRequest.Header.To.ToURI.ToString();
                }
                else
                {
                    _identity = new SIPURI(CallDescriptor.Username, CallDescriptor.GetFromHeader().FromURI.Host,
                        null, CallDescriptor.GetFromHeader().FromURI.Scheme, CallDescriptor.GetFromHeader().FromURI.Protocol).ToString();
                }
            }
            else
            {
                if (_lastSIPRequest != null)
                {
                    _identity = _lastSIPRequest.Header.From.FromURI.ToString();
                }
                else
                {
                    var toURI = SIPURI.ParseSIPURI(CallDescriptor.To);
                    _identity = new SIPURI(CallDescriptor.Username, toURI.Host, null, toURI.Scheme, toURI.Protocol).ToString();
                }
            }

            return _identity;
        }

        /// <summary>
        /// Returns the From header.
        /// </summary>
        /// <returns></returns>
        public SIPURI GetFromURI()
        {
            if (_lastSIPRequest != null)
                return _lastSIPRequest.Header.From.FromURI;
            else if (Dialogue != null)
                return Dialogue.LocalUserField.URI;
            else if (CallDescriptor != null)
                return SIPURI.ParseSIPURI(CallDescriptor.From);
            else
            {
                _logger.LogWarning("Could not find From header.");
                return null;
            }
        }

        /// <summary>
        /// Creates a new RTPStreamAnalyzer object.
        /// Subscribes to silence and tone events.
        /// </summary>
        private void CreateAnalyzer()
        {
            _analyzer = new RTPStreamAnalyzer(GetCallID(), CPAConfiguration.LoadFromFile("cpasettings.json"));

            if (_dtmfMode == SIPCallDtmfMode.Inband)
                _analyzer.OnInbandDtmf += (digit) => OnInbandDtmf(digit);

            _analyzer.OnFaxTone += () => OnPositiveFaxDetection?.Invoke();
            _analyzer.OnRingbackTone += () => OnRingbackToneDetection?.Invoke();
            _analyzer.OnPVD += () =>
            {
                _startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                OnPositiveVoiceDetection?.Invoke();
            };
            _analyzer.OnPAMD += () => OnPositiveAnswerMachineDetection?.Invoke();
            _analyzer.OnSilenceStateChange += (state) =>
            {
                OnSilenceStateChange?.Invoke(state);
            };

        }

        /// <summary>
        /// Catch incoming rtp packets to perform silence and tone detection and record packets to file.
        /// </summary>
        /// <param name="type">Only audio is supported at this time.</param>
        /// <param name="rtpPacket">the rtp packet to be processed.</param>
        private void OnRtpPacketReceived(SDPMediaTypesEnum type, RTPPacket rtpPacket)
        {
            if (GetCallID() == null)
                return;

            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (type == SDPMediaTypesEnum.audio)
                {

                    if (_analyzer == null) CreateAnalyzer();

                    _logger.LogTrace($"Raw rtp packet: {BitConverter.ToString(rtpPacket.Payload)}");

                    short[] linearSample = audioEncoder.DecodeAudio(rtpPacket.Payload, new AudioFormat((SDPWellKnownMediaFormatsEnum)rtpPacket.Header.PayloadType));
                    WriteSample(linearSample);
                    OnLinearSampleAvailable?.Invoke(GetCallID(), linearSample);
                    _analyzer.Analyze(linearSample);
                }
                else
                {
                    _logger.LogWarning("Rtp packet not processed because only audio payloads are currently supported.");
                }
            }

        }

        /// <summary>
        /// Send a single inband digit using a byte input.
        /// </summary>
        /// <param name="digit">The byte representation of the digit.</param>
        /// <param name="duration">The duration of the tone.</param>
        private async Task SendInbandDigit(byte digit, int duration)
        {
            var key = "";
            if (digit == 10) key = "*";
            else if (digit == 11) key = "#";
            else key = digit.ToString();

            if (_analyzer == null)
            {
                _analyzer = new RTPStreamAnalyzer(GetCallID());

                if (_dtmfMode == SIPCallDtmfMode.Inband)
                    _analyzer.OnInbandDtmf += (d) => OnInbandDtmf(d);

                _analyzer.OnSilenceStateChange += (state) =>
                {
                    using (LogContext.PushProperty("CallContext", GetCallID()))
                    {
                        _logger.LogInformation($"Silence: {state}");
                    }
                };
            }

            int[] freqs = _analyzer.GetDtmfFrequencies(key);

            if (IsWebRTC)
            {
                RtcPeerConnection.AudioExtrasSource.SetSource(new AudioSourceOptions { AudioSource = AudioSourcesEnum.SineWave, Frequency = freqs[0], Frequency2 = freqs[1] });
                await Task.Delay(duration);
                RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
            }
            else
            {
                RtpSession.AudioExtrasSource.SetSource(new AudioSourceOptions { AudioSource = AudioSourcesEnum.SineWave, Frequency = freqs[0], Frequency2 = freqs[1] });
                await Task.Delay(duration);
                RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
            }

        }

        /// <summary>
        /// Send a single INFO digit using a byte input.
        /// </summary>
        /// <param name="digit">The byte representation of the digit.</param>
        /// <param name="duration">The duration of the tone.</param>
        private void SendInfoDigit(byte digit, int duration)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (Dialogue == null)
                {
                    _logger.LogWarning("No dialog available, INFO request cannot be sent.");
                }
                else
                {
                    var infoRequest = Dialogue.GetInDialogRequest(SIPMethodsEnum.INFO);

                    var key = "";
                    if (digit == 10) key = "*";
                    else if (digit == 11) key = "#";
                    else key = digit.ToString();

                    if (_dtmfMode == SIPCallDtmfMode.DtmfRelay)
                    {
                        infoRequest.Header.ContentType = "application/dtmf-relay";
                        infoRequest.Body = "Signal=" + key;
                        infoRequest.Body += Environment.NewLine;
                        infoRequest.Body += "Duration=" + duration;
                    }
                    else
                    {
                        infoRequest.Header.ContentType = "application/dtmf";
                        infoRequest.Body = key;
                    }

                    if (_sipClientUserAgent != null)
                    {
                        infoRequest.Header.Contact = _sipClientUserAgent.ServerTransaction.TransactionRequest.Header.Contact;
                        infoRequest.SetSendFromHints(_sipClientUserAgent.ServerTransaction.TransactionRequest.LocalSIPEndPoint);
                    }
                    else if (_sipServerUserAgent != null)
                    {
                        infoRequest.Header.Contact = _sipServerUserAgent.ClientTransaction.TransactionFinalResponse.Header.Contact;
                        infoRequest.SetSendFromHints(_sipServerUserAgent.ClientTransaction.TransactionFinalResponse.LocalSIPEndPoint);
                    }
                    else
                    {
                        infoRequest.Header.Contact = new List<SIPContactHeader>() { SIPContactHeader.GetDefaultSIPContactHeader() };
                    }

                    UACInviteTransaction infoTransaction = new UACInviteTransaction(_sipTransport, infoRequest, null);
                    _sipTransport.SendTransaction(infoTransaction);
                    //infoTransaction.UACInviteTransactionFinalResponseReceived +=
                    //  (localSIPEndPoint, remoteEndPoint, sipTransaction, sipResponse) => _logger.LogDebug($"got ");
                }
            }
        }

        /// <summary>
        /// Converts a char to byte to be sent as dtmf.
        /// </summary>
        /// <param name="c">the character to be interpreted.</param>
        private byte DigitCharToByte(char c)
        {
            byte tone;
            if (c == '*') tone = 10;
            else if (c == '#') tone = 11;
            else tone = (byte)(Convert.ToByte(c) - 48);
            return tone;
        }

        /// <summary>
        /// Send a single digit using a byte input with a default delay of 200ms.
        /// </summary>
        /// <param name="digit">The byte to be sent as a digit.</param>
        public async Task SendDigit(byte digit)
        {
            await SendDigit(digit, 200);
        }

        /// <summary>
        /// Send a single digit using a byte input with a delay after the digit is sent.
        /// </summary>
        /// <param name="digit">The byte to be sent as a digit.</param>
        /// <param name="delay">The delay in milliseconds to wait for after the figit is sent.</param>
        public async Task SendDigit(byte digit, int delay)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation($"Sending digit {digit}");
                _logger.LogDebug($"Sending digit {digit} with {delay}ms delay.");
                _action = SIPCallAction.SendingDigits;
                switch (_dtmfMode)
                {
                    case SIPCallDtmfMode.Inband:
                        await SendInbandDigit(digit, delay);
                        break;
                    case SIPCallDtmfMode.Dtmf:
                    case SIPCallDtmfMode.DtmfRelay:
                        SendInfoDigit(digit, delay);
                        await Task.Delay(delay);
                        break;
                    default:
                        await this.SendDtmf(digit);
                        await Task.Delay(delay);
                        break;
                }
                _action = SIPCallAction.Idle;
                _logger.LogDebug($"Send digit done.");
                OnSendDigitDone?.Invoke();
            }
        }

        /// <summary>
        /// Send a single digit using a char input with a default delay of 96ms.
        /// </summary>
        /// <param name="digit">The char to be sent as a digit.</param>
        public async Task SendDigit(char digit)
        {
            await SendDigit(digit, 96);
        }

        /// <summary>
        /// Send a single digit using a char input with a delay after the digit is sent.
        /// </summary>
        /// <param name="digit">The char to be sent as a digit.</param>
        /// <param name="delay">The delay in milliseconds to wait for after the figit is sent.</param>
        public async Task SendDigit(char digit, int delay)
        {
            await SendDigit(DigitCharToByte(digit), delay);
        }

        /// <summary>
        /// Send a string of multiple digits using a default delay of 96ms between each digit.
        /// </summary>
        /// <param name="digits">The digits string to be sent.</param>
        public async Task SendDigits(string digits)
        {
            foreach (char c in digits)
            {
                await this.SendDigit(c, 96);
            }
            OnSendDigitsDone?.Invoke();
        }

        /// <summary>
        /// Collects and returns a number of digits from the remote party.
        /// </summary>
        /// <param name="duration">The time in milliseconds to wait for user input.</param>
        /// <param name="numOfDigits">The maximum number of digits to collect.</param>
        /// <param name="terminationDigit">The digit char that will be determine the end of the input.</param>
        /// <param name="clearDigitsBuffer">When true, clear the digit buffer before digits collection.</param>
        public async Task<string> GetDigits(int duration, int numOfDigits, char terminationDigit, bool clearDigitsBuffer)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (!IsCallActive)
                {
                    _logger.LogWarning("Getdigits; doing nothing because call is not active.");
                    return "";
                }

                _logger.LogInformation($"Getting {numOfDigits} digits...");
                _logger.LogDebug($"Getting {numOfDigits} digits during max {duration} milliseconds, ending by {terminationDigit}, clear buffer is {clearDigitsBuffer}.");

                if (clearDigitsBuffer)
                {
                    _digitsBuffer = "";
                    _logger.LogDebug("Digits buffer was cleared.");
                }

                _action = SIPCallAction.ReceivingDigits;

                duration *= 4;

                while (duration > 0)
                {
                    if (_digitsBuffer.Length == numOfDigits)
                    {
                        _logger.LogDebug($"Digits buffer reached max len, digits: {_digitsBuffer}, len: {_digitsBuffer.Length}");
                        break;
                    }
                    if (_digitsBuffer.EndsWith(terminationDigit.ToString()))
                    {
                        _digitsBuffer = _digitsBuffer.Substring(0, _digitsBuffer.Length - 1);
                        _logger.LogDebug($"Detected termination digit: {terminationDigit}, digits: {_digitsBuffer}, len: {_digitsBuffer.Length}");
                        break;
                    }
                    await Task.Delay(250);
                    duration = duration - 1000;
                }

                _logger.LogDebug($"Returning digits buffer, digits: {_digitsBuffer}, len: {_digitsBuffer.Length}");

                _action = SIPCallAction.Idle;

                OnGetDigitsDone?.Invoke();

                return _digitsBuffer;
            }
        }

        /// <summary>
        /// Initiates an outbound call. 
        /// Sends a SIP INVITE and create an RTP session
        /// </summary>
        /// <param name="to">SIP URI of the destination (To Header)</param>
        /// <param name="username">Optional, registered username account to use to place the call</param>
        /// <param name="password">Optional, password of the account to be used to place the call</param>
        /// <param name="cpaTimeout">If > 0, triggers call progress analysis procedure with the specified timeout in milliseconds</param>
        /// <returns></returns>
        internal Task<bool> MakeCall(string to, string username, string password, int cpaTimeout)
        {
            return MakeCall(to, username, password, null, cpaTimeout);
        }

        /// <summary>
        /// Initiates an outbound call. 
        /// Sends a SIP INVITE and create an RTP session
        /// </summary>
        /// <param name="to">SIP URI of the destination (To Header)</param>
        /// <param name="username">Optional, registered username account to use to place the call</param>
        /// <param name="password">Optional, password of the account to be used to place the call</param>
        /// <param name="cpaTimeout">If > 0, triggers call progress analysis procedure with the specified timeout in milliseconds</param>
        /// <returns></returns>
        internal Task<bool> MakeCall(string to, string username, string password, string[] sipHeaders, int cpaTimeout)
        {
            _logger.LogDebug($"Creating rtp session for new call.");
            RtpSession = CreateRtpSession();

            _sipClientUserAgent = GetSIPClientUserAgent();
            _direction = SIPCallDirection.Out;

            ClientCallAnswered += (uac, resp) =>
            {
                if (_analyzer == null) CreateAnalyzer();

                if (cpaTimeout > 0)
                {
                    _logger.LogInformation("Starting CPA procedures");
                    _analyzer.StartCallProgressAnalysis(cpaTimeout);
                }
                else
                {
                    _startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                }
            };

            _logger.LogDebug($"Sending INVITE.");
            var toURI = SIPURI.ParseSIPURI(to);

            if (_sipTransport.GetSIPChannels()[0].SIPProtocol != SIPProtocolsEnum.udp)
            {
                toURI.Protocol = _sipTransport.GetSIPChannels()[0].SIPProtocol;
            }

            var fromURI = new SIPURI(username, toURI.Host, null, toURI.Scheme, toURI.Protocol);
            var res = Call(toURI.ToString(), username, password, sipHeaders, RtpSession);
            CDR = new SIPCallDataRecord(SIPCallDirection.Out, toURI, fromURI, CallDescriptor.CallId, null, null);
            LogContext.PushProperty("CallContext", CallDescriptor.CallId);

            return res;
        }

        /// <summary>
        /// Plays back a text prompt to the remote party.
        /// </summary>
        /// <param name="text">The text to be played back.</param>
        /// <param name="clearDigitsBuffer">When true, clear the digit buffer before playback.</param>
        /// <param name="stopOnDigit">When true, stop the playback upon digit detection.</param>
        public async Task Speak(string text, bool clearDigitsBuffer, bool stopOnDigit)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (!IsCallActive)
                {
                    _logger.LogWarning("Speak; doing nothing because call is not active.");
                    return;
                }

                var tmpFile = $".{GetCallID()}";
                WaveStream fs = null;
                try
                {
                    _action = SIPCallAction.Playing;

                    _logger.LogInformation($"Speaking text {text}...");
                    _logger.LogDebug($"Speak; text: {text}, clear digits: {clearDigitsBuffer}, stop on digit: {stopOnDigit}.");

                    if (clearDigitsBuffer)
                    {
                        _digitsBuffer = "";
                        _logger.LogDebug("Digits buffer was cleared.");
                    }

                    var synthesizer = new SpeechSynthesizer();

                    foreach (var v in synthesizer.GetInstalledVoices().Select(v => v.VoiceInfo))
                    {
                        if (v.Name == "Microsoft Asaf")
                            synthesizer.SelectVoice(v.Name);
                    }

                    MemoryStream streamAudio = new MemoryStream();
                    synthesizer.SetOutputToAudioStream(streamAudio, new SpeechAudioFormatInfo(8000, AudioBitsPerSample.Sixteen, AudioChannel.Mono));
                    synthesizer.Speak(text);
                    synthesizer.Dispose();

                    streamAudio.Position = 0;
                    fs = new RawSourceWaveStream(streamAudio, _waveFormat);

                    if (stopOnDigit)
                    {
                        _logger.LogDebug($"Speaking while waiting for digit...");
                        if (IsWebRTC)
                        {
                            _ = RtcPeerConnection.AudioExtrasSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);
                            while (RtcPeerConnection.AudioExtrasSource.IsAudioSourceSending())
                            {
                                if (_digitsBuffer.Length > 0)
                                {
                                    _logger.LogDebug($"Stopping playback because digit detected, digits: {_digitsBuffer}");
                                    RtcPeerConnection.AudioExtrasSource.CancelSendAudioFromStream();
                                    break;
                                }
                                await Task.Delay(250);
                            }
                        }
                        else
                        {
                            _ = RtpSession.AudioExtrasSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);
                            while (RtpSession.AudioExtrasSource.IsAudioSourceSending())
                            {
                                if (_digitsBuffer.Length > 0)
                                {
                                    _logger.LogDebug($"Stopping playback because digit detected, digits: {_digitsBuffer}");
                                    RtpSession.AudioExtrasSource.CancelSendAudioFromStream();
                                    break;
                                }
                                await Task.Delay(250);
                            }
                        }
                    }
                    else
                    {
                        _logger.LogDebug($"Speaking with no interrupt...");
                        if (IsWebRTC)
                        {
                            await RtcPeerConnection.AudioExtrasSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);
                        }
                        else
                        {
                            await RtpSession.AudioExtrasSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);
                        }
                    }


                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message, e);
                }
                finally
                {
                    if (IsWebRTC)
                    {
                        RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                    }
                    else
                    {
                        RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                    }
                    _action = SIPCallAction.Idle;
                    fs?.Close();
                    if (File.Exists(tmpFile)) File.Delete(tmpFile);
                    _logger.LogInformation($"Speaking text {text} completed.");
                    OnSpeakDone?.Invoke();
                }
            }
        }

        /// <summary>
        /// Plays back a text prompt to the remote party.
        /// </summary>
        /// <param name="text">The text to be played back.</param>
        /// <param name="clearDigitsBuffer">When true, clear the digit buffer before playback.</param>
        /// <param name="stopOnDigit">When true, stop the playback upon digit detection.</param>
        public async Task SpeakSsml(string ssml, bool clearDigitsBuffer, bool stopOnDigit)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (!IsCallActive)
                {
                    _logger.LogWarning("Speakssml; doing nothing because call is not active.");
                    return;
                }

                var synthesizer = new SpeechSynthesizer();

                foreach (var v in synthesizer.GetInstalledVoices().Select(v => v.VoiceInfo))
                {
                    _logger.LogInformation($"Name:{v.Name}, Description:{v.Description}, Gender:{v.Gender}, Age:{3}");
                }

                var tmpFile = $".{GetCallID()}";
                WaveStream fs = null;
                try
                {
                    _action = SIPCallAction.Playing;

                    _logger.LogInformation($"Speaking ssml {ssml}...");
                    _logger.LogDebug($"Speakssml; ssml: {ssml}, clear digits: {clearDigitsBuffer}, stop on digit: {stopOnDigit}.");

                    if (clearDigitsBuffer)
                    {
                        _digitsBuffer = "";
                        _logger.LogDebug("Digits buffer was cleared.");
                    }

                    foreach (var v in synthesizer.GetInstalledVoices().Select(v => v.VoiceInfo))
                    {
                        if (v.Name == "Microsoft Asaf")
                            synthesizer.SelectVoice(v.Name);
                    }

                    MemoryStream streamAudio = new MemoryStream();
                    synthesizer.SetOutputToAudioStream(streamAudio, new SpeechAudioFormatInfo(8000, AudioBitsPerSample.Sixteen, AudioChannel.Mono));
                    synthesizer.SpeakSsml(ssml);
                    synthesizer.Dispose();

                    streamAudio.Position = 0;
                    fs = new RawSourceWaveStream(streamAudio, _waveFormat);

                    if (stopOnDigit)
                    {
                        _logger.LogDebug($"Speaking while waiting for digit...");

                        if (IsWebRTC)
                        {
                            _ = RtcPeerConnection.AudioExtrasSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);

                            while (RtcPeerConnection.AudioExtrasSource.IsAudioSourceSending())
                            {
                                if (_digitsBuffer.Length > 0)
                                {
                                    _logger.LogDebug($"Stopping playback because digit detected, digits: {_digitsBuffer}");
                                    RtcPeerConnection.AudioExtrasSource.CancelSendAudioFromStream();
                                    break;
                                }
                                await Task.Delay(250);
                            }
                        }
                        else
                        {
                            _ = RtpSession.AudioExtrasSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);

                            while (RtpSession.AudioExtrasSource.IsAudioSourceSending())
                            {
                                if (_digitsBuffer.Length > 0)
                                {
                                    _logger.LogDebug($"Stopping playback because digit detected, digits: {_digitsBuffer}");
                                    RtpSession.AudioExtrasSource.CancelSendAudioFromStream();
                                    break;
                                }
                                await Task.Delay(250);
                            }
                        }
                    }
                    else
                    {
                        _logger.LogDebug($"Speaking with no interrupt...");
                        if (IsWebRTC)
                        {
                            await RtcPeerConnection.AudioExtrasSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);
                        }
                        else
                        {
                            await RtpSession.AudioExtrasSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);
                        }
                    }


                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message, e);
                }
                finally
                {
                    if (IsWebRTC)
                    {
                        RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                    }
                    else
                    {
                        RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                    }
                    fs?.Close();
                    if (File.Exists(tmpFile)) File.Delete(tmpFile);
                    _action = SIPCallAction.Idle;
                    _logger.LogInformation($"Speaking ssml completed.");
                    OnSpeakDone?.Invoke();
                }
            }
        }

        /// <summary>
        /// Plays back a file to the remote party.
        /// </summary>
        /// <param name="fileName">The full path file name to be played back in pcm format 8KHz/16bit.</param>
        /// <param name="clearDigitsBuffer">When true, clear the digit buffer before playback.</param>
        /// <param name="stopOnDigit">When true, stop the playback upon digit detection.</param>
        public async Task PlayFile(string fileName, bool clearDigitsBuffer, bool stopOnDigit)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (!IsCallActive)
                {
                    _logger.LogWarning("Playfile; doing nothing because call is not active.");
                    return;
                }

                try
                {
                    _action = SIPCallAction.Playing;

                    _logger.LogInformation($"Playing file {fileName}...");
                    _logger.LogDebug($"Playfile; filename: {fileName}, clear digits: {clearDigitsBuffer}, stop on digit: {stopOnDigit}.");

                    if (clearDigitsBuffer)
                    {
                        _digitsBuffer = "";
                        _logger.LogDebug("Digits buffer was cleared.");
                    }

                    Stream fs;
                    if (fileName.EndsWith(".wav"))
                    {
                        WaveFileReader waveReader = new WaveFileReader(fileName);
                        fs = WaveFormatConversionStream.CreatePcmStream(waveReader);
                    }
                    else
                    {
                        _logger.LogDebug($"Opening file {fileName}...");
                        fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    }

                    AudioExtrasSource audioSource = null;
                    if (IsWebRTC)
                    {
                        audioSource = RtcPeerConnection.AudioExtrasSource;
                    }
                    else
                    {
                        audioSource = RtpSession.AudioExtrasSource;
                    }

                    if (stopOnDigit)
                    {
                        _logger.LogDebug($"Playing while waiting for digit...");
                        _ = audioSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);

                        while (audioSource.IsAudioSourceSending())
                        {
                            if (_digitsBuffer.Length > 0)
                            {
                                _logger.LogDebug($"Stopping playback because digit detected, digits: {_digitsBuffer}");
                                audioSource.CancelSendAudioFromStream();

                                break;
                            }
                            await Task.Delay(250);
                        }
                    }
                    else
                    {
                        _logger.LogDebug($"Playing with no interrupt...");
                        _ = audioSource.SendAudioFromStream(fs, AudioSamplingRatesEnum.Rate8KHz);
                    }


                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message, e);
                    OnPlayError?.Invoke(e.Message);
                }
                finally
                {
                    if (IsWebRTC)
                    {
                        RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                    }
                    else
                    {
                        RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                    }

                    _action = SIPCallAction.Idle;
                    _logger.LogInformation($"Playing file {fileName} completed.");
                    OnPlayDone?.Invoke();
                }
            }
        }

        /// <summary>
        /// Plays a file in loop to the remote party.
        /// </summary>
        /// <param name="fileName">An array of full path file names to be played back in pcm format 8KHz/16bit.</param>
        /// <param name="interval">Interval in milliseconds between files.</param>
        /// <param name="clearDigitsBuffer">When true, clear the digit buffer before playback.</param>
        /// <param name="stopOnDigit">When true, stop the playback upon digit detection.</param>
        public async Task PlayLoop(string fileName, int interval, bool clearDigitsBuffer, bool stopOnDigit)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                PlayLoopOn = true;

                if (clearDigitsBuffer)
                {
                    _digitsBuffer = "";
                    _logger.LogDebug("Playloop; digits buffer was cleared.");
                }

                _logger.LogDebug($"Playing {fileName}; interval: {interval}, clear digits: {clearDigitsBuffer}, stop on digit: {stopOnDigit}.");

                try
                {
                    while (IsCallActive && PlayLoopOn)
                    {
                        _logger.LogDebug("Playing next file.");
                        await PlayFile(fileName, false, stopOnDigit);
                        if (stopOnDigit && _digitsBuffer.Length > 0)
                        {
                            _logger.LogDebug("Playloop; loop stopped because digit was detected.");
                            break;
                        }
                        await Task.Delay(interval);
                    }
                }
                catch
                {

                }

                _logger.LogDebug($"Playloop; all done.");
            }
        }

        /// <summary>
        /// Plays back a list of files to the remote party.
        /// </summary>
        /// <param name="fileName">An array of full path file names to be played back in pcm format 8KHz/16bit.</param>
        /// <param name="interval">Interval in milliseconds between files.</param>
        /// <param name="clearDigitsBuffer">When true, clear the digit buffer before playback.</param>
        /// <param name="stopOnDigit">When true, stop the playback upon digit detection.</param>
        public async Task PlayFiles(string[] fileName, int interval, bool clearDigitsBuffer, bool stopOnDigit)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (clearDigitsBuffer)
                {
                    _digitsBuffer = "";
                    _logger.LogDebug("Playfiles; digits buffer was cleared.");
                }

                _logger.LogDebug($"Playing {fileName.Length} files; interval: {interval}, clear digits: {clearDigitsBuffer}, stop on digit: {stopOnDigit}.");

                foreach (string s in fileName)
                {
                    _logger.LogDebug("Playing next file.");
                    await PlayFile(s, false, stopOnDigit);
                    if (stopOnDigit && _digitsBuffer.Length > 0)
                    {
                        _logger.LogDebug("Playfiles; loop stopped because digit was detected.");
                        break;
                    }
                    await Task.Delay(interval);
                }

                _logger.LogDebug($"Playfiles; all done.");
                OnPlayFilesDone?.Invoke();
            }
        }

        /// <summary>
        /// Plays back a file to the remote party and then record the remote party incoming rtp with defaults.
        /// # terminates the record
        /// 10s of silence terminates the record
        /// </summary>
        /// <param name="playFileName">The full path file name to be played back in pcm format 8KHz/16bit.</param>
        /// <param name="recordFileName">The full path file name to be record in pcm format 8KHz/16bit.</param>
        /// <param name="duration">The maximum duration in milliseconds of the record time.</param>
        public async Task PlayRecord(string playFileName, string recordFileName, int duration)
        {
            await PlayRecord(playFileName, recordFileName, duration, '#', true, 10 * 1000);
        }

        /// <summary>
        /// Plays back a file to the remote party and then record the remote party incoming rtp.
        /// </summary>
        /// <param name="playFileName">The full path file name to be played back in pcm format 8KHz/16bit.</param>
        /// <param name="recordFileName">The full path file name to be record in pcm format 8KHz/16bit.</param>
        /// <param name="duration">The maximum duration in milliseconds of the record time.</param>
        /// <param name="terminationDigit">The digit char that will determine if the record shall be stopped.</param>
        /// <param name="playTone">When true, the record tone is played back before recording starts.</param>
        /// <param name="stopOnSilence">When > 0, stop recording after n milliseconds of silence.</param>
        public async Task PlayRecord(string playFileName, string recordFileName, int duration, char terminationDigit, bool playTone, int stopOnSilence)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Playrecord starting.");
                _logger.LogDebug($"Playrecord; play file: {playFileName}, recordFile: {recordFileName}, duration: {duration}, stop on digit: {terminationDigit}, play tone: {playTone}.");
                await PlayFile(playFileName, true, false);
                await RecordFile(recordFileName, duration, terminationDigit, playTone, stopOnSilence);
                _logger.LogInformation("Playrecord done.");
                OnPlayRecordDone?.Invoke();
            }
        }

        /// <summary>
        /// Plays back music to the remote party for a period of time.
        /// </summary>
        /// <param name="duration">The duration in milliseconds of the playback.</param>
        public async Task PlayMusic(int duration)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (!IsCallActive)
                {
                    _logger.LogWarning("Playmusic; doing nothing because call is not active.");
                    return;
                }

                _logger.LogInformation("Playmusic starting.");
                _logger.LogDebug($"Playmusic; duration {duration} milliseconds.");
                _action = SIPCallAction.Playing;
                if (IsWebRTC)
                {
                    RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Music);
                    await Task.Delay(duration);
                    RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                }
                else
                {
                    RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Music);
                    await Task.Delay(duration);
                    RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                }
                _action = SIPCallAction.Idle;
                _logger.LogInformation("Playmusic done.");
                OnPlayMusicDone?.Invoke();
            }
        }

        /// <summary>
        /// Plays back music to the remote party forever.
        /// </summary>
        public void PlayMusic()
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (!IsCallActive)
                {
                    _logger.LogWarning("Playmusic; doing nothing because call is not active.");
                    return;
                }

                _logger.LogInformation("Playmusic forever starting.");
                _action = SIPCallAction.Playing;
                if (IsWebRTC)
                {
                    RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Music);
                }
                else
                {
                    RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Music);
                }
            }
        }

        /// <summary>
        /// Stops any ongoing playback or recording on the call.
        /// </summary>
        public void PlayStop()
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Playstop; starting");

                PlayLoopOn = false;

                if (_action == SIPCallAction.Playing || _action == SIPCallAction.Recording)
                {
                    _logger.LogDebug("Playstop; stopping any previous stream action");

                    if (RtpSession != null)
                        RtpSession.AudioExtrasSource.CancelSendAudioFromStream();

                    if (RtcPeerConnection != null)
                        RtcPeerConnection.AudioExtrasSource.CancelSendAudioFromStream();
                }

                _logger.LogDebug("Playstop; switching rtp to send silence.");
                if (RtpSession != null)
                    RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                if (RtcPeerConnection != null)
                    RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                _action = SIPCallAction.Idle;
                _logger.LogInformation("Playstop; done.");
            }
        }

        /// <summary>
        /// Captures rtp packets to a file with defaults
        /// # terminates the record
        /// 10s of silence terminates the record
        /// </summary>
        /// <param name="fileName">The full path file name to be record in pcm format 8KHz/16bit.</param>
        /// <param name="duration">The maximum duration in milliseconds of the record time.</param>
        public async Task RecordFile(string fileName, int duration)
        {
            await RecordFile(fileName, duration, '#', true, 10 * 1000);
        }

        /// <summary>
        /// Captures rtp packets to a file.
        /// </summary>
        /// <param name="fileName">The full path file name to be record in pcm format 8KHz/16bit.</param>
        /// <param name="duration">The maximum duration in milliseconds of the record time.</param>
        /// <param name="terminationDigit">The digit char that will determine if the record shall be stopped.</param>
        /// <param name="playTone">When true, the record tone is played back before recording starts.</param>
        /// <param name="stopOnSilence">Stops if silence duration is larger than this value (in milliseconds).</param>
        public async Task RecordFile(string fileName, int duration, char terminationDigit, bool playTone, int stopOnSilence)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (!IsCallActive)
                {
                    _logger.LogWarning("Recordfile; doing nothing because call is not active.");
                    return;
                }

                try
                {
                    _action = SIPCallAction.Recording;

                    _logger.LogInformation($"recording file {fileName}...");
                    _logger.LogDebug($"Recordfile; filename: {fileName}, duration: {duration}, stop on digit: {terminationDigit}, play tone: {playTone}.");
                    _digitsBuffer = "";

                    if (playTone)
                    {
                        _logger.LogDebug($"Recordfile; playing 825.0Mhz sine wave tone for 250ms...");
                        if (IsWebRTC)
                        {
                            RtcPeerConnection.AudioExtrasSource.SetSource(new AudioSourceOptions { AudioSource = AudioSourcesEnum.SineWave, Frequency = 825.0 });
                            await Task.Delay(250);
                            RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                        }
                        else
                        {
                            RtpSession.AudioExtrasSource.SetSource(new AudioSourceOptions { AudioSource = AudioSourcesEnum.SineWave, Frequency = 825.0 });
                            await Task.Delay(250);
                            RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                        }
                        _logger.LogDebug($"Recordfile; switching back rtp to send silence.");
                    }

                    _logger.LogDebug("resetting slience timers.");
                    _analyzer.ResetSilenceTimers();
                    _waveFileWriter = new WaveFileWriter(fileName, _waveFormat);

                    duration *= 4;

                    while (duration > 0)
                    {
                        if (_digitsBuffer.Contains(terminationDigit.ToString()))
                        {
                            _logger.LogInformation($"Recordfile; stopping recording because termination digit detected.");
                            break;
                        }

                        _digitsBuffer = "";

                        if (stopOnSilence > 0)
                        {
                            if (_analyzer.GetSilenceOnDuration() > (stopOnSilence))
                            {
                                _logger.LogInformation($"Recordfile; stopping recording because silence detected for {_analyzer.GetSilenceOnDuration()}ms.");
                                break;
                            }
                        }
                        await Task.Delay(250);
                        duration = duration - 1000;
                    }

                    _waveFileWriter.Close();
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message, e);
                    OnRecordError?.Invoke(e.Message);
                }
                finally
                {
                    _action = SIPCallAction.Idle;
                    _logger.LogInformation($"Recording file {fileName} completed.");
                    OnRecordDone?.Invoke();
                }
            }
        }

        /// <summary>
        /// Process a mid-call request, currently supports only INFO messages with dtmf signals.
        /// </summary>
        /// <param name="sipRequest">The sip request to process.</param>
        internal void MidCallRequest(SIPRequest sipRequest)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                if (sipRequest.Method == SIPMethodsEnum.INFO)
                {
                    if (IsCallActive)
                    {
                        _logger.LogInformation($"Midcallrequest; got INFO with content type {sipRequest.Header.ContentType}.");
                        var key = (byte)0; var duration = 100;
                        if ((sipRequest.Header.ContentType == "application/dtmf-relay") &&
                            (_dtmfMode == SIPCallDtmfMode.DtmfRelay))
                        {
                            var body = sipRequest.Body.Replace("\r", "=").Replace("\n", "=").Split('=');
                            if (body[1] == "*") key = 10;
                            else if (body[1] == "#") key = 11;
                            else key = byte.Parse(body[1]);
                            duration = int.Parse(body[4]);
                            _logger.LogDebug($"Midcallrequest; parsed key: {key} and duration: {duration}.");
                            UpdateDigitsBuffer(key, duration);
                        }
                        else if ((sipRequest.Header.ContentType == "application/dtmf") &&
                                 (_dtmfMode == SIPCallDtmfMode.Dtmf))
                        {
                            if (sipRequest.Body.Trim() == "*") key = 10;
                            else if (sipRequest.Body.Trim() == "#") key = 11;
                            else key = Byte.Parse(sipRequest.Body.Trim());
                            _logger.LogDebug($"Midcallrequest; parsed key: {key} and no duration using default {duration}");
                            UpdateDigitsBuffer(key, duration);
                        } else
                        {
                            _logger.LogWarning($"Wrong dtmf mode detected; received {sipRequest.Header.ContentType} while configured for {_dtmfMode}");
                        }

                    }
                    else
                    {
                        _logger.LogWarning($"Midcallrequest; doing nothing because call is not active.");
                    }
                }
                else
                {
                    _logger.LogWarning($"Midcallrequest; doing nothing because only method {sipRequest.Method} is implemented.");
                }
                _logger.LogInformation($"Midcallrequest; all done.");
            }
        }

        internal void OnInbandDtmf(string digit)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                var key = (byte)0; var duration = 100;
                if (digit == "*") key = 10;
                else if (digit == "#") key = 11;
                else key = Byte.Parse(digit);
                _logger.LogDebug($"Inband dtmf; parsed key: {key} and no duration using default {duration}");
                UpdateDigitsBuffer(key, duration);
            }
        }

        /// <summary>
        /// Performs a blind transfer to a non established call. Might requires a hold depending on the service pbx.
        /// </summary>
        /// <param name="to">The sip uri of the destination.</param>
        /// <param name="timeout">The maximum time to wait for the transfer to be completed.</param>
        /// <param name="disconnect">When true, the call is disconnected when treansfer is completed.</param>
        public async Task BlindTransfer(string to, int timeout, bool disconnect)
        {
            await BlindTransfer(to, null, timeout, disconnect);
        }

        /// <summary>
        /// Performs a blind transfer to a non established call. Might requires a hold depending on the service pbx.
        /// </summary>
        /// <param name="to">The sip uri of the destination.</param>
        /// <param name="sipHeaders">Optional. SIP headers to add or override.</param>
        /// <param name="timeout">The maximum time to wait for the transfer to be completed.</param>
        /// <param name="disconnect">When true, the call is disconnected when treansfer is completed.</param>
        public async Task BlindTransfer(string to, string[] sipHeaders, int timeout, bool disconnect)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Blindtransfer; starting...");
                _logger.LogDebug($"Blindtransfer; destination: {to}, timeout: {timeout}s, disconnect: {disconnect}.");
                var transferURI = SIPURI.ParseSIPURI(to);
                _logger.LogDebug($"Blindtransfer; transfering to {transferURI}");
                bool result = await this.BlindTransfer(transferURI, TimeSpan.FromMilliseconds(timeout), _exitCts.Token, sipHeaders);
                if (result)
                {
                    // If the transfer was accepted the original call will already have been hungup.
                    // Wait a second for the transfer NOTIFY request to arrive.
                    _logger.LogDebug($"Blindtransfer; waiting for NOTIFY");
                    await Task.Delay(1000);
                    _exitCts.Cancel();

                    if (disconnect)
                    {
                        _logger.LogDebug($"Blindtransfer; disconnecting original call.");
                        this.Hangup();
                    }
                }
                _logger.LogInformation("Blindtransfer; all done.");
            }
        }

        /// <summary>
        /// Performs an attended transfer between 2 established calls.
        /// </summary>
        /// <param name="ua2">The sip user agent of the second call.</param>
        /// <param name="timeout">The maximum time to wait for the transfer to be completed.</param>
        public async Task AttendedTransfer(SIPUserAgent ua2, int timeout)
        {
            await AttendedTransfer(ua2, null, timeout);
        }

        /// <summary>
        /// Performs an attended transfer between 2 established calls.
        /// </summary>
        /// <param name="ua2">The sip user agent of the second call.</param>
        /// <param name="sipHeaders">Optional. SIP headers to add or override.</param>
        /// <param name="timeout">The maximum time to wait for the transfer to be completed.</param>
        public async Task AttendedTransfer(SIPUserAgent ua2, string[] sipHeaders, int timeout)

        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Attendedtransfer; starting...");
                _logger.LogDebug($"Attendedtransfer; destination: {ua2}, timeout: {timeout}s.");
                bool result = await this.AttendedTransfer(ua2.Dialogue, TimeSpan.FromMilliseconds(timeout), _exitCts.Token, sipHeaders);
                if (!result)
                {
                    _logger.LogWarning($"Attendedtransfer; transfer failed.");
                }
                else
                {
                    _logger.LogDebug($"Attendedtransfer; waiting for NOTIFY");
                    await Task.Delay(1000);
                    _exitCts.Cancel();
                }
                _logger.LogInformation("Attendedtransfer; all done.");
            }
        }

        /// <summary>
        /// Saves a sip request as last sip request of the call.
        /// </summary>
        internal void SetLastSIPRequest(SIPRequest sipRequest)
        {
            _lastSIPRequest = sipRequest;
        }

        /// <summary>
        /// Returns the last sip request of the call.
        /// </summary>
        public SIPRequest GetLastSIPRequest() => _lastSIPRequest;

        /// <summary>
        /// Returns the true if the call is a webRTC call.
        /// </summary>
        public bool IsWebRTC { get; private set; } = false;

                /// <summary>
        /// Returns the true if the call is a webRTC call.
        /// </summary>
        public bool hasAudio() {
            if (IsWebRTC) {
                if (RtcPeerConnection != null) {
                    return RtcPeerConnection.HasAudio;
                }
            } else {
                if (RtpSession != null) {
                    return RtpSession.HasAudio;
                }
            }

            return false;
        }

        /// <summary>
        /// Accept the incoming call. Creates the rtp session and initiates the ringback tone.
        /// </summary>
        /// <param name="protocol">Protocol used to setup the session.</param>
        /// <param name="sipRequest"></param>
        /// <param name="delay">The delay in milliseconds to insert in order for the ringback tone to be audible.</param>
        public async Task<SIPServerUserAgent> Accept(SIPProtocolsEnum protocol, SIPRequest sipRequest, int delay)
        {
            return await Accept(protocol, sipRequest, null, delay);
        }

        /// <summary>
        /// Accept the incoming call. Creates the rtp session and initiates the ringback tone.
        /// </summary>
        /// <param name="protocol">Protocol used to setup the session.</param>
        /// <param name="sipRequest"></param>
        /// <param name="sipHeaders">Optional. SIP headers to add or override defaults.</param>
        /// <param name="delay">The delay in milliseconds to insert in order for the ringback tone to be audible.</param>
        public async Task<SIPServerUserAgent> Accept(SIPProtocolsEnum protocol, SIPRequest sipRequest, string[] sipHeaders, int delay)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Accept; starting...");
                _logger.LogDebug($"Accept; delay: {delay}s.");
                _sipServerUserAgent = AcceptCall(sipRequest, sipHeaders);
                _logger.LogDebug($"Accept; create rtp session for incoming call.");
                if (protocol == SIPProtocolsEnum.ws ||
                    protocol == SIPProtocolsEnum.wss)
                {
                    CreatePeerConnection();
                }
                else
                {
                    CreateRtpSession();
                }
                _logger.LogDebug($"Accept; waiting {delay}s to allow ringback tone.");
                // Insert a brief delay to allow testing of the "Ringing" progress response.
                // Without the delay the call gets answered before it can be sent.
                await Task.Delay(delay);
                _logger.LogInformation("Accept; all done.");
            }

            return _sipServerUserAgent;
        }

        /// <summary>
        /// Accept the incoming call. Creates the rtp session and initiates the ringback tone.
        /// </summary>
        /// <param name="sipRequest"></param>
        /// <param name="sipHeaders">Optional. SIP headers to add or override default.</param>
        /// <param name="delay">The delay in milliseconds to insert in order for the ringback tone to be audible.</param>
        public async Task<SIPServerUserAgent> Accept(SIPRequest sipRequest, string[] sipHeaders, int delay)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Accept; starting...");
                _logger.LogDebug($"Accept; delay: {delay}s.");
                _sipServerUserAgent = AcceptCall(sipRequest, sipHeaders);
                _logger.LogDebug($"Accept; create rtp session for incoming call.");
                CreateRtpSession();
                _logger.LogDebug($"Accept; waiting {delay}s to allow ringback tone.");
                // Insert a brief delay to allow testing of the "Ringing" progress response.
                // Without the delay the call gets answered before it can be sent.
                await Task.Delay(delay);
                _logger.LogInformation("Accept; all done.");
            }

            return _sipServerUserAgent;
        }

        /// <summary>
        /// Accept the incoming call. Creates the rtp session and initiates the ringback tone.
        /// </summary>
        /// <param name="sipRequest"></param>
        /// <param name="delay">The delay in milliseconds to insert in order for the ringback tone to be audible.</param>
        public async Task<SIPServerUserAgent> Accept(SIPRequest sipRequest, int delay)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Accept; starting...");
                _logger.LogDebug($"Accept; delay: {delay}s.");
                _sipServerUserAgent = AcceptCall(sipRequest);
                _logger.LogDebug($"Accept; create rtp session for incoming call.");
                CreateRtpSession();
                _logger.LogDebug($"Accept; waiting {delay}s to allow ringback tone.");
                // Insert a brief delay to allow testing of the "Ringing" progress response.
                // Without the delay the call gets answered before it can be sent.
                await Task.Delay(delay);
                _logger.LogInformation("Accept; all done.");
            }

            return _sipServerUserAgent;
        }

        /// <summary>
        /// Accept the incoming call. Creates the rtp session and initiates the ringback tone.
        /// </summary>
        /// <param name="sipHeaders">Optional. SIP headers to add or override default.</param>
        /// <param name="delay">The delay in milliseconds to insert in order for the ringback tone to be audible.</param>
        public async Task Accept(string[] sipHeaders, int delay)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Accept; starting...");
                _logger.LogDebug($"Accept; delay: {delay}s.");

                if (_state == SIPCallState.Offered)
                {
                    _sipServerUserAgent = AcceptCall(_lastSIPRequest, sipHeaders);
                    _logger.LogDebug($"Accept; create rtp session for incoming call.");
                    if (IsWebRTC)
                    {
                        CreatePeerConnection();
                    }
                    else
                    {
                        CreateRtpSession();
                    }
                    _logger.LogDebug($"Accept; waiting {delay}s to allow ringback tone.");
                    // Insert a brief delay to allow testing of the "Ringing" progress response.
                    // Without the delay the call gets answered before it can be sent.
                    await Task.Delay(delay);

                    _state = SIPCallState.Accepted;
                    OnCallAccepted?.Invoke();

                    _logger.LogInformation("Accept; all done.");
                }
                else
                {
                    _logger.LogInformation($"Accept; not doing anything because state is {this.GetCallState()}.");
                }
                
            }
        }

        /// <summary>
        /// Accept the incoming call. Creates the rtp session and initiates the ringback tone.
        /// </summary>
        /// <param name="delay">The delay in milliseconds to insert in order for the ringback tone to be audible.</param>
        public async Task Accept(int delay)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Accept; starting...");
                _logger.LogDebug($"Accept; delay: {delay}s.");
                
                if (_state == SIPCallState.Offered)
                {
                    _sipServerUserAgent = AcceptCall(_lastSIPRequest);
                    _logger.LogDebug($"Accept; create rtp session for incoming call.");
                    if (IsWebRTC)
                    {
                        CreatePeerConnection();
                    }
                    else
                    {
                        CreateRtpSession();
                    }
                    _logger.LogDebug($"Accept; waiting {delay}s to allow ringback tone.");
                    // Insert a brief delay to allow testing of the "Ringing" progress response.
                    // Without the delay the call gets answered before it can be sent.
                    await Task.Delay(delay);

                    _state = SIPCallState.Accepted;
                    OnCallAccepted?.Invoke();

                    _logger.LogInformation("Accept; all done.");
                }
                else
                {
                    _logger.LogInformation($"Accept; not doing anything because state is {this.GetCallState()}.");
                }
        }
        }

        /// <summary>
        /// Accept the call with a default delay of 1 second.
        /// </summary>
        public async Task Accept()
        {
            await Accept(1);
        }

        /// <summary>
        /// Answer the incoming call and starts the rtp streaming.
        /// <param name="sipHeaders">Optional. SIP headers to add or override default.</param>
        /// </summary>
        public async Task Answer()
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Answer; starting...");

                if (_state == SIPCallState.Accepted)
                {
                    if (IsWebRTC)
                    {
                        await Answer(_sipServerUserAgent, RtcPeerConnection);

                        if (IsCallActive)
                        {
                            _startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                            _logger.LogDebug("Answer; starting rtc sending.");
                            await RtcPeerConnection.Start();
                            RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                            await Task.Delay(1000);
                        }
                        else
                        {
                            _logger.LogWarning("Answer; rtc sending not started because call is not active.");
                        }
                    }
                    else
                    {
                        await Answer(_sipServerUserAgent, RtpSession);

                        if (IsCallActive)
                        {
                            _startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                            _logger.LogDebug("Answer; starting rtp sending.");
                            await RtpSession.Start();
                            RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                            await Task.Delay(1000);
                        }
                        else
                        {
                            _logger.LogWarning("Answer; rtp sending not started because call is not active.");
                        }
                    }

                    _state = SIPCallState.Active;
                    OnCallAnswered?.Invoke();
                    _logger.LogInformation("Answer; all done.");
                }
                else
                {
                    _logger.LogInformation($"Answer; not doing anything because state is {this.GetCallState()}.");
                }
            }
        }

        /// <summary>
        /// Answer the incoming call and starts the rtp streaming.
        /// </summary>
        public async Task Answer(string[] sipHeaders)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _logger.LogInformation("Answer; starting...");

                if (_state == SIPCallState.Accepted)
                {
                    if (IsWebRTC)
                    {
                        await Answer(_sipServerUserAgent, RtcPeerConnection, sipHeaders);

                        if (IsCallActive)
                        {
                            _startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                            _logger.LogDebug("Answer; starting rtc sending.");
                            await RtcPeerConnection.Start();
                            RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                            await Task.Delay(1000);
                        }
                        else
                        {
                            _logger.LogWarning("Answer; rtc sending not started because call is not active.");
                        }
                    }
                    else
                    {
                        await Answer(_sipServerUserAgent, RtpSession, sipHeaders);

                        if (IsCallActive)
                        {
                            _startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                            _logger.LogDebug("Answer; starting rtp sending.");
                            await RtpSession.Start();
                            RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                            await Task.Delay(1000);
                        }
                        else
                        {
                            _logger.LogWarning("Answer; rtp sending not started because call is not active.");
                        }
                    }

                    _state = SIPCallState.Active;
                    OnCallAnswered?.Invoke();
                    _logger.LogInformation("Answer; all done.");
                }
                else
                {
                    _logger.LogInformation($"Answer; not doing anything because state is {this.GetCallState()}.");
                }
            }
        }

        /// <summary>
        /// Answer the incoming call and starts the rtp streaming.
        /// </summary>
        public async Task Answer(SIPServerUserAgent uas)
        {
            await Answer(uas, null);
        }

        /// <summary>
        /// Answer the incoming call and starts the rtp streaming.
        /// <param name="sipHeaders">Optional. SIP headers to add or override default.</param>
        /// </summary>
        public async Task Answer(SIPServerUserAgent uas, string[] sipHeaders)
        {
            using (LogContext.PushProperty("CallContext", GetCallID()))
            {
                _sipServerUserAgent = uas;

                _logger.LogInformation("Answer; starting...");

                if (_state == SIPCallState.Accepted)
                {
                    if (IsWebRTC)
                    {
                        await Answer(_sipServerUserAgent, RtcPeerConnection, sipHeaders);
                        if (IsCallActive)
                        {
                            _startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                            _logger.LogDebug("Answer; starting rtc sending.");
                            await RtcPeerConnection.Start();
                            RtcPeerConnection.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                            await Task.Delay(1000);
                        }
                        else
                        {
                            _logger.LogWarning("Answer; rtc sending not started because call is not active.");
                        }
                    }
                    else
                    {
                        await Answer(_sipServerUserAgent, RtpSession, sipHeaders);
                        if (IsCallActive)
                        {
                            _startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                            _logger.LogDebug("Answer; starting rtp sending.");
                            await RtpSession.Start();
                            RtpSession.AudioExtrasSource.SetSource(AudioSourcesEnum.Silence);
                            await Task.Delay(1000);
                        }
                        else
                        {
                            _logger.LogWarning("Answer; rtp sending not started because call is not active.");
                        }
                    }

                    _state = SIPCallState.Active;
                    OnCallAnswered?.Invoke();
                    _logger.LogInformation("Answer; all done.");
                }
                else
                {
                   _logger.LogInformation($"Answer; not doing anything because state is {this.GetCallState()}.");
                }
            }
        }

        public void Bridge(string bridgeId)
        {
            if (RtpSession != null)
            {
                RtpSession.AudioExtrasSource.SetSource(SIPSorcery.Media.AudioSourcesEnum.None);
                BridgeId = bridgeId;
                _action = SIPCallAction.Bridged;
            }

            if (RtcPeerConnection != null)
            {
                RtcPeerConnection.AudioExtrasSource.SetSource(SIPSorcery.Media.AudioSourcesEnum.None);
                BridgeId = bridgeId;
                _action = SIPCallAction.Bridged;
            }
        }

        public void Unbridge()
        {
            if (RtpSession != null)
            {
                RtpSession.AudioExtrasSource.SetSource(SIPSorcery.Media.AudioSourcesEnum.Silence);
                _action = SIPCallAction.Idle;
                BridgeId = null;
            }

            if (RtcPeerConnection != null)
            {
                RtcPeerConnection.AudioExtrasSource.SetSource(SIPSorcery.Media.AudioSourcesEnum.Silence);
                _action = SIPCallAction.Idle;
                BridgeId = null;
            }
        }

    }
}
