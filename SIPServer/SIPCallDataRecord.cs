using System;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.Extensions.Logging;
using SIPSorcery.SIP;

namespace SeriousSIP
{
    public delegate void CDRReadyDelegate(SIPCallDataRecord cdr);                // Used to inform CDR handlers when a CDR has been udpated.

    /// <summary>
    /// Call detail record for a SIP call.
    /// </summary>
    [DataContract]
    public class SIPCallDataRecord
    {
        private ILogger _logger; 
        private static string m_newLine = Environment.NewLine;

        public static event CDRReadyDelegate CDRCreated = c => { if (c.WriteOnCreated) c.WriteCDR(); };
        public static event CDRReadyDelegate CDRProgress = c => { if (c.WriteOnProgress) c.WriteCDR(); };
        public static event CDRReadyDelegate CDRAnswered = c => { if (c.WriteOnAnswered) c.WriteCDR(); };
        public static event CDRReadyDelegate CDRHungup = c => { if (c.WriteOnHungup) c.WriteCDR(); };

        public void WriteCDR()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append($"\"type\":\"call\",");
            sb.Append($"\"callId\":\"{this.CallId}\",");
            sb.Append($"\"createdTime\":\"{this.Created}\",");
            sb.Append($"\"calldirection\":\"{this.CallDirection}\",");
            sb.Append($"\"from\":\"{this.From}\",");
            sb.Append($"\"to\":\"{this.Destination}\",");
            sb.Append($"\"localEndPoint\":\"{this.LocalSIPEndPoint}\",");
            sb.Append($"\"remoteEndPoint\":\"{this.RemoteSIPEndPoint}\",");
            sb.Append($"\"answerReason\":\"{this.AnswerReasonPhrase}\",");
            sb.Append($"\"answerStatus\":\"{this.AnswerStatus}\",");
            sb.Append($"\"answerTime\":\"{this.AnswerTime}\",");
            sb.Append($"\"progressReason\":\"{this.ProgressReasonPhrase}\",");
            sb.Append($"\"progressStatus\":\"{this.ProgressStatus}\",");
            sb.Append($"\"progressTime\":\"{this.ProgressTime}\",");
            sb.Append($"\"hangupReason\":\"{this.HangupReason}\",");
            sb.Append($"\"hangupTime\":\"{this.HangupTime}\",");
            sb.Append($"\"ringDuration\":\"{this.GetProgressDuration()}\",");
            sb.Append($"\"callDuration\":\"{this.GetAnsweredDuration()}\"");
            sb.Append("}");
            _logger.LogTrace(sb.ToString());
        }

        public bool WriteOnCreated { get; set; }

        public bool WriteOnProgress { get; set; }

        public bool WriteOnAnswered { get; set; }

        public bool WriteOnHungup { get; set; }

        public string Owner { get; set; }

        public string AdminMemberId { get; set; }

        public DateTimeOffset Created { get; set; }

        public SIPCallDirection CallDirection { get; set; }

        public SIPURI Destination { get; set; }

        public SIPURI From { get; set; }

        public string callDuration { get; set; }

        public int ProgressStatus { get; set; }

        public string ProgressReasonPhrase { get; set; }

        private DateTimeOffset? m_progressTime;

        public DateTimeOffset? ProgressTime
        {
            get { return m_progressTime; }
            set
            {
                if (value != null)
                {
                    m_progressTime = value.Value.ToUniversalTime();
                }
                else
                {
                    m_progressTime = null;
                }
            }
        }

        public int AnswerStatus { get; set; }

        public string AnswerReasonPhrase { get; set; }

        private DateTimeOffset? m_answerTime;

        public DateTimeOffset? AnswerTime
        {
            get { return m_answerTime; }
            set
            {
                if (value != null)
                {
                    m_answerTime = value.Value.ToUniversalTime();
                }
                else
                {
                    m_answerTime = null;
                }
            }
        }

        private DateTimeOffset? m_hangupTime;

        public DateTimeOffset? HangupTime
        {
            get { return m_hangupTime; }
            set
            {
                if (value != null)
                {
                    m_hangupTime = value.Value.ToUniversalTime();
                }
                else
                {
                    m_hangupTime = null;
                }
            }
        }

        public string HangupReason { get; set; }

        public DateTime? AnsweredAt { get; set; }

        public string CallId { get; set; }
        public SIPEndPoint LocalSIPEndPoint { get; set; }
        public SIPEndPoint RemoteSIPEndPoint { get; set; }
        public bool InProgress { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsHungup { get; set; }

        public SIPCallDataRecord(
            SIPCallDirection callDirection,
            SIPURI destination,
            SIPURI from,
            string callId,
            SIPEndPoint localSIPEndPoint,
            SIPEndPoint remoteSIPEndPoint)
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}"); 
            Created = DateTimeOffset.UtcNow;
            CallDirection = callDirection;
            Destination = destination;
            From = from;
            CallId = callId;
            LocalSIPEndPoint = localSIPEndPoint;
            RemoteSIPEndPoint = remoteSIPEndPoint;
            InProgress = false;
            IsAnswered = false;
            IsHungup = false;
            IsCancelled = false;

            WriteOnCreated = false;
            WriteOnProgress = false;
            WriteOnAnswered = false;
            WriteOnHungup = true;

            CDRCreated(this);
        }

        public void Progress(SIPResponseStatusCodesEnum progressStatus, string progressReason, SIPEndPoint localEndPoint, SIPEndPoint remoteEndPoint)
        {
            InProgress = true;
            ProgressTime = DateTimeOffset.UtcNow;
            ProgressStatus = (int)progressStatus;
            ProgressReasonPhrase = progressReason;

            if (localEndPoint != null)
            {
                LocalSIPEndPoint = localEndPoint;
            }

            if (remoteEndPoint != null)
            {
                RemoteSIPEndPoint = remoteEndPoint;
            }
        }

        public void Answered(int answerStatusCode, SIPResponseStatusCodesEnum answerStatus, string answerReason, SIPEndPoint localEndPoint, SIPEndPoint remoteEndPoint)
        {
            try
            {
                IsAnswered = true;
                AnswerTime = DateTimeOffset.UtcNow;
                AnswerStatus = (int)answerStatus;
                AnswerReasonPhrase = answerReason;
                AnsweredAt = DateTime.Now;

                if (localEndPoint != null)
                {
                    LocalSIPEndPoint = localEndPoint;
                }

                if (remoteEndPoint != null)
                {
                    RemoteSIPEndPoint = remoteEndPoint;
                }

                CDRAnswered(this);
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPCDR Answered. " + excp);
            }
        }

        public void Cancelled(string cancelReason = null)
        {
            try
            {
                IsCancelled = true;
                HangupTime = DateTimeOffset.UtcNow;
                HangupReason = (cancelReason != null) ? cancelReason : "Client cancelled";
                CDRHungup(this);
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPCDR Cancelled. " + excp);
            }
        }

        public void TimedOut()
        {
            try
            {
                HangupTime = DateTimeOffset.UtcNow;
                HangupReason = "Timed out";
                CDRHungup(this);
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPCDR TimedOut. " + excp);
            }
        }

        public void Hungup(string hangupReason)
        {
            try
            {
                IsHungup = true;
                HangupTime = DateTimeOffset.UtcNow;
                HangupReason = hangupReason;

                CDRHungup(this);
            }
            catch (Exception excp)
            {
                _logger.LogError("Exception SIPCDR Hungup. " + excp);
            }
        }

        public int GetProgressDuration()
        {
            return (ProgressTime != null && AnswerTime != null) ? Convert.ToInt32(AnswerTime.Value.Subtract(ProgressTime.Value).TotalSeconds) : 0;
        }

        public string GetAnsweredDuration()
        {
            if (AnswerTime != null && HangupTime != null) {
                callDuration = $"{HangupTime.Value.Subtract(AnswerTime.Value).TotalSeconds}";
            }
            else
            {
                callDuration = "0";
            }

            return callDuration;
        }
    }
}
