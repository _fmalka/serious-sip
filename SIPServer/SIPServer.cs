﻿using JamaaTech.Smpp.Net.Client;
using JamaaTech.Smpp.Net.Lib;
using Microsoft.Extensions.Logging;
using Serilog.Context;
using SIPSorcery.Media;
using SIPSorcery.Net;
using SIPSorcery.SIP;
using SIPSorcery.SIP.App;
using SIPSorceryMedia.Abstractions.V1;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using static SeriousSIP.SIPTrunk;

namespace SeriousSIP
{
    /// <summary>
    /// Manage the sip transports, registrations and calls for an application instance.
    /// Provides an interface to initiate a call and some events for the application to get notified of inbound calls.
    /// </summary>
    public class SIPServer

    {
        /// <summary>
        /// Stores the accounts credentials for sip registrations.
        /// </summary>
        struct SIPRegisterAccount
        {
            public string Username;
            public string Password;
            public string Domain;
            public int Expiry;

            /// <summary>
            /// Creates an account to register with the pbx.
            /// </summary>
            /// <param name="username">The username or extension to register with.</param>
            /// <param name="password">The registration password.</param>
            /// <param name="domain">The domain or ip address of the sip registrar / ip-pbx.</param>
            /// <param name="expiry">The expiry header value to use for the periodic registration.</param>
            public SIPRegisterAccount(string username, string password, string domain, int expiry)
            {
                Username = username;
                Password = password;
                Domain = domain;
                Expiry = expiry;
            }
        }

        /// <summary>
        /// Stores high level statistics for sip requests/responses.
        /// </summary>
        public struct CallStats
        {
            public long Offered;
            public long Accepted;
            public long Answered;
            public long Calling;
            public long Trying;
            public long Progress;
            public long Ringing;
            public long Connected;
            public long Diverted;
            public long Failed;
            public long Timeout;
            public long Hangup;
            public long Cancelled;
        }

        /// <summary>
        /// A delegate for sip server start events triggering.
        /// </summary>
        public delegate Task SIPServerStartAsyncDelegate(String message);

        /// <summary>
        /// A delegate for calls events triggering.
        /// </summary>
        public delegate Task SIPServerCallAsyncDelegate(SIPCall call);

        /// <summary>
        /// A delegate for bridges events triggering.
        /// </summary>
        public delegate Task SIPServerBridgeAsyncDelegate(SIPBridge bridge);

        /// <summary>
        /// A delegate for registrations error events triggering.
        /// </summary>
        public delegate Task SIPServerRegistrationErrorAsyncDelegate(SIPURI uri, string reason);

        /// <summary>
        /// A delegate for registrations success events triggering.
        /// </summary>
        public delegate Task SIPServerRegistrationReportAsyncDelegate(SIPURI uri);

        /// <summary>
        /// A delegate for calls error events triggering.
        /// </summary>
        public delegate Task SIPServerCallErrorAsyncDelegate(SIPCall call, string reason);

        /// <summary>
        /// A delegate for calls error events triggering.
        /// </summary>
        public delegate Task SIPServerCallDisconnectedAsyncDelegate(string callId, string identity, string reason);

        /// <summary>
        /// A delegate for messages error events triggering.
        /// </summary>
        public delegate Task SIPServerMessageErrorAsyncDelegate(string callId, string reason);

        /// <summary>
        /// A delegate for messages receive events triggering.
        /// </summary>
        public delegate Task SIPServerMessageReceivedAsyncDelegate(SIPMessage message, string reason);

        /// <summary>
        /// A delegate for text messages receive events triggering.
        /// </summary>
        public delegate Task SIPServerTextMessageReceivedAsyncDelegate(string identity, string callId, string from, string message);

        /// <summary>
        /// A delegate for text messages general events triggering.
        /// </summary>
        public delegate Task SIPServerTextMessageAsyncDelegate(string identity, string callId);

        /// <summary>
        /// A delegate for messages receive events triggering.
        /// </summary>
        public delegate Task SIPServerConcatStateUpdateddAsyncDelegate(string identity, SIPContact contact);

        /// <summary>
        /// Allows application to register to server start failed events triggering.
        /// </summary>
        public event SIPServerStartAsyncDelegate OnSIPServerStartFailed;

        /// <summary>
        /// Allows application to register to server start success events triggering.
        /// </summary>
        public event SIPServerStartAsyncDelegate OnSIPServerStartSuccess;

        /// <summary>
        /// Allows application to register to server stop success events triggering.
        /// </summary>
        public event SIPServerStartAsyncDelegate OnSIPServerStopSuccess;

        /// <summary>
        /// Allows application to register to server stop failed events triggering.
        /// </summary>
        public event SIPServerStartAsyncDelegate OnSIPServerStopFailed;

        /// <summary>
        /// Allows application to register to the bridge creation event when a new bridge is created.
        /// </summary>
        public event SIPServerBridgeAsyncDelegate OnBridgeCreated;

        /// <summary>
        /// Triggers the trunk UP event
        /// </summary>
        public event SIPTrunkMonitorDelegate OnTrunkUp;

        /// <summary>
        /// Triggers the trunk DOWN event
        /// </summary>
        public event SIPTrunkMonitorErrorDelegate OnTrunkDown;

        /// <summary>
        /// Allows application to register to the registration failure events
        /// </summary>
        public event SIPServerRegistrationErrorAsyncDelegate OnRegistrationFailed;

        /// <summary>
        /// Allows application to register to the registration temporary failure events
        /// </summary>
        public event SIPServerRegistrationErrorAsyncDelegate OnRegistrationTemporaryFailure;

        /// <summary>
        /// Allows application to register to the registration removed events
        /// </summary>
        public event SIPServerRegistrationReportAsyncDelegate OnRegistrationRemoved;

        /// <summary>
        /// Allows application to register to the registration success events
        /// </summary>
        public event SIPServerRegistrationReportAsyncDelegate OnRegistrationSuccess;

        /// <summary>
        /// Allows application to register to the call offered event when a new call is received.
        /// </summary>
        public event SIPServerCallAsyncDelegate OnCallOffered;

        /// <summary>
        /// Allows application to register to the call accepted event when the call accept is completed.
        /// </summary>
        public event SIPServerCallAsyncDelegate OnCallAccepted;

        /// <summary>
        /// Allows application to register to the call connected event when the call connected is completed.
        /// </summary>
        public event SIPServerCallAsyncDelegate OnCallAnswered;

        /// <summary>
        /// Allows application to register to the call connected event when the call connected is completed.
        /// </summary>
        public event SIPServerCallAsyncDelegate OnCallConnected;

        /// <summary>
        /// Allows application to register to the call ringing event.
        /// </summary>
        public event SIPServerCallAsyncDelegate OnCallRinging;

        /// <summary>
        /// Allows application to register to the call diverted event.
        /// </summary>
        public event SIPServerCallErrorAsyncDelegate OnCallDiverted;

        /// <summary>
        /// Allows application to register to the call failed event.
        /// </summary>
        public event SIPServerCallErrorAsyncDelegate OnCallFailed;

        /// <summary>
        /// Allows application to register to the call tmeout event.
        /// </summary>
        public event SIPServerCallErrorAsyncDelegate OnCallTimeout;

        /// <summary>
        /// Allows application to register to the call tmeout event.
        /// </summary>
        public event SIPServerCallErrorAsyncDelegate OnCallCancelled;

        /// <summary>
        /// Allows application to register to the call tmeout event.
        /// </summary>
        public event SIPServerCallDisconnectedAsyncDelegate OnCallDisconnected;

        /// <summary>
        /// Allows application to register to the message sent event.
        /// </summary>
        public event SIPServerMessageErrorAsyncDelegate OnMessageSent;

        /// <summary>
        /// Allows application to register to the message sent event.
        /// </summary>
        public event SIPServerMessageReceivedAsyncDelegate OnMessageReceived;

        /// <summary>
        /// Allows application to register to the message failed event.
        /// </summary>
        public event SIPServerMessageErrorAsyncDelegate OnMessageFailed;

        /// <summary>
        /// Allows application to register to the message timeout event.
        /// </summary>
        public event SIPServerMessageErrorAsyncDelegate OnMessageTimeout;

        /// <summary>
        /// Allows aplication to register to the text message received event.
        /// </summary>
        public event SIPServerTextMessageReceivedAsyncDelegate OnTextMessageReceived;

        /// <summary>
        /// Allows aplication to register to the text message sent event.
        /// </summary>
        public event SIPServerTextMessageAsyncDelegate OnTextMessageSent;

        /// <summary>
        /// Allows aplication to register to the text message delivered event.
        /// </summary>
        public event SIPServerTextMessageAsyncDelegate OnTextMessageDelivered;


        /// <summary>
        /// Allows application to register to the presence events.
        /// </summary>
        public event SIPServerConcatStateUpdateddAsyncDelegate OnContactStateUpdated;

        /// <summary>
        /// Keeps an internal cancellation token source.
        /// </summary>
        private CancellationTokenSource _exitCts = new CancellationTokenSource(); // Cancellation token to stop the SIP transport and RTP stream.

        /// <summary>
        /// Keeps the sip transport used to send and receive calls and messages.
        /// </summary>
        private SIPTransport _sipTransport;

        /// <summary>
        /// Keeps the smpp cliend used to send and receive short text messages (sms).
        /// </summary>
        private SmppClient _smppClient = null;

        /// <summary>
        /// Keeps the smpp client config.
        /// </summary>
        public ISmppConfiguration SmppConfig { get; set; }

        /// <summary>
        /// Keeps an internal state of the call server.
        /// </summary>
        public SIPServerState State { get; set; } = SIPServerState.NotStarted;

        /// <summary>
        /// When set, the rtp stack use windows multimedia timers when playing files.
        /// </summary>
        public bool UseMultiMediaTimers { get; set; } = false;

        /// <summary>
        /// Set the interval in milliseconds between each registration.
        /// </summary>
        public int RegistrationInterval { get; set; } = 250;

        /// <summary>
        /// Keeps the logger created by the main application.
        /// </summary>
        private ILogger _logger;

        /// <summary>
        /// Keeps track of the current active calls. It includes both received and placed calls.
        /// </summary>
        public ConcurrentDictionary<string, SIPCall> Calls { get; set; } = new ConcurrentDictionary<string, SIPCall>();

        /// <summary>
        /// Keeps track of the current active calls. It includes both received and placed calls.
        /// </summary>
        public ConcurrentDictionary<string, SIPMessage> Messages { get; set; } = new ConcurrentDictionary<string, SIPMessage>();

        /// <summary>
        /// Keeps all the contacts
        /// </summary>
        public ConcurrentDictionary<string, SIPContact> Contacts { get; set; } = new ConcurrentDictionary<string, SIPContact>();

        /// <summary>
        /// Keeps all the contact groups
        /// </summary>
        public ConcurrentDictionary<string, SIPGroup> Groups { get; set; } = new ConcurrentDictionary<string, SIPGroup>();

        /// <summary>
        /// Keeps track of the current active apps. 
        /// </summary>
        //private ConcurrentDictionary<string, ISIPCallServerApplication> Apps { get; set; } = new ConcurrentDictionary<string, ISIPCallServerApplication>();

        /// <summary>
        /// Keeps track of the sip account registrations.
        /// </summary>
        private ConcurrentDictionary<string, SIPRegistrationUserAgent> _registrations = new ConcurrentDictionary<string, SIPRegistrationUserAgent>();

        /// <summary>
        /// Keeps track of the sip trunks.
        /// </summary>
        private ConcurrentDictionary<SIPURI, SIPTrunk> _trunks = new ConcurrentDictionary<SIPURI, SIPTrunk>();

        /// <summary>
        /// Keeps track of the sip account subscriptions.
        /// </summary>
        private ConcurrentDictionary<string, SIPNotifierClient<SIPEvent>> _subscriptions = new ConcurrentDictionary<string, SIPNotifierClient<SIPEvent>>();

        /// <summary>
        /// Keeps track of the sip conferences.
        /// </summary>
        public ConcurrentDictionary<string, SIPBridge> Bridges { get; set; } = new ConcurrentDictionary<string, SIPBridge>();

        /// <summary>
        /// Keeps track of per method statistics counters.
        /// </summary>
        public static CallStats[] _stats = new CallStats[60];

        /// <summary>
        /// Keeps a timer that reset stats every hour.
        /// </summary>
        private Timer _statsTimer = new Timer(new TimerCallback(OnStatsTimerEvent), null, 3600*1000, 3600*1000);

        /// <summary>
        /// Keeps the list of codecs that are allowed to be used.
        /// </summary>
        public List<AudioCodecsEnum> Codecs { get; set; } = new List<AudioCodecsEnum> { AudioCodecsEnum.PCMU, AudioCodecsEnum.PCMA, AudioCodecsEnum.G722, AudioCodecsEnum.G729 };
        
        /// <summary>
        /// Keeps the list of local ip endpoints for the underlaying sip transport
        /// </summary>
        public StringCollection LocalIPEndPoints { get; set; }

        /// <summary>
        /// Keeps the list of accounts to register with the PABX
        /// </summary>
        public StringCollection RegisterAccounts { get; set; }

        /// <summary>
        /// Keeps the list of sip trunks to monitor
        /// </summary>
        public StringCollection MonitorTrunks { get; set; } = new StringCollection();

        /// <summary>
        /// Keeps the dtmf mode to be used for new calls.
        /// </summary>
        public SIPCallDtmfMode DtmfMode { get; set; } = SIPCallDtmfMode.Rfc2833;

        /// <summary>
        /// Returns the opened channels of the sip server underlaying transport.
        /// </summary>
        public List<SIPChannel> GetTransportChannels() { return _sipTransport.GetSIPChannels(); }

        /// <summary>
        /// Max number of calls for this instance
        /// </summary>
        public int MaxCalls { get; set; } = 0;

        /// <summary>
        /// Keeps the rtp IPv4 address to use for the call.
        /// </summary>
        public IPAddress RtpAddress { get; set; } = null;

        /// <summary>
        /// When true, disconnect the call if no RTP is received for 30s.
        /// </summary>
        public bool HangupWhenNoRtp { get; set; } = true;

        /// <summary>
        /// Returns the call statistics for the last hour.
        /// </summary>
        /// <returns></returns>
        public CallStats[] GetStats() => _stats;

        /// <summary>
        /// The UserAgent header string.
        /// </summary>
        public string UserAgent { get; set; } = SIPConstants.SIP_USERAGENT_STRING;

        /// <summary>
        /// Set default codecs to be offered for inbound and outbound calls.
        /// </summary>
        /// <param name="codecs"></param>
        public void SetCodecs(StringCollection codecs)
        {
            if (codecs.Count > 0)
            {
                Codecs = new List<AudioCodecsEnum>();
                foreach (var s in codecs)
                {
                    Codecs.Add((AudioCodecsEnum)Enum.Parse(typeof(AudioCodecsEnum), s));
                }
            }
        }

        /// <summary>
        /// Set the AutoAcceptCalls property
        /// When true, send trying, ringing automatically to the network.
        /// </summary>
        public bool AutoAcceptCalls { get; set; } = true;

        /// <summary>
        /// Set the AcceptDelay property
        /// Time to wait between ringing and ACK to let ringback tone to be heard in milliseconds
        /// </summary>
        public int AcceptDelay { get; set; } = 0;

        /// <summary>
        /// Set the AutoAnswerCalls property
        /// When true, automatically answer call.
        /// </summary>
        public bool AutoAnswerCalls { get; set; } = true;

        /// <summary>
        /// Set the AutoAcceptMessages property
        /// When true, messages are acked automatically.
        ///</summary>
        public bool AutoAcceptMessages { get; set; } = true;

        /// <summary>
        /// Set the AutoAcceptMessages property
        /// </summary>
        /// <param name="on">When true, auto accept is on</param>
        public bool EnableRtpTestMode { get; set; } = false;

        /// <summary>
        /// Returns the sip call matching a given call id. Null if no call matched.
        /// </summary>
        /// <param name="callID">The call-Id header value of a specific call</param>
        public SIPCall GetCall(string callID)
        {
            SIPCall call = null;
            if (Calls.TryGetValue(callID, out call))
                _logger.LogDebug($"Found call with id {callID}");
            return call;
        }

        /// <summary>
        /// Returns the sip message matching a given call id. Null if no call matched.
        /// </summary>
        /// <param name="callID">The call-Id header value of a specific call</param>
        public SIPMessage GetMessage(string callID)
        {
            SIPMessage msg = null;
            if (Messages.TryGetValue(callID, out msg))
                _logger.LogDebug($"Found message with id {callID}");
            return msg;
        }

        public Dictionary<string, SIPIdentity> GetIdentities()
        {
            Dictionary<string, SIPIdentity> identities = new Dictionary<string, SIPIdentity>();

            foreach (var identity in _registrations)
            {
                var inCall = false;
                try
                {
                    if (Calls.Values.First(x => x.GetIdentity().StartsWith(identity.Key)) != null)
                        inCall = true;
                }
                catch { }

                if (identity.Value == null)
                {
                    identities.Add(identity.Key, new SIPIdentity(identity.Key, false, inCall));
                }
                else
                {
                    identities.Add(identity.Key, new SIPIdentity(identity.Key, identity.Value.IsRegistered, inCall));
                }
            }

            return identities;
        }

        private static void OnStatsTimerEvent(object state)
        {
            _stats = new CallStats[60];
        }

        /// <summary>
        /// Enables the sip level log for a sip transport object.
        /// </summary>
        /// <param name="sipTransport">The sip transort object to log</param>
        /// <param name="fullSIP">When true, print out in the log the full sip message</param>
        private void EnableTraceLogs(SIPTransport sipTransport, bool fullSIP)
        {
            sipTransport.SIPRequestInTraceEvent += (localEP, remoteEP, req) =>
            {
                if (!fullSIP)
                {
                    _logger.LogDebug(req.StatusLine);
                }
                else
                {
                    _logger.LogDebug(req.ToString());
                }
            };

            sipTransport.SIPRequestOutTraceEvent += (localEP, remoteEP, req) =>
            {
                _logger.LogDebug($"Request sent: {localEP}->{remoteEP}");

                if (!fullSIP)
                {
                    _logger.LogDebug(req.StatusLine);
                }
                else
                {
                    _logger.LogDebug(req.ToString());
                }
            };

            sipTransport.SIPResponseInTraceEvent += (localEP, remoteEP, resp) =>
            {
                _logger.LogDebug($"Response received: {localEP}<-{remoteEP}");

                if (!fullSIP)
                {
                    _logger.LogDebug(resp.ShortDescription);
                }
                else
                {
                    _logger.LogDebug(resp.ToString());
                }
            };

            sipTransport.SIPResponseOutTraceEvent += (localEP, remoteEP, resp) =>
            {
                _logger.LogDebug($"Response sent: {localEP}->{remoteEP}");

                if (!fullSIP)
                {
                    _logger.LogDebug(resp.ShortDescription);
                }
                else
                {
                    _logger.LogDebug(resp.ToString());
                }
            };

            sipTransport.SIPRequestRetransmitTraceEvent += (tx, req, count) =>
            {
                _logger.LogDebug($"Request retransmit {count} for request {req.StatusLine}, initial transmit {DateTime.Now.Subtract(tx.InitialTransmit).TotalSeconds.ToString("0.###")}s ago.");
            };

            sipTransport.SIPResponseRetransmitTraceEvent += (tx, resp, count) =>
            {
                _logger.LogDebug($"Response retransmit {count} for response {resp.ShortDescription}, initial transmit {DateTime.Now.Subtract(tx.InitialTransmit).TotalSeconds.ToString("0.###")}s ago.");
            };
        }

        private void CreateSIPTransport()
        {
            _sipTransport = new SIPTransport();

            _logger.LogInformation($"New sip transport created.");

            foreach (String s in LocalIPEndPoints)
            {
                String[] ipEndPoint = s.Split(',');
                SIPProtocolsEnum protocol = (SIPProtocolsEnum)Enum.Parse(typeof(SIPProtocolsEnum), ipEndPoint[0]);
                int port = int.Parse(ipEndPoint[2]);
                switch (protocol)
                {
                    case SIPProtocolsEnum.udp:

                        if (ipEndPoint[1].Equals("0.0.0.0"))
                        {
                            _logger.LogDebug($"Adding udp channel on 0.0.0.0:{port}.");
                            _sipTransport.AddSIPChannel(new SIPUDPChannel(new IPEndPoint(IPAddress.Any, port)));
                        }
                        else
                        {
                            _logger.LogDebug($"Adding udp channel on {ipEndPoint[1]}:{port}.");
                            IPAddress ipAddress = IPAddress.Parse(ipEndPoint[1]);
                            _sipTransport.AddSIPChannel(new SIPUDPChannel(new IPEndPoint(ipAddress, port)));
                        }
                        break;
                    case SIPProtocolsEnum.tcp:
                        if (ipEndPoint[1].Equals("0.0.0.0"))
                        {
                            _logger.LogDebug($"Adding tcp channel on 0.0.0.0:{port}.");
                            _sipTransport.AddSIPChannel(new SIPTCPChannel(new IPEndPoint(IPAddress.Any, port)));
                        }
                        else
                        {
                            _logger.LogDebug($"Adding tcp channel on {ipEndPoint[1]}:{port}.");
                            IPAddress ipAddress = IPAddress.Parse(ipEndPoint[1]);
                            _sipTransport.AddSIPChannel(new SIPTCPChannel(new IPEndPoint(ipAddress, port)));
                        }
                        break;
                    case SIPProtocolsEnum.ws:
                        if (ipEndPoint[1].Equals("0.0.0.0"))
                        {
                            _logger.LogDebug($"Adding ws channel on 0.0.0.0:{port}.");
                            _sipTransport.AddSIPChannel(new SIPWebSocketChannel(IPAddress.Any, port));
                        }
                        else
                        {
                            _logger.LogDebug($"Adding ws channel on {ipEndPoint[1]}:{port}.");
                            IPAddress ipAddress = IPAddress.Parse(ipEndPoint[1]);
                            _sipTransport.AddSIPChannel(new SIPWebSocketChannel(ipAddress, port));
                        }
                        break;
                    case SIPProtocolsEnum.wss:
                        if (ipEndPoint[1].Equals("0.0.0.0"))
                        {
                            _logger.LogDebug($"Adding wss channel on 0.0.0.0:{port}.");
                            if (File.Exists("certs/localhost.pfx"))
                            {
                                X509Certificate2 cert = new X509Certificate2("certs/localhost.pfx");
                                _sipTransport.AddSIPChannel(new SIPWebSocketChannel(IPAddress.Any, port, cert));
                            } else
                            {
                                _logger.LogWarning("channel ignored because certificate is not present.");
                            }
                        }
                        else
                        {
                            _logger.LogDebug($"Adding wss channel on {ipEndPoint[1]}:{port}.");
                            IPAddress ipAddress = IPAddress.Parse(ipEndPoint[1]);
                            if (File.Exists("certs/localhost.pfx"))
                            {
                                X509Certificate2 cert = new X509Certificate2("certs/localhost.pfx");
                                _sipTransport.AddSIPChannel(new SIPWebSocketChannel(ipAddress, port, cert));
                            }
                            else
                            {
                                _logger.LogWarning("channel ignored because certificate is not present.");
                            }
                        }
                        break;
                    default:
                        _logger.LogError($"Only udp and tcp channels are currently implemented.");
                        throw new NotImplementedException();
                }
            }

            EnableTraceLogs(_sipTransport, true);

            _sipTransport.SIPTransportRequestReceived += OnRequest;
        }

        /// <summary>
        /// Creates a new call server.
        /// </summary>
        public SIPServer()
        {
            _logger = SIPSorcery.LogFactory.CreateLogger($"{this.GetType().FullName}");
            
            LoadContacts();

            State = SIPServerState.NotStarted;
        }

        public async Task Start()
        {
            if (State != SIPServerState.NotStarted)
            {
                OnSIPServerStartFailed?.Invoke("Already started or starting in progress.");
                return;
            }

            State = SIPServerState.Starting;

            try
            {
                CreateSIPTransport();
                if (_sipTransport == null)
                    OnSIPServerStartFailed?.Invoke("Could not start SIP transport.");
            }
            catch(Exception e)
            {
                OnSIPServerStartFailed?.Invoke(e.Message);
            }


            if (RtpAddress == null)
            {
                if (_sipTransport.GetSIPChannels()[0].ListeningIPAddress != IPAddress.Any)
                    // get the address from the first sip transport
                    RtpAddress = _sipTransport.GetSIPChannels()[0].ListeningIPAddress;
                else
                    // get the address from the first system interface
                    RtpAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(a => a.AddressFamily == AddressFamily.InterNetwork);
            }

            _ = Task.Run(async () =>
              {
                  await StartMonitorTrunks();
                  await StartRegistrations();
                  await StartSubscriptions();
                  await StartSmpp();

                  State = SIPServerState.Running;

                  _logger.LogWarning($"Call server is now running.");
                  OnSIPServerStartSuccess?.Invoke("Server is now running.");

              });
        }

        /// <summary>
        /// Triggered by the underlaying sip transport when a new sip request is received.
        /// </summary>
        /// <param name="localEndPoint">The local end point receiving the request.</param>
        /// <param name="remoteEndPoint">The remote end point information from where the request came in.</param>
        /// <param name="sipRequest">The actual sip request to process.</param>
        private async Task OnNewInviteRequest(SIPEndPoint localEndPoint, SIPEndPoint remoteEndPoint, SIPRequest sipRequest)
        {
            _logger.LogInformation($"Incoming call request: {localEndPoint}<-{remoteEndPoint} {sipRequest.URI}.");

            if (State != SIPServerState.Running)
            {
                SIPResponse busyResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.BusyHere, "Busy Here");
                await _sipTransport.SendResponseAsync(busyResponse);
                return;
            }

            var username = sipRequest.Header.To.ToURI.User;
            var password = "";
            var lookupURI = new SIPURI(sipRequest.Header.To.ToURI.User, remoteEndPoint.Address.ToString(), null, sipRequest.Header.To.ToURI.Scheme, sipRequest.Header.To.ToURI.Protocol);

            try
            {
                if (_registrations.Count > 0)
                {
                    if (_registrations.TryGetValue(lookupURI.ToString(), out var rua))
                    {
                        username = rua.GetUserName();
                        password = rua.GetPassword();
                    }
                    else
                    {
                        if (_registrations.Values.First() != null)
                        {
                            username = _registrations.Values.First().GetUserName();
                            password = _registrations.Values.First().GetPassword();
                        }
                    }
                }
            }
            catch { }

            SIPCall call;
            if (remoteEndPoint.Protocol == SIPProtocolsEnum.ws ||
                remoteEndPoint.Protocol == SIPProtocolsEnum.wss)
            {
                if (sipRequest.Body.Contains("rtpmap:101"))
                {
                    call = new SIPCall(_sipTransport, localEndPoint, remoteEndPoint, sipRequest, Codecs, SIPCallDtmfMode.Rfc2833, RtpAddress);
                    _logger.LogDebug($"Setting dtmf mode to {DtmfMode}, because offered by remote party.");
                }
                else
                {
                    call = new SIPCall(_sipTransport, localEndPoint, remoteEndPoint, sipRequest, Codecs, DtmfMode, RtpAddress);
                }
            }
            else
            {
                if (sipRequest.Body.Contains("rtpmap:101"))
                {
                    call = new SIPCall(_sipTransport, username, password, sipRequest, Codecs, SIPCallDtmfMode.Rfc2833, RtpAddress);
                    _logger.LogDebug($"Setting dtmf mode to {DtmfMode}, because offered by remote party.");
                }
                else
                {
                    call = new SIPCall(_sipTransport, username, password, sipRequest, Codecs, DtmfMode, RtpAddress);
                }
            }

            Calls.TryAdd(sipRequest.Header.CallId, call);
            call.UserAgent = UserAgent;
            call.UseMultiMediaTimers = UseMultiMediaTimers;
            call.OnCallHungup += (dialogue) => OnHangup(call);
            call.ServerCallCancelled += (uas) => OnCancel(uas, call);

            //call.OnRtpEvent += (evt, hdr) => _logger.LogDebug($"rtp event {evt.EventID}, duration {evt.Duration}, end of event {evt.EndOfEvent}, timestamp {hdr.Timestamp}, marker {hdr.MarkerBit}.");
            //call.OnTransactionTraceMessage += (tx, msg) => _logger.LogDebug($"uas tx {tx.TransactionId}: {msg}");
            call.ServerCallRingTimeout += (uas) => OnTimeout(uas, call);

            var t = (DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60);
            _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Offered++;

            if ((MaxCalls > 0) && (Calls.Count >= MaxCalls))
            {

                _logger.LogInformation($"Returning Busy because maximum calls is {MaxCalls}.");
                SIPResponse busyResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.BusyHere, "Busy Here");
                await _sipTransport.SendResponseAsync(busyResponse);

                return;
            }

            if (AutoAcceptCalls)
            {
                _logger.LogDebug($"AutoAccept is ON, accepting call.");

                SIPServerUserAgent uas = await call.Accept(remoteEndPoint.Protocol, sipRequest, AcceptDelay);

                _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Accepted++;

                if (AutoAnswerCalls)
                {
                    _logger.LogDebug($"AutoAnswer is ON, answering call.");
                    await call.Answer(uas);

                    _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Answered++;

                    if (call.IsCallActive)
                    {
                        _logger.LogDebug($"Raising OnCallConnected event.");
                        call.GetIdentity();

                        if (EnableRtpTestMode)
                        {
                            //rtp testing mode
                            _ = call.PlayFile(@"clips/rtpTestMode.wav", true, true);
                            call.OnPlayDone += () => call.PlayFile(@"clips/rtpTestMode.wav", true, true);
                            // end of rtp testing mode
                        }

                        OnCallAnswered?.Invoke(call);
                    }
                }
                else
                {
                    _logger.LogDebug($"AutoAnswer is OFF, raising OnCallAccepted event.");
                    OnCallAccepted?.Invoke(call);
                }

            }
            else
            {
                _logger.LogDebug($"AutoAccept is OFF, raising OnCallOffered event.");
                call.OnCallAccepted += () => this.OnCallAccepted?.Invoke(call);
                call.OnCallAnswered += () => this.OnCallAnswered?.Invoke(call);
                call.OnCallOffered += () => OnCallOffered?.Invoke(call);
                call.Offered();
            }
        }

        /// <summary>
        /// Triggered by the underlaying sip transport when a new sip request is received.
        /// </summary>
        /// <param name="localEndPoint">The local end point receiving the request.</param>
        /// <param name="remoteEndPoint">The remote end point information from where the request came in.</param>
        /// <param name="sipRequest">The actual sip request to process.</param>
        private async Task OnRequest(SIPEndPoint localEndPoint, SIPEndPoint remoteEndPoint, SIPRequest sipRequest)
        {
            using (LogContext.PushProperty("CallContext", sipRequest.Header.CallId))
            {
                try
                {
                    _logger.LogDebug($"OnRequest; handling new sip request; {localEndPoint}<-{remoteEndPoint} {sipRequest.Method}");

                    if (sipRequest.Header.From != null &&
                    sipRequest.Header.From.FromTag != null &&
                    sipRequest.Header.To != null &&
                    sipRequest.Header.To.ToTag != null)
                    {
                        using (LogContext.PushProperty("CallContext", sipRequest.Header.CallId))
                        {
                            _logger.LogDebug($"Request is a mid-call request");
                            // This is an in-dialog request that will be handled directly by a user agent instance.
                            if (sipRequest.Method == SIPMethodsEnum.INFO)
                            {
                                SIPCall call;
                                if (Calls.TryGetValue(sipRequest.Header.CallId, out call))
                                {
                                    call.MidCallRequest(sipRequest);
                                }
                                else
                                {
                                    _logger.LogWarning($"Could not find an active call with this id, mid-call request ignored.");
                                }

                                _logger.LogDebug($"Got INFO, sending 200 OK.");
                                SIPResponse infoResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.Ok, null);
                                await _sipTransport.SendResponseAsync(infoResponse);
                            }
                            else if (sipRequest.Method == SIPMethodsEnum.NOTIFY)
                            {
                                _logger.LogDebug($"Got NOTIFY for event {sipRequest.Header.Event}.");

                                if (sipRequest.Header.Event == "presence")
                                {
                                    try
                                    {
                                        var contact = SIPContact.UpdateState(sipRequest, Contacts);
                                        if (contact != null)
                                        {
                                            SIPResponse notifyResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.Ok, null);
                                            await _sipTransport.SendResponseAsync(notifyResponse);
                                            OnContactStateUpdated?.Invoke(sipRequest.Header.To.ToURI.ToString(), contact);
                                        }
                                        else
                                        {
                                            SIPResponse notifyResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.BadEvent, null);
                                            await _sipTransport.SendResponseAsync(notifyResponse);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        _logger.LogWarning(e.Message, e);
                                        SIPResponse notifyResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.BadEvent, null);
                                        await _sipTransport.SendResponseAsync(notifyResponse);
                                    }
                                }
                                else // ack any other mid-call notify event
                                {
                                    SIPResponse notifyResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.Ok, null);
                                    await _sipTransport.SendResponseAsync(notifyResponse);
                                }
                            }
                            //else
                            //{
                            //    _logger.LogInformation($"only INFO and NOTIFY mid-call request are implemented at this time.");
                            //}
                        }
                    }
                    else if (sipRequest.Method == SIPMethodsEnum.INVITE)
                    {
                        SIPCall call;
                        if (!Calls.TryGetValue(sipRequest.Header.CallId, out call))
                        {
                            await OnNewInviteRequest(localEndPoint, remoteEndPoint, sipRequest);
                        }
                        else
                        {
                            _logger.LogWarning($"Duplicate INVITE request will be ignored.");
                        }
                    }
                    else if (sipRequest.Method == SIPMethodsEnum.CANCEL)
                    {
                        SIPCall call;
                        if (Calls.TryGetValue(sipRequest.Header.CallId, out call))
                        {
                            call.Hangup();

                        }
                    }
                    else if (sipRequest.Method == SIPMethodsEnum.BYE)
                    {
                        SIPCall call;
                        if (Calls.TryGetValue(sipRequest.Header.CallId, out call))
                            OnHangup(call);

                        _logger.LogDebug($"Got BYE, sending 200 OK.");
                        SIPResponse byeResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.CallLegTransactionDoesNotExist, null);
                        await _sipTransport.SendResponseAsync(byeResponse);
                    }
                    else if (sipRequest.Method == SIPMethodsEnum.SUBSCRIBE)
                    {
                        _logger.LogDebug($"Got SUBSCRIBE, sending 405 MethodNotAllowed.");
                        SIPResponse notAllowededResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.MethodNotAllowed, null);
                        await _sipTransport.SendResponseAsync(notAllowededResponse);
                    }
                    else if (sipRequest.Method == SIPMethodsEnum.OPTIONS)
                    {
                        _logger.LogDebug($"Got {sipRequest.Method}, sending 200 OK.");
                        SIPResponse optionsResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.Ok, null);
                        await _sipTransport.SendResponseAsync(optionsResponse);
                    }
                    else if (sipRequest.Method == SIPMethodsEnum.REGISTER)
                    {
                        _logger.LogDebug($"Got REGISTER for event {sipRequest.Header.Event}.");

                        if (remoteEndPoint.Protocol == SIPProtocolsEnum.ws ||
                            remoteEndPoint.Protocol == SIPProtocolsEnum.wss)
                        {

                            SIPResponse notifyResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.Ok, null);
                            notifyResponse.Header.Contact = sipRequest.Header.Contact;
                            await _sipTransport.SendResponseAsync(notifyResponse);
                        }
                        else
                        {
                            SIPResponse notifyResponse = SIPResponse.GetResponse(sipRequest, SIPResponseStatusCodesEnum.BadEvent, null);
                            await _sipTransport.SendResponseAsync(notifyResponse);
                        }
                    }
                    else if (sipRequest.Method == SIPMethodsEnum.MESSAGE)
                    {
                        _logger.LogDebug($"Got {sipRequest.Method}, raising event.");

                        SIPMessage msg = new SIPMessage(_sipTransport, sipRequest);

                        Messages.TryAdd(msg.GetCallID(), msg);

                        if (AutoAcceptMessages)
                        {
                            await msg.Accept();
                        }

                        OnMessageReceived?.Invoke(msg, sipRequest.Body);
                    }
                }
                catch (Exception reqExcp)
                {
                    _logger.LogWarning($"Exception handling {sipRequest.Method}. {reqExcp.Message}");
                    _logger.LogDebug(reqExcp.ToString());
                }

                _logger.LogDebug($"OnRequest; event completed.");
            }
        }

        private void OnCancel(ISIPServerUserAgent uas, SIPCall call)
        {
            using (LogContext.PushProperty("CallContext", call.GetCallID()))
            {
                if (Calls.ContainsKey(call.GetCallID()))
                {
                    if (Calls.TryRemove(call.GetCallID(), out var callRemoved))
                    {
                        _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Cancelled++;
                        OnCallCancelled?.Invoke(call, "dropped before connect");

                        // hangup bridged calls if there are some
                        if (call.BridgeId != null)
                        {
                            if (Bridges.TryRemove(call.BridgeId, out var bridge))
                            {
                                foreach (SIPCall c in bridge.Calls)
                                {
                                    if (c != call && c.IsCallActive)
                                        c.Hangup();
                                }
                                bridge = null;
                            }
                        }

                        call.PlayStop();
                        call.Close();
                    }
                }

                _logger.LogDebug($"OnCancel; all done.");
            }
        }

        private void OnTimeout(ISIPServerUserAgent uas, SIPCall call)
        {
            using (LogContext.PushProperty("CallContext", call.GetCallID()))
            {
                _logger.LogWarning($"Incoming call timed out in {uas.ClientTransaction.TransactionState} state waiting for client ACK, terminating.");
                call.Hangup();
                _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Timeout++;
                OnCallTimeout?.Invoke(call, "timeout");
            }
        }

        /// <summary>
        /// Triggered by the underlaying sip transport when a call is being disconnected.
        /// </summary>
        /// <param name="dialogue">The BYE sip dialogue.</param>
        private void OnHangup(SIPCall call)
        {
            using (LogContext.PushProperty("CallContext", call.GetCallID()))
            {
                _logger.LogDebug($"OnHangup;");

                string callID = call.GetCallID();
                _logger.LogInformation("Call is now disconnected.");
                _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Hangup++;

                if (Calls.ContainsKey(callID))
                {
                    if (Calls.TryRemove(callID, out var callToRemove))
                    {
                        OnCallDisconnected?.Invoke(callID, call.GetIdentity(), "normal release");

                        // hangup bridged calls if there are some
                        if (call.BridgeId != null)
                        {
                            if (Bridges.TryRemove(call.BridgeId, out var bridge))
                            {
                                foreach (SIPCall c in bridge.Calls)
                                {
                                    if (c != call && c.IsCallActive)
                                        c.Hangup();
                                }
                                bridge = null;
                            }
                        }

                        call.PlayStop();
                        call.Close();
                    }
                }
            
                _logger.LogDebug($"OnHangup; all done.");
            }
        }

        /// <summary>
        /// Starts background tasks to monitor trunks
        /// </summary>
        private async Task StartMonitorTrunks()
        {
            if (MonitorTrunks.Count == 0)
                return;

            foreach (var entry in MonitorTrunks)
            {
                SIPURI from;
                if (_sipTransport.GetSIPChannels()[0].IsSecure)
                {
                    from = new SIPURI(SIPSchemesEnum.sips, _sipTransport.GetSIPChannels()[0].ListeningSIPEndPoint);
                }
                else
                {
                    from = new SIPURI(SIPSchemesEnum.sip, _sipTransport.GetSIPChannels()[0].ListeningSIPEndPoint);
                }
                var to = SIPURI.ParseSIPURI(entry.Split(',')[0]);
                var interval = int.Parse(entry.Split(',')[1]);
                var sipTrunkEndPoint = new SIPEndPoint(from);
                var sipTrunk = new SIPTrunk(_sipTransport, from, to, interval);
                sipTrunk.UserAgent = UserAgent;
                sipTrunk.OnTrunkDown += (uri, msg) => OnTrunkDown?.Invoke(uri, msg);
                sipTrunk.OnTrunkUp += (uri) => OnTrunkUp?.Invoke(uri);
                _trunks.TryAdd(to, sipTrunk);
                sipTrunk.Start();
                await Task.Delay(RegistrationInterval);
            }

            while (MonitorTrunks.Count != _trunks.Count)
                await Task.Delay(500);
        }

        /// <summary>
        /// Starts a registration agent for each of the supplied SIP accounts.
        /// </summary>
        /// <param name="registerAccounts">The list of SIP accounts to create a registration for.</param>
        private async Task StartRegistrations()
        {
            foreach (var sipAccount in RegisterAccounts)
            {
                String[] sipAccountCredentials = sipAccount.Split(',');
                String username = sipAccountCredentials[0];
                String authUsername = sipAccountCredentials[1];
                String password = sipAccountCredentials[2];
                String domain = sipAccountCredentials[3];
                int expiry = int.Parse(sipAccountCredentials[4]);

                if (expiry == 0)
                {
                    // list identity but don't register
                    var scheme = SIPSchemesEnum.sip;
                    if (_sipTransport.GetSIPChannels()[0].ListeningSIPEndPoint.Protocol == SIPProtocolsEnum.tls)
                        scheme = SIPSchemesEnum.sips;

                    SIPURI uri = new SIPURI(username, domain, null, scheme, _sipTransport.GetSIPChannels()[0].ListeningSIPEndPoint.Protocol);
                    _registrations.TryAdd($"{uri.ToString()}", null);
                    continue;
                }

                var regUserAgent = new SIPRegistrationUserAgent(_sipTransport, username, authUsername, password, domain, expiry);

                regUserAgent.UserAgent = UserAgent;

                // Event handlers for the different stages of the registration.
                regUserAgent.RegistrationFailed += (uri, err) =>
                {
                    _logger.LogError($"{uri.ToString()}: {err}");
                    OnRegistrationFailed?.Invoke(uri, err);
                };

                regUserAgent.RegistrationTemporaryFailure += (uri, msg) =>
                {
                    _logger.LogWarning($"{uri.ToString()}: {msg}");
                    OnRegistrationTemporaryFailure?.Invoke(uri, msg);
                };

                regUserAgent.RegistrationRemoved += (uri) =>
                {
                    _logger.LogInformation($"Registration for {uri.ToString()} is removed.");
                    SIPRegistrationUserAgent rua;
                    _registrations.TryRemove($"{uri.ToString()}", out rua);
                    OnRegistrationRemoved?.Invoke(uri);
                };

                regUserAgent.RegistrationSuccessful += (uri) =>
                {
                    _logger.LogInformation($"Registration for {uri.ToString()} succeeded.");
                    _registrations.TryAdd($"{uri.ToString()}", regUserAgent);
                    OnRegistrationSuccess?.Invoke(uri);
                };

                _logger.LogInformation($"Starting registration for {username}...");

                // Start the thread to perform the initial registration and then periodically resend it.
                regUserAgent.Start();
                
                await Task.Delay(RegistrationInterval);
            }

            while (RegisterAccounts.Count != _registrations.Count)
                await Task.Delay(500);
        }

        /// <summary>
        /// Stops a registration agent for each of the supplied SIP accounts.
        /// </summary>
        private async Task StopMonitorTrunks()
        {
            _logger.LogInformation("Stopping trunks monitors...");

            foreach (var sipTrunk in _trunks)
            {
                if (sipTrunk.Value != null)
                    sipTrunk.Value.Stop();
                    _trunks.TryRemove(sipTrunk.Key, out var val);
            }

            while (!_trunks.IsEmpty)
                await Task.Delay(500);
        }

        /// <summary>
        /// Stops a registration agent for each of the supplied SIP accounts.
        /// </summary>
        private async Task StopRegistrations()
        {
            _logger.LogInformation("Stopping registrations...");

            foreach (var regUserAgent in _registrations)
            {
                if (regUserAgent.Value != null)
                    regUserAgent.Value.Stop();
                else
                    _registrations.TryRemove(regUserAgent.Key, out var val);
            }

            while (!_registrations.IsEmpty)
                await Task.Delay(500);
        }

        /// <summary>
        /// Starts a registration agent for each of the supplied SIP accounts.
        /// </summary>
        /// <param name="registerAccounts">The list of SIP accounts to create a registration for.</param>
        private async Task StartSubscriptions()
        {
            foreach (var sipAccount in RegisterAccounts)
            {
                String[] sipAccountCredentials = sipAccount.Split(',');
                String username = sipAccountCredentials[0];
                String authUsername = sipAccountCredentials[1];
                String password = sipAccountCredentials[2];
                String domain = sipAccountCredentials[3];
                int expiry = int.Parse(sipAccountCredentials[4]);

                foreach (var contact in Contacts.Values)
                {
                    if (contact.Type == SIPContactType.Extension)
                    {
                        var to = new SIPURI(contact.UserName, domain, null);                        
                        var serverURI = SIPURI.ParseSIPURIRelaxed(domain);

                        var notifyUserAgent = new SIPNotifierClient<SIPEvent>(_sipTransport, serverURI, SIPEventPackage.Presence,
                                                                              to, authUsername, domain, password, expiry, null);

                        notifyUserAgent.UserAgent = UserAgent;

                        notifyUserAgent.NotificationReceived += (evt) =>
                        {
                            _logger.LogDebug($"Notification received: {evt}.");
                        };

                        notifyUserAgent.SubscriptionFailed += (uri, statusCode, reason) =>
                        {
                            _logger.LogDebug($"Subscription failed: {uri}, {statusCode}, {reason}.");
                        };


                        notifyUserAgent.SubscriptionSuccessful += (uri, expires) =>
                        {
                            _logger.LogDebug($"Subscription successful: {uri}, {expires}.");
                            if (expires == 0)
                                _subscriptions.TryRemove($"{uri.ToString()}", out var nofityUac);
                            else
                                _subscriptions.TryAdd($"{uri.ToString()}", notifyUserAgent);
                        };

                        _logger.LogInformation($"Starting subcriptions for {contact.UserName}...");
                        notifyUserAgent.Start();
                        await Task.Delay(RegistrationInterval);
                    }
                }

                // Subscribe only with the first identity
                break;
            }

            while (Contacts.Count != _subscriptions.Count)
                await Task.Delay(500);
        }

        /// <summary>
        /// Stops a registration agent for each of the supplied SIP accounts.
        /// </summary>
        private async Task StopSubscriptions()
        {
            _logger.LogInformation("Stopping subcriptions...");
            foreach (var notifyUserAgent in _subscriptions.Values)
            {
                notifyUserAgent.Stop();
            }

            while (!_subscriptions.IsEmpty)
                await Task.Delay(500);
        }

        /// <summary>
        /// Stops any ongoing call.
        /// </summary>
        private async Task StopCalls()
        {
            _logger.LogInformation("Stopping calls...");
            foreach (var call in Calls)
            {
                if (call.Value.IsCallActive)
                {
                    call.Value.Hangup();
                }
            }

            while (Calls.Count > 0)
                await Task.Delay(500);
        }

        /// <summary>
        /// Shutdown the call server and its underlaying sip transport.
        /// </summary>
        public async Task Shutdown()
        {
            try
            {
                if (_sipTransport != null)
                {

                    State = SIPServerState.Stopping;

                    await StopSmpp();
                    await StopMonitorTrunks();
                    await StopSubscriptions();
                    await StopRegistrations();
                    await StopCalls();

                    await Task.Delay(1000);

                    _logger.LogInformation("Shutting down sip transport...");
                    _sipTransport.Shutdown();

                    SaveContacts();

                    OnSIPServerStopSuccess?.Invoke("Service is down.");
                }

                State = SIPServerState.NotStarted;

                _logger.LogWarning("Call server is now down.");
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                OnSIPServerStopFailed?.Invoke($"Failed to stop server {e.Message}.");
            }
        }

        /// <summary>
        /// Initiates a new call based on a given identity.
        /// </summary>
        /// <param name="to">The sip uri representing the destination of the call.</param>
        /// <param name="identity">
        /// The sip uri representing the origination of the call. (a registerd account)
        /// When null, trying to make an anonymous call with no authentication.
        /// </param>
        /// <param name="cpaTimeout">If > 0, triggers call progress analysis procedure with the specified timeout in seconds</param>
        public async Task<SIPCall> MakeCall(string to, string identity, int cpaTimeout)
        {
            if (State == SIPServerState.Running)
                return await MakeCall(to, identity, null, cpaTimeout, false);

            return null;
        }

        /// <summary>
        /// Initiates a new call based on a given identity.
        /// </summary>
        /// <param name="to">The sip uri representing the destination of the call.</param>
        /// <param name="identity">
        /// The sip uri representing the origination of the call. (a registerd account)
        /// When null, trying to make an anonymous call with no authentication.
        /// </param>
        /// <param name="cpaTimeout">If > 0, triggers call progress analysis procedure with the specified timeout in seconds</param>
        public async Task<SIPCall> MakeCall(string to, string[] sipHeaders, string identity, int cpaTimeout)
        {
            if (State == SIPServerState.Running)
                return await MakeCall(to, identity, sipHeaders, cpaTimeout, false);

            return null;
        }

        /// <summary>
        /// Initiates a new call based on a given identity.
        /// </summary>
        /// <param name="to">The sip uri representing the destination of the call.</param>
        /// <param name="identity">
        /// The sip uri representing the origination of the call. (a registerd account)
        /// When null, trying to make an anonymous call with no authentication.
        /// </param>
        /// <param name="cpaTimeout">If > 0, triggers call progress analysis procedure with the specified timeout in seconds</param>
        /// <param name="waitForResult">When true, wait for call completion, when false return immediately</param>
        public async Task<SIPCall> MakeCall(string to, string identity, int cpaTimeout, bool waitForResult)
        {
            if (State == SIPServerState.Running)
                return await MakeCall(to, identity, null, cpaTimeout, waitForResult);

            return null;
        }

        /// <summary>
        /// Initiates a new call based on a given identity.
        /// </summary>
        /// <param name="to">The sip uri representing the destination of the call.</param>
        /// <param name="identity">
        /// The sip uri representing the origination of the call. (a registerd account)
        /// When null, trying to make an anonymous call with no authentication.
        /// </param>
        /// <param name="cpaTimeout">If > 0, triggers call progress analysis procedure with the specified timeout in seconds</param>
        /// <param name="waitForResult">When true, wait for call completion, when false return immediately</param>
        public async Task<SIPCall> MakeCall(string to, string identity, string[] sipHeaders, int cpaTimeout, bool waitForResult)
        {
            // First look for a registered identity
            string username = null;
            string password = null;
            SIPRegistrationUserAgent uar = null;

            if (identity != null)
            {
                username = SIPURI.ParseSIPURI(identity).User;
                if (_sipTransport.GetSIPChannels()[0].SIPProtocol != SIPProtocolsEnum.udp)
                {
                    identity = $"{identity};transport={_sipTransport.GetSIPChannels()[0].SIPProtocol}";
                }

                if (_registrations.TryGetValue(identity, out uar))
                {
                    if (uar != null)
                    {
                        if (uar.IsRegistered)
                        {
                            username = uar.GetUserName();
                            password = uar.GetPassword();
                        }
                        else
                        {
                            _logger.LogWarning($"Identity {identity}, is not registered.");
                        }
                    }
                    else
                    {
                        SIPURI from = SIPURI.ParseSIPURI(identity);
                        username = from.User;
                    }
                }
                else
                {
                    _logger.LogWarning($"Could not find identity {identity}, make sure a registered identity is used.");
                }
            }
            else
            {
                _logger.LogInformation($"Making call with no identity.");
            }

            // Place an outgoing call.
            var call = new SIPCall(_sipTransport, username, password, null, Codecs, DtmfMode, RtpAddress);

            call.UserAgent = UserAgent;

            call.UseMultiMediaTimers = UseMultiMediaTimers;

            call.OnCallHungup += (dialogue) => OnHangup(call);

            call.ClientCallTrying += (uac, resp) =>
            {
                Calls.TryAdd(call.GetCallID(), call);

                // detecting a call forwarding
                if (resp.StatusCode == 181)
                {
                    using (LogContext.PushProperty("CallContext", resp.Header.CallId))
                    {
                        _logger.LogInformation($"Call was diverted; {resp.Header.GetUnknownHeaderValue("Diversion")}");
                        _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Diverted++;
                        OnCallDiverted?.Invoke(call, resp.Header.GetUnknownHeaderValue("Diversion"));
                    }
                }
                else if (resp.StatusCode == 100)
                    _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Trying++;
            };

            call.ClientCallRinging += (uac, resp) =>
            {
                _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Ringing++;
                OnCallRinging?.Invoke(call);
            };

            call.ClientCallFailed += (uac, err, resp) =>
            {
                _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Failed++;
                OnCallFailed?.Invoke(call, err);

                Calls.TryRemove(call.GetCallID(), out call);
            };

            call.ClientCallAnswered += (uac, resp) =>
            {
                if (resp.Status == SIPResponseStatusCodesEnum.Ok)
                {
                    _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Connected++;
                    _logger.LogInformation($"{call.GetIdentity()} -> {uac.CallDescriptor.To} Answered: {resp.StatusCode} {resp.ReasonPhrase}.");
                    OnCallConnected?.Invoke(call);
                }
                else
                {
                    _logger.LogWarning($"{uac.CallDescriptor.To} Answered: {resp.StatusCode} {resp.ReasonPhrase}.");
                }
            };


            _stats[(DateTimeOffset.Now.ToUnixTimeMilliseconds() % 60)].Calling++;

            if (waitForResult)
            {
                var callResult = await call.MakeCall(to, username, password, cpaTimeout);

                if (callResult)
                {

                }
                else
                {
                    _logger.LogError($"Failed to make a new call from {identity} to {to}.");
                }
            }
            else
            {
                _ = call.MakeCall(to, username, password, sipHeaders, cpaTimeout);
            }
            

            return call;
        }

        /// <summary>
        /// Initiates a new call based on a given identity.
        /// </summary>
        /// <param name="to">The sip uri representing the destination of the call.</param>
        /// <param name="identity">
        /// The sip uri representing the origination of the call. (a registerd account)
        /// When null, trying to make an anonymous call with no authentication.
        /// </param>
        /// <param name="message">the text of the message to be sent</param>
        public string SendMessage(string to, string identity, string message)
        {
            // First look for a registered identity
            string username = null;
            string password = null;
            string domain = null;
            SIPRegistrationUserAgent uar = null;

            if (identity != null)
            {
                //if (identity.StartsWith("sip:"))
                //    identity = identity.Substring("sip:".Length);

                if (_registrations.TryGetValue(identity, out uar))
                {
                    if (uar.IsRegistered)
                    {
                        username = uar.GetUserName();
                        password = uar.GetPassword();
                        domain = SIPURI.ParseSIPURI(identity).Host;
                    }
                    else
                    {
                        _logger.LogWarning($"Identity {identity}, is not registered.");
                    }
                }
                else
                {
                    _logger.LogWarning($"Could not find identity {identity}, make sure a registered identity is used.");
                }
            }
            else
            {
                _logger.LogInformation($"Making call with no identity.");
            }

            // Send the message.
            var msg = new SIPMessage(_sipTransport, username, password, domain);

            Messages.TryAdd(msg.GetCallID(), msg);

            msg.GetUac().UserAgent = UserAgent;

            msg.GetUac().MessageFailed += (uri, reason) =>
            {
                OnMessageFailed?.Invoke(msg.GetUac().GetCallID(), reason);
                Messages.TryRemove(msg.GetCallID(), out var m);
            };

            msg.GetUac().MessageTimeout += (uri, reason) =>
            {
                OnMessageTimeout?.Invoke(msg.GetUac().GetCallID(), reason);
                Messages.TryRemove(msg.GetCallID(), out var m);
            };

            msg.GetUac().MessageSent += (uri, reason) =>
            {
                OnMessageSent?.Invoke(msg.GetUac().GetCallID(), reason);
            };

            msg.Send(to, message);

            return msg.GetUac().GetCallID();
        }

        public async Task AckMessage(SIPMessage msg)
        {
            Messages.TryRemove(msg.GetCallID(), out var m);
            await msg.Ack();
        }

        private void LoadContacts()
        {
            if (File.Exists(@"contacts.json"))
            {
                IEnumerable<string> contacts = File.ReadLines(@"contacts.json");
                _logger.LogInformation($"Loading contacts...");

                foreach (var item in contacts)
                {
                    SIPContact c = JsonSerializer.Deserialize<SIPContact>(item);
                    Contacts.TryAdd(c.Id, c);
                }

                _logger.LogInformation($"All contacts loaded.");
            } 
            else
            {
                _logger.LogInformation("No contacts to load.");
            }

            _ = Task.Run(async () =>
            {
                while (true)
                {
                    SaveContacts();
                    await Task.Delay(60 * 1000, _exitCts.Token);
                    if (_exitCts.IsCancellationRequested)
                        break;
                }
            });
        }

        private void SaveContacts()
        {
            if (File.Exists(@"contacts.bak"))
                File.Delete(@"contacts.bak");
            
            if (File.Exists(@"contacts.json"))
                File.Move(@"contacts.json", @"contacts.bak");
            
            foreach (var item in Contacts)
            {
                File.AppendAllText(@"contacts.json", JsonSerializer.Serialize(item.Value) + Environment.NewLine);
            }
        }

        private async Task StopSmpp()
        {
            if (_smppClient == null)
                return;

            _smppClient.Shutdown();

            while (_smppClient.ConnectionState != SmppConnectionState.Closed)
                await Task.Delay(250);
        }

        private async Task StartSmpp()
        {
            if (SmppConfig == null)
                return;

            _smppClient = new SmppClient();
            _smppClient.Name = SmppConfig.Name;

            _smppClient.ConnectionStateChanged += (s, e) => {
                _logger.LogInformation($"Smpp connection state changed from {e.PreviousState} to {e.CurrentState}.");
            };

            _smppClient.MessageSent += new EventHandler<MessageEventArgs>(TextMessageSent);
            _smppClient.MessageDelivered += new EventHandler<MessageEventArgs>(TextMessageDelivered);
            _smppClient.MessageReceived += new EventHandler<MessageEventArgs>(TextMessageReceived);
            
            SmppConnectionProperties properties = _smppClient.Properties;
            properties.SystemID = SmppConfig.SystemID;
            properties.Password = SmppConfig.Password;
            properties.Port = SmppConfig.Port;
            properties.Host = SmppConfig.Host;
            properties.SystemType = SmppConfig.SystemType;
            properties.DefaultServiceType = SmppConfig.DefaultServiceType;
            properties.DefaultEncoding = SmppConfig.Encoding;
            properties.UseSeparateConnections = SmppConfig.UseSeparateConnections;

            //Resume a lost connection after 30 seconds
            _smppClient.AutoReconnectDelay = SmppConfig.AutoReconnectDelay;

            //Send Enquire Link PDU every 15 seconds
            _smppClient.KeepAliveInterval = SmppConfig.KeepAliveInterval;

            _smppClient.Start();

            while (_smppClient.ConnectionState != SmppConnectionState.Connected)
                await Task.Delay(250);
        }

        private string GenerateUserMessageReference()
        {
            switch (SmppConfig.UserMessageReferenceType)
            {
                case UserMessageReferenceType.Guid:
                    return Guid.NewGuid().ToString();
                case UserMessageReferenceType.Int:
                    return new Random().Next(0, int.MaxValue).ToString();
                case UserMessageReferenceType.IntHex:
                    return new Random().Next(0, int.MaxValue).ToString("X");
                default:
                    return null;
            }
        }

        public string SendTextMessage(string to, string identity, string message)
        {
            if (_smppClient == null)
            {
                _logger.LogWarning("Cannot send text messages when SMPP is not configured.");
                return null;
            }

            // First look for a registered identity
            string username = null;
            string password = null;
            string domain = null;
            SIPRegistrationUserAgent uar = null;

            if (identity != null)
            {
                if (_registrations.TryGetValue(identity, out uar))
                {
                    if (uar.IsRegistered)
                    {
                        username = uar.GetUserName();
                        password = uar.GetPassword();
                        domain = SIPURI.ParseSIPURI(identity).Host;
                    }
                    else
                    {
                        _logger.LogWarning($"Identity {identity}, is not registered.");
                    }
                }
                else
                {
                    _logger.LogWarning($"Could not find identity {identity}, make sure a registered identity is used.");
                }
            }
            else
            {
                _logger.LogInformation($"Aborting because identity not found.");
                return null;
            }

            TextMessage sms = new TextMessage();
            sms.DestinationAddress = to;
            sms.SourceAddress = username;
            sms.Text = message;
            sms.RegisterDeliveryNotification = true;
            sms.UserMessageReference = GenerateUserMessageReference();
            _logger.LogInformation($"Sending text message to: {sms.DestinationAddress}");
            _logger.LogDebug($"From: {sms.SourceAddress}");
            _logger.LogDebug($"Text length: {sms.Text.Length}");
            _logger.LogDebug($"Id: {sms.UserMessageReference}");

            try
            {
                _smppClient.SendMessage(sms);
                return sms.UserMessageReference;
            }
            catch (SmppException smppEx)
            {
                _logger.LogError($"smppEx.ErrorCode: {(int)smppEx.ErrorCode} {smppEx.ErrorCode}.");
            }
            catch (Exception e)
            {
                _logger.LogError($"SendTextMessage: {e.Message}", e);
            }

            return null;
        }

        private void TextMessageReceived(object sender, MessageEventArgs e)
        {
            TextMessage sms = e.ShortMessage as TextMessage;

            _logger.LogInformation($"Text message received from: {sms.SourceAddress}");

            var identity = _registrations.FirstOrDefault(x => x.Value.GetUserName() == sms.DestinationAddress).Key;

            OnTextMessageReceived?.Invoke(identity, sms.UserMessageReference, sms.SourceAddress, sms.Text);

            TextMessageDataRecord tdr = new TextMessageDataRecord(SIPCallDirection.In, sms.DestinationAddress, sms.SourceAddress,
                sms.UserMessageReference, sms.Text.Length, null, null);

            tdr.Response((int)sms.MessageState, sms.MessageState.ToString(), new IPEndPoint(IPAddress.Any, SmppConfig.Port),
                new IPEndPoint(IPAddress.Parse(SmppConfig.Host), SmppConfig.Port));
        }

        private void TextMessageDelivered(object sender, MessageEventArgs e)
        {
            TextMessage sms = e.ShortMessage as TextMessage;

            _logger.LogInformation($"Text message delivered to: {sms.DestinationAddress}");

            var identity = _registrations.FirstOrDefault(x => x.Value.GetUserName() == sms.SourceAddress).Key;

            OnTextMessageDelivered?.Invoke(identity, sms.UserMessageReference);

            TextMessageDataRecord tdr = new TextMessageDataRecord(SIPCallDirection.Out, sms.DestinationAddress, sms.SourceAddress,
                sms.UserMessageReference, sms.Text.Length, null, null);

            tdr.Response((int)sms.MessageState, sms.MessageState.ToString(), new IPEndPoint(IPAddress.Any, SmppConfig.Port),
                new IPEndPoint(IPAddress.Parse(SmppConfig.Host), SmppConfig.Port));
        }

        private void TextMessageSent(object sender, MessageEventArgs e)
        {
            TextMessage sms = e.ShortMessage as TextMessage;

            _logger.LogInformation($"Text message sent to: {sms.DestinationAddress}");

            var identity = _registrations.FirstOrDefault(x => x.Value.GetUserName() == sms.SourceAddress).Key;

            OnTextMessageSent?.Invoke(identity, sms.UserMessageReference);
        }

        public SIPBridge CreateBridge(SIPCall aLeg, SIPCall bLeg)
        {
            return CreateBridge(aLeg, bLeg, false);
        }
        public SIPBridge CreateBridge(SIPCall aLeg, SIPCall bLeg, bool relayDtmf)
        {
            SIPBridge sipBridge = new SIPBridge(aLeg, bLeg, relayDtmf);
            Bridges.TryAdd(sipBridge.Id, sipBridge);
            OnBridgeCreated?.Invoke(sipBridge);
            return sipBridge;
        }

        public void DeleteBridge(string bridgeId)
        {
            Bridges.TryRemove(bridgeId, out var sipBridge);
            if (sipBridge != null)
            {
                sipBridge.Terminate();
            }
        }

        public bool Shutdown(bool force)
        {
            if (State == SIPServerState.Running)
            {
                if (force)
                {
                    State = SIPServerState.Shutdown;
                     _ = Shutdown();
                } else
                {
                    State = SIPServerState.GracefulShutdown;
                    Task.Run(() => { 
                        while (Calls.Count > 0)
                        {
                            Task.Delay(500);
                        }
                        _ = Shutdown();
                    });
                }

                return true;
            } 
            else
            {
                return false;
            }
        }
    }
}
