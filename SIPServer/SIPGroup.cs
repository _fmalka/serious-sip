﻿using System;
using System.Collections.Concurrent;
using System.IO;

namespace SeriousSIP
{
    public class SIPGroup
    {
        public SIPGroup() { }
        public SIPGroup(string name)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public ConcurrentDictionary<string, SIPContact> Contacts { get; set; } = new ConcurrentDictionary<string, SIPContact>();
        public ConcurrentDictionary<string, Object> Tags { get; set; } = new ConcurrentDictionary<string, Object>();
       
    }
}
