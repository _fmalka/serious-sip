﻿namespace SeriousSIP
{
    public enum SIPServerState
    {
        NotStarted,
        Starting,
        Stopping,
        Running,
        GracefulShutdown,
        Shutdown,
        GracefulRestart,
        Restart
    }
}
