﻿namespace SeriousSIP
{
    public enum SIPCallState
    {
        Dialing,
        Offered,
        Accepted,
        Ringing,
        Calling,
        Active,
        LocalHold,
        RemoteHold,
        HangingUp,
        Unknown
    }
}
