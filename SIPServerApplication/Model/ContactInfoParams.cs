/* 
 * keypad
 *
 * A suite of communication APIs for web and enterprise applications
 *
 * OpenAPI spec version: v1
 * Contact: fmalka@modulo.co.il
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = SeriousSIP.Client.SwaggerDateConverter;
using SIPSorcery.SIP;
using System.Collections.Concurrent;

namespace SeriousSIP.Model
{
    [DataContract]
    public partial class ContactInfoParams :  IEquatable<ContactInfoParams>, IValidatableObject
    {
        public ContactInfoParams(string id = default(string), string display = default(string), string username = default(string),
            string firstname = default(string), string lastname = default(string),
            SIPContactType type = SIPContactType.Extension, SIPContactPresenceState presenceState = SIPContactPresenceState.Offline,
            SIPContactPhoneState phoneState = SIPContactPhoneState.Available, string tel = default(string),
            string email = default(string), ConcurrentDictionary<string, Object> tags = null)
        {
            this.Id = id;
            this.Display = display;
            this.Username = username;
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Type = type;
            this.PresenceState = presenceState;
            this.PhoneState = phoneState;
            this.Tel = tel;
            this.Email = email;
            this.Tags = tags;
        }
        
        /// <summary>
        /// Gets or Sets OnCallPlayDone
        /// </summary>
        [DataMember(Name="id", EmitDefaultValue=false)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets OnCallPlayFilesDone
        /// </summary>
        [DataMember(Name="display", EmitDefaultValue=false)]
        public string Display { get; set; }

        /// <summary>
        /// Gets or Sets OnCallPlayRecordDone
        /// </summary>
        [DataMember(Name="username", EmitDefaultValue=false)]
        public string Username { get; set; }

        /// <summary>
        /// Gets or Sets OnCallRecordDone
        /// </summary>
        [DataMember(Name="firtname", EmitDefaultValue=false)]
        public string Firstname { get; set; }

        /// <summary>
        /// Gets or Sets OnCallGetDigitsDone
        /// </summary>
        [DataMember(Name="lastname", EmitDefaultValue=false)]
        public string Lastname { get; set; }

        /// <summary>
        /// Gets or Sets OnCallSendDigitsDone
        /// </summary>
        [DataMember(Name="type", EmitDefaultValue=false)]
        public SIPContactType Type { get; set; }

        /// <summary>
        /// Gets or Sets OnCallSpeakDone
        /// </summary>
        [DataMember(Name="presenceState", EmitDefaultValue=false)]
        public SIPContactPresenceState PresenceState { get; set; }

        /// <summary>
        /// Gets or Sets OnCallSpeakDone
        /// </summary>
        [DataMember(Name = "phoneState", EmitDefaultValue = false)]
        public SIPContactPhoneState PhoneState { get; set; }

        /// <summary>
        /// Gets or Sets OnCallPlayRecordDone
        /// </summary>
        [DataMember(Name = "tel", EmitDefaultValue = false)]
        public string Tel { get; set; }

        /// <summary>
        /// Gets or Sets OnCallPlayRecordDone
        /// </summary>
        [DataMember(Name = "email", EmitDefaultValue = false)]
        public string Email { get; set; }


        [DataMember(Name = "tags", EmitDefaultValue = false)]
        public ConcurrentDictionary<string, Object> Tags { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ContactInfoParams {\n");
            sb.Append("  id: ").Append(Id).Append("\n");
            sb.Append("  display: ").Append(Display).Append("\n");
            sb.Append("  username: ").Append(Username).Append("\n");
            sb.Append("  firstname: ").Append(Firstname).Append("\n");
            sb.Append("  lastname: ").Append(Lastname).Append("\n");
            sb.Append("  type: ").Append(Type).Append("\n");
            sb.Append("  presenceState: ").Append(PresenceState).Append("\n");
            sb.Append("  phoneState: ").Append(PhoneState).Append("\n");
            sb.Append("  tel: ").Append(Tel).Append("\n");
            sb.Append("  email: ").Append(Email).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as ContactInfoParams);
        }

        /// <summary>
        /// Returns true if CallMediaSubscriptions instances are equal
        /// </summary>
        /// <param name="input">Instance of CallMediaSubscriptions to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ContactInfoParams input)
        {
            if (input == null)
                return false;

            return true;
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
               
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
