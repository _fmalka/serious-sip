﻿using Microsoft.AspNetCore.Mvc;
using SeriousSIP.Model;
using SeriousSIP.Server;

namespace SeriousSIP.Webhooks
{
    public class Bridges : Controller
    {
        [Route("api/v1/bridges/bridge/play/done")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnBridgePlayDone([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnBridgePlayDoneEvent(id);

            return Ok(new ResponseParams("success", new { bridgeId = id },
                $"Ack."));
        }

        [Route("api/v1/bridges/bridge/record/done")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnBridgeRecordDone([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnBridgeRecordDoneEvent(id);

            return Ok(new ResponseParams("success", new { bridgeId = id },
                $"Ack."));
        }
    }
}
