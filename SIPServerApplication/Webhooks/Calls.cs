﻿using Microsoft.AspNetCore.Mvc;
using SeriousSIP.Model;
using SeriousSIP.Server;

namespace SeriousSIP.Webhooks
{
    public class Calls : Controller
    {
        /// <summary>
        /// Webhook for OnCallDisconnected
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("api/v1/calls/call/disconnected")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallDisconnected([FromQuery] string id, [FromQuery] string reason)
        {
            SIPServerApplication.Instance.OnCallDisconnectedEvent(id, reason);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        /// <summary>
        /// Webhook for OnCallConnected
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("api/v1/calls/call/connected")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallConnected([FromQuery] string id, [FromBody] CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallConnectedEvent(id, callInfoParams);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        /// <summary>
        /// Webhook for OnCallPlayDone
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("api/v1/calls/call/play/done")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallPlayDone([FromQuery] string id, [FromBody] CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallPlayDoneEvent(id, callInfoParams);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/play/error")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallPlayError([FromQuery] string id, [FromQuery] string reason, [FromBody] CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallPlayErrorEvent(id, reason, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/record/done")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallRecordDone([FromQuery] string id, [FromBody] CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallRecordDoneEvent(id, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/record/error")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallRecordError([FromQuery] string id, [FromQuery] string reason, [FromBody] CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallRecordErrorEvent(id, reason, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/speak/done")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallSpeakDone([FromQuery] string id, [FromBody] CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallSpeakDoneEvent(id, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/playfiles/done")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallPlayFilesDone([FromQuery] string id, [FromBody] CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallPlayFilesDoneEvent(id, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/playrecord/done")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallPlayRecordDone([FromQuery] string id, [FromBody] CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallPlayRecordDoneEvent(id, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/dtmf/send/done")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallSendDtmfDone([FromQuery] string id, [FromBody] CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallSendDtmfDoneEvent(id, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/positiveanswermachinedetection")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallPositiveAnswerMachineDetection([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnCallPositiveAnswerMachineDetectionEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/positivefaxdetection")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallPositiveFaxDetection([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnCallPositiveFaxDetectionEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/positivevoicedetection")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallPositiveVoiceDetection([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnCallPositiveVoiceDetectionEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/ringbacktonedetection")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallRingbackToneDetection([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnCallRingbackToneDetectionEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/dtmf/get/done")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallGetDtmfDone([FromQuery] string id, [FromQuery] string digits, [FromBody] CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallGetDtmfDoneEvent(id, digits, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/failed")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallFailed([FromQuery] string id, [FromQuery] string reason)
        {
            SIPServerApplication.Instance.OnCallFailedEvent(id, reason);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/diverted")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallDiverted([FromQuery] string id, [FromQuery] string reason)
        {
            SIPServerApplication.Instance.OnCallDivertedEvent(id, reason);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/calls/call/ringing")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallRinging([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnCallRingingEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        /// <summary>
        /// Webhook for OnCallAnswered
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("api/v1/calls/call/answered")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallAnswered([FromQuery] string id, [FromBody] CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallAnsweredEvent(id, callInfoParams);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        /// <summary>
        /// Webhook for OnCallAccepted
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("api/v1/calls/call/accepted")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallAccepted([FromQuery] string id, [FromBody] CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallAcceptedEvent(id, callInfoParams);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        /// <summary>
        /// Webhook for OnCallAccepted
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("api/v1/calls/call/offered")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnCallOffered([FromQuery] string id, [FromBody] CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallOfferedEvent(id, callInfoParams);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }
    }
}
