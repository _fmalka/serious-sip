﻿using Microsoft.AspNetCore.Mvc;
using SeriousSIP.Model;
using SeriousSIP.Server;

namespace SeriousSIP.Webhooks
{
    public class Messages : Controller
    {
        [Route("api/v1/messages/message/text/delivered")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnTextMessageDelivered([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnTextMessageDeliveredEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/messages/message/text/sent")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnTextMessageSent([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnTextMessageSentEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/messages/message/text/received")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnTextMessageReceived([FromQuery] string id, [FromBody] MessageInfoParams p)
        {
            SIPServerApplication.Instance.OnTextMessageReceivedEvent(id, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/messages/message/received")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnMessageReceived([FromQuery] string id, [FromBody] MessageInfoParams p)
        {
            SIPServerApplication.Instance.OnMessageReceivedEvent(id, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/messages/message/sent")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnMessageSent([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnMessageSentEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/messages/message/failed")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnMessageFailed([FromQuery] string id, [FromQuery] string reason)
        {
            SIPServerApplication.Instance.OnMessageFailedEvent(id, reason);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }

        [Route("api/v1/messages/message/timeout")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnMessageTimeout([FromQuery] string id)
        {
            SIPServerApplication.Instance.OnMessageTimeoutEvent(id);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }
    }
}
