﻿using Microsoft.AspNetCore.Mvc;
using SeriousSIP.Model;
using SeriousSIP.Server;

namespace SeriousSIP.Webhooks
{
    public class Contacts : Controller
    {
        [Route("api/v1/contacts/contact/state")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnContactStateUpdated([FromQuery] string id, [FromBody] ContactInfoParams p)
        {
            SIPServerApplication.Instance.OnContactStateUpdatedEvent(id, p);

            return Ok(new ResponseParams("success", new { callId = id },
                $"Ack."));
        }
    }
}
