﻿using Microsoft.AspNetCore.Mvc;
using SeriousSIP.Model;
using SeriousSIP.Server;

namespace SeriousSIP.Webhooks
{
    public class Admin : Controller
    {
        private string _apiVersion = SIPServerApplication.ApiVersion;
        private string _apiPrefix = SIPServerApplication.ApiPrefix;
        private string _apiName = SIPServerApplication.ApiName;

        [Route("api/v1/admin/system/trunk/up")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnTrunkUp([FromQuery] string uri)
        {
            SIPServerApplication.Instance.OnTrunkUpEvent(uri);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/system/trunk/down")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnTrunkDown([FromQuery] string uri)
        {
            SIPServerApplication.Instance.OnTrunkDownEvent(uri);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/system/stop/success")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnSystemStoppedSuccess([FromQuery] string uri)
        {
            SIPServerApplication.Instance.OnSystemStoppedSuccessEvent();

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/system/stop/failed")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnSystemStoppedFailed([FromQuery] string message)
        {
            SIPServerApplication.Instance.OnSystemStoppedFailedEvent(message);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/system/start/success")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnSystemStartedSuccess([FromQuery] string uri)
        {
            SIPServerApplication.Instance.OnSystemStartedSuccessEvent();

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/system/start/failed")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnSystemStartedFailed([FromQuery] string message)
        {
            SIPServerApplication.Instance.OnSystemStartedFailedEvent(message);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/identity/registration/success")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnRegistrationSuccess([FromQuery] string uri)
        {
            SIPServerApplication.Instance.OnRegistrationSuccessEvent(uri);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/identity/registration/temporaryfailure")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnRegistrationTemporaryFailureEvent([FromQuery] string uri, [FromQuery] string reason)
        {
            SIPServerApplication.Instance.OnRegistrationTemporaryFailureEvent(uri, reason);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/identity/registration/failed")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnRegistrationFailedEvent([FromQuery] string uri, [FromQuery] string reason)
        {
            SIPServerApplication.Instance.OnRegistrationFailedEvent(uri, reason);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/identity/registration/remove")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnRegistrationRemovedEvent([FromQuery] string uri)
        {
            SIPServerApplication.Instance.OnRegistrationRemovedEvent(uri);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/system/application/start")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnApplicationStart([FromQuery] string instance)
        {
            SIPServerApplication.Instance.OnApplicationStartEvent(instance);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }

        [Route("api/v1/admin/system/application/stop")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult OnApplicationStop([FromQuery] string instance)
        {
            SIPServerApplication.Instance.OnApplicationStopEvent(instance);

            return Ok(new ResponseParams("success", null,
                $"Ack."));
        }
    }
}
