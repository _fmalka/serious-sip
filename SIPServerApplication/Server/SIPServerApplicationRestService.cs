﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Routing;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using SeriousSIP.Webhooks;
using System.Threading;

namespace SeriousSIP.Server
{
    public class SIPServerApplicationRestService 
    {
        private static string _useUrls = "http://localhost:6060";
        private CancellationTokenSource cts = new CancellationTokenSource();

        public SIPServerApplicationRestService(string useUrls)
        {
            _useUrls = useUrls;
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls(_useUrls);
                });
            
        public void Start()
        {
            CreateHostBuilder(null).Build().RunAsync(cts.Token);
        }
        public void Stop()
        {
            cts.Cancel();
        }
    }

    /// <summary>
    /// Web host configuration builder.
    /// </summary>
    public class Startup
    {
        private string _apiVersion = SIPServerApplication.ApiVersion;
        private string _apiPrefix = SIPServerApplication.ApiPrefix;
        private string _apiName = SIPServerApplication.ApiName;

        /// <summary>
        /// Startup web server
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Keeps the configuration of the web server
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="s">Collection of services.</param>
        public void ConfigureServices(IServiceCollection s)
        {
            s.AddMvc(options => options.EnableEndpointRouting = false);
            s.AddMvcCore().AddApiExplorer();
            s.AddControllers()
            .AddJsonOptions(x =>
            {
                x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseHttpsRedirection();
            app.UseRouting();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/trunk/up",
                    "{controller=Admin}/{action=OnTrunkUp}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}//admin/system/trunk/down",
                    "{controller=Admin}/{action=OnTrunkDown}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/stop/success",
                    "{controller=Admin}/{action=OnSystemStoppedSuccess}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/stop/failed",
                    "{controller=Admin}/{action=OnSystemStoppedFailed}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/start/success",
                    "{controller=Admin}/{action=OnSystemStartedSuccess}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/start/failed",
                    "{controller=Admin}/{action=OnSystemStartedFailed}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/identity/registration/success",
                    "{controller=Admin}/{action=OnIdentityRegistrationSuccess}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/identity/registration/temporaryfailure",
                    "{controller=Admin}/{action=OnIdentityRegistrationTemporaryFailure}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/identity/registration/failed",
                    "{controller=Admin}/{action=OnIdentityRegistrationFailed}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/identity/registration/remove",
                    "{controller=Admin}/{action=OnIdentityRegistrationRemoved}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/application/start",
                    "{controller=Admin}/{action=OnApplicationStart}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/application/stop",
                    "{controller=Admin}/{action=OnApplicationStop}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/bridges/bridge/record/done",
                    "{controller=Bridges}/{action=OnBridgeRecordDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/bridges/bridge/play/done",
                    "{controller=Bridges}/{action=OnBridgePlayDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/disconnected",
                    "{controller=Calls}/{action=OnCallDisconnected}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/connected",
                    "{controller=Calls}/{action=OnCallConnected}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/failed",
                    "{controller=Calls}/{action=OnCallFailed}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/diverted",
                    "{controller=Calls}/{action=OnCallDiverted}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/ringing",
                    "{controller=Calls}/{action=OnCallRinging}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/offered",
                    "{controller=Calls}/{action=OnCallOffered}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/accepted",
                    "{controller=Calls}/{action=OnCallAccepted}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/answered",
                    "{controller=Calls}/{action=OnCallAnswered}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/play/done",
                    "{controller=Calls}/{action=OnCallPlayDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/play/error",
                    "{controller=Calls}/{action=OnCallPlayError}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/record/done",
                    "{controller=Calls}/{action=OnCallRecordDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/record/error",
                    "{controller=Calls}/{action=OnCallRecordError}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/speak/done",
                    "{controller=Calls}/{action=OnCallSpeakDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/playfiles/done",
                    "{controller=Calls}/{action=OnCallPlayFilesDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/playrecord/done",
                    "{controller=Calls}/{action=OnCallPlayRecordDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/dtmf/send/done",
                    "{controller=Calls}/{action=OnCallSendDtmfDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/dtmf/get/done",
                    "{controller=Calls}/{action=OnCallGetDtmfDone}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/ringbacktonedetection",
                    "{controller=Calls}/{action=OnCallRingbackToneDetection}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/positivefaxdetection",
                    "{controller=Calls}/{action=OnCallPositiveFaxDetection}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/positivevoicedetection",
                    "{controller=Calls}/{action=OnCallPositiveVoiceDetection}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/positiveanswermachinedetection",
                    "{controller=Calls}/{action=OnCallPositiveAnswerMachineDetection}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/messages/message/text/delivered",
                    "{controller=Messages}/{action=OnTextMessageDelivered}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/messages/message/text/sent",
                    "{controller=Messages}/{action=OnTextMessageSent}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/messages/message/text/received",
                    "{controller=Messages}/{action=OnTextMessageReceived}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/messages/message/received",
                    "{controller=Contacts}/{action=OnMessageReceived}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/messages/message/sent",
                    "{controller=Contacts}/{action=OnMessageSent}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/messages/message/falied",
                    "{controller=Contacts}/{action=OnMessageFailed}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/messages/message/timeout",
                    "{controller=Contacts}/{action=OnMessageTimeout}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/contacts/contact/state",
                    "{controller=Contacts}/{action=OnContactStateUpdated}");
            });
        }
    }
}

