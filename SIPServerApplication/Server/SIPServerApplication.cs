﻿using SeriousSIP.Api;
using SeriousSIP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeriousSIP.Server
{
    public class SIPServerApplication
    {
        /// <summary>
        /// A delegate for generic webhooks events triggering.
        /// </summary>
        public delegate Task SIPServerWebhookAsyncDelegate(string callId);
        public delegate Task SIPServerWebhookAppStartAsyncDelegate(string instance);
        public delegate Task SIPServerWebhook2AsyncDelegate(string callId, string reason);
        public delegate Task SIPServerWebhookNoParamsAsyncDelegate();

        /// <summary>
        /// A delegate for new call webhooks events triggering.
        /// </summary>
        public delegate Task SIPServerNewCallWebhookdAsyncDelegate(string callId, CallInfoParams callInfoParams);
        public delegate Task SIPServerNewMessageWebhookdAsyncDelegate(string callId, MessageInfoParams messageInfoParams);
        public delegate Task SIPServerContacteWebhookdAsyncDelegate(string callId, ContactInfoParams messageInfoParams);
        public delegate Task SIPServerNewCallWebhookd2AsyncDelegate(string callId, string reason, CallInfoParams callInfoParams);

        public event SIPServerWebhookAsyncDelegate OnTrunkUp;
        public event SIPServerWebhookAsyncDelegate OnTrunkDown;
        public event SIPServerWebhookAsyncDelegate OnSystemStoppedFailed;
        public event SIPServerWebhookNoParamsAsyncDelegate OnSystemStoppedSuccess;
        public event SIPServerWebhookAsyncDelegate OnSystemStartedFailed;
        public event SIPServerWebhookNoParamsAsyncDelegate OnSystemStartedSuccess;
        public event SIPServerWebhookAsyncDelegate OnRegistrationSuccess;
        public event SIPServerWebhook2AsyncDelegate OnRegistrationTemporaryFailure;
        public event SIPServerWebhook2AsyncDelegate OnRegistrationFailed;
        public event SIPServerWebhookAsyncDelegate OnRegistrationRemoved;
        public event SIPServerWebhookAppStartAsyncDelegate OnAplicationStart;
        public event SIPServerWebhookAppStartAsyncDelegate OnApplicationStop;

        public event SIPServerWebhookAsyncDelegate OnBridgePlayDone;
        public event SIPServerWebhookAsyncDelegate OnBridgeRecordDone;

        public event SIPServerWebhookAsyncDelegate OnTextMessageDelivered;
        public event SIPServerWebhookAsyncDelegate OnTextMessageSent;
        public event SIPServerNewMessageWebhookdAsyncDelegate OnTextMessageReceived;
        public event SIPServerNewMessageWebhookdAsyncDelegate OnMessageReceived;
        public event SIPServerWebhookAsyncDelegate OnMessageSent;
        public event SIPServerWebhook2AsyncDelegate OnMessageFailed;
        public event SIPServerWebhookAsyncDelegate OnMessageTimeout;

        public event SIPServerContacteWebhookdAsyncDelegate OnContactStateUpdated;

        public event SIPServerNewCallWebhookdAsyncDelegate OnCallPlayDone;
        public event SIPServerNewCallWebhookd2AsyncDelegate OnCallPlayError;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallRecordDone;
        public event SIPServerNewCallWebhookd2AsyncDelegate OnCallRecordError;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallSpeakDone;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallPlayFilesDone;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallPlayRecordDone;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallSendDtmfDone;
        public event SIPServerNewCallWebhookd2AsyncDelegate OnCallGetDtmfDone;
        public event SIPServerWebhookAsyncDelegate OnCallPositiveAnswerMachineDetection;
        public event SIPServerWebhookAsyncDelegate OnCallPositiveFaxDetection;
        public event SIPServerWebhookAsyncDelegate OnCallPositiveVoiceDetection;
        public event SIPServerWebhookAsyncDelegate OnCallPositiveRingbackToneDetection;
        public event SIPServerWebhook2AsyncDelegate OnCallFailed;
        public event SIPServerWebhook2AsyncDelegate OnCallDiverted;
        public event SIPServerWebhookAsyncDelegate OnCallRinging;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallAccepted;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallOffered;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallAnswered;
        public event SIPServerNewCallWebhookdAsyncDelegate OnCallConnected;
        public event SIPServerWebhook2AsyncDelegate OnCallDisconnected;
        

        public static SIPServerApplication Instance { get; private set; }

        private SIPServerApplicationRestService _restService;

        private string _apiUrl = "https://localhost:6050/";
        private string _webhookUrl = "https://localhost:6060/";

        public AdminApi Admin { get; private set; }
        public BridgesApi Bridges { get; private set; }
        public CallsApi Calls { get; private set; }
        public ContactsApi Contacts { get; private set; }
        public MessagesApi Messages { get; private set; }

        public static string ApiVersion = "v1";
        public static string ApiPrefix = $"/api/";
        public static string ApiName = "keypad";

        public SIPServerApplication()
        {
            Instance = this;
        }

        public SIPServerApplication(string apiUrl, string webhookUrl)
        {
            _apiUrl = apiUrl;
            _webhookUrl = webhookUrl;
            Instance = this;
        }

        public void Start()
        {
            _restService = new SIPServerApplicationRestService(_webhookUrl);
            _restService.Start();

            Admin = new AdminApi(_apiUrl);
            Bridges = new BridgesApi(_apiUrl);
            Calls = new CallsApi(_apiUrl);
            Contacts = new ContactsApi(_apiUrl);
            Messages = new MessagesApi(_apiUrl);
        }

        internal void OnTrunkUpEvent(string uri)
        {
            this.OnTrunkUp?.Invoke(uri);
        }

        internal void OnTrunkDownEvent(string uri)
        {
            this.OnTrunkDown?.Invoke(uri);
        }

        internal void OnSystemStartedSuccessEvent()
        {
            this.OnSystemStartedSuccess?.Invoke();
        }

        internal void OnSystemStartedFailedEvent(string message)
        {
            this.OnSystemStartedFailed?.Invoke(message);
        }

        internal void OnSystemStoppedSuccessEvent()
        {
            this.OnSystemStoppedSuccess?.Invoke();
        }

        internal void OnSystemStoppedFailedEvent(string message)
        {
            this.OnSystemStoppedFailed?.Invoke(message);
        }

        internal void OnRegistrationSuccessEvent(string uri)
        {
            this.OnRegistrationSuccess?.Invoke(uri);
        }

        internal void OnRegistrationTemporaryFailureEvent(string uri, string reason)
        {
            this.OnRegistrationTemporaryFailure?.Invoke(uri, reason);
        }

        internal void OnRegistrationFailedEvent(string uri, string reason)
        {
            this.OnRegistrationFailed?.Invoke(uri, reason);
        }

        internal void OnRegistrationRemovedEvent(string uri)
        {
            this.OnRegistrationRemoved?.Invoke(uri);
        }

        internal void OnApplicationStartEvent(string instance)
        {
            this.OnAplicationStart?.Invoke(instance);
        }

        internal void OnApplicationStopEvent(string instance)
        {
            this.OnApplicationStop?.Invoke(instance);
        }

        internal void OnBridgePlayDoneEvent(string id)
        {
            this.OnBridgePlayDone?.Invoke(id);
        }

        internal void OnBridgeRecordDoneEvent(string id)
        {
            this.OnBridgeRecordDone?.Invoke(id);
        }

        internal void OnTextMessageDeliveredEvent(string id)
        {
            this.OnTextMessageDelivered?.Invoke(id);
        }

        internal void OnTextMessageSentEvent(string id)
        {
            this.OnTextMessageSent?.Invoke(id);
        }

        internal void OnTextMessageReceivedEvent(string id, MessageInfoParams p)
        {
            this.OnTextMessageReceived?.Invoke(id, p);
        }

        internal void OnMessageReceivedEvent(string id, MessageInfoParams p)
        {
            this.OnMessageReceived?.Invoke(id, p);
        }

        internal void OnMessageSentEvent(string id)
        {
            this.OnMessageSent?.Invoke(id);
        }

        internal void OnMessageFailedEvent(string id, string reason)
        {
            this.OnMessageFailed?.Invoke(id, reason);
        }

        internal void OnMessageTimeoutEvent(string id)
        {
            this.OnMessageTimeout?.Invoke(id);
        }

        internal void OnContactStateUpdatedEvent(string id, ContactInfoParams p)
        {
            this.OnContactStateUpdated?.Invoke(id, p);
        }

        internal void OnCallDisconnectedEvent(string id, string reason)
        {
            SIPServerApplication.Instance.OnCallDisconnected?.Invoke(id, reason);
        }

        internal void OnCallPlayDoneEvent(string id, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallPlayDone?.Invoke(id, p);
        }

        internal void OnCallRecordDoneEvent(string id, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallRecordDone?.Invoke(id, p);
        }

        internal void OnCallSendDtmfDoneEvent(string id, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallSendDtmfDone?.Invoke(id, p);
        }

        internal void OnCallPlayRecordDoneEvent(string id, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallPlayRecordDone?.Invoke(id, p);
        }

        internal void OnCallPlayFilesDoneEvent(string id, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallPlayFilesDone?.Invoke(id, p);
        }

        internal void OnCallSpeakDoneEvent(string id, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallSpeakDone?.Invoke(id, p);
        }

        internal void OnCallPositiveVoiceDetectionEvent(string id)
        {
            SIPServerApplication.Instance.OnCallPositiveVoiceDetection?.Invoke(id);
        }

        internal void OnCallPositiveFaxDetectionEvent(string id)
        {
            SIPServerApplication.Instance.OnCallPositiveFaxDetection?.Invoke(id);
        }

        internal void OnCallPositiveAnswerMachineDetectionEvent(string id)
        {
            SIPServerApplication.Instance.OnCallPositiveAnswerMachineDetection?.Invoke(id);
        }

        internal void OnCallRingbackToneDetectionEvent(string id)
        {
            SIPServerApplication.Instance.OnCallPositiveRingbackToneDetection?.Invoke(id);
        }

        internal void OnCallFailedEvent(string id, string reason)
        {
            SIPServerApplication.Instance.OnCallDiverted?.Invoke(id, reason);
        }

        internal void OnCallRecordErrorEvent(string id, string reason, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallRecordError?.Invoke(id, reason, p);
        }

        internal void OnCallPlayErrorEvent(string id, string reason, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallPlayError?.Invoke(id, reason, p);
        }

        internal void OnCallGetDtmfDoneEvent(string id, string digits, CallInfoParams p)
        {
            SIPServerApplication.Instance.OnCallGetDtmfDone?.Invoke(id, digits, p);
        }

        internal void OnCallDivertedEvent(string id, string reason)
        {
            SIPServerApplication.Instance.OnCallDiverted?.Invoke(id, reason);
        }

        internal void OnCallRingingEvent(string id)
        {
            SIPServerApplication.Instance.OnCallRinging?.Invoke(id);
        }

        internal void OnCallAnsweredEvent(string id)
        {
            SIPServerApplication.Instance.OnCallAnswered?.Invoke(id, null);
        }

        internal void OnCallAnsweredEvent(string id, CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallAnswered?.Invoke(id, callInfoParams);
        }

        internal void OnCallConnectedEvent(string id, CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallConnected?.Invoke(id, callInfoParams);
        }

        internal void OnCallAcceptedEvent(string id)
        {
            SIPServerApplication.Instance.OnCallAccepted?.Invoke(id, null);
        }

        internal void OnCallAcceptedEvent(string id, CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallAccepted?.Invoke(id, callInfoParams);
        }

        internal void OnCallOfferedEvent(string id, CallInfoParams callInfoParams)
        {
            SIPServerApplication.Instance.OnCallOffered?.Invoke(id, callInfoParams);
        }

        public void Stop()
        {
            _restService.Stop();
            _restService = null;
        }
    }
}
