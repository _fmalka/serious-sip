/* 
 * keypad
 *
 * A suite of communication APIs for web and enterprise applications
 *
 * OpenAPI spec version: v1
 * Contact: fmalka@modulo.co.il
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using RestSharp;

namespace SeriousSIP.Client
{
    /// <summary>
    /// A delegate to ExceptionFactory method
    /// </summary>
    /// <param name="methodName">Method name</param>
    /// <param name="response">Response</param>
    /// <returns>Exceptions</returns>
        public delegate Exception ExceptionFactory(string methodName, IRestResponse response);
}
