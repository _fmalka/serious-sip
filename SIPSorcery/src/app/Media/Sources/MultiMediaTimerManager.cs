﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace SIPSorcery.Media
{
    public class MultiMediaTimerManager
    {
        private static MultiMediaTimerManager _instance = null;

        public static MultiMediaTimerManager Instance()
        {
            if (_instance == null) 
            {
                _instance = new MultiMediaTimerManager();
                _instance.Start();
            }

            return _instance;
        }

        private List<MultiMediaTimer> _timers = new List<MultiMediaTimer>();
        private int _nextTimer = 0;
        private int _maxTimers = 16;
        private bool _started = false;

        public MultiMediaTimerManager()
        {
        }

        public void Start()
        {
            if (_started == true)
            {
                return;
            }

            for (var i =0; i< _maxTimers; i++)
            {
                var t = new MultiMediaTimer(20);
                t.Start();
                _timers.Add(t);
            }

            _started = true;
        }

        public void Stop()
        {
            if (_started == false)
            {
                return;
            }

            foreach (var t in _timers)
            {
                t.Stop();
            }

            _started = false;
        }

        private MultiMediaTimer Next()
        {
            if (_nextTimer >= _maxTimers) 
            {
                _nextTimer = 0;
            }

            return _timers[_nextTimer++];
        }

        public MultiMediaTimer SetTimer(MultiMediaTimer.ElapsedTimerDelegate handler)
        {
            var t = Next();
            t.ElapsedTimerHandler += handler;
            return t;
        }

        public void ClearTimer(MultiMediaTimer t, MultiMediaTimer.ElapsedTimerDelegate handler)
        {
            t.ElapsedTimerHandler -= handler;
        }

    }

    public class MultiMediaTimer
    {

        internal event ElapsedTimerDelegate ElapsedTimerHandler;
        private int _timerId;
        private int _interval;
        private TimerEventHandler _handler;

        // P/Invoke declarations
        private delegate void TimerEventHandler(int id, int msg, IntPtr user, int dw1, int dw2);

        private const int TIME_PERIODIC = 1;
        private const int EVENT_TYPE = TIME_PERIODIC;// + 0x100;  // TIME_KILL_SYNCHRONOUS causes a hang ?!
        [DllImport("winmm.dll")]
        private static extern int timeSetEvent(int delay, int resolution,
                                                TimerEventHandler handler, IntPtr user, int eventType);
        [DllImport("winmm.dll")]
        private static extern int timeKillEvent(int id);
        [DllImport("winmm.dll")]
        private static extern int timeBeginPeriod(int msec);
        [DllImport("winmm.dll")]
        private static extern int timeEndPeriod(int msec);

        public MultiMediaTimer(int intervalMS)
        {
            _interval = intervalMS;
        }

        public delegate void ElapsedTimerDelegate(object state);

        private void TimerHandler(int id, int msg, IntPtr user, int dw1, int dw2)
        {
            Task.Run(() =>
            ElapsedTimerHandler?.Invoke(id)
            );
        }

        public void Start()
        {
            timeBeginPeriod(1);
            _handler = new TimerEventHandler(TimerHandler);
            _timerId = timeSetEvent(_interval, 0, _handler, IntPtr.Zero, EVENT_TYPE);
        }

        public void Stop()
        {
            int err = timeKillEvent(_timerId);
            timeEndPeriod(1);
            _timerId = 0;
            _handler = null;
            ElapsedTimerHandler = null;
        }
    }
}
