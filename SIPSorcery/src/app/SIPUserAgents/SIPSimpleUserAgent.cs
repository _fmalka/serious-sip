﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SIPSorcery.Sys;

namespace SIPSorcery.SIP.App
{
    public class SIPSimpleUserAgent
    {
        private static ILogger logger = Log.Logger;

        private static readonly string m_userAgent = SIPConstants.SIP_USERAGENT_STRING;

        private SIPTransport m_sipTransport;
        private SIPURI m_sipAccountAOR;
        private string m_authUsername;
        private string m_password;
        private string m_serverHost;
        private SIPURI m_contactURI;
        private SIPAuthenticationHeader m_authenticationHeader;

        private int m_cseq;
        private string m_callID;
        private SIPURI m_toURI;

        public string UserAgent;                // If not null this value will replace the default user agent value in the REGISTER request.
        public string UserDisplayName;			//rj2: if not null, used in fromheader and contactheader
        
        public string GetUserName()
        {
            return this.m_authUsername;
        }
        public string GetPassword()
        {
            return this.m_password;
        }
        public SIPURI GetToURI()
        {
            return this.m_toURI;
        }

        public SIPURI GetFromURI()
        {
            return this.m_sipAccountAOR;
        }

        public event Action<SIPResponse, string> MessageFailed;
        public event Action<SIPResponse, string> MessageSent;
        public event Action<SIPResponse, string> MessageTimeout;

        public Func<SIPRequest, SIPRequest> AdjustMessage;

        public string GetCallID() => m_callID;

        public SIPSimpleUserAgent(
            SIPTransport sipTransport,
            string username,
            string password,
            string server)
        {
            m_sipTransport = sipTransport;
            m_sipAccountAOR = new SIPURI(username, server, null, SIPSchemesEnum.sip, m_sipTransport.GetSIPChannels()[0].SIPProtocol);
            m_authUsername = username;
            m_password = password;
            m_serverHost = server;
            m_callID = Guid.NewGuid().ToString();

            // Setting the contact to "0.0.0.0" tells the transport layer to populate it at send time.
            m_contactURI = new SIPURI(m_sipAccountAOR.Scheme, IPAddress.Any, 0);
            m_contactURI.User = username;
        }

        public SIPSimpleUserAgent(
            SIPTransport sipTransport,
            string username,
            string password,
            string server,
            SIPURI contactURI)
        {
            m_sipTransport = sipTransport;
            m_sipAccountAOR = new SIPURI(username, server, null, SIPSchemesEnum.sip, m_sipTransport.GetSIPChannels()[0].SIPProtocol);
            m_authUsername = username;
            m_password = password;
            m_serverHost = server;
            m_callID = Guid.NewGuid().ToString();
            m_contactURI = contactURI;
        }

    

        public void SendMessage(string to, string message)
        {
            try
            {
                m_toURI = SIPURI.ParseSIPURI(to);

                SIPEndPoint simpleSIPEndPoint;
                SIPURI uri = SIPURI.ParseSIPURIRelaxed(m_serverHost);

                if (m_sipTransport.GetSIPChannels()[0].SIPProtocol != SIPProtocolsEnum.udp)
                {
                    m_toURI.Protocol = m_sipTransport.GetSIPChannels()[0].SIPProtocol;
                    uri.Protocol = m_sipTransport.GetSIPChannels()[0].SIPProtocol;
                }

                var lookupResult = m_sipTransport.ResolveSIPUriAsync(uri).ConfigureAwait(false).GetAwaiter().GetResult();
                if (lookupResult == null)
                {
                    logger.LogWarning("SIPSimpleUserAgent could not resolve " + m_serverHost + ".");
                    MessageFailed?.Invoke(null, "Could not resolve " + m_serverHost + ".");
                }
                else
                {
                    simpleSIPEndPoint = lookupResult;
                    logger.LogDebug("Sending message to " + m_serverHost + " at " + simpleSIPEndPoint.ToString() + " for " + m_sipAccountAOR.ToString() + ".");
                    SIPRequest regRequest = GetSimpleMessageRequest(to, message);
                    if (m_authenticationHeader != null)
                    {
                        regRequest.Header.AuthenticationHeader = m_authenticationHeader;
                    }
                    SIPNonInviteTransaction msgTransaction = new SIPNonInviteTransaction(m_sipTransport, regRequest, simpleSIPEndPoint);
                    // These handlers need to be on their own threads to take the processing off the SIP transport layer.
                    msgTransaction.NonInviteTransactionFinalResponseReceived += (lep, rep, tn, rsp) => { ServerResponseReceived(lep, rep, tn, rsp); return Task.FromResult(SocketError.Success); };
                    msgTransaction.NonInviteTransactionTimedOut += (tn) => { MessageTimedOut(tn); };

                    m_sipTransport.SendTransaction(msgTransaction);
                }
                
            }
            catch (Exception excp)
            {
                logger.LogError("Exception SendInitialRegister to " + m_serverHost + ". " + excp.Message);
                MessageFailed?.Invoke(null, "Exception SendMessage to " + m_serverHost + ". " + excp.Message);
            }
        }

        private void MessageTimedOut(SIPTransaction sipTransaction)
        {
            MessageTimeout?.Invoke(null, "Message transaction to " + m_serverHost + " for " + m_sipAccountAOR.ToString() + " timed out.");
        }

        /// <summary>
        /// The event handler for responses to the initial register request.
        /// </summary>
        private void ServerResponseReceived(SIPEndPoint localSIPEndPoint, SIPEndPoint remoteEndPoint, SIPTransaction sipTransaction, SIPResponse sipResponse)
        {
            try
            {
                logger.LogDebug("Server response " + sipResponse.Status + " received for " + m_sipAccountAOR.ToString() + ".");

                if (sipResponse.Status == SIPResponseStatusCodesEnum.ProxyAuthenticationRequired || sipResponse.Status == SIPResponseStatusCodesEnum.Unauthorised)
                {
                    if (sipResponse.Header.AuthenticationHeader != null)
                    {
                        SIPRequest authenticatedRequest = GetAuthenticatedSimpleMessageRequest(sipTransaction.TransactionRequest, sipResponse);
                        m_authenticationHeader = authenticatedRequest.Header.AuthenticationHeader;
                        SIPEndPoint registrarSIPEndPoint;
                        SIPURI uri = SIPURI.ParseSIPURIRelaxed(m_serverHost);

                        if (m_sipTransport.GetSIPChannels()[0].SIPProtocol != SIPProtocolsEnum.udp)
                        {
                            uri.Protocol = m_sipTransport.GetSIPChannels()[0].SIPProtocol;
                        }

                        var lookupResult = m_sipTransport.ResolveSIPUriAsync(uri).ConfigureAwait(false).GetAwaiter().GetResult();
                        if (lookupResult == null)
                        {
                            logger.LogWarning("SIPSimpleUserAgent could not resolve " + m_serverHost + ".");
                            MessageFailed?.Invoke(sipResponse, "Could not resolve " + m_serverHost + ".");
                        }
                        else
                        {
                            registrarSIPEndPoint = lookupResult;
                            SIPNonInviteTransaction msgAuthTransaction = new SIPNonInviteTransaction(m_sipTransport, authenticatedRequest, registrarSIPEndPoint);
                            msgAuthTransaction.NonInviteTransactionFinalResponseReceived += (lep, rep, tn, rsp) => { AuthResponseReceived(lep, rep, tn, rsp); return Task.FromResult(SocketError.Success); };
                            msgAuthTransaction.NonInviteTransactionTimedOut += (tn) => {  MessageTimedOut(tn); };
                            m_sipTransport.SendTransaction(msgAuthTransaction);
                        }
                    }
                    else
                    {
                        logger.LogWarning("Message failed with " + sipResponse.Status + " but no authentication header was supplied for " + m_sipAccountAOR.ToString() + ".");
                        MessageFailed?.Invoke(sipResponse, "Message failed with " + sipResponse.Status + " but no authentication header was supplied.");
                    }
                }
                else
                {
                    if (sipResponse.Status == SIPResponseStatusCodesEnum.Ok)
                    {
                            MessageSent?.Invoke(sipResponse, "success");
                    }
                    else
                    {
                        // SIP account does not appear to exist.
                        logger.LogWarning("Message failed with " + sipResponse.Status + " for " + m_sipAccountAOR.ToString() + ".");
                        string reasonPhrase = (sipResponse.ReasonPhrase.IsNullOrBlank()) ? sipResponse.Status.ToString() : sipResponse.ReasonPhrase;
                        MessageFailed?.Invoke(sipResponse, "Registration failed with " + (int)sipResponse.Status + " " + reasonPhrase + ".");
                    }
                }
            }
            catch (Exception excp)
            {
                logger.LogError("Exception SIPSimpleUserAgent ServerResponseReceived (" + remoteEndPoint + "). " + excp.Message);
            }
        }

        /// <summary>
        /// The event handler for responses to the authenticated register request.
        /// </summary>
        private void AuthResponseReceived(SIPEndPoint localSIPEndPoint, SIPEndPoint remoteEndPoint, SIPTransaction sipTransaction, SIPResponse sipResponse)
        {
            try
            {
                logger.LogDebug("Server auth response " + sipResponse.Status + " received for " + m_sipAccountAOR.ToString() + ".");

                if (sipResponse.Status == SIPResponseStatusCodesEnum.Ok)
                {
                    MessageSent?.Invoke(sipResponse, "success");
                }
                else
                {
                    logger.LogWarning("Message failed with " + sipResponse.Status + " for " + m_sipAccountAOR.ToString() + ".");
                    MessageFailed?.Invoke(sipResponse, "Registration failed with " + sipResponse.Status + ".");
                }
            }
            catch (Exception excp)
            {
                logger.LogError("Exception SIPSimpleUserAgent AuthResponseReceived. " + excp.Message);
            }
        }

        private SIPRequest GetSimpleMessageRequest(string to, string message)
        {
            try
            {
                SIPURI simpleURI = m_sipAccountAOR.CopyOf();
                simpleURI.User = null;

                SIPRequest messageRequest = SIPRequest.GetRequest(
                    SIPMethodsEnum.MESSAGE,
                    simpleURI,
                    new SIPToHeader(null, SIPURI.ParseSIPURI(to), null),
                    new SIPFromHeader(this.UserDisplayName, m_sipAccountAOR, CallProperties.CreateNewTag()));

                messageRequest.Header.Contact = new List<SIPContactHeader> { new SIPContactHeader(this.UserDisplayName, m_contactURI) };
                messageRequest.Header.CSeq = ++m_cseq;
                messageRequest.Header.CallId = m_callID;
                messageRequest.Header.UserAgent = (!UserAgent.IsNullOrBlank()) ? UserAgent : m_userAgent;
                messageRequest.Header.ContentType = "text/plain";
                messageRequest.Body = message;

                if (AdjustMessage == null)
                {
                    return messageRequest;
                }

                return AdjustMessage(messageRequest);
            }
            catch (Exception excp)
            {
                logger.LogError("Exception GetSimpleMessageRequest. " + excp.Message);
                throw excp;
            }
        }

        private SIPRequest GetAuthenticatedSimpleMessageRequest(SIPRequest messageRequest, SIPResponse sipResponse)
        {
            try
            {
                SIPAuthorisationDigest authRequest = sipResponse.Header.AuthenticationHeader.SIPDigest;
                string username = (m_authUsername != null) ? m_authUsername : m_sipAccountAOR.User;
                authRequest.SetCredentials(username, m_password, messageRequest.URI.ToString(), SIPMethodsEnum.REGISTER.ToString());

                SIPRequest regRequest = messageRequest.Copy();
                regRequest.SetSendFromHints(messageRequest.LocalSIPEndPoint);

                regRequest.Header.Vias.TopViaHeader.Branch = CallProperties.CreateBranchId();
                regRequest.Header.From.FromTag = CallProperties.CreateNewTag();
                regRequest.Header.To.ToTag = null;
                regRequest.Header.CSeq = ++m_cseq;

                regRequest.Header.AuthenticationHeader = new SIPAuthenticationHeader(authRequest);
                regRequest.Header.AuthenticationHeader.SIPDigest.Response = authRequest.Digest;

                return regRequest;
            }
            catch (Exception excp)
            {
                logger.LogError("Exception GetAuthenticatedSimpleMessageRequest. " + excp.Message);
                throw excp;
            }
        }
    }
}
