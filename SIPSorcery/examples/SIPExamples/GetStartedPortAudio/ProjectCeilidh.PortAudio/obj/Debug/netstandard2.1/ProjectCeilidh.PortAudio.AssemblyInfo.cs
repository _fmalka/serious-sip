//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: System.Reflection.AssemblyCompanyAttribute("Project Ceilidh")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Debug")]
[assembly: System.Reflection.AssemblyCopyrightAttribute("Olivia Trewin 2018")]
[assembly: System.Reflection.AssemblyDescriptionAttribute("C# bindings for PortAudio, targeting .NET Standard 2.1")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("1.0.1.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("1.0.1")]
[assembly: System.Reflection.AssemblyProductAttribute("ProjectCeilidh.PortAudio")]
[assembly: System.Reflection.AssemblyTitleAttribute("ProjectCeilidh.PortAudio")]
[assembly: System.Reflection.AssemblyVersionAttribute("1.0.1.0")]
[assembly: System.Reflection.AssemblyMetadataAttribute("RepositoryUrl", "https://github.com/Ceilidh-Team/PortAudio")]

// Generated by the MSBuild WriteCodeFragment class.

