﻿using Microsoft.ClearScript.V8;
using Microsoft.Extensions.Logging;
using Serilog.Context;
using SIPSorcery.SIP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace SeriousSIP
{
    /// <summary>
    /// Utility class to run automated scripts
    /// </summary>
    public class ScriptRunner
    {
        /// <summary>
        /// Run a script for a call.
        /// </summary>
        /// <param name="call">The call to execute the script on.</param>
        /// <param name="scriptCommands">The array of commands representing the script</param>
        /// <returns></returns>
        public static async Task RunScript(SIPCall call, ScriptCommand[] scriptCommands)
        {
            if (scriptCommands.Length == 0)
                return;

            ILogger _logger = SIPServerService.GetInstance().LoggerFactory.CreateLogger("SeriousSIP.ScriptRunner");

            var nextCommand = scriptCommands.Single(s => s.line == 1);
            var result = "";
            SIPCall newCall = null;
            SIPBridge bridge = null;

            _logger.LogDebug($"nextCommand: ${nextCommand}");

            while ((nextCommand != null) && (call.IsCallActive))
            {
                _logger.LogDebug($"nextCommand: ${nextCommand}");
                switch (nextCommand.command)
                {
                    case ScriptCommandType.Play:
                        PlayParams p = JsonSerializer.Deserialize<PlayParams>(nextCommand.parameters.ToString());
                        await call.PlayFile(Program.Config["storagePath"] + p.fileName, p.clearDigitsBuffer, p.stopOnDigit);
                        break;
                    case ScriptCommandType.PlayFiles:
                        PlayFilesParams pf = JsonSerializer.Deserialize<PlayFilesParams>(nextCommand.parameters.ToString());
                        await call.PlayFiles(pf.fileNames, pf.interval, pf.clearDigitsBuffer, pf.stopOnDigit);
                        break;
                    case ScriptCommandType.Record:
                        RecordParams r = JsonSerializer.Deserialize<RecordParams>(nextCommand.parameters.ToString());
                        await call.RecordFile(r.fileName, r.duration, r.terminationDigit, r.playTone, r.stopOnSilence);
                        break;
                    case ScriptCommandType.PlayRecord:
                        PlayRecordParams pr = JsonSerializer.Deserialize<PlayRecordParams>(nextCommand.parameters.ToString());
                        await call.PlayRecord(pr.playFileName, pr.recordFileName, pr.duration, pr.terminationDigit, pr.playTone, pr.stopOnSilence);
                        break;
                    case ScriptCommandType.MakeCall:
                        MakeCallParams mp = JsonSerializer.Deserialize<MakeCallParams>(nextCommand.parameters.ToString());
                        var to = mp.to;
                        if (mp.to.Contains("$to.user"))
                        {
                            to = mp.to.Replace("$to.user", call.GetToURI().User);
                        }
                        string domain = SIPURI.ParseSIPURI(mp.identity).Host;
                        string[] sipHeaders = new string[] { $"P-Asserted-Identity: <sip:{call.GetFromURI().User}@{domain}>" };
                        newCall = await SIPServerService.GetSIPServer().MakeCall(to, sipHeaders, mp.identity, 0);
                        // without this delay audio is not clean
                        await Task.Delay(5000); 
                        if (mp.waitForCompletion)
                            while (true) { 
                                await Task.Delay(500); 
                                if (newCall.IsCallActive)
                                    break;
                                if (!newCall.IsCalling && !newCall.IsRinging && ! newCall.IsCallActive) {
                                    break;
                                }
                            }
                        break;
                    case ScriptCommandType.Bridge:
                        BridgeCreateParams bcp = JsonSerializer.Deserialize<BridgeCreateParams>(nextCommand.parameters.ToString());
                        bridge = SIPServerService.GetSIPServer().CreateBridge(newCall, call);
                        break;
                    case ScriptCommandType.AttendedTransfer:
                        AttendedTransferParams at = JsonSerializer.Deserialize<AttendedTransferParams>(nextCommand.parameters.ToString());
                        await call.AttendedTransfer(newCall, at.timeout);
                        call.Hangup();
                        break;
                    case ScriptCommandType.BlindTransfer:
                        BlindTransferParams b = JsonSerializer.Deserialize<BlindTransferParams>(nextCommand.parameters.ToString());
                        await call.BlindTransfer(b.to, b.timeout, b.disconnect);
                        break;
                    case ScriptCommandType.GetDigits:
                        GetDigitsParams g = JsonSerializer.Deserialize<GetDigitsParams>(nextCommand.parameters.ToString());
                        result = await call.GetDigits(g.duration, g.numOfDigits, g.terminationDigit, g.clearDigitsBuffer);
                        break;
                    case ScriptCommandType.Drop:
                        call.Hangup();
                        break;
                    case ScriptCommandType.Goto:
                        break;
                    case ScriptCommandType.Log:
                        LogParams l = JsonSerializer.Deserialize<LogParams>(nextCommand.parameters.ToString());
                        using (LogContext.PushProperty("CallContext", call.GetCallID()))
                        {
                            _logger.Log(l.level, l.message);
                        }
                        break;
                    case ScriptCommandType.LogResult:
                        LogParams lr = JsonSerializer.Deserialize<LogParams>(nextCommand.parameters.ToString());
                        using (LogContext.PushProperty("CallContext", call.GetCallID()))
                        {
                            _logger.Log(lr.level, result);
                        }
                        break;
                }

                if (nextCommand.expression != null)
                {
                    if (result == "")
                    {
                        nextCommand = scriptCommands.Single(s => s.line == nextCommand.whenFalse);
                    }
                    else
                    {
                        var exprStr = nextCommand.expression.Replace("$res", result);
                        using (var engine = new V8ScriptEngine())
                        {
                            if ((bool)engine.Evaluate(exprStr))
                                nextCommand = scriptCommands.Single(s => s.line == nextCommand.whenTrue);
                            else
                                nextCommand = scriptCommands.Single(s => s.line == nextCommand.whenFalse);
                        }
                    }
                }
                else
                {
                    if ((nextCommand.whenTrue == 0) && (nextCommand.whenFalse == 0))
                    {
                        break;
                    }
                    else 
                    {
                        nextCommand = scriptCommands.Single(s => s.line == nextCommand.whenTrue);
                        _logger.LogDebug($"nextCommand is {nextCommand}");
                    }
                }
            } 
           
            // if (newCall != null && newCall.IsCallActive)
            //     newCall.Hangup();

            _logger.LogDebug($"no more commands, all done.");
        }
    }
}
