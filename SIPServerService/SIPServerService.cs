﻿using System;
using Topshelf;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Concurrent;
using RestSharp;
using SIPSorcery.SIP;
using System.Text.Json;
using System.IO;
using SIPSorcery.Media;
using Serilog.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Serilog;
using RestSharp.Serializers.SystemTextJson;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using SIPSorceryMedia.Abstractions.V1;
using System.Collections.ObjectModel;
using System.Net.Sockets;
using Microsoft.ClearScript.V8;
using System.Text.RegularExpressions;

namespace SeriousSIP
{
    /// <summary>
    /// Event subscription types
    /// </summary>
    public enum EventUriType
    {
        /// <summary>
        /// Triggered when an inbound call is offered. (INVITE)
        /// </summary>
        OnCallOffered, 
        /// <summary>
        /// Triggered when an inbound call is ringing. (RINGING)
        /// </summary>
        OnCallAccepted, 
        /// <summary>
        /// Triggered when an inbound call is connected. (OK)
        /// </summary>
        OnCallAnswered,
        /// <summary>
        /// Triggered when an outbound call is connected. (OK)
        /// </summary>
        OnCallConnected, 
        /// <summary>
        /// Triggered when an outbound call initial response is replied. (TRYING)
        /// </summary>
        OnCallTrying,
        /// <summary>
        /// Triggered when an outbound call progress response is replied. (RINGING or SESSION PROGRESS)
        /// </summary>
        OnCallRinging,
        /// <summary>
        /// Triggered when an outbound call failed to be issued.
        /// </summary>
        OnCallFailed, 
        /// <summary>
        /// Triggered when an outbound call is not responded.
        /// </summary>
        OnCallTimeout, 
        /// <summary>
        /// Triggered when a call is disconnected before it is established.
        /// </summary>
        OnCallCancelled,
        /// <summary>
        /// Triggered when a call is disconnected after it was established.
        /// </summary>
        OnCallDisconnected,
        /// <summary>
        /// Triggered when an outbound call is diverted to a 3rd party.
        /// </summary>
        OnCallDiverted,
        /// <summary>
        /// Triggered when a play command has completed.
        /// </summary>
        OnCallPlayDone,
        /// <summary>
        /// Triggered when am error occurs during a play command.
        /// </summary>
        OnCallPlayError,
        /// <summary>
        /// Triggered when am error occurs during a record command.
        /// </summary>
        OnCallRecordError,
        /// <summary>
        /// Triggered when a speak or speak ssml command has completed.
        /// </summary>
        OnCallSpeakDone,
        /// <summary>
        /// Triggered when a playfiles command has completed.
        /// </summary>
        OnCallPlayFilesDone,
        /// <summary>
        /// Triggered when a playrecord command has completed.
        /// </summary>
        OnCallPlayRecordDone, 
        /// <summary>
        /// Triggered when a record command has completed.
        /// </summary>
        OnCallRecordDone, 
        /// <summary>
        /// Triggered when a get digits command has completed.
        /// </summary>
        OnCallGetDigitsDone, 
        /// <summary>
        /// Triggered when a send digits command has completed.
        /// </summary>
        OnCallSendDigitsDone,
        /// <summary>
        /// Triggered when the call progress analysis of an outbound call detects an answering machine.
        /// </summary>
        OnCallPositiveAnswerMachineDetection,
        /// <summary>
        /// Triggered when the call progress analysis of an outbound call detects an fax machine.
        /// </summary>
        OnCallPositiveFaxDetection,
        /// Triggered when the call progress analysis of an outbound call detects human voice.
        OnCallPositiveVoiceDetection,
        /// Triggered when the call progress analysis of an outbound call detects a ringback tone.
        OnCallRingbackToneDetection,
        /// Triggered when an instant message is received.
        OnMessageReceived,
        /// Triggered when an instant message is sent.
        OnMessageSent,
        /// Triggered when an instant message failed to be sent.
        OnMessageFailed,
        /// Triggered when an instant message has no response.
        OnMessageTimeout,
        /// Triggered when a SMS was sent.
        OnTextMessageSent,
        /// Triggered when a SMS is received.
        OnTextMessageReceived,
        /// Triggered when a SMS sent was delivered to the remote party.
        OnTextMessageDelivered,
        /// Triggered when a contact presence or state has changed.
        OnContactStateUpdated,
        /// <summary>
        /// Triggered when a play command has completed.
        /// </summary>
        OnBridgePlayDone,
        /// <summary>
        /// Triggered when a record command has completed.
        /// </summary>
        OnBridgeRecordDone,
        /// <summary>
        /// Triggered when the container or service is started.
        /// </summary>
        OnApplicationStart,
        /// <summary>
        /// Triggered when the container or service is stopped.
        /// </summary>
        OnApplicationStop,
        /// <summary>
        /// Triggered when the sip server is started successfully.
        /// </summary>
        OnSIPServerStartSuccess,
        /// <summary>
        /// Triggered when the sip server failed to start.
        /// </summary>
        OnSIPServerStartFailed,
        /// <summary>
        /// Triggered when the sip server is stopped succesfully.
        /// </summary>
        OnSIPServerStopSuccess,
        /// <summary>
        /// Triggered when the sip server failed to stop.
        /// </summary>
        OnSIPServerStopFailed,
        /// <summary>
        /// Triggerd when a temporary failure occurs with a sip registration.
        /// </summary>
        OnRegistrationTemporaryFailure,
        /// <summary>
        /// Triggered when a sip registration fails.
        /// </summary>
        OnRegistrationFailed,
        /// <summary>
        /// Triggered when a sip registration is removed.
        /// </summary>
        OnRegistrationRemoved,
        /// <summary>
        /// Triggered when a sip registration is successfull.
        /// </summary>
        OnRegistrationSuccess,
        /// <summary>
        /// Triggered when a sip trunk is alive.
        /// </summary>
        OnTrunkUp,
        /// <summary>
        /// Triggered when a sip trunk is out of service.
        /// </summary>
        OnTrunkDown
    }

    class SIPServerService : ServiceControl
    {
        private string _apiVersion = Program.ApiVersion;
        private string _apiPrefix = Program.ApiPrefix;

        public static SIPServer GetSIPServer() => _sipServer;
        public static SIPServerService GetInstance() => _sipServerService;

        private static SIPServerService _sipServerService;
        private static SIPServer _sipServer;
        private static SIPServerRestService _sipServerRestService;
        public SerilogLoggerFactory LoggerFactory { get; private set; }
        private Microsoft.Extensions.Logging.ILogger _logger = null;

        public bool AutoStartSipServer { get; set; } = true;
        public string ClientUrl { get; set; } = "";

        /// <summary>
        /// Keeps track of the client applications and their subscriptions.
        /// </summary>
        private static ConcurrentDictionary<string, Uri> _OnCallOfferedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallAcceptedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallAnsweredSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallConnectedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallTryingSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallRingingSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallFailedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallTimeoutSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallCancelledSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallDisconnectedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallDivertedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallPlayDoneSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallPlayErrorSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallSpeakDoneSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallPlayFilesDoneSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallPlayRecordDoneSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallRecordDoneSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallRecordErrorSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallGetDigitsDoneSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallSendDigitsDoneSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallPositiveAnswerMachineDetectionSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallPositiveFaxDetectionSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallPositiveVoiceDetectionSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnCallRingbackToneDetectionSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnMessageReceivedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnMessageSentSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnMessageFailedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnMessageTimeoutSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnTextMessageReceivedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnTextMessageSentSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnTextMessageDeliveredSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnContactStateUpdatedSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnBridgePlayDoneSubscriptions = new ConcurrentDictionary<string, Uri>();
        private static ConcurrentDictionary<string, Uri> _OnBridgeRecordDoneSubscriptions = new ConcurrentDictionary<string, Uri>();

        private static ConcurrentDictionary<string, ScriptCommand[]> _registeredScripts = new ConcurrentDictionary<string, ScriptCommand[]>();

        public ScriptCommand[] GetRegisteredScript(string identity)
        {
            _registeredScripts.TryGetValue(identity, out var script);
            return script;
        }

        public bool SetRegisteredScript(string identity, ScriptCommand[] scriptCommands)
        {
            if (_registeredScripts.TryAdd(identity, scriptCommands))
            {
                Script script = new Script(identity, scriptCommands);
                SIPURI uri = SIPURI.ParseSIPURI(identity);
                File.WriteAllText($"script-{uri.User}.json", global::System.Text.Json.JsonSerializer.Serialize(script));
                return true;
            }

            return false;
        }

        public bool DelRegisteredScript(string identity)
        {
            if (_registeredScripts.TryRemove(identity, out var script))
            {
                SIPURI uri = SIPURI.ParseSIPURI(identity);
                File.Delete($"{Program.Config["storagePath"]}script-{uri.User}.json");
                return true;
            }

            return false;
        }

        public bool Subscribe(string identity, Uri eventUri, EventUriType eventType)
        {
            switch (eventType)
            {
                case EventUriType.OnCallOffered:
                    return _OnCallOfferedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallAccepted:
                    return _OnCallAcceptedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallAnswered:
                    return _OnCallAnsweredSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallConnected:
                    return _OnCallConnectedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallTrying:
                    return _OnCallTryingSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallRinging:
                    return _OnCallRingingSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallFailed:
                    return _OnCallFailedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallTimeout:
                    return _OnCallTimeoutSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallCancelled:
                    return _OnCallCancelledSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallDisconnected:
                    return _OnCallDisconnectedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallDiverted:
                    return _OnCallDivertedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallPlayDone:
                    return _OnCallPlayDoneSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallSpeakDone:
                    return _OnCallSpeakDoneSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallPlayFilesDone:
                    return _OnCallPlayFilesDoneSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallPlayRecordDone:
                    return _OnCallPlayRecordDoneSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallRecordDone:
                    return _OnCallRecordDoneSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallSendDigitsDone:
                    return _OnCallSendDigitsDoneSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallGetDigitsDone:
                    return _OnCallGetDigitsDoneSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallPositiveAnswerMachineDetection:
                    return _OnCallPositiveAnswerMachineDetectionSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallPositiveFaxDetection:
                    return _OnCallPositiveFaxDetectionSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallPositiveVoiceDetection:
                    return _OnCallPositiveVoiceDetectionSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnCallRingbackToneDetection:
                    return _OnCallRingbackToneDetectionSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnMessageReceived:
                    return _OnMessageReceivedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnMessageSent:
                    return _OnMessageSentSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnMessageFailed:
                    return _OnMessageFailedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnMessageTimeout:
                    return _OnMessageTimeoutSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnTextMessageReceived:
                    return _OnTextMessageReceivedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnTextMessageSent:
                    return _OnTextMessageSentSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnTextMessageDelivered:
                    return _OnTextMessageDeliveredSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnContactStateUpdated:
                    return _OnContactStateUpdatedSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnBridgePlayDone:
                    return _OnBridgePlayDoneSubscriptions.TryAdd(identity, eventUri);
                case EventUriType.OnBridgeRecordDone:
                    return _OnBridgeRecordDoneSubscriptions.TryAdd(identity, eventUri);
            }

            return false;
        }

        public bool UnsubscribeCalls(string identity)
        {
            _OnCallOfferedSubscriptions.TryRemove(identity, out var val);
            _OnCallAcceptedSubscriptions.TryRemove(identity, out val);
            _OnCallAnsweredSubscriptions.TryRemove(identity, out val);
            _OnCallConnectedSubscriptions.TryRemove(identity, out val);
            _OnCallTryingSubscriptions.TryRemove(identity, out val);
            _OnCallRingingSubscriptions.TryRemove(identity, out val);
            _OnCallFailedSubscriptions.TryRemove(identity, out val);
            _OnCallTimeoutSubscriptions.TryRemove(identity, out val);
            _OnCallCancelledSubscriptions.TryRemove(identity, out val);
            _OnCallDisconnectedSubscriptions.TryRemove(identity, out val);
            _OnCallDivertedSubscriptions.TryRemove(identity, out val);
            _OnCallPlayDoneSubscriptions.TryRemove(identity, out val);
            _OnCallSpeakDoneSubscriptions.TryRemove(identity, out val);
            _OnCallPlayFilesDoneSubscriptions.TryRemove(identity, out val);
            _OnCallPlayRecordDoneSubscriptions.TryRemove(identity, out val);
            _OnCallRecordDoneSubscriptions.TryRemove(identity, out val);
            _OnCallGetDigitsDoneSubscriptions.TryRemove(identity, out val);
            _OnCallSendDigitsDoneSubscriptions.TryRemove(identity, out val);
            _OnCallPositiveAnswerMachineDetectionSubscriptions.TryRemove(identity, out val);
            _OnCallPositiveFaxDetectionSubscriptions.TryRemove(identity, out val);
            _OnCallPositiveVoiceDetectionSubscriptions.TryRemove(identity, out val);
            _OnCallRingbackToneDetectionSubscriptions.TryRemove(identity, out val);

            return true;
        }

        public bool UnsubscribeBridges(string identity)
        {
            _OnBridgePlayDoneSubscriptions.TryRemove(identity, out var val);
            _OnBridgePlayDoneSubscriptions.TryRemove(identity, out val);

            return true;
        }

        public bool UnsubscribeMessages(string identity)
        {
            _OnMessageReceivedSubscriptions.TryRemove(identity, out var val);
            _OnMessageSentSubscriptions.TryRemove(identity, out val);
            _OnMessageFailedSubscriptions.TryRemove(identity, out val);
            _OnMessageTimeoutSubscriptions.TryRemove(identity, out val);
            _OnTextMessageReceivedSubscriptions.TryRemove(identity, out val);
            _OnTextMessageSentSubscriptions.TryRemove(identity, out val);
            _OnTextMessageDeliveredSubscriptions.TryRemove(identity, out val);
            _OnContactStateUpdatedSubscriptions.TryRemove(identity, out val);

            return true;
        }

        public bool UnsubscribeContacts(string identity)
        {
            return _OnContactStateUpdatedSubscriptions.TryRemove(identity, out var val);
        }

        private bool ValidateProductKey()
        {
            string decodedKey = Program.DecryptString(Program.Key, Program.Config.GetValue("productKey", "ICzLx0XAu10JBgSvAIKWcS/euQqaf2k02PGk224tUfHTVqDONZBYcRKD/DUHII2h"));
            string[] keyParams = decodedKey.Split(";");

            _logger.LogDebug($"product key for {keyParams[1]} on {keyParams[0]}");

            string instanceName = keyParams[0];
            IPAddress ipAddress = IPAddress.Parse(keyParams[1]);
            DateTime expirationDate = DateTime.Parse(keyParams[2]);

            if (instanceName == "any" || instanceName == Program.Config["instance"])
            {
                if (ipAddress.Equals(IPAddress.Any) || Dns.GetHostAddresses(Dns.GetHostName()).Contains(ipAddress))
                {
                    if (DateTime.Now < expirationDate)
                    {
                        if (DateTime.Now.Year == expirationDate.Year)
                        {
                            _logger.LogInformation($"product key expires in {(expirationDate - DateTime.Now).Days} days.");
                        }
                        return true;
                    }
                }
            }

            _logger.LogWarning("invalid product key.");
            return false;
        }

        private bool ValidateConfiguration()
        {
            if (!ValidateProductKey())
                return false;

            if (!Program.Config.GetValue("validateConfiguration", true))
                return true;

            if (Program.Config["instance"] == null)
            {
                _logger.LogWarning($"instance name not defined, using default instance name {Program.Config.GetValue("instance", "SIPServerService#1")}");
            }
            else
            {
                _logger.LogInformation($"instance: {Program.Config["instance"]}");
            }

            if (Program.Config["storagePath"] == null)
            {
                _logger.LogWarning($"storagePath not defined, using default storagePath {Program.Config.GetValue("storagePath", $"{Directory.GetCurrentDirectory()}")}");
            }
            else
            {
                _logger.LogInformation($"storagePath: {Program.Config["storagePath"]}");

            }

            if (Program.Config["logConfigurationFile"] == null)
            {
                _logger.LogWarning($"logConfigurationFile not defined, running without properly defined logs and cdrs.");
            }
            else
            {
                _logger.LogInformation($"logConfigurationFile: {Program.Config["logConfigurationFile"]}");

            }

            if (Program.Config["useUrls"] == null)
            {
                _logger.LogWarning($"useUrls not defined, running with default dotnet ports.");
            }
            else
            {
                _logger.LogInformation($"useUrls: {Program.Config["useUrls"]}");
            }

            if (Program.Config["clientUrl"] == null)
            {
                _logger.LogWarning($"clientUrl not defined, expecting client to subscribe via API.");
            }
            else
            {
                _logger.LogInformation($"clientUrl: {Program.Config["clientUrl"]}");
            }

            if (Program.Config["autoStartSipServer"] == null)
            {
                _logger.LogWarning($"autoStartSipServer not defined, assuming 'true' as default.");
            }
            else
            {
                _logger.LogInformation($"autoStartSipServer: {Program.Config["autoStartSipServer"]}");
            }

            if (Program.Config["oAuthServiceUrl"] == null)
            {
                if (!ValidatePrivateTokenConfiguration(Program.Config.GetSection("usePrivateToken")))
                {
                    _logger.LogWarning($"security warning: service with run with no authentication or authorization mechanism, at your own risks.");
                }
                else
                {
                    _logger.LogInformation($"usePrivateToken: {true}");
                }
            }
            else
            {
                _logger.LogInformation($"oAuthServiceUrl: {Program.Config["oAuthServiceUrl"]}");
            }

            return ValidateSipServerConfiguration(Program.Config.GetSection("sipServer"));
        }

        private bool ValidatePrivateTokenConfiguration(IConfigurationSection jwtTokenConfig)
        {
            if (jwtTokenConfig["secret"] == null)
            {
                _logger.LogWarning($"mandatory parameter 'secret' is missing.");
                return false;
            }

            if (jwtTokenConfig["issuer"] == null)
            {
                _logger.LogWarning($"mandatory parameter 'issuer' is missing.");
                return false;
            }

            if (jwtTokenConfig["audience"] == null)
            {
                _logger.LogWarning($"mandatory parameter 'audience' is missing.");
                return false;
            }

            if (jwtTokenConfig["accessTokenExpiration"] == null)
            {
                _logger.LogWarning($"mandatory parameter 'accessTokenExpiration' is missing.");
                return false;
            }

            if (jwtTokenConfig["refreshTokenExpiration"] == null)
            {
                _logger.LogWarning($"mandatory parameter 'audience' is missing.");
                return false;
            }


            return true;
        }
        private bool ValidateSipServerConfiguration(IConfigurationSection sipServerConfig)
        {
            if (!ValidateLocalIPEndPoints(sipServerConfig.GetSection("localIPEndPoints")))
                return false;

            if (sipServerConfig["hangupWhenNoRtp"] == null)
            {
                _logger.LogWarning($"hangupWhenNoRtp not defined, assuming 'true' as default.");
            }
            else
            {
                _logger.LogInformation($"hangupWhenNoRtp: {Program.Config.GetSection("sipServer")["hangupWhenNoRtp"]}");
            }

            if (sipServerConfig["enableRtpTestMode"] == null)
            {
                _logger.LogWarning($"enableRtpTestMode not defined, assuming 'false' as default.");
            }
            else
            {
                _logger.LogInformation($"enableRtpTestMode: {Program.Config.GetSection("sipServer")["enableRtpTestMode"]}");
            }

            if (sipServerConfig["maxCalls"] == null)
            {
                _logger.LogWarning($"maxCalls not defined, assuming 0 (no limit) as default.");
            }
            else
            {
                _logger.LogInformation($"maxCalls: {Program.Config.GetSection("sipServer")["maxCalls"]}");
            }

            if (sipServerConfig["registrationInterval"] == null)
            {
                _logger.LogWarning($"registrationInterval not defined, assuming 250ms as default.");
            }
            else
            {
                _logger.LogInformation($"registrationInterval: {Program.Config.GetSection("sipServer")["registrationInterval"]}");
            }

            if (sipServerConfig["autoAcceptCalls"] == null)
            {
                _logger.LogWarning($"autoAcceptCalls not defined, assuming 'true' as default.");
            }
            else
            {
                _logger.LogInformation($"autoAcceptCalls: {Program.Config.GetSection("sipServer")["autoAcceptCalls"]}");
            }

            if (sipServerConfig["acceptDelay"] == null)
            {
                _logger.LogWarning($"acceptDelay not defined, assuming 0 as default.");
            }
            else
            {
                _logger.LogInformation($"acceptDelay: {Program.Config.GetSection("sipServer")["acceptDelay"]}");
            }

            if (sipServerConfig["autoAnswerCalls"] == null)
            {
                _logger.LogWarning($"autoAnswerCalls not defined, assuming 'true' as default.");
            }
            else
            {
                _logger.LogInformation($"autoAnswerCalls: {Program.Config.GetSection("sipServer")["autoAnswerCalls"]}");
            }

            if (sipServerConfig["useMultiMediaTimers"] == null)
            {
                _logger.LogWarning($"useMultiMediaTimers not defined, assuming 'false' as default.");
            }
            else
            {
                _logger.LogInformation($"useMultiMediaTimers: {Program.Config.GetSection("sipServer")["useMultiMediaTimers"]}");
            }

            var validCodecs = new string[] { "PCMA", "PCMU", "G722", "G729" };
            if (sipServerConfig.GetSection("codecs").Get<List<AudioCodecsEnum>>() == null)
            {
                _logger.LogInformation($"codecs list not defined, will use defautl codecs list {validCodecs}");
            }
            else
            {
                foreach (var codec in Program.Config.GetSection("sipServer").GetSection("codecs").Get<List<string>>())
                {
                    if (!validCodecs.Contains(codec))
                    {
                        _logger.LogError($"codec {codec} is not a valid codec, use one of the following {validCodecs}");
                        return false;
                    }
                }
            }

            if (sipServerConfig["dtmfMode"] == null)
            {
                _logger.LogWarning($"dtmfMode not defined, assuming 'Rfc2833' as default.");
            }
            else
            {
                _logger.LogInformation($"dtmfMode: {Program.Config.GetSection("sipServer")["dtmfMode"]}");
            }

            if (sipServerConfig["rtpAddress"] == null)
            {
                _logger.LogWarning($"rtpAddress not defined, first localIPEndPoint address will be used as default");
            }
            else
            {
                try
                {
                    var ip = IPAddress.Parse(sipServerConfig["rtpAddress"]);
                }
                catch
                {
                    _logger.LogWarning($"rtpAddress {Program.Config.GetSection("sipServer")["rtpAddress"]} must be a valid IP address");
                    return false;
                }
                _logger.LogInformation($"rtpAddress: {Program.Config.GetSection("sipServer")["rtpAddress"]}");
            }

            if (!ValidateRegisterAccounts(Program.Config.GetSection("sipServer").GetSection("registerAccounts")))
                return false;

            if (sipServerConfig.GetSection("monitorTrunks").Get<List<string>>() == null)
            {
                _logger.LogWarning($"monitorTrunks not defined, assuming extension mode");
            }
            else
            {
                foreach (var entry in Program.Config.GetSection("sipServer").GetSection("monitorTrunks").Get<List<string>>())
                {
                    var entryVals = entry.Split(',');

                    if (entryVals.Length < 2)
                    {
                        _logger.LogError($"entry {entry} do not contains 2 parameters sipuri,interval");
                        return false;
                    }

                    try
                    {
                        var uri = SIPURI.ParseSIPURI(entryVals[0]);
                    }
                    catch
                    {
                        _logger.LogError($"sipuri {entryVals[0]} is not a valid sip uri");
                        return false;
                    }

                    try
                    {
                        var expire = int.Parse(entryVals[1]);
                    }
                    catch
                    {
                        _logger.LogError($"interval must be a valid number in milliseconds");
                        return false;
                    }
                }
            }

            return true;
        }

        private bool ValidateRegisterAccounts(IConfigurationSection registerAccountsConfig)
        {
            if (registerAccountsConfig.Get<List<string>>() == null)
            {
                _logger.LogWarning("registerAccounts not defined, assuming trunk mode");
            }
            else
            {
                foreach (var entry in registerAccountsConfig.Get<List<string>>())
                {
                    var entryVals = entry.Split(',');

                    if (entryVals.Length < 5)
                    {
                        _logger.LogError($"entry {entry} do not contains 5 parameters username,authuser,password,domain,expire");
                        return false;
                    }

                    var username = entryVals[0];
                    var domain = entryVals[3];
                    if (username == "")
                    {
                        _logger.LogError($"username in entry {entry} cannot be empty");
                        return false;
                    }
                    if (domain == "")
                    {
                        _logger.LogError($"domain in entry {entry} cannot be empty");
                        return false;
                    }
                    try
                    {
                        var expire = int.Parse(entryVals[4]);
                    }
                    catch
                    {
                        _logger.LogError($"expire must be a valid number");
                        return false;
                    }
                }
            }

            return true;
        }


        private bool ValidateLocalIPEndPoints(IConfigurationSection ipEndPointsConfig)
        {
            if (ipEndPointsConfig.Get<List<string>>() == null)
            {
                _logger.LogError("sip server must be configured with at least 1 localIPEndPoint");
                return false;
            }
            else
            {
                var protocols = new string[] { "udp", "tcp", "ws", "tls", "wss" };
                foreach (var entry in ipEndPointsConfig.Get<List<string>>())
                {
                    var entryVals = entry.Split(',');
                    var protocol = entryVals[0];
                    if (!protocols.Contains(protocol))
                    {
                        _logger.LogError($"unsupported protocol {protocol} defined in {entry}, please use one of the following {protocols}");
                        return false;
                    }

                    IPAddress ip = null;
                    try
                    {
                        ip = IPAddress.Parse(entryVals[1]);
                    }
                    catch
                    {
                        _logger.LogError($"malformated IP address {ip}");
                        return false;
                    }

                    try
                    {
                        if (entryVals[1] != "0.0.0.0")
                        {
                            if (!Dns.GetHostEntry(Dns.GetHostName()).AddressList.Contains(ip))
                            {
                                _logger.LogError($"configured ip address {ip} is not a valid ip for hostname {Dns.GetHostName()}");
                                return false;
                            }
                        }
                    }
                    catch
                    {
                        _logger.LogError($"could not resolve host ip addresses");
                        return false;
                    }

                    //var port = int.Parse(entryVals[2]);
                    //if (!(port > 1024 && port < 49151))
                    //{
                    //    _logger.LogError($"invalid port configured, port must be between 1024 and 49151");
                    //    return false;
                    //}

                }
            }

            return true;
        }

        public bool Start(HostControl hostControl)
        {
            _sipServerService = this;

            AppDomain.CurrentDomain.ProcessExit += (s, e) => Stop(hostControl);

            Environment.CurrentDirectory = Program.Config.GetValue("storagePath", $"{Directory.GetCurrentDirectory()}/run/");

            IConfigurationRoot configuration;
            if (File.Exists($"{Environment.CurrentDirectory}/{Program.Config["logConfigurationFile"]}"))
            {
                configuration = new ConfigurationBuilder()
                                      .SetBasePath($"{Environment.CurrentDirectory}")
                                      .AddJsonFile($"{Environment.CurrentDirectory}/{Program.Config["logConfigurationFile"]}")
                                      .AddEnvironmentVariables()
                                      .Build();
            }
            else
            {
                configuration = new ConfigurationBuilder()
                                       .SetBasePath($"{Environment.CurrentDirectory}")
                                       .AddEnvironmentVariables()
                                       .Build();
            }

            LoggerFactory = new SerilogLoggerFactory(new LoggerConfiguration()
                                .ReadFrom.Configuration(configuration)
                                .CreateLogger());

            _logger = LoggerFactory.CreateLogger(this.GetType().FullName);

            _logger.LogInformation($"Validating configuration for service {Program.Config.GetValue("instance", "SIPServerService#1")}...");
            if (!ValidateConfiguration())
                return false;

            _logger.LogInformation($"Starting service {Program.Config.GetValue("instance", "SIPServerService#1")}...");

            Task.Factory.StartNew(() =>
            {
                _sipServerRestService = new SIPServerRestService();
                _sipServerRestService.Start();
            });

            SIPSorcery.LogFactory.Set(LoggerFactory);

            LoadScripts();

            ClientUrl = Program.Config.GetValue("clientUrl", "");
            AutoStartSipServer = Program.Config.GetValue("autoStartSipServer", true);

            // create the sip server
            _sipServer = new SIPServer();

            IConfigurationSection sipServerConfig = Program.Config.GetSection("sipServer");

            // configure the sip server parameters
            _sipServer.AutoAcceptCalls = sipServerConfig.GetValue("autoAcceptCalls", true);
            _sipServer.AcceptDelay = sipServerConfig.GetValue("acceptDelay", 0);
            _sipServer.AutoAnswerCalls = sipServerConfig.GetValue("autoAnswerCalls", true);
            _sipServer.UseMultiMediaTimers = sipServerConfig.GetValue("useMultiMediaTimers", false);
            _sipServer.MaxCalls = sipServerConfig.GetValue("maxCalls", 0);
            _sipServer.HangupWhenNoRtp = sipServerConfig.GetValue("hangupWhenNoRtp", true);
            _sipServer.EnableRtpTestMode = sipServerConfig.GetValue("enableRtpTestMode", false);
            _sipServer.RegistrationInterval = sipServerConfig.GetValue("registrationInterval", 250);
            _sipServer.DtmfMode = sipServerConfig.GetValue("dtmfMode", SIPCallDtmfMode.Rfc2833);
            _sipServer.UserAgent = sipServerConfig.GetValue("userAgentHeader", "Keypad-1.0.0");

            if (sipServerConfig.GetSection("localIPEndPoints").Get<List<string>>() != null)
            {
                StringCollection col = new StringCollection();
                col.AddRange(sipServerConfig.GetSection("localIPEndPoints").Get<List<string>>().ToArray());
                _sipServer.LocalIPEndPoints = col;
            }

            if (sipServerConfig.GetSection("registerAccounts").Get<List<string>>() != null)
            {
                StringCollection col = new StringCollection();
                col.AddRange(sipServerConfig.GetSection("registerAccounts").Get<List<string>>().ToArray());
                _sipServer.RegisterAccounts = col;
            }

            if (sipServerConfig.GetSection("monitorTrunks").Get<List<string>>() != null)
            {
                StringCollection col = new StringCollection();
                col.AddRange(sipServerConfig.GetSection("monitorTrunks").Get<List<string>>().ToArray());
                _sipServer.MonitorTrunks = col;
            }

            if (sipServerConfig.GetSection("codecs").Get<List<AudioCodecsEnum>>() != null)
                _sipServer.Codecs = sipServerConfig.GetSection("codecs").Get<List<AudioCodecsEnum>>();

            if (sipServerConfig["rtpAddress"] != null)
                _sipServer.RtpAddress = IPAddress.Parse(sipServerConfig["rtpAddress"]);

            if (Program.Config["smpp"] != null)
            {
                SmppConfiguration _smppConfiguration = new SmppConfiguration()
                {
                    SystemID = Program.Config.GetSection("smpp").GetValue("systemId", "keypad"),
                    Password = Program.Config.GetSection("smpp").GetValue("password", "keypad"),
                    Host = Program.Config.GetSection("smpp").GetValue("host", "localhost"),
                    Port = Program.Config.GetSection("smpp").GetValue("port", 2775),
                    SystemType = Program.Config.GetSection("smpp").GetValue("systemType", "5750"),
                    DefaultServiceType = Program.Config.GetSection("smpp").GetValue("serviceType", "5750"),
                    SourceAddress = Program.Config.GetSection("smpp").GetValue("sourceAddress", "1234"),
                    Encoding = JamaaTech.Smpp.Net.Lib.DataCoding.UCS2,
                    AddressNpi = JamaaTech.Smpp.Net.Lib.NumberingPlanIndicator.Unknown,
                    AddressTon = JamaaTech.Smpp.Net.Lib.TypeOfNumber.Alphanumeric,
                    UserMessageReferenceType = UserMessageReferenceType.None,
                    RegisterDeliveryNotification = true,
                    UseSeparateConnections = true
                };
            }

            if (AutoStartSipServer)
                _ = StartSIPServer();

            _ = SendApplicationStart();

            return true;
        }

        internal async Task<bool> StartSIPServer()
        {
            if ((_sipServer != null) && (!(_sipServer.State == SIPServerState.NotStarted)))
                return false;

            // incoming call events
            _sipServer.OnCallOffered += OnCallOffered;
            _sipServer.OnCallAccepted += OnCallAccepted;
            _sipServer.OnCallAnswered += OnCallAnswered;
            _sipServer.OnCallCancelled += _sipServer_OnCallCancelled;

            // outgoing call events
            _sipServer.OnCallRinging += OnCallRinging;
            _sipServer.OnCallDiverted += OnCallDiverted;
            _sipServer.OnCallFailed += OnCallFailed;
            _sipServer.OnCallConnected += OnCallConnected;
            _sipServer.OnCallTimeout += _sipServer_OnCallTimeout;

            // common call events
            _sipServer.OnCallDisconnected += OnCallDisconnected;

            // message events
            _sipServer.OnMessageReceived += OnMessageReceived;
            _sipServer.OnMessageSent += OnMessageSent;
            _sipServer.OnMessageFailed += OnMessageFailed;
            _sipServer.OnMessageTimeout += OnMessageTimeout;
            _sipServer.OnTextMessageReceived += OnTextMessageReceived;
            _sipServer.OnTextMessageSent += OnTextMessageSent;
            _sipServer.OnTextMessageDelivered += OnTextMessageDelivered;

            // contact events
            _sipServer.OnContactStateUpdated += OnContactStateUpdated;

            // bridges events
            _sipServer.OnBridgeCreated += OnBridgeCreated;

            // registration events
            _sipServer.OnRegistrationRemoved += OnRegistrationRemoved;
            _sipServer.OnRegistrationSuccess += OnRegistrationSuccess;
            _sipServer.OnRegistrationTemporaryFailure += OnRegistrationTemporaryFailure;
            _sipServer.OnRegistrationFailed += OnRegistrationFailed;

            // trunks events
            _sipServer.OnTrunkUp += OnTrunkUp;
            _sipServer.OnTrunkDown += OnTrunkDown;

            // server start events
            _sipServer.OnSIPServerStartFailed += OnSIPServerStartFailed;
            _sipServer.OnSIPServerStartSuccess += OnSIPServerStartSuccess;
            _sipServer.OnSIPServerStopFailed += OnSIPServerStopFailed;
            _sipServer.OnSIPServerStopSuccess += OnSIPServerStopSuccess;

            await _sipServer.Start();

            return true;
        }
        
        internal async Task<bool> StopSIPServer(bool force)
        {
            if ((_sipServer != null) && (!(_sipServer.State == SIPServerState.Running)))
                return false;

             _sipServer.Shutdown(force);

            while (_sipServer.State != SIPServerState.NotStarted)
                await Task.Delay(500);

            // incoming call events
            _sipServer.OnCallOffered -= OnCallOffered;
            _sipServer.OnCallAccepted -= OnCallAccepted;
            _sipServer.OnCallAnswered -= OnCallAnswered;
            _sipServer.OnCallCancelled -= _sipServer_OnCallCancelled;

            // outgoing call events
            _sipServer.OnCallRinging -= OnCallRinging;
            _sipServer.OnCallDiverted -= OnCallDiverted;
            _sipServer.OnCallFailed -= OnCallFailed;
            _sipServer.OnCallConnected -= OnCallConnected;
            _sipServer.OnCallTimeout -= _sipServer_OnCallTimeout;

            // common call events
            _sipServer.OnCallDisconnected += OnCallDisconnected;

            // message events
            _sipServer.OnMessageReceived -= OnMessageReceived;
            _sipServer.OnMessageSent -= OnMessageSent;
            _sipServer.OnMessageFailed -= OnMessageFailed;
            _sipServer.OnMessageTimeout -= OnMessageTimeout;
            _sipServer.OnTextMessageReceived -= OnTextMessageReceived;
            _sipServer.OnTextMessageSent -= OnTextMessageSent;
            _sipServer.OnTextMessageDelivered -= OnTextMessageDelivered;

            // contact events
            _sipServer.OnContactStateUpdated -= OnContactStateUpdated;

            // bridges events
            _sipServer.OnBridgeCreated -= OnBridgeCreated;

            // registration events
            _sipServer.OnRegistrationRemoved -= OnRegistrationRemoved;
            _sipServer.OnRegistrationSuccess -= OnRegistrationSuccess;
            _sipServer.OnRegistrationTemporaryFailure -= OnRegistrationTemporaryFailure;
            _sipServer.OnRegistrationFailed -= OnRegistrationFailed;

            // trunks events
            _sipServer.OnTrunkUp -= OnTrunkUp;
            _sipServer.OnTrunkDown -= OnTrunkDown;

            // server start events
            _sipServer.OnSIPServerStartFailed -= OnSIPServerStartFailed;
            _sipServer.OnSIPServerStartSuccess -= OnSIPServerStartSuccess;
            _sipServer.OnSIPServerStopFailed -= OnSIPServerStopFailed;
            _sipServer.OnSIPServerStopSuccess -= OnSIPServerStopSuccess;

            return true;
        }

        private void LoadScripts()
        {
            string[] files = Directory.GetFiles(Program.Config["storagePath"], "script-*", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                try
                {
                    Script script = global::System.Text.Json.JsonSerializer.Deserialize<Script>(File.ReadAllText(file));
                    SetRegisteredScript(script.identity, script.scriptCommands);
                }
                catch (Exception e)
                {
                    _logger.LogWarning(e.Message, e);
                }
            }
        }

        private async Task OnTrunkUp(SIPURI sipUri)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/system/trunk/up");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                request.AddParameter("uri", sipUri, ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnTrunkUp} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnTrunkUp} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnTrunkUp} because no client url defined.");
            }
        }

        private async Task OnTrunkDown(SIPURI sipUri, string message)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/system/trunk/down");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                request.AddParameter("uri", sipUri, ParameterType.QueryString);
                request.AddParameter("message", message, ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnTrunkDown} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnTrunkDown} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnTrunkDown} because no client url defined.");
            }
        }

        private async Task OnSIPServerStopSuccess(string message)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/system/stop/success");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                request.AddParameter("message", message, ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnSIPServerStopSuccess} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnSIPServerStopSuccess} failed with response {response.StatusCode}.");
                }

                _ = Task.Delay(20 * 1000).ContinueWith((_) => SendApplicationStart());
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnSIPServerStopSuccess} because no client url defined.");
            }

        }

        private async Task OnSIPServerStopFailed(string message)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/system/stop/failed");
                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                request.AddParameter("message", message, ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnSIPServerStopFailed} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnSIPServerStopFailed} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnSIPServerStopFailed} because no client url defined.");
            }
        }

        private async Task OnSIPServerStartSuccess(string message)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/system/start/success");
                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnSIPServerStartSuccess} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnSIPServerStartSuccess} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnSIPServerStartSuccess} because no client url defined.");
            }
        }

        private async Task OnSIPServerStartFailed(string message)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/system/start/failed");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                request.AddParameter("message", message, ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnSIPServerStartFailed} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnSIPServerStartFailed} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnSIPServerStartFailed} because no client url defined.");
            }
        }

        private async Task OnRegistrationSuccess(SIPURI regUri)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/identity/registration/success");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                request.AddParameter("uri", regUri.ToString(), ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnRegistrationSuccess} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnRegistrationSuccess} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnRegistrationSuccess} because no client url defined.");
            }
        }

        private async Task OnRegistrationTemporaryFailure(SIPURI regUri, string reason)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/identity/registration/temporaryfailure");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                request.AddParameter("uri", regUri.ToString(), ParameterType.QueryString);
                request.AddParameter("reason", reason, ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnRegistrationTemporaryFailure} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnRegistrationTemporaryFailure} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnRegistrationTemporaryFailure} because no client url defined.");
            }
        }

        private async Task OnRegistrationFailed(SIPURI regUri, string reason)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/identity/registration/failed");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.PUT);
                request.AddParameter("uri", regUri.ToString(), ParameterType.QueryString);
                request.AddParameter("reason", reason, ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnRegistrationFailed} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnRegistrationFailed} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnRegistrationFailed} because no client url defined.");
            }
        }

        private async Task OnRegistrationRemoved(SIPURI regUri)
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/identity/registration/remove");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.DELETE);
                request.AddParameter("uri", regUri.ToString(), ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnRegistrationRemoved} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnRegistrationRemoved} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning($"Not sending {EventUriType.OnRegistrationRemoved} because no client url defined.");
            }
        }

        private async Task SendApplicationStart()
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/system/application/start");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.POST);
                request.AddParameter("instance", Program.Config.GetValue("instance", "SIPServerService#1"), ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnApplicationStart} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnApplicationStart} failed with response {response.StatusCode}.");
                    _logger.LogInformation($"Retrying in 5s");
                    _ = Task.Delay(5 * 1000).ContinueWith((_) => SendApplicationStart());
                }
            } else
            {
                _logger.LogWarning("Not sending application start because no client url defined.");
            }
        }
        private async Task SendApplicationStop()
        {
            if (ClientUrl != "")
            {
                var uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/admin/system/application/stop");

                IRestClient client = new RestClient(uri);
                IRestRequest request = new RestRequest(Method.DELETE);
                request.AddParameter("instance", Program.Config.GetValue("instance", "SIPServerService#1"), ParameterType.QueryString);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    _logger.LogDebug($"Webhook for {EventUriType.OnApplicationStop} response {response.StatusCode}.");
                }
                else
                {
                    _logger.LogWarning($"Webhook for {EventUriType.OnApplicationStop} failed with response {response.StatusCode}.");
                }
            }
            else
            {
                _logger.LogWarning("Not sending application stop because no client url defined.");
            }
        }

        private Task OnBridgeCreated(SIPBridge bridge)
        {
            bridge.OnPlayDone += () => OnBridgePlayDone(bridge);
            bridge.OnRecordDone += () => OnBridgeRecordDone(bridge);
            return Task.CompletedTask;
        }

        private async Task OnBridgeRecordDone(SIPBridge bridge)
        {

            if (!_OnBridgeRecordDoneSubscriptions.TryGetValue(bridge.Id, out var uri)) {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/bridges/bridge/record/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnBridgeRecordDone} webhook subscription for identity {bridge.Id}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            RestRequest request = new RestRequest(uri.PathAndQuery, Method.PUT);
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", bridge.Id, ParameterType.QueryString);

            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnBridgeRecordDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnBridgeRecordDone} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnBridgePlayDone(SIPBridge bridge)
        {
            if (!_OnBridgePlayDoneSubscriptions.TryGetValue(bridge.Id, out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/bridges/bridge/play/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnBridgePlayDone} webhook subscription for identity {bridge.Id}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            RestRequest request = new RestRequest(uri.PathAndQuery, Method.PUT);
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", bridge.Id, ParameterType.QueryString);

            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnBridgePlayDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnBridgePlayDone} failed with response {response.StatusCode}.");
            }

        }

        private async Task OnTextMessageDelivered(string callId, string from)
        {
            if (!_OnTextMessageDeliveredSubscriptions.TryGetValue(from, out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/messages/message/text/delivered");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnTextMessageDelivered} webhook subscription for identity {from}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", callId, ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnTextMessageDelivered} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnTextMessageDelivered} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnTextMessageSent(string callId, string from)
        {
            if (!_OnTextMessageSentSubscriptions.TryGetValue(from, out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/messages/message/text/sent");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnTextMessageSent} webhook subscription for identity {from}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", callId, ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnTextMessageSent} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnTextMessageSent} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnTextMessageReceived(string callId, string from, string to, string message)
        {
            if (!_OnTextMessageReceivedSubscriptions.TryGetValue(to, out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/messages/message/text/received");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnTextMessageReceived} webhook subscription for identity {to}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.POST);
            request.AddParameter("id", callId, ParameterType.QueryString);
            request.AddJsonBody(new MessageInfoParams(callId, from, to, SIPSorcery.SIP.SIPCallDirection.In, message));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnTextMessageReceived} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnTextMessageReceived} failed with response {response.StatusCode}.");
            }
        }

        private string GetMessageIdentity(SIPMessage msg)
        {
            string identity;
            if (msg.GetCallDirection() == SIPSorcery.SIP.SIPCallDirection.In)
                identity = msg.GetToURI().ToString();
            else
                identity = msg.GetFromURI().ToString();

            return identity;
        }

        private async Task OnContactStateUpdated(string identity, SIPContact contact)
        {
            if (!_OnContactStateUpdatedSubscriptions.TryGetValue(identity, out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/contacts/contact/state");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnContactStateUpdated} webhook subscription for identity {identity}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.POST);
            request.AddParameter("id", contact.Id, ParameterType.QueryString);
            request.AddJsonBody(new ContactInfoParams(contact.Id, contact.DisplayName, contact.UserName, contact.FirstName, contact.LastName,
                                    contact.Type, contact.PresenceState, contact.PhoneState, contact.Tel, contact.Email, contact.Tags));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnContactStateUpdated} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnContactStateUpdated} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnMessageReceived(SIPMessage msg, string message)
        {
            if (!_OnMessageReceivedSubscriptions.TryGetValue(GetMessageIdentity(msg), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/messages/message/received");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnMessageReceived} webhook subscription for identity {msg.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.POST);
            request.AddParameter("id", msg.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new MessageInfoParams(msg.GetCallID(), msg.GetFromURI().ToString(),
                                msg.GetToURI().ToString(), msg.GetCallDirection(), message));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnMessageReceived} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnMessageReceived} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnMessageSent(String id, string message)
        {
            if (!_sipServer.Messages.TryGetValue(id, out var msg)) {
                _logger.LogError($"Could not find call with given id {id}.");
                return;
            }

            if (!_OnMessageSentSubscriptions.TryGetValue(GetMessageIdentity(msg), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/messages/message/sent");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnMessageSent} webhook subscription for identity {msg.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", msg.GetCallID(), ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnMessageSent} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnMessageSent} failed with response {response.StatusCode}.");
            }

        }

        private async Task OnMessageFailed(string id, string message)
        {
            if (!_sipServer.Messages.TryGetValue(id, out var msg))
            {
                _logger.LogError($"Could not find call with given id {id}.");
                return;
            }

            if (!_OnMessageFailedSubscriptions.TryGetValue(GetMessageIdentity(msg), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/messages/message/failed");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnMessageFailed} webhook subscription for identity {msg.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", msg.GetCallID(), ParameterType.QueryString);
            request.AddParameter("reason", message, ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnMessageFailed} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnMessageFailed} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnMessageTimeout(string id, string message)
        {
            if (!_sipServer.Messages.TryGetValue(id, out var msg))
            {
                _logger.LogError($"Could not find call with given id {id}.");
                return;
            }

            if (!_OnMessageTimeoutSubscriptions.TryGetValue(GetMessageIdentity(msg), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/messages/message/timeout");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnMessageTimeout} webhook subscription for identity {msg.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", msg.GetCallID(), ParameterType.QueryString);
            request.AddParameter("message", message, ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnMessageTimeout} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnMessageTimeout} failed with response {response.StatusCode}.");
            }
        }

        private async Task _sipServer_OnCallCancelled(SIPCall call, string reason)
        {
            if (!_OnCallCancelledSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/disconnected");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallFailed} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.DELETE);
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddParameter("reason", reason, ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallFailed} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallFailed} failed with response {response.StatusCode}.");
            }
        }

        private async Task _sipServer_OnCallTimeout(SIPCall call, string reason)
        {
            if (!_OnCallTimeoutSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/disconnected");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallFailed} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.DELETE);
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddParameter("reason", reason, ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallFailed} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallFailed} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallDisconnected(string callId, string identity, string reason)
        {
            if (!_OnCallDisconnectedSubscriptions.TryGetValue(identity, out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/disconnected");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallDisconnected} webhook subscription for identity {identity}.");
                    await Task.CompletedTask;
                    return;
                }
            }
            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.DELETE);
            request.AddParameter("id", callId, ParameterType.QueryString);
            request.AddParameter("reason", reason, ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallDisconnected} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallDisconnected} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallConnected(SIPCall call)
        {
            call.OnPlayDone += () => OnCallPlayDone(call);
            call.OnPlayError += (m) => OnCallPlayError(call, m);
            call.OnSpeakDone += () => OnCallSpeakDone(call);
            call.OnRecordDone += () => OnCallRecordDone(call);
            call.OnRecordError += (m) => OnCallRecordError(call, m);
            call.OnPlayFilesDone += () => OnCallPlayFilesDone(call);
            call.OnPlayRecordDone += () => OnCallPlayRecordDone(call);
            call.OnSendDigitsDone += () => OnCallSendDigitsDone(call);
            call.OnGetDigitsDone += () => OnCallGetDigitsDone(call);

            if (!_OnCallConnectedSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/connected");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallConnected} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            string[] sipHeaders = GetSipHeaders(call);
            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"), 
                                call.UserContext,
                                sipHeaders));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallConnected} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallConnected} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallPlayDone(SIPCall call)
        {
            if (!_OnCallPlayDoneSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/play/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallPlayDone} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                }
            }

            IRestClient client = new RestClient(uri);
            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));

            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallPlayDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallPlayDone} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallPlayError(SIPCall call, string message)
        {
            if (!_OnCallPlayErrorSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/play/error");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallPlayError} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                }
            }

            IRestClient client = new RestClient(uri);
            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddParameter("reason", message, ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallPlayError} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallPlayError} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallRecordDone(SIPCall call)
        {
            if (!_OnCallRecordDoneSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/record/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallRecordDone} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                }
            }
            
            IRestClient client = new RestClient(uri);

            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));

            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallRecordDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallRecordDone} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallRecordError(SIPCall call, string message)
        {
            if (!_OnCallRecordErrorSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/record/error");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallRecordError} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                }
            }

            IRestClient client = new RestClient(uri);

            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddParameter("reason", message, ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));

            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallRecordError} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallRecordError} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallSpeakDone(SIPCall call)
        {
            if (!_OnCallSpeakDoneSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/speak/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallSpeakDone} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallSpeakDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallSpeakDone} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallPlayFilesDone(SIPCall call)
        {
            if (!_OnCallPlayFilesDoneSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/playfiles/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallPlayRecordDone} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallPlayFilesDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallPlayFilesDone} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallPlayRecordDone(SIPCall call)
        {
            if (!_OnCallPlayRecordDoneSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/playrecord/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallPlayRecordDone} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallPlayRecordDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallPlayRecordDone} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallSendDigitsDone(SIPCall call)
        {
            if (!_OnCallSendDigitsDoneSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/dtmf/send/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallSendDigitsDone} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallSendDigitsDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallSendDigitsDone} failed with response {response.StatusCode}.");
            }
        }

        internal async Task OnCallPositiveAnswerMachineDetection(SIPCall call)
        {
            if (@_OnCallPositiveAnswerMachineDetectionSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/positiveanswermachinedetection");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallPositiveAnswerMachineDetection} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallPositiveAnswerMachineDetection} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallPositiveAnswerMachineDetection} failed with response {response.StatusCode}.");
            }
        }

        internal async Task OnCallPositiveFaxDetection(SIPCall call)
        {
            if (!_OnCallPositiveFaxDetectionSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/positivefaxdetection");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallPositiveFaxDetection} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallPositiveFaxDetection} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallPositiveFaxDetection} failed with response {response.StatusCode}.");
            }
        }

        internal async Task OnCallPositiveVoiceDetection(SIPCall call)
        {
            if (!_OnCallPositiveVoiceDetectionSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/positivevoicedetection");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallPositiveVoiceDetection} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallPositiveVoiceDetection} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallPositiveVoiceDetection} failed with response {response.StatusCode}.");
            }
        }

        internal async Task OnCallRingbackToneDetection(SIPCall call)
        {
            if (!_OnCallRingbackToneDetectionSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/ringbacktonedetection");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallRingbackToneDetection} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallRingbackToneDetection} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallRingbackToneDetection} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallGetDigitsDone(SIPCall call)
        {
            if (!_OnCallGetDigitsDoneSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/dtmf/get/done");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallGetDigitsDone} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddParameter("digits", call.GetDigitsBuffer(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallGetDigitsDone} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallGetDigitsDone} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallFailed(SIPCall call, string reason)
        {
            if (!_OnCallFailedSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/failed");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallFailed} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            string[] sipHeaders = GetSipHeaders(call);
            RestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddParameter("reason", reason, ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext,
                                sipHeaders));
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallFailed} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallFailed} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallDiverted(SIPCall call, string reason)
        {
            if (!_OnCallDivertedSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/diverted");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallDiverted} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddParameter("reason", reason, ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallDiverted} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallDiverted} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallRinging(SIPCall call)
        {
            if (!_OnCallRingingSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/ringing");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallRinging} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.PUT);
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallRinging} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallRinging} failed with response {response.StatusCode}.");
            }
        }

        private string[] GetSipHeaders(SIPCall call)
        {
            if (call.GetLastSIPRequest() == null)
                return new List<string>().ToArray();

            return call.GetLastSIPRequest().Header.ToString().Split(SIPConstants.CRLF);
        }

        private async Task OnCallAnswered(SIPCall call)
        {
            //if (_registeredScripts.TryGetValue(call.GetIdentity().ToString(), out var scriptCommands))
            //{
            //    _ = ScriptRunner.RunScript(call, scriptCommands);
            //    return;
            //} 
            
            foreach (var entry in _registeredScripts)
            {
                var keyUri = SIPURI.ParseSIPURI(entry.Key);
                if (call.GetToURI().User.StartsWith(keyUri.User) ||
                    call.GetIdentity() == entry.Key)
                {
                    _ = ScriptRunner.RunScript(call, entry.Value);
                    return;
                }
            }

            call.OnPlayDone += () => OnCallPlayDone(call);
            call.OnPlayError += (m) => OnCallPlayError(call, m);
            call.OnSpeakDone += () => OnCallSpeakDone(call);
            call.OnRecordDone += () => OnCallRecordDone(call);
            call.OnRecordError += (m) => OnCallRecordError(call, m);
            call.OnPlayFilesDone += () => OnCallPlayFilesDone(call);
            call.OnPlayRecordDone += () => OnCallPlayRecordDone(call);
            call.OnSendDigitsDone += () => OnCallSendDigitsDone(call);
            call.OnGetDigitsDone += () => OnCallGetDigitsDone(call);

            if (!_OnCallAnsweredSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/answered");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallAnswered} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            IRestClient client = new RestClient(uri);

            IRestRequest request = request = new RestRequest(Method.POST);
            string[] sipHeaders = GetSipHeaders(call);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddHeader("Accept", "application/json");
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                0,
                                0,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext, sipHeaders));

            IRestResponse response = await client.ExecuteAsync(request);

            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallAnswered} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallAnswered} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallAccepted(SIPCall call)
        {
            if (_sipServer.AutoAnswerCalls)
            {
                _logger.LogInformation($"Ignoring {EventUriType.OnCallAccepted} because auto answer is {_sipServer.AutoAnswerCalls}");
                return;
            }

            if (!_OnCallAcceptedSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/accepted");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallAccepted} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }
             
            IRestClient client = new RestClient(uri);

            IRestRequest request = request = new RestRequest(Method.POST);
            string[] sipHeaders = GetSipHeaders(call);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddHeader("Accept", "application/json");
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                0,
                                0,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext, sipHeaders));

            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallAccepted} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallAccepted} failed with response {response.StatusCode}.");
            }
        }

        private async Task OnCallOffered(SIPCall call)
        {
            if (_sipServer.AutoAcceptCalls)
            {
                _logger.LogInformation($"Ignoring {EventUriType.OnCallOffered} because auto accept is {_sipServer.AutoAcceptCalls}");
                return;
            }
            if (!_OnCallOfferedSubscriptions.TryGetValue(call.GetIdentity().ToString(), out var uri))
            {
                if (ClientUrl != "")
                {
                    uri = new Uri($"{ClientUrl}{ _apiPrefix}{_apiVersion}/calls/call/offered");
                }
                else
                {
                    _logger.LogDebug($"No {EventUriType.OnCallOffered} webhook subscription for identity {call.GetFromURI().ToString()}.");
                    await Task.CompletedTask;
                    return;
                }
            }

            string[] sipHeaders = GetSipHeaders(call);
            IRestClient client = new RestClient(uri);
            IRestRequest request = new RestRequest(Method.POST);
            request.JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddParameter("id", call.GetCallID(), ParameterType.QueryString);
            request.AddJsonBody(new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                call.GetCallDirection(),
                                0,
                                0,
                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                call.UserContext, sipHeaders));

            IRestResponse response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                _logger.LogDebug($"Webhook for {EventUriType.OnCallOffered} response {response.StatusCode}.");
            }
            else
            {
                _logger.LogWarning($"Webhook for {EventUriType.OnCallOffered} failed with response {response.StatusCode}.");
            }
        }

        public CallHistoryParams[] GetRecentCalls()
        {
            string file = Program.Config["storagePath"] + $"logs/cdr-{DateTime.Now.ToString("yyyyMMdd")}.log";
            FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            var sr = new StreamReader(fs);
            var lineCount = 0;
            while (sr.ReadLine() != null) lineCount++;

            CallHistoryParams[] calls = new CallHistoryParams[lineCount];
            var line = "";
            var i = 0;
            fs.Position = 0;
            while ((line = sr.ReadLine()) != null)
            {
                dynamic cdr = JObject.Parse(line);
                calls[i++] = new CallHistoryParams(cdr.callId.Value, cdr.from.Value, cdr.to.Value,
                                                   cdr.createdTime.Value, cdr.calldirection.Value, cdr.callDuration.Value);
            }

            sr.Close();
            fs.Close();

            return calls;
        }

        public StatsParams[] GetCallsStats()
        {
            StatsParams[] graphdata = new StatsParams[24];
            for (int i = 0; i < 24; i++) graphdata[i] = new StatsParams($"{string.Format("{0:D2}:00", i)}", 0);

            string[] files = Directory.GetFiles(Program.Config["storagePath"], $"logs/cdr-{DateTime.Now.ToString("yyyyMMdd")}.log", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var sr = new StreamReader(fs);
                var line = String.Empty;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Contains("\"type\":\"call\""))
                    {
                        SIPCallDataRecord cdr = global::System.Text.Json.JsonSerializer.Deserialize<SIPCallDataRecord>(line);
                        //graphdata[cdr.Created.Hour] = new StatsParams(graphdata[cdr.Created.Hour].time, graphdata[cdr.Created.Hour].amount + (int)double.Parse(cdr.callDuration));
                        graphdata[cdr.Created.Hour] = new StatsParams(graphdata[cdr.Created.Hour].time, graphdata[cdr.Created.Hour].amount + (int)(double.Parse(cdr.callDuration) / 60));
                    }
                }
                sr.Close();
                fs.Close();
            }

            return (graphdata);
        }

        public StatsParams[] GetInstantMessagesStats()
        {
            StatsParams[] graphdata = new StatsParams[24];
            for (int i = 0; i < 24; i++) graphdata[i] = new StatsParams($"{string.Format("{0:D2}:00", i)}", 0);

            string[] files = Directory.GetFiles(Program.Config["storagePath"], $"logs/cdr-{DateTime.Now.ToString("yyyyMMdd")}.log", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var sr = new StreamReader(fs);
                var line = String.Empty;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Contains("\"type\":\"message\""))
                    {
                        SIPMessageDataRecord cdr = global::System.Text.Json.JsonSerializer.Deserialize<SIPMessageDataRecord>(line);
                        graphdata[cdr.Created.Hour] = new StatsParams(graphdata[cdr.Created.Hour].time, graphdata[cdr.Created.Hour].amount + 1);
                    }
                }
                sr.Close();
                fs.Close();
            }

            return (graphdata);
        }

        public StatsParams[] GetTextMessagesStats()
        {
            StatsParams[] graphdata = new StatsParams[24];
            for (int i = 0; i < 24; i++) graphdata[i] = new StatsParams($"{string.Format("{0:D2}:00", i)}", 0);

            string[] files = Directory.GetFiles(Program.Config["storagePath"], $"logs/cdr-{DateTime.Now.ToString("yyyyMMdd")}.log", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var sr = new StreamReader(fs);
                var line = String.Empty;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Contains("\"type\":\"text\""))
                    {
                        TextMessageDataRecord cdr = global::System.Text.Json.JsonSerializer.Deserialize<TextMessageDataRecord>(line);
                        graphdata[cdr.Created.Hour] = new StatsParams(graphdata[cdr.Created.Hour].time, graphdata[cdr.Created.Hour].amount + 1);
                    }
                }
                sr.Close();
                fs.Close();
            }

            return (graphdata);
        }

        public FilesParams[] GetClips()
        {
            return GetFileListing("clips");
        }

        public bool DelClip(string filename)
        {
            try
            {
                if (File.Exists($"{Program.Config["storagePath"]}{filename}"))
                {
                    File.Delete($"{Program.Config["storagePath"]}{filename}");
                    return true;
                }
            }
            catch
            {

            }

            return false;
        }

        public FilesParams[] GetLogs()
        {
            return GetFileListing("logs");
        }

        public bool DelLog(string filename)
        {
            try
            {
                if (File.Exists($"{Program.Config["storagePath"]}{filename}"))
                {
                    File.Delete($"{Program.Config["storagePath"]}{filename}");
                    return true;
                }
            }
            catch
            {

            }

            return false;
        }

        public FilesParams[] GetFileListing(string path)
        {
            string[] files = Directory.GetFiles(Program.Config["storagePath"], $"{path}/*", SearchOption.AllDirectories);
            FilesParams[] fileParams = new FilesParams[files.Length];

            for (int i = 0; i < files.Length; i++)
            {
                var fileInfo = new FileInfo(files[i]);
                var fileName = fileInfo.Name.Split(".")[0];
                if (fileName.Length > 21)
                    fileName = fileName.Substring(0, 18) + "...";
                fileParams[i] = new FilesParams(i, $"/{path}/{fileInfo.Name}", fileName, fileInfo.CreationTime.ToString(),
                     fileInfo.LastAccessTime.ToString(), fileInfo.Extension.Replace(".",""), fileInfo.Length);                
            }

            return fileParams;
        }

        public bool Stop(HostControl hostControl)
        {
            _logger.LogInformation($"Stopping service {Program.Config.GetValue("instance", "SIPServerService#1")}...");

            _ = SendApplicationStop();

            if (_sipServer != null)
            {
                if (_sipServer.State == SIPServerState.Running)
                {
                    _ = _sipServer.Shutdown();
                    while (_sipServer.State != SIPServerState.NotStarted)
                    {
                        Task.Delay(500);
                    }
                }
            }

            return true;
        }

        public bool Restart(bool force)
        {
            var res = _sipServer.Shutdown(force);
            if (res)
            {
                Task.Run(() =>
                {
                    while (_sipServer.State != SIPServerState.NotStarted)
                    {
                        Task.Delay(500);
                    }
                    _ = StartSIPServer();
                });
            }

            return res;
        }
    }
}