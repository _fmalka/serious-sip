﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.PlatformAbstractions;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Authorization.Policy;
using System.Threading.Tasks;
using System.Security.Claims;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Logging;
using Serilog;

namespace SeriousSIP
{
    class SIPServerRestService 
    {
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseContentRoot(Program.Config["storagePath"]);
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls(Program.Config["useUrls"]);
                });

        public void Start()
        {
            CreateHostBuilder(null).Build().Run();
        }
    }

    internal class DisableAuthenticationPolicyEvaluator : IPolicyEvaluator
    {
        public async Task<AuthenticateResult> AuthenticateAsync(AuthorizationPolicy policy, HttpContext context)
        {
            // Always pass authentication.
            var authenticationTicket = new AuthenticationTicket(new ClaimsPrincipal(), new AuthenticationProperties(), JwtBearerDefaults.AuthenticationScheme);
            return await Task.FromResult(AuthenticateResult.Success(authenticationTicket));
        }

        public async Task<PolicyAuthorizationResult> AuthorizeAsync(AuthorizationPolicy policy, AuthenticateResult authenticationResult, HttpContext context, object resource)
        {
            // Always pass authorization
            return await Task.FromResult(PolicyAuthorizationResult.Success());
        }
    }

    /// <summary>
    /// Web host configuration builder.
    /// </summary>
    public class Startup
    {
        private string _apiVersion = Program.ApiVersion;
        private string _apiPrefix = Program.ApiPrefix;
        private string _apiName = Program.ApiName;

        /// <summary>
        /// Startup web server
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Keeps the configuration of the web server
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="s">Collection of services.</param>
        public void ConfigureServices(IServiceCollection s)
        {
            s.AddLogging(configure => configure.AddSerilog());
            
            s.AddControllers(o =>
            {
                o.Conventions.Add(new ControllerDocumentationConvention());
            })
            .AddJsonOptions(x =>
            {
                x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
            s.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = $"{Program.Config["storagePath"]}gui";
                //configuration.RootPath = "C:\\Users\\User\\source\\repos\\serious-sip\\SIPServerService\\gui\\build";
            });

            if (Program.Config["oAuthServiceUrl"] != null)
            {
                s.AddAuthentication("Bearer")
                        .AddIdentityServerAuthentication(options =>
                        {
                            options.Authority = Program.Config["oAuthServiceUrl"];
                            options.RequireHttpsMetadata = false;
                            options.ApiName = _apiName;
                        });
            }
            else if (Program.Config.GetSection("usePrivateToken")["secret"] != null)
            {
                var jwtTokenConfig = Program.Config.GetSection("usePrivateToken").Get<JwtTokenConfig>();
                if (jwtTokenConfig != null)
                {
                    s.AddSingleton(jwtTokenConfig);
                    s.AddAuthentication(x =>
                    {
                        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                        x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    }).AddJwtBearer(x =>
                    {
                        x.RequireHttpsMetadata = false;
                        x.SaveToken = true;
                        x.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer = jwtTokenConfig.Issuer,
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtTokenConfig.Secret)),
                            ValidAudience = jwtTokenConfig.Audience,
                            ValidateAudience = false,
                            ValidateLifetime = false,
                            ClockSkew = TimeSpan.FromMinutes(1)
                        };
                    });
                    s.AddSingleton<IJwtAuthManager, JwtAuthManager>();
                    s.AddHostedService<JwtRefreshTokenCache>();
                }
            } else
            {
                s.RemoveAll<IPolicyEvaluator>();
                s.AddSingleton<IPolicyEvaluator, DisableAuthenticationPolicyEvaluator>();
            }

            s.AddAuthorization(options =>
            {
                options.AddPolicy("keypad-system", policy =>
                {
                   policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "keypad-system");
                });
                options.AddPolicy("keypad-bridges", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "keypad-bridges");
                });
                options.AddPolicy("keypad-calls", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "keypad-calls");
                });
                options.AddPolicy("keypad-contacts", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "keypad-contacts");
                });
                options.AddPolicy("keypad-messages", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "keypad-messages");
                });
            });

            s.AddMvc(options => options.EnableEndpointRouting = false);
            s.AddMvcCore().AddApiExplorer();
            s.AddSwaggerGen(c =>
            {
                if (Program.Config["oAuthServiceUrl"] != null)
                {
                    var oAuthSecurityScheme = new OpenApiSecurityScheme
                    {
                        Scheme = "bearer",
                        BearerFormat = "JWT",
                        Name = "JWT Authentication",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.OAuth2,
                        Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",
                        OpenIdConnectUrl = new Uri($"{Program.Config["oAuthServiceUrl"]}/.well-known/openid-configuration"),
                        Flows = new OpenApiOAuthFlows
                        {
                            ClientCredentials = new OpenApiOAuthFlow
                            {
                                AuthorizationUrl = new Uri($"{Program.Config["oAuthServiceUrl"]}/connect/authorize"),
                                TokenUrl = new Uri($"{Program.Config["oAuthServiceUrl"]}/connect/token"),
                                Scopes = new Dictionary<string, string>
                            {
                                { "keypad-system", "Keypad administration functions" },
                                { "keypad-bridges", "Keypad bridged calls functions" },
                                { "keypad-calls", "Keypad call and media functions" },
                                { "keypad-contacts", "Keypad contact management functions" },
                                { "keypad-messages", "Keypad instant and text messaging functions" }
                            }
                            }
                        },
                        Reference = new OpenApiReference
                        {
                            Id = JwtBearerDefaults.AuthenticationScheme,
                            Type = ReferenceType.SecurityScheme
                        }
                    };
                    c.AddSecurityDefinition(oAuthSecurityScheme.Reference.Id, oAuthSecurityScheme);
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        { oAuthSecurityScheme, Array.Empty<string>() }
                    });
                }
                else if (Program.Config.GetSection("usePrivateToken")["secret"] != null)
                {

                    var jwtTokenConfig = Program.Config.GetSection("usePrivateToken").Get<JwtTokenConfig>();
                    if (jwtTokenConfig != null)
                    {
                        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                        {
                            Name = "Authorization",
                            Type = SecuritySchemeType.ApiKey,
                            Scheme = "Bearer",
                            BearerFormat = "JWT",
                            In = ParameterLocation.Header,
                            Description = "Enter 'Bearer' [space] and then your valid token in the text input below.\r\n\r\nExample: \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\"",
                        });
                        c.AddSecurityRequirement(new OpenApiSecurityRequirement
                        {
                            {
                                  new OpenApiSecurityScheme
                                    {
                                        Reference = new OpenApiReference
                                        {
                                            Type = ReferenceType.SecurityScheme,
                                            Id = "Bearer"
                                        }
                                    },
                                    new string[] {}

                            }
                        });
                    }
                }

                c.SwaggerDoc(_apiVersion, new OpenApiInfo
                {
                    Title = _apiName,
                    Version = _apiVersion,
                    Description = "A suite of communication APIs for web and enterprise applications",
                    TermsOfService = new Uri("https://modulo.co.il/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Franck Malka",
                        Email = "fmalka@modulo.co.il"
                    },
                });
            });

            s.ConfigureSwaggerGen(options => {
                //Determine base path for the application.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;

                if (Program.Config.GetSection("usePrivateToken")["secret"] == null)
                    options.DocumentFilter<SwaggerFilterOutControllers>();

                //Set the comments path for the swagger json and ui.
                options.IncludeXmlComments(basePath + "SIPServerService.xml");
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();

            if ((Program.Config["oAuthServiceUrl"] != null) ||
                (Program.Config.GetSection("usePrivateToken")["secret"] != null))
            {
                app.UseAuthentication();
                app.UseAuthorization();
            }

            if (Program.Config.GetValue("useHttpsRedirection", false))
            {
                app.UseHttpsRedirection();
            }

            if (Program.Config.GetValue("allowOrigins", "") != "")
            {
                app.UseCors(builder => builder.WithOrigins(Program.Config.GetValue("allowOrigins", ""))
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            // Set up custom content types -associating file extension to MIME type
            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".pcm"] = "audio/PCMA";
            provider.Mappings[".wav"] = "audio/x-wav";
            provider.Mappings[".log"] = "text/plain";

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
           Path.Combine(Directory.GetCurrentDirectory(), @"clips")),
                RequestPath = new PathString("/clips"),
                ContentTypeProvider = provider
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
           Path.Combine(Directory.GetCurrentDirectory(), @"logs")),
                RequestPath = new PathString("/logs"),
                ContentTypeProvider = provider
            });
            //app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            //{
            //    FileProvider = new PhysicalFileProvider(
            //        Path.Combine(Directory.GetCurrentDirectory(), @"clips")),
            //    RequestPath = new PathString("/clips")
            //});
            //app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            //{
            //    FileProvider = new PhysicalFileProvider(
            //        Path.Combine(Directory.GetCurrentDirectory(), @"logs")),
            //    RequestPath = new PathString("/logs")
            //});

            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();
            app.UseMvc(routes =>
            {
                if (Program.Config.GetSection("usePrivateToken")["secret"] != null)
                {
                    var jwtTokenConfig = Program.Config.GetSection("usePrivateToken").Get<JwtTokenConfig>();
                    if (jwtTokenConfig != null)
                    {
                        // authenticator routes
                        routes.MapRoute(
                                        $"{ _apiPrefix}{_apiVersion}/aauth/connect",
                                        "{controller=AAuth}/{action=Connect}");
                        routes.MapRoute(
                                        $"{ _apiPrefix}{_apiVersion}/aauth/disconnect",
                                        "{controller=AAuth}/{action=Disconnect}");
                        routes.MapRoute(
                                        $"{ _apiPrefix}{_apiVersion}/aauth/refresh-token",
                                        "{controller=AAuth}/{action=RefreshToken}");
                    }
                }

                // account routes
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/admin/config",
                    "{controller=Admin}/{action=Config}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/admin/identities",
                    "{controller=Admin}/{action=GetIdentities}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/admin/identity/script/",
                    "{controller=Admin}/{action=Script}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/files/clips",
                    "{controller=Admin}/{action=Clips}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/files/logs",
                    "{controller=Admin}/{action=Logs}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/status",
                    "{controller=Admin}/{action=Status}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/start",
                    "{controller=Admin}/{action=Start}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/restart",
                    "{controller=Admin}/{action=Restart}/{force}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/shutdown",
                    "{controller=Admin}/{action=Shutdown}/{force}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/admin/system/cleanup",
                    "{controller=Admin}/{action=Cleanup}");

                // bridges routes
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/bridges/subscribe",
                    "{controller=Bridges}/{action=Subscribe}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/bridges/unsubscribe",
                    "{controller=Bridges}/{action=Unsubscribe}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/bridges",
                    "{controller=Bridges}/{action=GetBridges}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/bridges/bridge",
                    "{controller=Bridges}/{action=Bridge}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/bridges/terminate",
                    "{controller=Bridges}/{action=Terminate}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/bridges/bridge/play",
                    "{controller=Bridges}/{action=Play}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/bridges/bridge/play/stop",
                    "{controller=Bridges}/{action=PlayStop}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/bridges/bridge/playfiles",
                    "{controller=Bridges}/{action=PlayFiles}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/bridges/bridge/playfiles/stop",
                    "{controller=Bridges}/{action=PlayStop}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/bridges/bridge/record",
                    "{controller=Bridges}/{action=Record}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/bridges/bridge/record/stop",
                    "{controller=Bridges}/{action=PlayStop}/{id}");

                // calls routes
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/calls/options",
                    "{controller=Calls}/{action=Options}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/calls/statistics",
                    "{controller=Calls}/{action=Stats}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/calls/subscribe",
                    "{controller=Calls}/{action=Subscribe}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/calls/unsubscribe",
                    "{controller=Calls}/{action=Unsubscribe}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/calls",
                    "{controller=Calls}/{action=GetCalls}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/calls/recent",
                    "{controller=Calls}/{action=GetRecentCalls}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call",
                    "{controller=Calls}/{action=GetCall}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/state",
                    "{controller=Calls}/{action=GetCallState}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/action",
                    "{controller=Calls}/{action=GetCallAction}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/duration",
                    "{controller=Calls}/{action=GetCallDuration}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/make",
                    "{controller=Calls}/{action=MakeCall}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/accept",
                    "{controller=Calls}/{action=AcceptCall}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/answer",
                    "{controller=Calls}/{action=AnswerCall}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/drop",
                    "{controller=Calls}/{action=DropCall}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/hold",
                    "{controller=Calls}/{action=Hold}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/retrieve",
                    "{controller=Calls}/{action=Retrieve}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/transfer/blind",
                    "{controller=Calls}/{action=BlindTransfer}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/transfer/attended",
                    "{controller=Calls}/{action=AttendedTransfer}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/playloop",
                    "{controller=Calls}/{action=PlayLoop}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/play",
                    "{controller=Calls}/{action=Play}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/play/music",
                    "{controller=Calls}/{action=PlayMusic}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/play/stop",
                    "{controller=Calls}/{action=PlayStop}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/playfiles",
                    "{controller=Calls}/{action=PlayFiles}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/playfiles/stop",
                    "{controller=Calls}/{action=PlayStop}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/record",
                    "{controller=Calls}/{action=Record}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/record/stop",
                    "{controller=Calls}/{action=PlayStop}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/playrecord",
                    "{controller=Calls}/{action=PlayRecord}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/playrecord/stop",
                    "{controller=Calls}/{action=PlayStop}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/speak",
                    "{controller=Calls}/{action=Speak}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/speakssml",
                    "{controller=Calls}/{action=SpeakSsml}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/dtmf/get",
                    "{controller=Calls}/{action=GetDigits}/{id}");
                routes.MapRoute(
                    $"{_apiPrefix}{_apiVersion}/calls/call/dtmf/send",
                    "{controller=Calls}/{action=SendDigits}/{id}/{digits}");

                // messages routes
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/messages/subscribe",
                    "{controller=Messages}/{action=Subscribe}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/messages/unsubscribe",
                    "{controller=Messages}/{action=Unsubscribe}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/messages/message/statistics",
                    "{controller=Messages}/{action=InstantMessagesStats}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/messages/text/statistics",
                    "{controller=Messages}/{action=TextMessagesStats}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/messages/message/send",
                    "{controller=Messages}/{action=SendSimple}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/messages/message/ack",
                    "{controller=Messages}/{action=AckSimple}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/messages/text/send",
                    "{controller=Messages}/{action=SendText}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/messages/text/send",
                //    "{controller=Messages}/{action=SendText}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/messages/text/ack",
                //    "{controller=Messages}/{action=AckText}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/messages/voice/send",
                //    "{controller=Messages}/{action=SendVoice}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/messages/voice/ack",
                //    "{controller=Messages}/{action=AckVoice}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/messages/fax/send",
                //    "{controller=Messages}/{action=SendFax}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/messages/fax/ack",
                //    "{controller=Messages}/{action=AckFax}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/messages/email/send",
                //    "{controller=Messages}/{action=SendEMail}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/messages/email/ack",
                //    "{controller=Messages}/{action=AckEMail}");

                // contacts routes
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/contacts/subscribe",
                    "{controller=Contacts}/{action=Subscribe}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/contacts/unsubscribe",
                   "{controller=Contacts}/{action=Unsubscribe}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/contacts",
                    "{controller=Contacts}/{action=GetContacts}");
                routes.MapRoute(
                    $"{ _apiPrefix}{_apiVersion}/contacts/contact",
                    "{controller=Contacts}/{action=Contact}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/contacts/contact/{id}",
                //    "{controller=Contacts}/{action=UpdateContact}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/contacts/contact/{id}",
                //    "{controller=Contacts}/{action=GetContact}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/contacts/contact/{id}/presence",
                //    "{controller=Contacts}/{action=GetContactPresence}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/contacts/contact/{id}/page",
                //    "{controller=Contacts}/{action=PageContact}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/contacts/contact/{id}/call",
                //    "{controller=Contacts}/{action=CallContact}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/groups/group/add",
                //    "{controller=Contacts}/{action=AddGroup}");
                //    $"{ _apiPrefix}{_apiVersion}/groups/group/remove",
                //    "{controller=Contacts}/{action=RemoveGroup}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/groups/group/{id}/contacts/contact/add",
                //    "{controller=Contacts}/{action=AddContactToGroup}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/groups/group/{id}/contacts/",
                //    "{controller=Contacts}/{action=GetAllContacts}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/groups/group/{id}/contacts/online",
                //    "{controller=Contacts}/{action=GetOnlineContacts}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/groups/group/{id}/contacts/contact/remove",
                //    "{controller=Contacts}/{action=RemoveContactFromGroup}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/groups/group/{id}/page",
                //    "{controller=Contacts}/{action=PageGroup}");
                //routes.MapRoute(
                //    $"{ _apiPrefix}{_apiVersion}/groups/group/{id}/call",
                //    "{controller=Contacts}/{action=CallGroup}");


            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"{_apiVersion}/swagger.json", _apiName);
            });
            app.UseSpa(spa =>
            {
               spa.Options.SourcePath = $"{Program.Config["storagePath"]}..\\gui";

                //Remove for prod
                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }

            });
            
        }
    }

    internal class SwaggerFilterOutControllers : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var remoteRoutes = swaggerDoc.Paths
                .Where(x => x.Key.ToLower().Contains("aauth"))
                .ToList();
            remoteRoutes.ForEach(x => { swaggerDoc.Paths.Remove(x.Key); });
        }

    }

    internal class ControllerDocumentationConvention : IControllerModelConvention
    {
        void IControllerModelConvention.Apply(ControllerModel controller)
        {
            if (controller == null)
                return;

            foreach (var attribute in controller.Attributes)
            {
                if (attribute.GetType() == typeof(RouteAttribute))
                {
                    var routeAttribute = (RouteAttribute)attribute;
                    if (!string.IsNullOrWhiteSpace(routeAttribute.Name))
                        controller.ControllerName = routeAttribute.Name;
                }
            }

        }
    }
}

