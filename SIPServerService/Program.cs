﻿using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using System;
using System.IO;
using Topshelf;
using System.Text;
using System.Runtime.InteropServices;
using Topshelf.Runtime.DotNetCore;

namespace SeriousSIP
{
    class Program
    {
        public const string ApiVersion = "v1";
        public const string ApiPrefix = "/api/";
        public const string ApiName = "keypad";
        public const string Key = "cqnjj@XdC5Q.qfRz";
        public static IConfiguration Config { get; private set; } = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", false, true)
                        .Build();

        static void Main(string[] args)
        {
            if (args.Length > 3 && args[0] == "--generate-key")
            {
                GenerateKey(args[1], args[2], int.Parse(args[3]), args[4]);
            }
            else
            {
                HostFactory.Run(x =>
                {
                    if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ||
                        RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                    {
                        x.UseEnvironmentBuilder(
                        target => new DotNetCoreEnvironmentBuilder(target));
                    }
                    x.Service<SIPServerService>();
                    x.EnableServiceRecovery(r => r.RestartService(TimeSpan.FromSeconds(10)));
                    x.SetServiceName(Config["instance"]);
                    x.StartAutomatically();
                });
            }
        }

        static void GenerateKey(string instance, string ip, int days, string key)
        {
            if (key == Key)
            {
                string s = instance + ";" + ip + ";" + DateTime.Now.AddDays(days);
                Console.WriteLine($"product key: {EncryptString(Key, s)}");
            }
        }

        public static string EncryptString(string key, string plainText)
        {
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }

        public static string DecryptString(string key, string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }
    }
}
    