import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ThemeProvider } from '@material-ui/styles';
import { IconButton } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import { display } from '@material-ui/system';
import Fab from '@material-ui/core/Fab';

function createSmppParams(systemId, password, host, port, systemType, serviceType, sourceAddress) {
    return { systemId, password, host, port, systemType, serviceType, sourceAddress };
}
                                                
function createSipParams(acceptDelay, autoAcceptCalls, autoAnswerCalls, codecs, dtmfMode, hangupWhenNoRtp, localIPEndPoints, monitorTrunks, registerAccounts, registrationInterval, rtpAddress, useMultiMediaTimers) {
    return { acceptDelay, autoAcceptCalls, autoAnswerCalls, codecs, dtmfMode, hangupWhenNoRtp, localIPEndPoints, monitorTrunks, registerAccounts, registrationInterval, rtpAddress, useMultiMediaTimers };
}

function createServiceParams(instanceName, clientUrl, autoStartSipServer) {
    return { instanceName, clientUrl, autoStartSipServer };
}

function createConfig(serviceParams, sipParams, smppParams) {
    return { serviceParams, sipParams, smppParams };
}

const defaultConfig = createConfig(
    createServiceParams("not set", "not set", true), 
    createSipParams(0, true, true, [""], "Rfc2833", true, [""], [""], [""], 250, "", true),
    createSmppParams("", "", "", 2775, "", "", "")
);

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#054f8d',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            //light: '#054f8d',
            main: '#054f8d',
            // dark: will be calculated from palette.secondary.main,
            contrastText: '#ffcc00',
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});

export default function Settings(props) {
    const classes = useStyles();
    const fixedHeightPaperGeneral = clsx(classes.paper, classes.fixedHeightGeneral);
    const fixedHeightPaperSip = clsx(classes.paper, classes.fixedHeightSip);
    const fixedHeightPaperSmpp = clsx(classes.paper, classes.fixedHeightSmpp);
    const [config, setConfig] = useState(defaultConfig);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [dialogSavedOpen, setDialogSavedOpen] = useState(false);
    const [param, setParam] = useState('');
    const [value, setValue] = useState('');
    const [sipOn, setSipOn] = useState(true);
    const [localEndPoints, setLocalEndPoints] = useState([""]);
    const [localIdentities, setLocalIdentities] = useState([""]);
    const [codecs, setCodecs] = useState([""]);

    const handleDialogSavedOpen = () => {
        setDialogSavedOpen(true);
    };

    const handleDialogSavedClose = () => {
        setDialogSavedOpen(false);
    };

    const handleDialogOpen = () => {
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    const handleDialogDenied = () => {
        handleDialogClose();
    };

    const handleDialogConfirmed = () => {

        if (config.smppParams[param] != '')
            config.smppParams[param] = value;
        else
            config[param] = value;

        handleDialogClose();
    };

    const handleEditParam = (param, value, e) => {
        setParam(param);
        setValue(value);
        handleDialogOpen();
    };

    const handleToggleSipOn = () => {
        setSipOn(!sipOn);
        config.serviceParams.autoStartSipServer = !config.serviceParams.autoStartSipServer;
    }

    const handleRevertChanges = () => {
        let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
        if (props.apiKey != null)
            requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
        fetch('/api/v1/admin/config', requestOptions)
            .then(response => response.json())
            .then((cfg) => {
                if ((cfg == null) || (cfg.data == null))
                    setConfig(defaultConfig);
                else {
                    console.log(cfg.data);
                    setConfig(cfg.data);
                    setLocalEndPoints(cfg.data.sipParams.localIpEndPoints);
                    setLocalIdentities(cfg.data.sipParams.registerAccounts);
                    setCodecs(cfg.data.sipParams.codecs);
                }
            });
    }

    const handleApplyChanges = () => {
        let requestOptions = { headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, method: "PUT" , body: config};
        if (props.apiKey != null)
            requestOptions = { headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "PUT", body: config };

        fetch('/api/v1/admin/config', requestOptions)
            .then(response => {
                if (response.status == 200) {
                    handleDialogSavedOpen();
                }
            });
    }

    useEffect(() => {
        handleRevertChanges();
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <Dialog
                open={dialogSavedOpen}
                onClose={handleDialogSavedClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"Done"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Changes has been saved and will be applied at next restart.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDialogSavedClose} color="primary" autoFocus>
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={dialogOpen}
                onClose={handleDialogClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"Edit"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <TextField
                            variant="outlined"
                            required
                            id="new_value"
                            defaultValue={value}
                            onChange={event => { setValue(event.target.value) }}
                            style={{ width: 500 }}
                            label={param}
                            name="edit"
                            autoFocus
                        />
                            </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDialogDenied} color="primary">
                        Cancel
                        </Button>
                    <Button onClick={handleDialogConfirmed} color="primary" autoFocus>
                        Save
                            </Button>
                </DialogActions>
            </Dialog>
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <Paper className={fixedHeightPaperGeneral}>
                        <Title>Service</Title>
                        <Table size="small" stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Parameter</TableCell>
                                    <TableCell>Value</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Instance name</TableCell>
                                    <TableCell className={classes.smallFont}>{config.serviceParams.instanceName}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('Instance name', config.serviceParams.instanceName, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Default application URL (Optional)</TableCell>
                                    <TableCell className={classes.smallFont}>{config.serviceParams.clientUrl}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('Default application URL', config.serviceParams.clientUrl, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Auto start SIP Server</TableCell>
                                    <TableCell className={classes.smallFont}>
                                    </TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <Switch
                                            disabled="true"
                                            checked={config.serviceParams.autoStartSipServer}
                                            onChange={handleToggleSipOn}
                                            color="primary"
                                            name="sipOn"
                                            size="small"
                                            inputProps={{ 'aria-label': 'primary checkbox' }}
                                        />
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={fixedHeightPaperSip}>
                        <Title>Sip</Title>
                        <Table size="small" stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Parameter</TableCell>
                                    <TableCell>Value</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Local addresses</TableCell>
                                    <TableCell className={classes.smallFont}>
                                        <Table size="small" stickyHeader aria-label="sticky table">
                                            <TableBody>
                                                {localEndPoints.map((row) => (
                                                  <TableRow>
                                                      <TableCell className={classes.smallFont}>{row}</TableCell>
                                                      <TableCell align="right" className={classes.smallFont}>
                                                            <IconButton
                                                                disabled="true"
                                                          aria-label="more"
                                                          aria-controls="long-menu"
                                                          aria-haspopup="true"
                                                          size="small"
                                                          onClick={e => handleEditParam('localIPEndPoint', localEndPoints[row.id], e)}>
                                                          <EditIcon />
                                                      </IconButton>
                                                      </TableCell>
                                                  </TableRow>
                                              ))}
                                            </TableBody>
                                        </Table>
                                    </TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <Fab
                                            disabled="true"
                                            color="primary"
                                            aria-label="Add"
                                            size="small">
                                            <AddIcon />
                                        </Fab>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Local identities</TableCell>
                                    <TableCell className={classes.smallFont}>
                                        <Table size="small" stickyHeader aria-label="sticky table" xs={12}>
                                            <TableBody>
                                                {localIdentities.map((row) => (
                                                  <TableRow>
                                                      <TableCell className={classes.smallFont}>{row}</TableCell>
                                                      <TableCell align="right" className={classes.smallFont}>
                                                            <IconButton
                                                                disabled="true"
                                                          aria-label="more"
                                                          aria-controls="long-menu"
                                                          aria-haspopup="true"
                                                          size="small"
                                                          onClick={e => handleEditParam('localIdentitiy', localIdentities[row.id], e)}>
                                                          <EditIcon />
                                                      </IconButton>
                                                      </TableCell>
                                                  </TableRow>
                                                ))}
                                            </TableBody>
                                         </Table>
                                    </TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <Fab
                                            disabled="true"
                                            color="primary"
                                            aria-label="Add"
                                            size="small">
                                            <AddIcon />
                                        </Fab>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Supported codecs</TableCell>
                                    <TableCell className={classes.smallFont}>
                                            <Table size="small" stickyHeader aria-label="sticky table">
                                                <TableBody>
                                                    {codecs.map((row) => (
                                                        <TableRow>
                                                            <TableCell className={classes.smallFont}>{row}</TableCell>
                                                            <TableCell align="right" className={classes.smallFont}>
                                                                <Switch
                                                                    disabled="true"
                                                                    checked={config.serviceParams.autoStartSipServer}
                                                                    onChange={handleToggleSipOn}
                                                                    color="primary"
                                                                    name="sipOn"
                                                                    size="small"
                                                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                                                />
                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>    
                                    </TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>DTMF support</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sipParams.dtmfMode}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('dtmfMode', config.sipParams.dtmfMode, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>RTP Address</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sipParams.rtpAddress}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('RTP Address', config.sipParams.rtpAddress, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Accept delay</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sipParams.acceptDelay}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('RTP Address', config.sipParams.acceptDelay, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Register interval</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sipParams.registrationInterval}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('RTP Address', config.sipParams.registrationInterval, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Auto accept calls</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sipParams.autoAcceptCalls}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <Switch
                                            disabled="true"
                                            checked={config.sipParams.autoAcceptCalls}
                                            onChange={handleToggleSipOn}
                                            color="primary"
                                            name="sipOn"
                                            size="small"
                                            inputProps={{ 'aria-label': 'primary checkbox' }}
                                        />
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Auto answer calls</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sipParams.autoAnswerCalls}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <Switch
                                            disabled="true"
                                            checked={config.sipParams.autoAnswerCalls}
                                            onChange={handleToggleSipOn}
                                            color="primary"
                                            name="sipOn"
                                            size="small"
                                            inputProps={{ 'aria-label': 'primary checkbox' }}
                                        />
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Use multimedia timers</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sipParams.useMultiMediaTimers}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <Switch
                                            disabled="true"
                                            checked={config.sipParams.useMultiMediaTimers}
                                            onChange={handleToggleSipOn}
                                            color="primary"
                                            name="sipOn"
                                            size="small"
                                            inputProps={{ 'aria-label': 'primary checkbox' }}
                                        />
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Hangup when no RTP</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sipParams.hangupWhenNoRtp}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <Switch
                                            disabled="true"
                                            checked={config.sipParams.hangupWhenNoRtp}
                                            onChange={handleToggleSipOn}
                                            color="primary"
                                            name="sipOn"
                                            size="small"
                                            inputProps={{ 'aria-label': 'primary checkbox' }}
                                        />
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                </Paper>
            </Grid>
            <Grid item xs={12}>
                <Paper className={fixedHeightPaperSmpp}>
                        <Title>Smpp</Title>
                        <Table size="small" stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Parameter</TableCell>
                                    <TableCell>Value</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>SystemId</TableCell>
                                    <TableCell className={classes.smallFont}>{config.systemId}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('systemId', config.smppParams.systemId, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Password</TableCell>
                                    <TableCell className={classes.smallFont}>{config.password}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('password', config.smppParams.password, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Server address</TableCell>
                                    <TableCell className={classes.smallFont}>{config.host}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('host', config.smppParams.host, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Server port</TableCell>
                                    <TableCell className={classes.smallFont}>{config.port}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('port', config.smppParams.port, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>System type</TableCell>
                                    <TableCell className={classes.smallFont}>{config.systemType}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('systemType', config.smppParams.systemType, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Service type</TableCell>
                                    <TableCell className={classes.smallFont}>{config.serviceType}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('serviceType', config.smppParams.serviceType, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell className={classes.smallFont}>Source address</TableCell>
                                    <TableCell className={classes.smallFont}>{config.sourceAddress}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            disabled="true"
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleEditParam('sourceAddress', config.smppParams.sourceAddress, e)}>
                                            <EditIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </Paper>
                </Grid>

                <Grid item xs={6}>
                    <Button
                        variant="contained"
                        disabled="true"
                    color="primary"
                    size="large"
                    onClick={handleRevertChanges}
                    className={classes.submit}>Revert</Button>
                </Grid>
                <Grid align="right" item xs={6}>
                    <Button
                        disabled="true"
                    variant="contained"
                    color="primary"
                    size="large"
                    onClick={handleApplyChanges}
                    className={classes.submit}>Save</Button>
                </Grid>
            </Grid>
        </ThemeProvider>
    );
}
