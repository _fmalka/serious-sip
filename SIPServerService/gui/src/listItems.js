import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import CallIcon from '@material-ui/icons/Call';
import SettingsApplicationsIcon from '@material-ui/icons/Settings';
import BuildIcon from '@material-ui/icons/Build';
import CodeIcon from '@material-ui/icons/Code';
import CallMergeIcon from '@material-ui/icons/CallMerge';
import HistoryIcon from '@material-ui/icons/History';

import { Link } from 'react-router-dom'

export const mainListItems = (
  <div>
        <ListItem component={Link}
            to="/console" button>
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItem>
        <ListItem component={Link}
            to="/console/calls" button>
            <ListItemIcon>
                <CallIcon />
            </ListItemIcon>
            <ListItemText primary="Calls" />
        </ListItem>
            <ListItem component={Link}
                to="/console/bridges" button>
                <ListItemIcon>
                    <CallMergeIcon />
                </ListItemIcon>
                <ListItemText primary="Bridges" />
            </ListItem>
            <ListItem component={Link}
                to="/console/history" button>
                <ListItemIcon>
                    <HistoryIcon />
                </ListItemIcon>
                <ListItemText primary="History" />
            </ListItem>
            <ListItem component={Link}
            to="/console/files" button>
            <ListItemIcon>
                <FileCopyIcon />
            </ListItemIcon>
            <ListItemText primary="Files" />
        </ListItem>
        <ListItem component={Link}
            to="/console/settings" button>
      <ListItemIcon>
        <SettingsApplicationsIcon />
      </ListItemIcon>
      <ListItemText primary="Settings" />
        </ListItem>
        <ListItem component={Link}
            to="/swagger" button>
            <ListItemIcon>
                <CodeIcon />
            </ListItemIcon>
            <ListItemText primary="API" />
        </ListItem>
        <ListItem component={Link}
            to="/console/maintenance" button>
      <ListItemIcon>
        <BuildIcon />
      </ListItemIcon>
      <ListItemText primary="Maintenance" />
    </ListItem>
  </div>
);

export const secondaryListItems = (
  <div>
   
  </div>
);
