import React, { useEffect, useState } from 'react';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import { IconButton } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ThemeProvider } from '@material-ui/styles';
import CallEndIcon from '@material-ui/icons/CallEnd';


// Generate Order Data
function createData(id, from, to, state, action, direction, rtpTx, rtpRx, duration) {
    return { id, from, to, state, action, direction, rtpTx, rtpRx, duration };
}

const rows = [
    //createData(0, 'No calls', '-', '-', '-', '-', 0, 0, '--:--:--')
];

const useStyles = makeStyles((theme) => ({
    smallFont: {
        fontSize: '0.75rem'
    },
    smallMenuFont: {
        fontSize: '0.75rem',
        fontWeight: 600
    }
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#054f8d',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            light: '#054f8d',
            main: '#054f8d',
            // dark: will be calculated from palette.secondary.main,
            contrastText: '#ffcc00',
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});

export default function CallTable(props) {
    const classes = useStyles();
    const [calls, setCalls] = useState(rows);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [call, setCall] = useState('');
    const [callCount, setCallCount] = useState(0);

    const handleDialogOpen = () => {
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    const handleDialogDenied = () => {

        handleDialogClose();
    };

    const handleDialogConfirmed = () => {
        let requestOptions = { headers: { 'Accept': 'application/json' }, method: "DELETE" };
        if (props.apiKey != null)
            requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "DELETE" };
        fetch('/api/v1/calls/call/drop?id=' + call, requestOptions)
            .then(response => response.json());

        handleDialogClose();
    };

    const handleDropCall = (id, e) => {
        setCall(id);
        handleDialogOpen();
    };

    useEffect(() => {
        console.log(calls);
        const interval = setInterval(() => {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
            fetch('/api/v1/calls', requestOptions)
                .then(response => response.json())
                .then((calls) => {
                    if ((calls == null) || (calls.data == null)) {
                        setCalls(rows);
                        setCallCount(0);
                    }
                    else {
                        console.log(calls.data);
                        setCalls(calls.data);
                        setCallCount(calls.data.length);
                    }
                });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

  return (
          <ThemeProvider theme={theme}>
          <Title>{callCount} Current Calls</Title>
          <Table size="small" stickyHeader aria-label="sticky table">
              <Dialog
                  open={dialogOpen}
                  onClose={handleDialogClose}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description">
                  <DialogTitle id="alert-dialog-title">{"Attention"}</DialogTitle>
                  <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                          Are you sure you want to delete this file?
                            </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                      <Button onClick={handleDialogDenied} color="primary">
                          No
                        </Button>
                      <Button onClick={handleDialogConfirmed} color="primary" autoFocus>
                          Yes
                            </Button>
                  </DialogActions>
              </Dialog>
        <TableHead>
                  <TableRow>
                    <TableCell>From</TableCell>
                    <TableCell>To</TableCell>
                    <TableCell>State</TableCell>
                    <TableCell>Action</TableCell>
                    <TableCell>Direction</TableCell>
                    <TableCell align="right">Tx</TableCell>
                    <TableCell align="right">Rx</TableCell>
                      <TableCell align="right">Duration</TableCell>
                      <TableCell align="right"></TableCell>
                </TableRow>
        </TableHead>
        <TableBody>
            {calls.map((row) => (
                <TableRow key={row.id}>
                    <TableCell className={classes.smallFont}>{row.from}</TableCell>
                    <TableCell className={classes.smallFont}>{row.to}</TableCell>
                    <TableCell className={classes.smallFont}>{row.state}</TableCell>
                    <TableCell className={classes.smallFont}>{row.action}</TableCell>
                    <TableCell className={classes.smallFont}>{row.direction}</TableCell>
                    <TableCell align="right" className={classes.smallFont}>{row.rtpTx}</TableCell>
                    <TableCell align="right" className={classes.smallFont}>{row.rtpRx}</TableCell>
                    <TableCell align="right" className={classes.smallFont}>{row.duration}</TableCell>
                    <TableCell align="right" className={classes.smallFont}>
                        <IconButton
                            aria-label="more"
                            aria-controls="long-menu"
                            aria-haspopup="true"
                            size="small"
                            onClick={e => handleDropCall(row.id, e)}
                        >
                            <CallEndIcon />
                        </IconButton>
                    </TableCell>
            </TableRow>
            ))}
        </TableBody>
      </Table>
              </ThemeProvider>
  );
}
