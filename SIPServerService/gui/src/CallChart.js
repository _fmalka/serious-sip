import React, { useEffect, useState } from 'react';
import { useTheme } from '@material-ui/core/styles';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './Title';

// Generate Sales Data
function createData(time, amount) {
  return { time, amount };
}

const data = [
  createData('00:00', 0),
  createData('03:00', 0),
  createData('06:00', 0),
  createData('09:00', 0),
  createData('12:00', 0),
  createData('15:00', 0),
  createData('18:00', 0),
  createData('21:00', 0),
  createData('24:00', undefined),
];

export default function CallChart(props) {
  const theme = useTheme();
    const [calls, setCalls] = useState(data);

    useEffect(() => {
        console.log(calls);
        const interval = setInterval(() => {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
            fetch('/api/v1/calls/statistics', requestOptions)
                .then(response => response.json())
                .then((calls) => {
                    if ((calls == null) || (calls.data == null))
                        setCalls(data);
                    else {
                        console.log(data);
                        console.log(calls.data);
                        setCalls(calls.data);
                    }
                });
        }, 5*1000);
        return () => clearInterval(interval);
    }, []);

  return (
    <React.Fragment>
      <Title>Today's Calls</Title>
      <ResponsiveContainer>
        <LineChart
          data={calls}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        >
          <XAxis dataKey="time" stroke={theme.palette.text.secondary} />
          <YAxis stroke={theme.palette.text.secondary}>
            <Label
              angle={270}
              position="left"
              style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
            >
              Calls (min)
            </Label>
          </YAxis>
                  <Line type="monotone" dataKey="amount" stroke="#054f8d" dot={false} />
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}
