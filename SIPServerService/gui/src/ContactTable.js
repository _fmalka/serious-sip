import React, { useEffect, useState } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';

// Generate Order Data
function createData(id, display, username, firstname, lastname, presenceState, phoneState, tel, email) {
    return { id, display, username, firstname, lastname, presenceState, phoneState, tel, email };
}

const rows = [
    createData(0, 'No contacts', '-', '-', '-', '-', '-', '-', '-')
];

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
    smallFont: {
        fontSize: '0.75rem'
    }
}));

export default function ContactTable(props) {
    const classes = useStyles();
    const [contacts, setContacts] = useState(rows);

    useEffect(() => {
        console.log(contacts);
        const interval = setInterval(() => {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
            fetch('/api/v1/contacts', requestOptions)
                .then(response => response.json())
                .then((contacts) => {
                    if ((contacts == null) || (contacts.data == null))
                        setContacts(rows);
                    else {
                        console.log(contacts.data);
                        setContacts(contacts.data);
                    }
                });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

  return (
    <React.Fragment>
      <Title>Contacts Statuses</Title>
      <Table size="small">
              <TableHead>   
                  <TableRow>
                    <TableCell>Username</TableCell>
                    <TableCell>First name</TableCell>
                    <TableCell>Last name</TableCell>
                    <TableCell>Presence</TableCell>
                    <TableCell>Status</TableCell>
                </TableRow>
        </TableHead>
        <TableBody>
            {contacts.map((row) => (
                <TableRow key={row.id}>
                    <TableCell className={classes.smallFont}>{row.username}</TableCell>
                    <TableCell className={classes.smallFont}>{row.firstname}</TableCell>
                    <TableCell className={classes.smallFont}>{row.lastname}</TableCell>
                    <TableCell className={classes.smallFont}>{row.presenceState}</TableCell>
                    <TableCell className={classes.smallFont}>{row.phoneState}</TableCell>
            </TableRow>
            ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
      </div>
    </React.Fragment>
  );
}
