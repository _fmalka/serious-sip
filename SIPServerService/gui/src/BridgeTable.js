import React, { useEffect, useState } from 'react';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import { IconButton } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ThemeProvider } from '@material-ui/styles';
import CallEndIcon from '@material-ui/icons/CallEnd';


// Generate Order Data
function createData(id, aLegCallId, bLegCallId, relayDtmf, action) {
    return { id, aLegCallId, bLegCallId, relayDtmf, action };
}

const rows = [
    //createData(0, 'No calls', '-', '-', '-', '-', 0, 0, '--:--:--')
];

const useStyles = makeStyles((theme) => ({
    smallFont: {
        fontSize: '0.75rem'
    },
    smallMenuFont: {
        fontSize: '0.75rem',
        fontWeight: 600
    }
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#054f8d',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            light: '#054f8d',
            main: '#054f8d',
            // dark: will be calculated from palette.secondary.main,
            contrastText: '#ffcc00',
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});

export default function BridgeTable(props) {
    const classes = useStyles();
    const [bridges, setBridges] = useState(rows);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [bridge, setBridge] = useState('');
    const [bridgeCount, setBridgeCount] = useState(0);

    const handleDialogOpen = () => {
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    const handleDialogDenied = () => {

        handleDialogClose();
    };

    const handleDialogConfirmed = () => {
        let requestOptions = { headers: { 'Accept': 'application/json' }, method: "DELETE" };
        if (props.apiKey != null)
            requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "DELETE" };
        fetch('/api/v1/bridges/bridge/terminate?id=' + bridge, requestOptions)
            .then(response => response.json());

        handleDialogClose();
    };

    const handleTerminateBridge = (id, e) => {
        setBridge(id);
        handleDialogOpen();
    };

    useEffect(() => {
        console.log(bridges);
        const interval = setInterval(() => {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
            fetch('/api/v1/bridges', requestOptions)
                .then(response => response.json())
                .then((bridges) => {
                    if ((bridges == null) || (bridges.data == null)) {
                        setBridges(rows);
                        setBridgeCount(0);
                    }
                    else {
                        console.log(bridges.data);
                        console.log(bridges.data[0]);
                        console.log(bridges.data[0].aLegCallId);
                        console.log(bridges.data[0].bLegCallId);
                        console.log(bridges.data[0].relayDtmf);
                        setBridges(bridges.data);
                        setBridgeCount(bridges.data.length);
                    }
                });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

  return (
          <ThemeProvider theme={theme}>
          <Title>{bridgeCount} Current Bridges</Title>
          <Table size="small" stickyHeader aria-label="sticky table">
              <Dialog
                  open={dialogOpen}
                  onClose={handleDialogClose}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description">
                  <DialogTitle id="alert-dialog-title">{"Attention"}</DialogTitle>
                  <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                          Are you sure you want to delete this file?
                            </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                      <Button onClick={handleDialogDenied} color="primary">
                          No
                        </Button>
                      <Button onClick={handleDialogConfirmed} color="primary" autoFocus>
                          Yes
                            </Button>
                  </DialogActions>
              </Dialog>
        <TableHead>
                  <TableRow>
                      <TableCell>A-Leg</TableCell>
                      <TableCell>B-Leg</TableCell>
                      <TableCell>RelayDtmf</TableCell>
                      <TableCell>Action</TableCell>
                      <TableCell align="right"></TableCell>
                </TableRow>
        </TableHead>
        <TableBody>
            {bridges.map((row) => (
                <TableRow key={row.id}>
                    <TableCell className={classes.smallFont}>{row.aLegCallId}</TableCell>
                    <TableCell className={classes.smallFont}>{row.bLegCallId}</TableCell>
                    <TableCell className={classes.smallFont}>{row.relayDtmf ? "True" : "False"}</TableCell>
                    <TableCell className={classes.smallFont}>{row.action}</TableCell>
                    <TableCell align="right" className={classes.smallFont}>
                        <IconButton
                            aria-label="more"
                            aria-controls="long-menu"
                            aria-haspopup="true"
                            size="small"
                            onClick={e => handleTerminateBridge(row.id, e)}
                        >
                            <CallEndIcon />
                        </IconButton>
                    </TableCell>
            </TableRow>
            ))}
        </TableBody>
      </Table>
              </ThemeProvider>
  );
}
