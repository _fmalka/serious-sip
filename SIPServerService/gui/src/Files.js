﻿import React, { useState, useEffect } from 'react';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ThemeProvider } from '@material-ui/styles';
import GraphicEqIcon from '@material-ui/icons/GraphicEq';
import DescriptionIcon from '@material-ui/icons/Description';

import clsx from 'clsx';
import Grid from '@material-ui/core/Grid';
import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

// Generate Order Data
function createData(id, fullname, name, created, modified, type, size) {
    return { id, fullname, name, created, modified, type, size };
}

const rows = [
    createData(0, 'No files', '-', '-', '-', '-', '-', '-')
];

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 480,
    },
    smallFont: {
        fontSize: '0.75rem'
    },
    smallMenuFont: {
        fontSize: '0.75rem',
        fontWeight: 600
    }
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#054f8d',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            light: '#054f8d',
            main: '#054f8d',
            // dark: will be calculated from palette.secondary.main,
            contrastText: '#ffcc00',
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});

export default function Files(props) {
    const [selectedIndex, setSelectedIndex] = useState(1);
    const [files, setFiles] = useState(rows);
    const [numOfLogs, setNumOfLogs] = useState('0 files.');
    const [numOfClips, setNumOfClips] = useState('0 files.');
    const [dialogOpen, setDialogOpen] = useState(false);
    const [filename, setFilename] = useState('');
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);


    const handleDialogOpen = () => {
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    const handleDialogDenied = () => {

        handleDialogClose();
    };

    const handleDialogConfirmed = () => {
        console.log(filename);
        if (filename.startsWith("/clips")) {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "DELETE" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "DELETE" };
            fetch('/api/v1/admin/files/clips?filename=' + filename, requestOptions)
                .then(response => response.json())
                .then(loadClips());
        } else if (filename.startsWith("/logs")) {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "DELETE" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "DELETE" };
            fetch('/api/v1/admin/files/logs?filename=' + filename, requestOptions)
                .then(response => response.json())
                .then(loadLogs());
        }

        handleDialogClose();
    };

    const handleDeleteFile = (id, filename, e) => {
        setFilename(filename);
        handleDialogOpen();
    };

    const loadClips = () => {
        setSelectedIndex(1);
        let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
        if (props.apiKey != null)
            requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
        fetch('/api/v1/admin/files/clips', requestOptions)
            .then(response => response.json())
            .then((files) => {
                if ((files == null) || (files.data == null))
                    setFiles(rows);
                else {
                    console.log(files.data);
                    setFiles(files.data);
                    setNumOfClips(files.data.length + ' files.');
                }
            });
    };

    const loadLogs = () => {
        setSelectedIndex(2);
        let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
        if (props.apiKey != null)
            requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
        fetch('/api/v1/admin/files/logs', requestOptions)
            .then(response => response.json())
            .then((files) => {
                if ((files == null) || (files.data == null))
                    setFiles(rows);
                else {
                    console.log(files.data);
                    setFiles(files.data);
                    setNumOfLogs(files.data.length + ' files.');
                }
            });
    };

    useEffect(() => {
        loadLogs();
        loadClips();
    }, []);

    return (
        <ThemeProvider theme={theme}>
        <Grid container spacing={3}>
            <Grid item xs={12} md={8} lg={3}>
                <Paper className={fixedHeightPaper}>
                    <List component="nav" aria-label="main mailbox folders">
                        <ListItem button selected={selectedIndex === 1} onClick={(event) => loadClips()}>
                            <ListItemAvatar>
                        <Avatar>
                            <GraphicEqIcon />
                        </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Media clips" secondary={numOfClips} />
                        </ListItem>
                        <ListItem button selected={selectedIndex === 2} onClick={(event) => loadLogs()}>
                    <ListItemAvatar>
                        <Avatar>
                            <DescriptionIcon />
                        </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Logs" secondary={numOfLogs} />
                </ListItem>
                    </List>
                </Paper>
            </Grid>
            <Grid item xs={12} md={8} lg={9}>
                <Paper className={fixedHeightPaper}>
                    <Table size="small" stickyHeader aria-label="sticky table">
                        <Dialog
                            open={dialogOpen}
                            onClose={handleDialogClose}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description">
                            <DialogTitle id="alert-dialog-title">{"Attention"}</DialogTitle>
                            <DialogContent>
                                <DialogContentText id="alert-dialog-description">
                                    Are you sure you want to delete this file?
                            </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleDialogDenied} color="primary">
                                    No
                        </Button>
                                <Button onClick={handleDialogConfirmed} color="primary" autoFocus>
                                    Yes
                            </Button>
                            </DialogActions>
                        </Dialog>
              <TableHead>   
                  <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell>Date created</TableCell>
                    <TableCell>Last accessed</TableCell>
                    <TableCell>Type</TableCell>
                                <TableCell>Size (B)</TableCell>
                                <TableCell align="right"></TableCell>
                </TableRow>
        </TableHead>
        <TableBody>
                            {files.map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell className={classes.smallFont}>{row.name}</TableCell>
                                    <TableCell className={classes.smallFont}>{row.created}</TableCell>
                                    <TableCell className={classes.smallFont}>{row.modified}</TableCell>
                                    <TableCell className={classes.smallFont}>{row.type}</TableCell>
                                    <TableCell className={classes.smallFont}>{row.size}</TableCell>
                                    <TableCell align="right" className={classes.smallFont}>
                                        <IconButton
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            size="small"
                                            onClick={e => handleDeleteFile(row.id, row.fullname, e)}>
                                            <DeleteIcon />
                                        </IconButton>
                                        <a href={row.fullname} download={row.fullname}>
                                            <IconButton
                                                aria-label="more"
                                                aria-controls="long-menu"
                                                aria-haspopup="true"
                                                size="small">
                                                <CloudDownloadIcon />
                                            </IconButton>
                                        </a>
                                    </TableCell>
                                </TableRow>
                            ))}
        </TableBody>
      </Table>
                </Paper>
                    </Grid>
            </Grid>
        </ThemeProvider>
    );
}
