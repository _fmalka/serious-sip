import React, { useEffect, useState } from 'react';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import { IconButton } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ThemeProvider } from '@material-ui/styles';
import CallEndIcon from '@material-ui/icons/CallEnd';


// Generate Order Data
function createData(id, from, to, time, direction, duration) {
    return { id, from, to, time, direction, duration };
}

//{ "type": "call", "callId": "47cfb4c31644faf349519c0b5ac47dad@10.0.0.100:5060", "createdTime": "1/26/2021 1:59:57 PM +00:00", "calldirection": "In", "from": "sip:722744002@10.0.0.100", "to": "sip:9980@192.168.88.97:5060", "localEndPoint": "udp:0.0.0.0:5060", "remoteEndPoint": "udp:10.0.0.100:5060", "answerReason": "Ok", "answerStatus": "200", "answerTime": "1/26/2021 1:59:59 PM +00:00", "progressReason": "Trying", "progressStatus": "100", "progressTime": "1/26/2021 1:59:58 PM +00:00", "hangupReason": "", "hangupTime": "1/26/2021 2:00:53 PM +00:00", "ringDuration": "1", "callDuration": "54.0422735" }
const rows = [
    //createData(0, 'No calls', '-', '-', '-', '-', 0, 0, '--:--:--')
];

const useStyles = makeStyles((theme) => ({
    smallFont: {
        fontSize: '0.75rem'
    },
    smallMenuFont: {
        fontSize: '0.75rem',
        fontWeight: 600
    }
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#054f8d',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            light: '#054f8d',
            main: '#054f8d',
            // dark: will be calculated from palette.secondary.main,
            contrastText: '#ffcc00',
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});

export default function HistoryTable(props) {
    const classes = useStyles();
    const [calls, setCalls] = useState(rows);
    const [callCount, setCallCount] = useState(0);

    useEffect(() => {
        console.log(calls);
        const interval = setInterval(() => {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
            fetch('/api/v1/calls/recent', requestOptions)
                .then(response => response.json())
                .then((calls) => {
                    if ((calls == null) || (calls.data == null)) {
                        setCalls(rows);
                        setCallCount(0);
                    }
                    else {
                        console.log(calls.data);
                        setCalls(calls.data);
                        setCallCount(calls.data.length);
                    }
                });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

  return (
          <ThemeProvider theme={theme}>
          <Title>{callCount} Recent Calls</Title>
          <Table size="small" stickyHeader aria-label="sticky table">
        <TableHead>
                  <TableRow>
                      <TableCell>Id</TableCell>
                    <TableCell>From</TableCell>
                    <TableCell>To</TableCell>
                    <TableCell>Time</TableCell>
                    <TableCell>Direction</TableCell>
                      <TableCell align="right">Duration</TableCell>
                </TableRow>
        </TableHead>
        <TableBody>
            {calls.map((row) => (
                <TableRow key={row.id}>
                    <TableCell className={classes.smallFont}>{row.id}</TableCell>
                    <TableCell className={classes.smallFont}>{row.from}</TableCell>
                    <TableCell className={classes.smallFont}>{row.to}</TableCell>
                    <TableCell className={classes.smallFont}>{row.time}</TableCell>
                    <TableCell className={classes.smallFont}>{row.direction}</TableCell>
                    <TableCell align="right" className={classes.smallFont}>{row.duration}</TableCell>
            </TableRow>
            ))}
        </TableBody>
      </Table>
              </ThemeProvider>
  );
}
