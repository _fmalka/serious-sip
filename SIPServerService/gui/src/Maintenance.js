﻿import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import WarningIcon from '@material-ui/icons/Warning';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';

function preventDefault(event) {
    event.preventDefault();
}

const maintenanceActions = [
    { title: 'Gracefull shutdown', description: 'Prohibit new calls, wait until active calls terminates and stop the service.' },
    { title: 'Shutdown', description: 'Terminates all calls and stop the service now.' },
    { title: 'Gracefull restart', description: 'Prohibit new calls, wait until active calls terminates and restart the service.' },
    { title: 'Restart', description: 'Terminates all calls and restart the service now.' },
    { title: 'Start', description: 'Starts the service now.' },
    { title: 'Cleanup storage', description: 'Cleanup old logs and temporary files.' }
];

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fixedHeight: {
        height: 500,
    },
    form: {
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 0,
        margin: 0
    }
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#054f8d',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            light: '#054f8d',
            main: '#054f8d',
            // dark: will be calculated from palette.secondary.main,
            contrastText: '#ffcc00',
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});

export default function Maintenance(props) {
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const [action, setAction] = useState({ title: 'Gracefull shutdown', description: 'Prohibit new calls, wait until active calls terminates and stop the service.' });
    const [pass, setPass] = useState("");
    const [status, setStatus] = useState("Running");

    const applyAction = (e) => {
        if (pass === 'Passw0rd') {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "PUT" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "PUT" };
            if (action.title === 'Gracefull shutdown') {
                fetch('/api/v1/admin/system/shutdown?force=false', requestOptions)
                    .then(response => response.json());
            } else if (action.title === 'Shutdown') {
                fetch('/api/v1/admin/system/shutdown?force=true', requestOptions)
                    .then(response => response.json());
            } else if (action.title === 'Gracefull restart') {
                fetch('/api/v1/admin/system/restart?force=false', requestOptions)
                    .then(response => response.json());
            } else if (action.title === 'Restart') {
                fetch('/api/v1/admin/system/restart?force=true', requestOptions)
                    .then(response => response.json());
            } else if (action.title === 'Start') {
                fetch('/api/v1/admin/system/start', requestOptions)
                    .then(response => response.json());
            } else if (action.title === 'Cleanup storage') {
                fetch('/api/v1/admin/system/cleanup', requestOptions)
                    .then(response => response.json());
            }
        }
        preventDefault(e);
    };

    const getStatus = () => {
        fetch('/api/v1/admin/system/status', { headers: { 'Accept': 'application/json' }, method: "GET" })
            .then(response => response.json())
            .then((status) => {
                if ((status == null) || (status.data == null))
                    setStatus(null);
                else {
                    console.log(status.data.status);
                    setStatus(status.data.status);
                }
            });
    };

    useEffect(() => {
        const interval = setInterval(() => {
            getStatus();
        }, 2 * 1000);
        return () => clearInterval(interval);
            
    }, []);

    return (
        <ThemeProvider theme={theme}>
        <Grid container spacing={3}>
            <Grid item xs={12}>
                    <Paper className={fixedHeightPaper}>
                        <Box m={3}>
                            <Avatar className={classes.avatar}>
                                <WarningIcon />
                            </Avatar>
                        </Box>
                        <form id="maintenance" className={classes.form}>
                            <Box m={3}>
                                System status: {status}
                            </Box>
                    <Box m={3}> 
                        <Autocomplete
                            id="action-select"
                            autoComplete="off"
                            defaultValue={action}
                            options={maintenanceActions}
                            getOptionLabel={(option) => option.title}
                                    style={{ width: 300 }}
                                    onChange={(event, value) => { console.log(value); setAction(value) }}
                            renderInput={(params) => <TextField {...params} label="Select a maintenance action" variant="outlined" />} />
                    </Box>
                    <Box m={3}>
                        <TextField
                            id="confirm-password"
                            label="Confirm admin password"
                            autoComplete="off"
                            type="password"
                            required
                            defaultValue={pass}
                            onChange={event => { setPass(event.target.value) }}
                            style={{ width: 300 }}
                            variant="outlined"/>
                    </Box>
                    <Box m={3}> 
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    size="large"
                                    onClick={event => { applyAction(event) }}
                                    className={classes.submit}>Apply</Button>
                            </Box>
                       </form>
                </Paper>
            </Grid>
            </Grid>
        </ThemeProvider>
    );
}
