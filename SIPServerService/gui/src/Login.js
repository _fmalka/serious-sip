﻿import React, { useState } from 'react';
import clsx from 'clsx';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';

const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#054f8d',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            light: '#054f8d',
            main: '#054f8d',
            // dark: will be calculated from palette.secondary.main,
            contrastText: '#ffcc00',
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(8),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 0
    },
    fixedHeight: {
        height: 480,
    },
    form: {
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 0,
        margin: 0
    }
}));



export default function Login(props) {
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const [user, setUser] = useState("");
    const [pass, setPass] = useState("");

    const formSubmit = () => {
        props.onLogin(user, pass)
    };

    return (
        <ThemeProvider theme={theme}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Paper className={fixedHeightPaper}>
                        <Box m={3}>
                            <Avatar className={classes.avatar}>
                                <LockOutlinedIcon />
                            </Avatar>
                        </Box>
                        <form id="login" className={classes.form}>
                            <Box m={3}>
                                <TextField
                                    variant="outlined"
                                    required
                                    id="email"
                                    defaultValue={user}
                                    onChange={event => { setUser(event.target.value) }}
                                    style={{ width: 300 }}
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    autoFocus
                                />
                            </Box>
                            <Box m={3}>
                                <TextField
                                    variant="outlined"
                                    required
                                    defaultValue={pass}
                                    onChange={event => { setPass(event.target.value) }}
                                    style={{ width: 300 }}
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />
                            </Box>
                            <Box m={3}>
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    size="large"
                                    onClick={formSubmit}
                                    className={classes.submit}>Login</Button>
                            </Box>
                        </form>
                    </Paper>
                </Grid>
            </Grid>
        </ThemeProvider>
    );
}