﻿import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import CallTable from './CallTable';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 480,
    },
}));

export default function Calls(props) {
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

    return (
        <Grid container spacing={3}>
            {/* Current Calls */}
            <Grid item xs={12}>
                <Paper className={fixedHeightPaper}>
                    <CallTable apiKey={props.apiKey}/>
                </Paper>
            </Grid>
        </Grid>
    );
}