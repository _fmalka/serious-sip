import React, { useEffect, useState } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';

// Generate Order Data
function createData(id, uri, isRegistered, isTalking) {
    return {id, uri, isRegistered, isTalking };
}

const rows = [
    createData(0, '-', '-', '-')
];

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
    smallFont: {
        fontSize: '0.75rem'
    }
}));

export default function IdentityTable(props) {
    const classes = useStyles();
    const [identities, setIdentities] = useState(rows);

    useEffect(() => {
        console.log(identities);
        const interval = setInterval(() => {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
            fetch('/api/v1/admin/identities', requestOptions)
                .then(response => response.json())
                .then((identities) => {
                    if ((identities == null) || (identities.data == null))
                        setIdentities(rows);
                    else {
                        console.log(identities.data);
                        var indexedIdentities = Array(identities.data.length);
                        for (let i = 0; i < identities.data.length; i++) {
                            indexedIdentities[i] = createData(i, identities.data[i].uri, identities.data[i].isRegistered, identities.data[0].isTalking);
                        }
                        setIdentities(indexedIdentities);
                    }
                });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

  return (
    <React.Fragment>
      <Title>Available Identities</Title>
      <Table size="small">
        <TableHead>
                  <TableRow>
                      <TableCell>Identity</TableCell>
                      <TableCell>Registered</TableCell>
                </TableRow>
        </TableHead>
              <TableBody>
                  {identities.map((row) => (
                <TableRow key={row.id}>
                          <TableCell className={classes.smallFont}>{row.uri}</TableCell>
                          <TableCell className={classes.smallFont}>{row.isRegistered ? "True" : "False"}</TableCell>
            </TableRow>
            ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
      </div>
    </React.Fragment>
  );
}
