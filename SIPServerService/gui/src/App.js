﻿import './App.css';
import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { mainListItems, secondaryListItems } from './listItems';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SwaggerUI from "swagger-ui-react"
import "swagger-ui-react/swagger-ui.css"

import {
    BrowserRouter as Router,
    Route,
    Link,
} from "react-router-dom";

import Login from './Login';
import Secret from './Secret';
import Console from './Console';
import Calls from './Calls';
import Bridges from './Bridges';
import History from './History';
import Files from './Files';
import Settings from './Settings';
import Maintenance from './Maintenance';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'v1.0.0'}
            {' | '}
            {'April, 1st 2021'}
            {' | '}
            Maintained and serviced by
            {' '}
            <a target="_blank" href="https://modulo.co.il/">
                www.modulo.co.il
            </a>
            {'.'}
        </Typography>
    );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        background: '#f7f7f9',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        background: '#054f8d',
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeightGeneral: {
        height: 200,
    },
    fixedHeightSip: {
        height: 240,
    },
    fixedHeightSmpp: {
        height: 320,
    },
}));

function App() {
    const [title, setTitle] = useState("Dashboard");
    const classes = useStyles();
    const [open, setOpen] = useState(true);
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [isSecretRequired, setIsSecretRequired] = useState(false);
    const [apiKey, setApiKey] = useState(null);

    useEffect(() => {
        console.log('isAuthenticated: ' + isAuthenticated);
        console.log('isSecretRequired: ' + isSecretRequired);
        console.log('apiKey: ' + apiKey);

        if (isSecretRequired)
            return;

        fetch('/api/v1/admin/config/', {
            headers: {
                'Accept': 'application/json', 'Authorization': 'Bearer ' + apiKey }, method: "GET" })
            .then((response) => {
                if (response.status == 200) {
                    setIsSecretRequired(false);
                } else {
                    setIsSecretRequired(true);
                }
            });
    }, []);

    const login = (u, p) => {
        if (u === 'admin@modulo.co.il' && p === 'Passw0rd') {
            setIsAuthenticated(true);
        }
    };

    const authenticate = (p) => {
        console.log('authenticate');
        const requestOptions = {
            method: 'POST',
            headers: { 'Accept': '*/*', 'Content-Type': 'application/json' },
            body: JSON.stringify({ secret: p })
        };
        fetch('/api/v1/aauth/connect', requestOptions)
            .then((response) => {
                console.log(response.status);
                if (response.status == 200) {
                    response.json().then((json) => {
                        setIsSecretRequired(false);
                        setApiKey(json.data.accessToken);
                        setIsAuthenticated(true);
                    })
                } else {
                    setIsSecretRequired(true);
                    setIsAuthenticated(false);
                } 
            });
    };

    const logout = () => {
        console.log('logout');
        if (apiKey != null)
            setIsSecretRequired(true);
        setApiKey(null);
        setIsAuthenticated(false);
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };
    
    return (
        <Router>
            
                <div className={classes.root}>
                    <CssBaseline />
                    <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
                    <Toolbar className={classes.toolbar}>
                            <IconButton
                                edge="start"
                                color="inherit"
                                aria-label="open drawer"
                                onClick={handleDrawerOpen}
                                className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            {title}
                        </Typography>
                        <IconButton color="inherit" onClick={() => { logout(); window.location = '/console'; }}>
                                <Badge color="secondary">
                                    <ExitToAppIcon />
                                </Badge>
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <Drawer
                        variant="permanent"
                        classes={{
                            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
                        }}
                        open={open}
                >
                    <div className={classes.toolbarIcon}>
                        <img src='/logo.png' alt="keypad" />
                            <IconButton onClick={handleDrawerClose}>
                                <ChevronLeftIcon />
                            </IconButton>
                        </div>
                        <Divider />
                        <List>{mainListItems}</List>
                        <Divider />
                        <List>{secondaryListItems}</List>
                    </Drawer>
                    <main className={classes.content}>
                        <div className={classes.appBarSpacer} />
                    <Container maxWidth="lg" className={classes.container}>
                    <Route exact path="/" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("Dashboard"); return (<Console apiKey={apiKey}/>)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route exact path="/console" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("Dashboard"); return (<Console apiKey={apiKey}/>)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route exact path="/console/calls" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("Calls"); return (<Calls apiKey={apiKey} />)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route exact path="/console/bridges" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("Bridges"); return (<Bridges apiKey={apiKey} />)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route exact path="/console/history" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("History"); return (<History apiKey={apiKey} />)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route exact path="/console/files" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("Files"); return (<Files apiKey={apiKey} />)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route exact path="/console/settings" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("Settings"); return (<Settings apiKey={apiKey} />)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route exact path="/console/maintenance" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("Maintenance"); return (<Maintenance apiKey={apiKey} />)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route exact path="/swagger" render={() => {
                            if (isSecretRequired) {
                                setTitle("Secret"); return (<Secret onSecret={authenticate} />)
                            } else {
                                if (isAuthenticated) {
                                    setTitle("Swagger"); return (<SwaggerUI url="/swagger/v1/swagger.json" />)
                                } else {
                                    setTitle("Login"); return (<Login onLogin={login} />)
                                }
                            }
                        }}></Route>
                        <Route path="/clips/*" render={() => {
                            console.log(window.location);
                            
                        }}></Route>
                            <Box pt={4}>
                                <Copyright />
                            </Box>
                        </Container>
                    </main>
                </div>             
        </Router>
  );
}

export default App;
