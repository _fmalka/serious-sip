import React, { useEffect, useState } from 'react';
import { useTheme } from '@material-ui/core/styles';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './Title';

// Generate Sales Data
function createData(time, amount) {
  return { time, amount };
}

const data = [
  createData('00:00', 0),
  createData('03:00', 0),
  createData('06:00', 0),
  createData('09:00', 0),
  createData('12:00', 0),
  createData('15:00', 0),
  createData('18:00', 0),
  createData('21:00', 0),
  createData('24:00', undefined),
];

export default function ImChart(props) {
  const theme = useTheme();
    const [messages, setMessages] = useState(data);

    useEffect(() => {
        console.log(messages);
        const interval = setInterval(() => {
            let requestOptions = { headers: { 'Accept': 'application/json' }, method: "GET" };
            if (props.apiKey != null)
                requestOptions = { headers: { 'Accept': 'application/json', 'Authorization': 'Bearer ' + props.apiKey }, method: "GET" };
            fetch('/api/v1/messages/message/statistics', requestOptions)
                .then(response => response.json())
                .then((messages) => {
                    if ((messages == null) || (messages.data == null))
                        setMessages(data);
                    else {
                        console.log(data);
                        console.log(messages.data);
                        setMessages(messages.data);
                    }
                });
        }, 5 * 1000);
        return () => clearInterval(interval);
    }, []);
  return (
    <React.Fragment>
      <Title>Today's Instant Messages</Title>
      <ResponsiveContainer>
        <LineChart
          data={messages}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        >
          <XAxis dataKey="time" stroke={theme.palette.text.secondary} />
          <YAxis stroke={theme.palette.text.secondary}>
            <Label
              angle={270}
              position="left"
              style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
            >
              Messages (unit)
            </Label>
          </YAxis>
                  <Line type="monotone" dataKey="amount" stroke="#054f8d" dot={false} />
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}
