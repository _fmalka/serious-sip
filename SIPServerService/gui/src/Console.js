﻿import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ImChart from './ImChart';
import SmsChart from './SmsChart';
import CallChart from './CallChart';
import ContactTable from './ContactTable';
import IdentityTable from './IdentityTable';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    }
}));

export default function Console(props) {
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

    return (
        <Grid container spacing={3}>
            {/* Call Chart */}
            <Grid item xs={12} md={8} lg={4}>
                <Paper className={fixedHeightPaper}>
                    <CallChart apiKey={props.apiKey}/>
                </Paper>
            </Grid>
            {/* Im Chart */}
            <Grid item xs={12} md={8} lg={4}>
                <Paper className={fixedHeightPaper}>
                    <ImChart apiKey={props.apiKey}/>
                </Paper>
            </Grid>
            {/* Sms Chart */}
            <Grid item xs={12} md={8} lg={4}>
                <Paper className={fixedHeightPaper}>
                    <SmsChart apiKey={props.apiKey}/>
                </Paper>
            </Grid>
            {/* Contact Table */}
            <Grid item xs={12} md={8} lg={8}>
                <Paper className={fixedHeightPaper}>
                    <ContactTable apiKey={props.apiKey}/>
                </Paper>
            </Grid>
            {/* Indentity Table */}
            <Grid item xs={12} md={8} lg={4}>
                <Paper className={fixedHeightPaper}>
                    <IdentityTable apiKey={props.apiKey}/>
                </Paper>
            </Grid>
        </Grid>
    );
}