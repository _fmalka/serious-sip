﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace SeriousSIP
{
    public class LoginRequest
    {
        [Required]
        [JsonPropertyName("secret")]
        public string secret { get; set; }
    }

    public class LoginResult
    {
        [JsonPropertyName("accessToken")]
        public string accessToken { get; set; }

        [JsonPropertyName("refreshToken")]
        public string refreshToken { get; set; }
    }

    public class RefreshTokenRequest
    {
        [JsonPropertyName("refreshToken")]
        public string refreshToken { get; set; }
    }
}
