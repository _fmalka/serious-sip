﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeriousSIP
{
    /// <summary>
    /// The list of commands available for scripting.
    /// </summary>
    public enum ScriptCommandType
    {
        /// <summary>
        /// Play an anouncement
        /// </summary>
        Play,
        /// <summary>
        /// Play a series of anouncements one after the other
        /// </summary>
        PlayFiles,
        /// <summary>
        /// Record to a file
        /// </summary>
        Record,
        /// <summary>
        /// Play an annoucement then record to a file
        /// </summary>
        PlayRecord,
        /// <summary>
        /// Initiate an outbound call.
        /// </summary>
        MakeCall,
        /// <summary>
        /// Attend transfer the call to a 3rd party.
        /// </summary>
        AttendedTransfer,
        /// <summary>
        /// Transfer the call to a 3rd party.
        /// </summary>
        BlindTransfer,
        /// <summary>
        /// Collect digits from remote party
        /// </summary>
        GetDigits,
        /// <summary>
        /// Drop the call and end the script
        /// </summary>
        Drop,
        /// <summary>
        /// Goto a specific script line
        /// </summary>
        Goto,
        /// <summary>
        /// Bridge 2 calls
        /// </summary>
        Bridge,
        /// <summary>
        /// Write to log
        /// </summary>
        Log,
        /// <summary>
        /// Write result of previous action to log
        /// </summary>
        LogResult
    }

#pragma warning disable CS1591
    /// <summary>
    /// identity - The identity that will be used to trigger the script.&#xD;&#xA;
    /// script - A list of commands to execute when a call reach the identity uri.
    /// </summary>
    public record Script(string identity, ScriptCommand[] scriptCommands);
    /// <summary>
    /// line - A number to reference the line of the script.&#xD;&#xA;
    /// command - One of the ScriptCommandType commands.&#xD;&#xA;
    /// parameters - The parameter block corresponding to the the command.&#xD;&#xA;
    /// expression - expression to evaluate to determine next step.&#xD;&#xA;
    /// whenTrue - Line number to go to in the script when expression returns true.&#xD;&#xA;
    /// whenFalse - Line number to go to in the script when expression returns false.
    /// </summary>
    public record ScriptCommand(int line, ScriptCommandType command, Object parameters, string expression, int whenTrue, int whenFalse);
    /// <summary>
    /// id - Index of the file in the list.&#xD;&#xA;
    /// fullname - Fullname of the file in the disk.&#xD;&#xA;
    /// name - Shortname of the file with no extension.&#xD;&#xA;
    /// created - Date of the file creation.&#xD;&#xA;
    /// modified - Last modification date of the file.&#xD;&#xA;
    /// type - Extension of the file.&#xD;&#xA;
    /// size - Size of the file in bytes.
    /// </summary>
    public record FilesParams(int id, string fullname, string name, string created, string modified, string type, long size);
    /// <summary>
    /// clientUrl - A base url to send rest events to (optional).&#xD;&#xA;
    /// autoStartSipServer - When set, starts the sip server automatically with the service.&#xD;&#xA;
    /// sipParams - Sip server configuration, see ConfigSipServerParams for more details.&#xD;&#xA;
    /// smppParams - Smpp client configuration, see ConfigSmppParams for more details.
    /// </summary>
    public record ConfigParams(ConfigServiceParams serviceParams, ConfigSipParams sipParams,
                               ConfigSmppParams smppParams);
    /// <summary>
    /// instanceName - The name of the microservice.&#xD;&#xA
    /// clientUrl - A base url to send rest events to (optional).&#xD;&#xA;
    /// autoStartSipServer - When set, starts the sip server automatically with the service.&#xD;&#xA;
    /// </summary>
    public record ConfigServiceParams(string instanceName, string clientUrl, bool autoStartSipServer);
    /// <summary>
    /// localIPEndPoints - A list of ip end points, see IpEndPointParams for details.&#xD;&#xA;
    /// registerAccounts - A list of identities that can be used to initiate or receive calls and messages, see RegisterAccountParams for more details.&#xD;&#xA;
    /// monitorTrunks - A list of sip uri to monitor using SIP OPTIONS.&#xD;&#xA;
    /// codecs - A list of codecs supported by this instance. Options are PCMA, PCMU, G729, G722.&#xD;&#xA;
    /// rtpAddress - IP address to use for media path in case different than sip address.&#xD;&#xA;
    /// dtmfMode - The preferred dtmf mode, Rfc2833, Inband, dtmf, dtmf/relay.&#xD;&#xA;
    /// autoAcceptCalls - When set, incoming calls are automatically accepted with a SIP RINGING.&#xD;&#xA;
    /// autoAnswerCalls - When set, incoming calls are automatically answered with a SIP OK.&#xD;&#xA;
    /// acceptDelay - Time in milliseconds to delay answer time in case we want to have the ring tone audible.&#xD;&#xA;
    /// useMultiMediaTimers - When set, use windows rtc multimedia timers instead of dotnet timers.&#xD;&#xA;
    /// hangupWhenNoRtp - When set, use rtcp to determine is rtp is available and drop the calls in case absent.&#xD;&#xA;
    /// registrationInterval - Time in milliseconds between account registration.&#xD;&#xA;
    /// </summary>
    public record ConfigSipParams(string[] localIpEndPoints, string[] registerAccounts, string[] monitorTrunks, 
                                           string[] codecs, string rtpAddress, string dtmfMode, bool autoAcceptCalls, bool autoAnswerCalls, 
                                           int acceptDelay, bool useMultiMediaTimers, bool hangupWhenNoRtp, int registrationInterval);
    /// <summary>
    /// systemId - A unique id of the smpp account.&#xD;&#xA;
    /// password - Smpp account password.&#xD;&#xA;
    /// host - Address of the Smpp server.&#xD;&#xA;
    /// port - Port of the Smpp server.&#xD;&#xA;
    /// systemType - Optional login parameter that should be set only if required by the Smpp server and according to the credentials provided.&#xD;&#xA;
    /// serviceType - Optional login parameter that should be set only if required by the Smpp server and according to the credentials provided.&#xD;&#xA;
    /// sourceAddress - The address(number) to use as caller identification when sending a message.
    /// </summary>
    public record ConfigSmppParams(string systemId, string password, string host, int port, string systemType, string serviceType, string sourceAddress);
    /// <summary>
    /// level - Trace = 0,  Debug = 1, Information = 2, Warning = 3, Error = 4, Critical = 5, None = 6.&#xD;&#xA;
    /// message - The message string to write to the log
    /// </summary>
    public record LogParams(LogLevel level, string message);
    /// <summary>
    /// uri - The sip uri of the sip entity.&#xD;&#xA;
    /// status - When true, the sip identity is successfully registered.
    /// </summary>
    public record IdentityParams(string uri, bool isRegistered, bool isTalking);
#pragma warning restore 1591
}
