﻿using SIPSorcery.SIP;
using System;

namespace SeriousSIP
{
#pragma warning disable CS1591
    /// <summary>
    /// OnCallDisconnected - Invoked when an inbound or outbound call is disconnected.;
    /// </summary>
    public record GeneralCallsSubscriptions(Uri OnCallDisconnected);

    /// <summary>
    /// OnCallOffered - Invoked when an inbound call is offered. (INVITE)&#xD;&#xA;
    /// OnCallAccepted - Invoked when an inbound call is accepted. (RINGING)&#xD;&#xA;
    /// OnCallAnswered - Invoked when an inbound call is answered. (OK)&#xD;&#xA;
    /// OnCallCancelled - Invoked when an inbound call is disconnected before it is answered. (CANCEL)&#xD;&#xA;
    /// </summary>
    public record InboundCallsSubscriptions(Uri OnCallOffered, Uri OnCallAccepted, Uri OnCallAnswered,
                                            Uri OnCallCancelled);

    /// <summary>
    /// OnCallTrying - Invoked when an outbound call is responded with a trying.&#xD;&#xA;
    /// OnCallRinging - Invoked when an outbound call is ringing.&#xD;&#xA;
    /// OnCallConnected - Invoked when an outbound call is answered.&#xD;&#xA;
    /// OnCallFailed - Invoked when an outbound call fails to be sent.&#xD;&#xA;
    /// OnCallTimeout - Invoked when an outbound call times out on no answer.&#xD;&#xA;
    /// OnCallDiverted - Invoked when an outbound call is diverted to a 3rd destination.&#xD;&#xA;
    /// </summary>
    public record OutboundCallsSubscriptions(Uri OnCallTrying, Uri OnCallRinging, Uri OnCallConnected,
                                                Uri OnCallFailed, Uri OnCallTimeout, Uri OnCallDiverted);

    /// <summary>
    /// OnCallPlayDone - Invoked when a play command is completed.&#xD;&#xA;
    /// OnCallPlayFilesDone - Invoked when play files command is completed.&#xD;&#xA;
    /// OnCallPlayRecordDone - Invoked when a play record command is completed.&#xD;&#xA;
    /// OnCallRecordDone - Invoked when a record command is completed.&#xD;&#xA;
    /// OnCallGetDigitsDone - Invoked when a get digits command is completed.&#xD;&#xA;
    /// OnCallSendDigitsDone - Invoked when a send digits command is completed.&#xD;&#xA;
    /// OnCallSpeakDone - Invoked when a speak or speak ssml command is completed.&#xD;&#xA;
    /// </summary>
    public record CallMediaSubscriptions(Uri OnCallPlayDone, Uri OnCallPlayFilesDone,
                                            Uri OnCallPlayRecordDone, Uri OnCallRecordDone, Uri OnCallGetDigitsDone,
                                            Uri OnCallSendDigitsDone, Uri OnCallSpeakDone);

    /// <summary>
    /// OnCallPositiveAnswerMachineDetection - Invoked when an answering machine is detected on an outbound call.&#xD;&#xA;
    /// OnCallPositiveFaxDetection - Invoked when a fax machine is detected on an outbound call.&#xD;&#xA;
    /// OnCallRingbackToneDetection - Invoked when a ringback tone is detected on an outbound call.&#xD;&#xA;
    /// OnCallPositiveVoiceDetection - Invoked when a human voice is detected on an outbound call.&#xD;&#xA;
    /// </summary>
    public record CallProgressSubscriptions(Uri OnCallPositiveAnswerMachineDetection, Uri OnCallPositiveFaxDetection, 
                                            Uri OnCallRingbackToneDetection, Uri OnCallPositiveVoiceDetection);

    /// <summary>
    /// identities - Array of identities in sip uri format.&#xD;&#xA;
    /// generalCallsSubscriptions - GeneralCallsSubsriptions parameter block.&#xD;&#xA;
    /// inboundCallsSubscriptions - InboundCallsSubscriptions parameter block.&#xD;&#xA;
    /// outboundCallsSubscriptions - OutboundCallsSubscriptions parameter block.&#xD;&#xA;
    /// mediaCallsSubscriptions - CallMediaSubscriptions parameter block.&#xD;&#xA;
    /// callProgressSubscriptions - CallProgressSubscriptions parameter block.&#xD;&#xA;
    /// </summary>
    public record CallsSubscribeParams(string[] identities,
                                    GeneralCallsSubscriptions generalCallsSubscriptions,
                                    InboundCallsSubscriptions inboundCallsSubscriptions,
                                    OutboundCallsSubscriptions outboundCallsSubscriptions,
                                    CallMediaSubscriptions mediaCallsSubscriptions,
                                    CallProgressSubscriptions callProgressSubscriptions);

    /// <summary>
    /// identities - Array of identities in sip uri format.&#xD;&#xA;
    /// </summary>
    public record CallsUnsubscribeParams(string[] identities);

    /// <summary>
    /// to - The sip uri represnting the destination of the call.&#xD;&#xA;
    /// identity - The sip uri representing the identity used to make the call, also the caller ID that will be presented to the callee.&#xD;&#xA;
    /// sipHeaders - A collection of sipHeader to add or override the default sip headers.&#xD;&#xA;
    /// cpaTimeout - If 0, will not perform call progress analysis, if > 0 will perform the call progress analysis for the given value (in milliseconds).&#xD;&#xA;
    /// waitForCompletion - If true, the http request will return only when the call is completed (established or failed), if false, the request returns immediately the webhook will be used to determine if the call was connected or not.
    /// </summary>
    public record MakeCallParams(string to, string identity, string[] sipHeaders, int cpaTimeout, string userContext, bool waitForCompletion);

    /// <summary>
    /// fileName - The file name to be played back.&#xD;&#xA;
    /// clearDigitsBuffer - If true, the digits buffer is cleared before the playback.&#xD;&#xA;
    /// stopOnDigit - Tf true, the playback is interrupted by any key pressed.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the playback will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record PlayParams(string fileName, bool clearDigitsBuffer, bool stopOnDigit, string userContext, bool waitForCompletion);

    /// <summary>
    /// fileName - The file name to be played back.&#xD;&#xA;
    /// interval - Time in milliseconds between each play instance.&#xD;&#xA;
    /// clearDigitsBuffer - If true, the digits buffer is cleared before the playback.&#xD;&#xA;
    /// stopOnDigit - Tf true, the playback is interrupted by any key pressed.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the playback will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record PlayLoopParams(string fileName, int interval, bool clearDigitsBuffer, bool stopOnDigit, string userContext, bool waitForCompletion);

    /// <summary>
    /// text - The text to be spoken to the call.&#xD;&#xA;
    /// clearDigitsBuffer - If true, the digits buffer is cleared before the playback.&#xD;&#xA;
    /// stopOnDigit - Tf true, the playback is interrupted by any key pressed.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the playback will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record SpeakParams(string text, bool clearDigitsBuffer, bool stopOnDigit, string userContext, bool waitForCompletion);

    /// <summary>
    /// ssml - The SSML to be spoken to the call.&#xD;&#xA;
    /// clearDigitsBuffer - If true, the digits buffer is cleared before the playback.&#xD;&#xA;
    /// stopOnDigit - Tf true, the playback is interrupted by any key pressed.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the playback will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record SpeakSsmlParams(string ssml, bool clearDigitsBuffer, bool stopOnDigit, string userContext, bool waitForCompletion);

    /// <summary>
    /// fileName s- An array of file names to be played back one after the other.&#xD;&#xA;
    /// interval - Time in milliseconds between each play instance.&#xD;&#xA;
    /// clearDigitsBuffer - If true, the digits buffer is cleared before the playback.&#xD;&#xA;
    /// stopOnDigit - Tf true, the playback is interrupted by any key pressed.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the playback will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record PlayFilesParams(string[] fileNames, int interval, bool clearDigitsBuffer, bool stopOnDigit, string userContext, bool waitForCompletion);

    /// <summary>
    /// fileName - The file name to be played back.&#xD;&#xA;
    /// duration - The maximum length of the recording in milliseconds.&#xD;&#xA;
    /// terminationDigit - A digit that will be used by the user to stop the recording.&#xD;&#xA;
    /// playTone - If true, play the record tone before recording starts.&#xD;&#xA;
    /// stopOnSilence - If > 0, the recording is interrupted if silence reach the value of this parameter (in milliseconds).&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the recording will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record RecordParams(string fileName, int duration, char terminationDigit, bool playTone, int stopOnSilence, string userContext, bool waitForCompletion);

    /// <summary>
    /// playFileName - The file name to store the recorded voice into.&#xD;&#xA;
    /// recordFileName - The file name to store the recorded voice into.&#xD;&#xA;
    /// duration - The maximum length of the recording in milliseconds.&#xD;&#xA;
    /// terminationDigit - A digit that will be used by the user to stop the recording.&#xD;&#xA;
    /// playTone - If true, play the record tone before recording starts.&#xD;&#xA;
    /// stopOnSilence - If > 0, the recording is interrupted if silence reach the value of this parameter (in milliseconds).&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the recording will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record PlayRecordParams(string playFileName, string recordFileName, int duration, char terminationDigit, bool playTone, int stopOnSilence, string userContext, bool waitForCompletion);

    /// <summary>
    /// duration - The maximum time in milliseconds to collect digits.&#xD;&#xA;
    /// numOfDigits - The maximum number of digits to collect.&#xD;&#xA;
    /// terminationDigit - A digit that will be used by the user to stop the digit collection.&#xD;&#xA;
    /// clearDigitsBuffer - If true, the digits buffer is cleared before the get digit action starts.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the recording will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record GetDigitsParams(int duration, int numOfDigits, char terminationDigit, bool clearDigitsBuffer, string userContext, bool waitForCompletion);

    /// <summary>
    /// digits - A string of legal digits to be dialed to the remote party.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the recording will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record SendDigitsParams(string digits, string userContext, bool waitForCompletion);

    /// <summary>
    /// to - The sip uri represnting the destination of the call.&#xD;&#xA;
    /// timeout - The time in milliseconds to wait for the completion of the transfer.&#xD;&#xA;
    /// disconnect - If true, disconnect the call after transfer is completed.
    /// </summary>
    public record BlindTransferParams(string to, string[] sipHeaders, int timeout, bool disconnect);

    /// <summary>
    /// id1 - The unique ID of the call to transfer from.&#xD;&#xA;
    /// id2 - The unique ID of the call to transfer to.&#xD;&#xA;
    /// timeout - The time in milliseconds to wait for the completion of the transfer.&#xD;&#xA;
    /// </summary>
    public record AttendedTransferParams(string id1, string id2, string[] sipHeaders, int timeout);

    /// <summary>
    /// id - The unique ID of the call..&#xD;&#xA;
    /// from - The sip uri representation of the calling party.&#xD;&#xA;
    /// to - The sip uri representation of the called party.&#xD;&#xA;
    /// state - The current state of the call (Active, Ringing, OnHold).&#xD;&#xA;
    /// action - The current action being executed on the call (Idle, Playing, Recording, SendingDigits, RetrievingDigits).&#xD;&#xA;
    /// direction - In for incoming call and Out for ougoing call.&#xD;&#xA;
    /// rtpTx - Number of RTP packets sent.&#xD;&#xA;
    /// rtpRx - Number of RTP packets received.&#xD;&#xA;
    /// duration - The time elapsed since the beginning of the call in "hh:mm:ss" format.
    /// </summary>
    public record CallInfoParams(string id, string from, string to, SIPCallState state, SIPCallAction action, 
                                    SIPCallDirection direction, uint rtpTx, uint rtpRx, string duration, string userContext,
                                    string[] sipHeaders);

    /// <summary>
    /// status - Success or Error, depending on if the operation failed or not.&#xD;&#xA;
    /// data - The respective payload conresponding to the operation result.&#xD;&#xA;
    /// message - Human readable message reflecting success or error message.
    /// </summary>
    public record ResponseParams(string status, object data, string message);

    /// <summary>
    /// autoAccept - When true, the call server autonatically accepts the call without raising the OnCallOffered webhook.&#xD;&#xA;
    /// autoAnswer - When true, the call server automatically answers the call without raising the OnCallAccepted webhook..&#xD;&#xA;
    /// dtmfMode - Switch from RFC2833 to Inband or Info dtmf mode (Rfc2833, Inband, Dtmf, DtmfRelay).
    /// </summary>
    public record OptionsParams(bool autoAccept, bool autoAnswer, SIPCallDtmfMode dtmfMode);

    /// <summary>
    /// time - Time of the day for last 24h
    /// amount - sum of call minutes for period
    /// </summary>
    public record StatsParams(string time, int amount);

    /// <summary>
    /// time - Time of the day for last 24h
    /// amount - summ of call minutes for period
    /// </summary>
    public record CallHistoryParams(string id, string from, string to, string time, string direction, string duration);
#pragma warning restore 1591    
}
