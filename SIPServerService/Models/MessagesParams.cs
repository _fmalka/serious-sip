﻿using SIPSorcery.SIP;
using System;

namespace SeriousSIP
{
#pragma warning disable CS1591
    /// <summary>
    /// to - The sip uri represnting the destination of the call.&#xD;&#xA;
    /// identity - The sip uri representing the identity used to make the call, also the caller ID that will be presented to the callee.&#xD;&#xA;
    /// message - The string containing the instant message to be sent.
    /// </summary>
    public record SendMessageParams(string to, string identity, string message);

    public record MessageInfoParams(string id, string from, string to, SIPCallDirection direction, string message);
    /// <summary>
    /// id - The unique ID of the message.&#xD;&#xA;
    /// from - For inbound messages, represent the number sending the message, for outbound messages, represent the sip uri identity of the sender.&#xD;&#xA;
    /// to - The sip uri represnting the destination of the call.&#xD;&#xA;
    /// direction - In for inbound message and Out for outbound message.&#xD;&#xA;
    /// message - The string containing the instant message.
    /// </summary>
    /// 
    /// <summary>
    /// OnMessageReceived - Invoked when a instant message is received.&#xD;&#xA;
    /// OnTextMessageReceived - Invoked when a sms is received.
    /// </summary>
    public record InboundMessagesSubscriptions(Uri OnMessageReceived, Uri OnTextMessageReceived);

    /// <summary>
    /// OnMessageSent - Invoked when a instant message is send.&#xD;&#xA;
    /// OnMessageFailed - Invoked when a instant message sending fails.&#xD;&#xA;
    /// OnMessageTimeout - Invoked when a instant message sending is timing out.&#xD;&#xA;
    /// OnTextMessageSent - Invoked when a sms is sent.&#xD;&#xA;
    /// OnTextMessageDelivered - Invoked when a sms is successfully delivered.
    /// </summary>
    public record OutboundMessagesSubscriptions(Uri OnMessageSent, Uri OnMessageFailed, Uri OnMessageTimeout,
                                                Uri OnTextMessageSent, Uri OnTextMessageDelivered);

    /// <summary>
    /// identities - Array of identities in sip uri format.&#xD;&#xA;
    /// inboundMessagesSubscriptions - InboundMessagesSubscriptions parameter block.&#xD;&#xA;
    /// outboundMessagesSubscriptions - OutboundMessagesSubscriptions parameter block.
    /// </summary>
    public record MessagesSubscribeParams(string[] identities,
                                    InboundMessagesSubscriptions inboundMessagesSubscriptions,
                                    OutboundMessagesSubscriptions outboundMessagesSubscriptions);

    /// <summary>
    /// identities - Array of identities in sip uri format.
    /// </summary>
    public record MessagesUnsubscribeParams(string[] identities);
#pragma warning restore 1591
}
