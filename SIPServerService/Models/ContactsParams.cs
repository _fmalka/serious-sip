﻿using System;
using System.Collections.Concurrent;

namespace SeriousSIP
{
#pragma warning disable CS1591
    /// <summary>
    /// display - The display text of a SIP extension.&#xD;&#xA;
    /// username - The username of the contact (used to registered to the PABX or Trunk).&#xD;&#xA;
    /// firstname - Firstname, just for commodity.&#xD;&#xA;
    /// lastname - LAstname, just for commodity.&#xD;&#xA;
    /// type - The type of the contact (Extension, Trunk, Mobile, External).&#xD;&#xA;
    /// tel - The sip uri to use to reach this contact.&#xD;&#xA;
    /// email - The email of the contact.&#xD;&#xA;
    /// tags - Let the user to add its own tags to its contacts.
    /// </summary>
    public record NewContactParams(string display, string username, string firstname,
                                    string lastname, SIPContactType type, string tel, string email,
                                    ConcurrentDictionary<string, Object> tags);

    /// <summary>
    /// id - The unique ID of the contact.&#xD;&#xA;
    /// display - The display text of a SIP extension.&#xD;&#xA;
    /// username - The username of the contact (used to registered to the PABX or Trunk).&#xD;&#xA;
    /// firstname - Firstname, just for commodity.&#xD;&#xA;
    /// lastname - LAstname, just for commodity.&#xD;&#xA;
    /// type - The type of the contact (Extension, Trunk, Mobile, External).&#xD;&#xA;
    /// presenceState - Reports if the contact is online or offline.&#xD;&#xA;
    /// phoneState - Reports id the contact is currently busy or available.&#xD;&#xA;
    /// tel - The sip uri to use to reach this contact.&#xD;&#xA;
    /// email - The email of the contact.&#xD;&#xA;
    /// tags - Let the user to add its own tags to its contacts.
    /// </summary>
    public record ContactInfoParams(string id, string display, string username, string firstname, 
                                    string lastname, SIPContactType type, SIPContactPresenceState presenceState,
                                    SIPContactPhoneState phoneState, string tel, string email,
                                    ConcurrentDictionary<string, Object> tags);

    /// <summary>
    /// OnContactStateUpdated - Invoked when a contact state has changed (presence or phone).
    /// </summary>
    public record ContactsSubscriptions(Uri OnContactStateUpdated);

    /// <summary>
    /// identities - Array of identities in sip uri format.&#xD;&#xA;
    /// contactsSubscriptions - ContactsSubscriptions parameter block.
    /// </summary>
    public record ContactsSubscribeParams(string[] identities,
                                    ContactsSubscriptions contactsSubscriptions);

    /// <summary>
    /// identities - Array of identities in sip uri format.
    /// </summary>
    public record ContactsUnsubscribeParams(string[] identities);
#pragma warning restore 1591
}
