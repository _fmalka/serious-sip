﻿using SIPSorcery.SIP;
using System;
using static SeriousSIP.SIPBridge;

namespace SeriousSIP
{
#pragma warning disable CS1591
    
    /// <summary>
    /// OnCallPlayDone - Invoked when a play command is completed.&#xD;&#xA;
    /// OnCallRecordDone - Invoked when a record command is completed.&#xD;&#xA;
    /// </summary>
    public record BridgesSubscriptions(Uri OnBridgePlayDone, Uri OnBridgeRecordDone);

    /// <summary>
    /// identities - Array of bridge IDs.&#xD;&#xA;
    /// bridgesSubscriptions - CallMediaSubscriptions parameter block.&#xD;&#xA;
    /// </summary>
    public record BridgesSubscribeParams(string[] identities,
                                    BridgesSubscriptions bridgesSubscriptions);

    /// <summary>
    /// identities - Array of bridge IDs.&#xD;&#xA;
    /// </summary>
    public record BridgesUnsubscribeParams(string[] identities);

    /// <summary>
    /// aLegCallId - The unique ID of the A sip leg.&#xD;&#xA;
    /// bLegCallId - The unique ID of the B sip leg.&#xD;&#xA;
    /// relayDtmf - True if the bridge relays DTMF as well.
    /// </summary>
    public record BridgeCreateParams(string aLeg, string bLeg, bool relayDtmf);

    /// <summary>
    /// leg - Record ALeg, BLeg or Any.&#xD;&#xA;
    /// fileName - The file name to be played back.&#xD;&#xA;
    /// clearDigitsBuffer - If true, the digits buffer is cleared before the playback.&#xD;&#xA;
    /// stopOnDigit - Tf true, the playback is interrupted by any key pressed.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the playback will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record BridgePlayParams(SIPLeg leg, string fileName, bool clearDigitsBuffer, bool stopOnDigit, bool waitForCompletion);

    /// <summary>
    /// leg - Record ALeg, BLeg or Any.&#xD;&#xA;
    /// fileName - The file name to be played back.&#xD;&#xA;
    /// waitForCompletion - If true, the http request will reply only when the recording will be completed, otherwise a webhook shall be used to be notified on completion.&#xD;&#xA;
    /// </summary>
    public record BridgeRecordParams(SIPLeg leg, string fileName, bool waitForCompletion);

    /// <summary>
    /// id - The unique ID of the bridge.&#xD;&#xA;
    /// aLegCallId - The unique ID of the A sip leg.&#xD;&#xA;
    /// bLegCallId - The unique ID of the B sip leg.&#xD;&#xA;
    /// relayDtmf - True if the bridge relays DTMF as well.&#xD;&#xA;
    /// action - The current action being executed on the call (Idle, Playing, Recording).&#xD;&#xA;
    /// </summary>
    public record BridgeInfoParams(string id, string aLegCallId, string bLegCallId, bool relayDtmf, SIPBridgeAction action);

#pragma warning restore 1591    
}
