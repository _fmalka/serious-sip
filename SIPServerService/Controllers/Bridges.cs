﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Threading.Tasks;
using System;
using SIPSorcery.Media;
using static SeriousSIP.SIPBridge;
using Microsoft.AspNetCore.Authorization;

namespace SeriousSIP
{
    /// <summary>
    /// Bridge management and media control API set to performs media actions like play, record on bridged calls.
    /// </summary>
    [Authorize(Policy = "keypad-bridges")]
    [Route("api/v1/bridges/", Name = "Bridges")]
    public class Bridges : Controller
    {
        /// <summary>
        /// Subscribe to call server events to recieve webhook notifications on bridge play/record events.
        /// </summary>
        /// <param name="bridgesSubscribeParams">See the BridgesSubscribeParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("subscribe")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Subscribe([FromBody] BridgesSubscribeParams bridgesSubscribeParams)
        {
            foreach (string identity in bridgesSubscribeParams.identities)
            {
                if ((bridgesSubscribeParams.bridgesSubscriptions.OnBridgePlayDone != null) &&
                    (Uri.IsWellFormedUriString(bridgesSubscribeParams.bridgesSubscriptions.OnBridgePlayDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        bridgesSubscribeParams.bridgesSubscriptions.OnBridgePlayDone, EventUriType.OnBridgePlayDone))
                        return NotFound(new ResponseParams("error",
                                          bridgesSubscribeParams, "Failed to subscribe to OnBridgePlayDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((bridgesSubscribeParams.bridgesSubscriptions.OnBridgeRecordDone != null) &&
                    (Uri.IsWellFormedUriString(bridgesSubscribeParams.bridgesSubscriptions.OnBridgeRecordDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        bridgesSubscribeParams.bridgesSubscriptions.OnBridgeRecordDone, EventUriType.OnBridgeRecordDone))
                        return NotFound(new ResponseParams("error",
                                          bridgesSubscribeParams, "Failed to subscribe to OnBridgeRecordDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));
            }

            return Ok(new ResponseParams("success", bridgesSubscribeParams, "Subscriptions saved."));
        }

        /// <summary>
        /// Unsubscribe, cancel all the webhook bridges for one or more identities.
        /// </summary>
        /// <param name="bridgesUnsubscribeParams">See the BridgesUnsubscribeParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("unsubscribe")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Unubscribe([FromBody] BridgesUnsubscribeParams bridgesUnsubscribeParams)
        {
            foreach (string identity in bridgesUnsubscribeParams.identities)
            {
                if (!SIPServerService.GetInstance().UnsubscribeBridges(identity))
                {
                    NotFound(new ResponseParams("error", bridgesUnsubscribeParams,
                        $"Could not remove subscriptions for given identity {identity}."));
                }
            }

            return Ok(new ResponseParams("success", bridgesUnsubscribeParams, "Subscriptions removed."));
        }

        /// <summary>
        /// Retrieve all the bridges currently being handled by the call server.
        /// </summary>
        /// <returns>A list of bridges identified by their unique ID. See the BridgeInfoParams parameter block for details.</returns>
        [Route("")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult GetBridges()
        {
            if (SIPServerService.GetSIPServer().Bridges.Keys.Count == 0)
                return NotFound(new ResponseParams("error", null, "No bridges found."));

            BridgeInfoParams[] bridges = new BridgeInfoParams[SIPServerService.GetSIPServer().Bridges.Keys.Count];
            var i = 0;
            foreach (SIPBridge bridge in SIPServerService.GetSIPServer().Bridges.Values)
            {
                bridges[i++] = new BridgeInfoParams(bridge.Id, bridge.Calls[0].GetCallID(), bridge.Calls[1].GetCallID(),
                                                    bridge.RelayDtmf, bridge.Action);
            }

            return Ok(new ResponseParams("success", bridges,
                    $"{SIPServerService.GetSIPServer().Bridges.Keys.Count} bridges found."));
        }

        /// <summary>
        /// Retrieve information of one specific bridge using its unique ID.
        /// </summary>
        /// <param name="id">The unique ID of the bridge.</param>
        /// <returns>The high level call inforation for the requested call ID. See the CallInfoParams parameter block for details.</returns>
        [Route("bridge")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Bridge([FromQuery] string id)
        {
            if (!SIPServerService.GetSIPServer().Bridges.TryGetValue(id, out var bridge))
            {
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find bridge with given id."));
            }

            return Ok(new ResponseParams("success", new BridgeInfoParams(bridge.Id, bridge.Calls[0].GetCallID(),
                                                    bridge.Calls[1].GetCallID(), bridge.RelayDtmf, bridge.Action),
                                         "Bridge found."));
        }

        /// <summary>
        /// Create a new bridge.
        /// </summary>
        /// <param name="bridgeCreateParams">See the BridgeCreateParams parameter block for details.</param>
        /// <returns>See the BridgeInfoParams parameter block for details.</returns>
        [Route("bridge")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Bridge([FromBody] BridgeCreateParams bridgeCreateParams)
        {
            SIPCall aLeg = SIPServerService.GetSIPServer().GetCall(bridgeCreateParams.aLeg);
            if (aLeg == null)
                return NotFound(new ResponseParams("error", bridgeCreateParams, $"Could not find call with id {bridgeCreateParams.aLeg}."));

            SIPCall bLeg = SIPServerService.GetSIPServer().GetCall(bridgeCreateParams.bLeg);
            if (bLeg == null)
                return NotFound(new ResponseParams("error", bridgeCreateParams, $"Could not find call with id {bridgeCreateParams.bLeg}."));

            var bridge = SIPServerService.GetSIPServer().CreateBridge(aLeg, bLeg, bridgeCreateParams.relayDtmf);
            if (bridge == null)
                return NotFound(new ResponseParams("error", bridgeCreateParams, "Could not create bridge, please check input parameters."));

            return Ok(new ResponseParams("success", new BridgeInfoParams(bridge.Id, bridge.Calls[0].GetCallID(),
                                                    bridge.Calls[1].GetCallID(), bridge.RelayDtmf, bridge.Action),
                                        "Bridge created."));
        }

        /// <summary>
        /// Terminate a previously established bridge.
        /// </summary>
        /// <param name="id">The unique ID of the call to disconnect.</param>
        /// <param name="dropCalls">If true, drops the calls associated with the bridge.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("bridge/terminate")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Terminate([FromQuery] string id, [FromQuery] bool dropCalls)
        {
            if (!SIPServerService.GetSIPServer().Bridges.TryGetValue(id, out var bridge))
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find bridge with given id."));

            bridge.Terminate(dropCalls);

            return Ok(new ResponseParams("success", new { id = id }, "Bridge terminated."));
        }

        /// <summary>
        /// Playback an audio clip to the bridge.
        /// </summary>
        /// <param name="id">The unique ID of the bridge to playback on.</param>
        /// <param name="playParams">See the BridgePlayParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("bridge/play")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> Play([FromQuery] string id, [FromBody] BridgePlayParams playParams)
        {
            if (!SIPServerService.GetSIPServer().Bridges.TryGetValue(id, out var bridge))
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find bridge with given id."));

            if (playParams.waitForCompletion)
            {
                await bridge.PlayFile(playParams.leg, playParams.fileName, playParams.clearDigitsBuffer, playParams.stopOnDigit);
                return Ok(new ResponseParams("success", playParams, "Play done."));
            }
            else
            {
                _ = bridge.PlayFile(playParams.leg, playParams.fileName, playParams.clearDigitsBuffer, playParams.stopOnDigit);
                return Ok(new ResponseParams("success", playParams, "Play started."));
            }
        }

        /// <summary>
        /// Record the 2 bridged legs to an audio clip.
        /// </summary>
        /// <param name="id">The unique ID of the bridge to record on.</param>
        /// <param name="recordParams">See the BridgeRecordParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("bridge/record")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Record([FromQuery] string id, [FromBody] BridgeRecordParams recordParams)
        {
            if (!SIPServerService.GetSIPServer().Bridges.TryGetValue(id, out var bridge))
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find bridge with given id."));

            if (recordParams.waitForCompletion)
            {
                throw new NotSupportedException("Sync recording not yet supported on a bridge.");
            }
            else
            {
                bridge.RecordFile(recordParams.leg, recordParams.fileName);
                return Ok(new ResponseParams("success", recordParams, "Record done."));
            }
        }

        /// <summary>
        /// Stop a previous media action for a bridge.
        /// </summary>
        /// <param name="id">The unique ID of the bridge to stop the media action.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("bridge/play/stop")]
        [Route("bridge/record/stop")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult PlayStop([FromQuery] string id)
        {
            if (!SIPServerService.GetSIPServer().Bridges.TryGetValue(id, out var bridge))
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find bridge with given id."));

            bridge.PlayStop(SIPLeg.Any);

            return Ok(new ResponseParams("success", new { id = id }, "Play stopped."));
        }
    }
}
