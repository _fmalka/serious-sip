﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Threading.Tasks;
using System;
using SIPSorcery.Media;
using SIPSorcery.SIP;
using Microsoft.AspNetCore.Authorization;
using SIPSorcery.Net;

namespace SeriousSIP
{
    /// <summary>
    /// Call management and media control API set to generate outbound calls and get inbound calls, performs media actions like play, record, get digits.
    /// </summary>
    [Authorize(Policy = "keypad-calls")]
    [Route("api/v1/calls/", Name = "Calls")]
    public class Calls : Controller
    {
        /// <summary>
        /// Retrieve the current options set on the call server (only these that can be changed add-hoc).
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("options")]
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        public IActionResult Options()
        {
            return Ok(new ResponseParams("success", new OptionsParams(SIPServerService.GetSIPServer().AutoAcceptCalls,
                                                                      SIPServerService.GetSIPServer().AutoAnswerCalls,
                                                                      SIPServerService.GetSIPServer().DtmfMode),
                                         "Get options done."));
        }

        /// <summary>
        /// Set the call server default behavior with these options.
        /// </summary>
        /// <param name="optionsParams">See the OptionsParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("options")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        public IActionResult Options(OptionsParams optionsParams)
        {
            SIPServerService.GetSIPServer().AutoAcceptCalls = optionsParams.autoAccept;
            SIPServerService.GetSIPServer().AutoAnswerCalls = optionsParams.autoAnswer;
            SIPServerService.GetSIPServer().DtmfMode = optionsParams.dtmfMode;

            return Ok(new ResponseParams("success", optionsParams, "Set options done."));
        }

        /// <summary>
        /// Retrieve the current options set on the call server (only these that can be changed add-hoc).
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("statistics")]
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        public IActionResult Stats()
        {
            return Ok(new ResponseParams("success", SIPServerService.GetInstance().GetCallsStats(),
                                         "Get statistics done."));
        }

        /// <summary>
        /// Subscribe to call server events to recieve webhook notifications on incoming and outgoing calls.
        /// </summary>
        /// <param name="callsSubscribeParams">See the CallsSubscribeParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("subscribe")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Subscribe([FromBody] CallsSubscribeParams callsSubscribeParams)
        {
            foreach (string identity in callsSubscribeParams.identities)
            {
                if ((callsSubscribeParams.inboundCallsSubscriptions.OnCallOffered != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.inboundCallsSubscriptions.OnCallOffered.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.inboundCallsSubscriptions.OnCallOffered, EventUriType.OnCallOffered))
                        return NotFound(new ResponseParams("error", 
                                          callsSubscribeParams, "Failed to subscribe to OnCallOffered web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.inboundCallsSubscriptions.OnCallAccepted != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.inboundCallsSubscriptions.OnCallAccepted.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.inboundCallsSubscriptions.OnCallAccepted, EventUriType.OnCallAccepted))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallAccepted web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.inboundCallsSubscriptions.OnCallAnswered != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.inboundCallsSubscriptions.OnCallAnswered.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.inboundCallsSubscriptions.OnCallAnswered, EventUriType.OnCallAnswered))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallAnswered web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.inboundCallsSubscriptions.OnCallCancelled != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.inboundCallsSubscriptions.OnCallCancelled.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.inboundCallsSubscriptions.OnCallCancelled, EventUriType.OnCallCancelled))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallCancelled web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.outboundCallsSubscriptions.OnCallConnected != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.outboundCallsSubscriptions.OnCallConnected.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.outboundCallsSubscriptions.OnCallConnected, EventUriType.OnCallConnected))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallConnected web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.outboundCallsSubscriptions.OnCallTrying != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.outboundCallsSubscriptions.OnCallTrying.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.outboundCallsSubscriptions.OnCallTrying, EventUriType.OnCallTrying))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallTrying web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.outboundCallsSubscriptions.OnCallRinging != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.outboundCallsSubscriptions.OnCallRinging.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.outboundCallsSubscriptions.OnCallRinging, EventUriType.OnCallRinging))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallRinging web event, " +
                                             "make sure that the identity is correct and that " + 
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.outboundCallsSubscriptions.OnCallFailed != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.outboundCallsSubscriptions.OnCallFailed.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.outboundCallsSubscriptions.OnCallFailed, EventUriType.OnCallFailed))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallFailed web event, " +
                                             "make sure that the identity is correct and that " + 
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.outboundCallsSubscriptions.OnCallTimeout != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.outboundCallsSubscriptions.OnCallTimeout.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.outboundCallsSubscriptions.OnCallTimeout, EventUriType.OnCallTimeout))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallTimeout web event, " +
                                             "make sure that the identity is correct and that " + 
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.outboundCallsSubscriptions.OnCallDiverted != null) && 
                    (Uri.IsWellFormedUriString(callsSubscribeParams.outboundCallsSubscriptions.OnCallDiverted.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity, 
                        callsSubscribeParams.outboundCallsSubscriptions.OnCallDiverted, EventUriType.OnCallDiverted))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallDiverted web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.generalCallsSubscriptions.OnCallDisconnected != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.generalCallsSubscriptions.OnCallDisconnected.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.generalCallsSubscriptions.OnCallDisconnected, EventUriType.OnCallDisconnected))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallDisconnected web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.mediaCallsSubscriptions.OnCallGetDigitsDone != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.mediaCallsSubscriptions.OnCallGetDigitsDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.mediaCallsSubscriptions.OnCallGetDigitsDone, EventUriType.OnCallGetDigitsDone))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallGetDigitsDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayDone != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayDone, EventUriType.OnCallPlayDone))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallPlayDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.mediaCallsSubscriptions.OnCallSpeakDone != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.mediaCallsSubscriptions.OnCallSpeakDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.mediaCallsSubscriptions.OnCallSpeakDone, EventUriType.OnCallSpeakDone))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallSpeakDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayFilesDone != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayFilesDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayFilesDone, EventUriType.OnCallPlayFilesDone))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallPlayFilesDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayRecordDone != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayRecordDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.mediaCallsSubscriptions.OnCallPlayRecordDone, EventUriType.OnCallPlayRecordDone))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallPlayRecordDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.mediaCallsSubscriptions.OnCallRecordDone != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.mediaCallsSubscriptions.OnCallRecordDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.mediaCallsSubscriptions.OnCallRecordDone, EventUriType.OnCallRecordDone))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallRecordDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.mediaCallsSubscriptions.OnCallSendDigitsDone != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.mediaCallsSubscriptions.OnCallSendDigitsDone.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.mediaCallsSubscriptions.OnCallSendDigitsDone, EventUriType.OnCallSendDigitsDone))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallSendDigitsDone web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.callProgressSubscriptions.OnCallPositiveAnswerMachineDetection!= null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.callProgressSubscriptions.OnCallPositiveAnswerMachineDetection.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.callProgressSubscriptions.OnCallPositiveAnswerMachineDetection, EventUriType.OnCallPositiveAnswerMachineDetection))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallPositiveAnswerMachineDetection web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.callProgressSubscriptions.OnCallPositiveFaxDetection != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.callProgressSubscriptions.OnCallPositiveFaxDetection.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.callProgressSubscriptions.OnCallPositiveFaxDetection, EventUriType.OnCallPositiveFaxDetection))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallPositiveFaxDetection web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.callProgressSubscriptions.OnCallPositiveVoiceDetection != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.callProgressSubscriptions.OnCallPositiveVoiceDetection.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.callProgressSubscriptions.OnCallPositiveVoiceDetection, EventUriType.OnCallPositiveVoiceDetection))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallPositiveVoiceDetection web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((callsSubscribeParams.callProgressSubscriptions.OnCallRingbackToneDetection != null) &&
                    (Uri.IsWellFormedUriString(callsSubscribeParams.callProgressSubscriptions.OnCallRingbackToneDetection.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        callsSubscribeParams.callProgressSubscriptions.OnCallRingbackToneDetection, EventUriType.OnCallRingbackToneDetection))
                        return NotFound(new ResponseParams("error",
                                          callsSubscribeParams, "Failed to subscribe to OnCallPositiveVoiceDetection web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));
            }

            return Ok(new ResponseParams("success", callsSubscribeParams, "Subscriptions saved."));
        }

        /// <summary>
        /// Unsubscribe, cancel all the webhook calls for one or more identities.
        /// </summary>
        /// <param name="callsUnsubscribeParams">See the CallsUnsubscribeParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("unsubscribe")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Unubscribe([FromBody] CallsUnsubscribeParams callsUnsubscribeParams)
        {
            foreach(string identity in callsUnsubscribeParams.identities)
            {
                if (!SIPServerService.GetInstance().UnsubscribeCalls(identity)) 
                {
                    NotFound(new ResponseParams("error", callsUnsubscribeParams, 
                        $"Could not remove subscriptions for given identity {identity}."));
                }
            }

            return Ok(new ResponseParams("success", callsUnsubscribeParams, "Subscriptions removed."));
        }

        /// <summary>
        /// Retrieve all the calls currently being handled by the call server.
        /// </summary>
        /// <returns>A list of calls identified by their unique call ID. See the CallInfoParams parameter block for details.</returns>
        [Route("")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult GetCalls()
        {
            if (SIPServerService.GetSIPServer().Calls.Keys.Count == 0)
                return NotFound(new ResponseParams("error",null,"No calls found."));

            CallInfoParams[] calls = new CallInfoParams[SIPServerService.GetSIPServer().Calls.Keys.Count];
            var i = 0;
            foreach (SIPCall call in SIPServerService.GetSIPServer().Calls.Values)
            {
                string[] sipHeaders = null;
                if (call.GetLastSIPRequest() != null) 
                    sipHeaders = call.GetLastSIPRequest().Header.ToString().Split(SIPConstants.CRLF);

                uint packetsSentCount = 0;
                uint packetsReceivedCount = 0;
                if (call.IsWebRTC)
                {
                    if (call.RtcPeerConnection != null)
                    {
                        packetsSentCount = call.RtcPeerConnection.AudioRtcpSession.PacketsSentCount;
                        packetsReceivedCount = call.RtcPeerConnection.AudioRtcpSession.PacketsReceivedCount;
                    }
                }
                else
                {
                    if (call.RtpSession != null)
                    {
                        packetsSentCount = call.RtpSession.AudioRtcpSession.PacketsSentCount;
                        packetsReceivedCount = call.RtpSession.AudioRtcpSession.PacketsReceivedCount;
                    }
                }

                calls[i++] = new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                            call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(), call.GetCallDirection(),
                            packetsSentCount, packetsReceivedCount,
                            TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"), 
                            call.UserContext,
                            sipHeaders);
            }

            return Ok(new ResponseParams("success", calls,
                    $"{SIPServerService.GetSIPServer().Calls.Keys.Count} calls found."));
        }

        /// <summary>
        /// Retrieve all the calls currently being handled by the call server.
        /// </summary>
        /// <returns>A list of calls identified by their unique call ID. See the CallInfoParams parameter block for details.</returns>
        [Route("recent")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult GetRecentCalls()
        {
            try
            {
                return Ok(new ResponseParams("success", SIPServerService.GetInstance().GetRecentCalls(),
                    $"{SIPServerService.GetSIPServer().Calls.Keys.Count} calls found."));
            }
            catch 
            {

            }

            return NotFound(new ResponseParams("error", null, "No recent calls found."));
        }

        /// <summary>
        /// Retrieve information of one specific call using its unique ID.
        /// </summary>
        /// <param name="id">The unique ID of the call.</param>
        /// <returns>The high level call inforation for the requested call ID. See the CallInfoParams parameter block for details.</returns>
        [Route("call")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult GetCall([FromQuery] string id)
        {
            SIPCall call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            string[] sipHeaders = null;
            if (call.GetLastSIPRequest() != null)
                sipHeaders = call.GetLastSIPRequest().Header.ToString().Split(SIPConstants.CRLF);

            if (call.IsWebRTC)
            {
                return Ok(new ResponseParams("success", new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                            call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(), call.GetCallDirection(),
                            (call.MediaSession as RTCPeerConnection).AudioRtcpSession.PacketsSentCount,
                            (call.MediaSession as RTCPeerConnection).AudioRtcpSession.PacketsReceivedCount,
                            TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"), 
                            call.UserContext, sipHeaders),
                            "Call found."));
            }
            else
            {
                return Ok(new ResponseParams("success", new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(),
                            call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(), call.GetCallDirection(),
                            (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                            (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                            TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                            call.UserContext, sipHeaders),
                            "Call found."));
            }
        }

        /// <summary>
        /// Retrieve the current state of a call.
        /// </summary>
        /// <param name="id">The unique ID of the call.</param>
        /// <returns>The current state of the call. (Active, Ringing, OnHold)</returns>
        [Route("call/state")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult GetCallState([FromQuery] string id)
        {
            SIPCall call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            return Ok(new ResponseParams("success", new { id = id, state = call.GetCallState() }, "Call state done."));
        }

        /// <summary>
        /// Retrieve the current action executed on a call.
        /// </summary>
        /// <param name="id">The unique ID of the call.</param>
        /// <returns>The current action executed on the call. (Idle, Playing, Recording, SendingDigits, ReceivingDigits)</returns>
        [Route("call/action")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult GetCallAction([FromQuery] string id)
        {
            SIPCall call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            return Ok(new ResponseParams("success", new { id = id, action = call.GetCallAction() }, "Call action done."));
        }

        /// <summary>
        /// Retrieve the time elapsed since the call has been established.
        /// </summary>
        /// <param name="id">The unique ID of the call.</param>
        /// <returns>The duration of the call in "hh:mm:ss" format.</returns>
        [Route("call/duration")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult GetCallDuration([FromQuery] string id)
        {
            SIPCall call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            return Ok(new ResponseParams("success", 
                        new { id = id, duration = TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss") },
                        "Call duration done."));
        }

        /// <summary>
        /// Create a new outbound call.
        /// </summary>
        /// <param name="makeCallParams">See the MakeCallParams parameter block for details.</param>
        /// <returns>See the CallInfoParams parameter block for details.</returns>
        [Route("call/make")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> MakeCall([FromBody] MakeCallParams makeCallParams)
        {
            var call = await SIPServerService.GetSIPServer().MakeCall(makeCallParams.to, makeCallParams.identity, 
                                                                        makeCallParams.sipHeaders, makeCallParams.cpaTimeout, 
                                                                        makeCallParams.waitForCompletion);

            if (call == null)
                return NotFound(new ResponseParams("error", makeCallParams, "Could not make call, please check input parameters."));
            if (call.GetCallID() == null)
                return NotFound("Something went wrong with the outbound call, please check input parameters.");

            call.UserContext = makeCallParams.userContext;
            call.OnPositiveAnswerMachineDetection += () => SIPServerService.GetInstance().OnCallPositiveAnswerMachineDetection(call);
            call.OnPositiveFaxDetection += () => SIPServerService.GetInstance().OnCallPositiveFaxDetection(call);
            call.OnPositiveVoiceDetection += () => SIPServerService.GetInstance().OnCallPositiveVoiceDetection(call);
            call.OnRingbackToneDetection += () => SIPServerService.GetInstance().OnCallRingbackToneDetection(call);

            string[] sipHeaders = null;
            if (call.GetLastSIPRequest() != null)
                sipHeaders = call.GetLastSIPRequest().Header.ToString().Split(SIPConstants.CRLF);

            return Ok(new ResponseParams("success", new CallInfoParams(call.GetCallID(), call.GetFromURI().ToString(), 
                                       call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(), call.GetCallDirection(),
                                       (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                       (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                       TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                       call.UserContext, sipHeaders),
                                        "Maked call done."));
        }

        /// <summary>
        /// Accepts an inbound call (response to the OnCallOffered event).
        /// </summary>
        /// <param name="id">The unique ID of the call to accept.</param>
        /// <param name="sipHeaders">Optional SIP headers to populate in the answer message.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/accept")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> AcceptCall([FromQuery] string id, [FromBody] string[] sipHeaders)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            await call.Accept(sipHeaders, 0);

            return Ok(new ResponseParams("success", new { id = id }, "Accept call done."));
        }

        /// <summary>
        /// Answers an inbound call (response to the OnCallAccepted event).
        /// </summary>
        /// <param name="id">The unique ID of the call to answer.</param>
        /// <param name="sipHeaders">Optional SIP headers to populate in the answer message.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/answer")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> AnswerCall([FromQuery] string id, [FromBody] string[] sipHeaders)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            await call.Answer(sipHeaders);

            return Ok(new ResponseParams("success", new { id = id }, "Answer call done."));
        }

        /// <summary>
        /// Terminate a previously established (or ringing) call.
        /// </summary>
        /// <param name="id">The unique ID of the call to disconnect.</param>
        /// <param name="sipHeaders">Custom sip headers to add to BYE headers</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/drop")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult DropCall([FromQuery] string id, [FromBody] string[] sipHeaders)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            if (call.GetCallDirection() == SIPCallDirection.Out &&
                !call.IsCallActive)
            {
                call.Cancel(sipHeaders);
            }
            else if (call.GetCallDirection() == SIPCallDirection.In &&
                (call.GetCallState() == SIPCallState.Offered) ||
                (call.GetCallState() == SIPCallState.Accepted))
            {
                _ = call.Reject(SIPResponseStatusCodesEnum.Gone, "Call denied by application");
            }
            else
            {
                call.Hangup(sipHeaders);
            }

            return Ok(new ResponseParams("success", new { id = id }, "Drop call done."));
        }

        /// <summary>
        /// Place a call on hold.
        /// </summary>
        /// <param name="id">The unique ID of the call to accept.</param>
        /// <param name="streamStatus">The holding type (Inactive, RecvOnly, SendOnly).</param>
        /// <returns>See CallInfoParams pareameter block for details.</returns>
        [Route("call/hold")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Hold([FromQuery] string id, string streamStatus)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            var s = (SIPSorcery.Net.MediaStreamStatusEnum)Enum.Parse(typeof(SIPSorcery.Net.MediaStreamStatusEnum), streamStatus);
            if (call.IsCallActive)
                call.PutOnHold(s);
            else
                return NotFound(new ResponseParams("error", new { id = id }, $"Could not put the call on hold because call state is {call.GetCallState()}."));

            string[] sipHeaders = null;
            if (call.GetLastSIPRequest() != null)
                sipHeaders = call.GetLastSIPRequest().Header.ToString().Split(SIPConstants.CRLF);

            return Ok(new ResponseParams("success", new CallInfoParams(id, call.GetFromURI().ToString(),
                                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                                call.GetCallDirection(),
                                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                                call.UserContext, sipHeaders),
                                                "Hold call done."));
        }

        /// <summary>
        /// Retrieve a call, take off hold.
        /// </summary>
        /// <param name="id">The unique ID of the call to accept.</param>
        /// <returns>See CallInfoParams pareameter block for details.</returns>
        [Route("call/retrieve")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Retrieve([FromQuery] string id)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            if (call.IsCallActive)
                call.TakeOffHold();
            else
                return NotFound(new ResponseParams("error", new { id = id }, $"Could not retreive the call because call state is {call.GetCallState()}."));

            string[] sipHeaders = null;
            if (call.GetLastSIPRequest() != null)
                sipHeaders = call.GetLastSIPRequest().Header.ToString().Split(SIPConstants.CRLF);

            return Ok(new ResponseParams("success", new CallInfoParams(id, call.GetFromURI().ToString(),
                                                call.GetToURI().ToString(), call.GetCallState(), call.GetCallAction(),
                                                call.GetCallDirection(),
                                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsSentCount,
                                                (call.MediaSession as VoIPMediaSession).AudioRtcpSession.PacketsReceivedCount,
                                                TimeSpan.FromMilliseconds(call.GetDuration()).ToString(@"hh\:mm\:ss"),
                                                call.UserContext, sipHeaders),
                                                "Retrieve call done."));
        }

        /// <summary>
        /// Performs a blind transfer to a non established destination.
        /// </summary>
        /// <param name="id">The unique ID of the call to transfer.</param>
        /// <param name="blindTransferParams">See the BlindTransferParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/transfer/blind")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult BlindTransfer([FromQuery] string id, [FromBody] BlindTransferParams blindTransferParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            if ((call.IsCallActive) || call.IsOnLocalHold || call.IsOnRemoteHold)
                _ = call.BlindTransfer(blindTransferParams.to, blindTransferParams.sipHeaders, blindTransferParams.timeout, blindTransferParams.disconnect);
            else
                return NotFound(new ResponseParams("error", new { id = id }, $"Could not transfer call because call state is {call.GetCallState()}."));

            return Ok(new ResponseParams("success", new { id = id }, "Transfer call done."));
        }

        /// <summary>
        /// Performs an attended transfer between 2 established calls.
        /// </summary>
        /// <param name="attendedTransferParams">See the AttendedTransferParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/transfer/attended")]
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult AttendedTransfer([FromBody] AttendedTransferParams attendedTransferParams)
        {
            var call1 = SIPServerService.GetSIPServer().GetCall(attendedTransferParams.id1);
            if (call1 == null)
                return NotFound(new ResponseParams("error", new { id = attendedTransferParams.id1 }, "Could not find call with given id."));

            var call2 = SIPServerService.GetSIPServer().GetCall(attendedTransferParams.id2);
            if (call2 == null)
                return NotFound(new ResponseParams("error", new { id = attendedTransferParams.id2 }, "Could not find call with given id."));

            if (((call1.IsCallActive) || call1.IsOnLocalHold || call1.IsOnRemoteHold) &&
                    ((call2.IsCallActive) || call2.IsOnLocalHold || call2.IsOnRemoteHold))
                _ = call1.AttendedTransfer(call2, attendedTransferParams.sipHeaders, attendedTransferParams.timeout);
            else
                return NotFound(new ResponseParams("error", new { id1 = attendedTransferParams.id1, id2 = attendedTransferParams.id2 }, 
                                    $"Could not transfer call because wrong call states" +
                                    $"call1 state is {call1.GetCallState()}" +
                                    $"call2 state is {call2.GetCallState()}."));

            return Ok(new ResponseParams("success", new { id1 = attendedTransferParams.id1, id2 = attendedTransferParams.id2 }, "Transfer call done."));
        }

        /// <summary>
        /// Playback an audio clip to the remote party.
        /// </summary>
        /// <param name="id">The unique ID of the call to playback on.</param>
        /// <param name="playParams">See the PlayParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/play")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> Play([FromQuery] string id, [FromBody] PlayParams playParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = playParams.userContext;

            if (playParams.waitForCompletion)
            {
                await call.PlayFile(playParams.fileName, playParams.clearDigitsBuffer, playParams.stopOnDigit);
                return Ok(new ResponseParams("success", playParams, "Play done."));
            }
            else
            {
                _ = call.PlayFile(playParams.fileName, playParams.clearDigitsBuffer, playParams.stopOnDigit);
                return Ok(new ResponseParams("success", playParams, "Play started."));
            }
         }

        /// <summary>
        /// Playback music to the remote party.
        /// </summary>
        /// <param name="id">The unique ID of the call to playback on.</param>
        /// <param name="playParams">See the PlayParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/play/music")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> PlayMusic([FromQuery] string id)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.PlayMusic();

            return Ok(new ResponseParams("success", new { callId = id }, "Play music started."));
        }

        /// <summary>
        /// Playback an audio clip to the remote party.
        /// </summary>
        /// <param name="id">The unique ID of the call to playback on.</param>
        /// <param name="playLoopParams">See the PlayParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/playloop")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> PlayLoop([FromQuery] string id, [FromBody] PlayLoopParams playLoopParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = playLoopParams.userContext;

            if (playLoopParams.waitForCompletion)
            {
                await call.PlayLoop(playLoopParams.fileName, playLoopParams.interval,
                    playLoopParams.clearDigitsBuffer, playLoopParams.stopOnDigit);
                return Ok(new ResponseParams("success", playLoopParams, "Play done."));
            }
            else
            {
                _ = call.PlayLoop(playLoopParams.fileName, playLoopParams.interval,
                        playLoopParams.clearDigitsBuffer, playLoopParams.stopOnDigit);
                return Ok(new ResponseParams("success", playLoopParams, "Play started."));
            }
        }

        /// <summary>
        /// Stop a previous media action for a call.
        /// </summary>
        /// <param name="id">The unique ID of the call to stop the media action.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/play/stop")]
        [Route("call/playfiles/stop")]
        [Route("call/record/stop")]
        [Route("call/playrecord/stop")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult PlayStop([FromQuery] string id)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.PlayStop();

            return Ok(new ResponseParams("success", new { id = id }, "Play stopped."));
        }

        /// <summary>
        /// Playback a set of audio clips one after the other to the remote party.
        /// </summary>
        /// <param name="id">The unique ID of the call to playback on.</param>
        /// <param name="playFilesParams">See the PlayFilesParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/playfiles")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> Play([FromQuery] string id, [FromBody] PlayFilesParams playFilesParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = playFilesParams.userContext;

            if (playFilesParams.waitForCompletion)
            {
                await call.PlayFiles(playFilesParams.fileNames, playFilesParams.interval, playFilesParams.clearDigitsBuffer, playFilesParams.stopOnDigit);
                return Ok(new ResponseParams("success", playFilesParams, "Play files done."));
            }
            else
            {
                _ = call.PlayFiles(playFilesParams.fileNames, playFilesParams.interval, playFilesParams.clearDigitsBuffer, playFilesParams.stopOnDigit);
                return Ok(new ResponseParams("success", playFilesParams, "Play files started."));
            }
        }

        /// <summary>
        /// Record the remote party audio to an audio clip.
        /// </summary>
        /// <param name="id">The unique ID of the call to record on.</param>
        /// <param name="recordParams">See the RecordParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/record")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> Play([FromQuery] string id, [FromBody] RecordParams recordParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = recordParams.userContext;

            if (recordParams.waitForCompletion)
            {
                await call.RecordFile(recordParams.fileName, recordParams.duration,
                                  recordParams.terminationDigit, recordParams.playTone, recordParams.stopOnSilence);
                return Ok(new ResponseParams("success", recordParams, "Record done."));
            }
            else
            {
                _ = call.RecordFile(recordParams.fileName, recordParams.duration,
                                  recordParams.terminationDigit, recordParams.playTone, recordParams.stopOnSilence);
                return Ok(new ResponseParams("success", recordParams, "Record started."));
            }
        }

        /// <summary>
        /// Play an audio clip and then record the remote party audio to an audio clip.
        /// </summary>
        /// <param name="id">The unique ID of the call to playback on.</param>
        /// <param name="playRecordParams">See the PlayRecordParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/playrecord")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> PlayRecord([FromQuery] string id, [FromBody] PlayRecordParams playRecordParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = playRecordParams.userContext;

            if (playRecordParams.waitForCompletion)
            {
                await call.PlayRecord(playRecordParams.playFileName, playRecordParams.recordFileName,
                                  playRecordParams.duration, playRecordParams.terminationDigit, playRecordParams.playTone, playRecordParams.stopOnSilence);
                return Ok(new ResponseParams("success", playRecordParams, "PlayRecord done."));
            }
            else
            {
                _ = call.PlayRecord(playRecordParams.playFileName, playRecordParams.recordFileName,
                                  playRecordParams.duration, playRecordParams.terminationDigit, playRecordParams.playTone, playRecordParams.stopOnSilence);
                return Ok(new ResponseParams("success", playRecordParams, "PlayRecord started."));
            }
        }

        /// <summary>
        /// Speak a text to the remote party audio stream.
        /// </summary>
        /// <param name="id">The unique ID of the call to playback on.</param>
        /// <param name="speakParams">See the SpeakParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/speak")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> Speak([FromQuery] string id, [FromBody] SpeakParams speakParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = speakParams.userContext;

            if (speakParams.waitForCompletion)
            {
                await call.Speak(speakParams.text, speakParams.clearDigitsBuffer, speakParams.stopOnDigit);
                return Ok(new ResponseParams("success", speakParams, "Speak done."));
            }
            else
            {
                _ = call.Speak(speakParams.text, speakParams.clearDigitsBuffer, speakParams.stopOnDigit);
                return Ok(new ResponseParams("success", speakParams, "Speak started."));
            }
        }

        /// <summary>
        /// Speak a SSML to the remote party audio stream.
        /// </summary>
        /// <param name="id">The unique ID of the call to playback on.</param>
        /// <param name="speakSsmlParams">See the SpeakSsmlParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/speakssml")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> SpeakSsml([FromQuery] string id, [FromBody] SpeakSsmlParams speakSsmlParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = speakSsmlParams.userContext;

            if (speakSsmlParams.waitForCompletion)
            {
                await call.SpeakSsml(speakSsmlParams.ssml, speakSsmlParams.clearDigitsBuffer, speakSsmlParams.stopOnDigit);
                return Ok(new ResponseParams("success", speakSsmlParams, "Speak SSML done."));
            }
            else
            {
                _ = call.SpeakSsml(speakSsmlParams.ssml, speakSsmlParams.clearDigitsBuffer, speakSsmlParams.stopOnDigit);
                return Ok(new ResponseParams("success", speakSsmlParams, "Speak SSML started."));
            }
        }

        /// <summary>
        /// Retrieve some DTMF digits from the incoming audio stream.
        /// </summary>
        /// <param name="id">The unique ID of the call to collect digits from.</param>
        /// <param name="getDigitsParams">See the GetDigitsParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/dtmf/get")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> GetDigits([FromQuery] string id, [FromBody] GetDigitsParams getDigitsParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = getDigitsParams.userContext;

            if (getDigitsParams.waitForCompletion)
            {
                var res = await call.GetDigits(getDigitsParams.duration, getDigitsParams.numOfDigits,
                                 getDigitsParams.terminationDigit, getDigitsParams.clearDigitsBuffer);
                return Ok(new ResponseParams("success", new { id = id, digits = res }, "Get dtmf done."));
            }
            else
            {
                _ = call.GetDigits(getDigitsParams.duration, getDigitsParams.numOfDigits,
                                 getDigitsParams.terminationDigit, getDigitsParams.clearDigitsBuffer);
                return Ok(new ResponseParams("success", getDigitsParams, "Get dtmf started."));
            }
        }

        /// <summary>
        /// Send some DTMF digits to the outgoing audio stream.
        /// </summary>
        /// <param name="id">The unique ID of the call to collect digits from.</param>
        /// <param name="sendDigitsParams">See the SendDigitsParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("call/dtmf/send")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> SendDigits([FromQuery] string id, [FromBody] SendDigitsParams sendDigitsParams)
        {
            var call = SIPServerService.GetSIPServer().GetCall(id);
            if (call == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            call.UserContext = sendDigitsParams.userContext;

            if (sendDigitsParams.waitForCompletion)
            {
                await call.SendDigits(sendDigitsParams.digits);
                return Ok(new ResponseParams("success", sendDigitsParams, "Send dtmf done."));
            }
            else
            {
                _ = call.SendDigits(sendDigitsParams.digits);
                return Ok(new ResponseParams("success", sendDigitsParams, "Send dtmf started."));
            }
        }

    }
}
