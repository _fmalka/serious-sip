﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace SeriousSIP
{
    /// <summary>
    /// Account management and resource allocation API set.
    /// </summary>
    [Authorize(Policy = "keypad-system")]
    public class Admin : Controller
    {
        /// <summary>
        /// Retrieve the system configuration.
        /// </summary>
        /// <returns>The ConfigParams structure.</returns>
        [Route("/api/v1/admin/config")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Config()
        {
            ConfigSipParams configSipParams = null;
            if (Program.Config.GetSection("sipServer") != null)
            {
                IConfigurationSection cfg = Program.Config.GetSection("sipServer");
                configSipParams = new ConfigSipParams(
                      cfg.GetSection("localIPEndPoints").GetChildren().Select(x => x.Value).ToArray(),
                      cfg.GetSection("registerAccounts").GetChildren().Select(x => x.Value).ToArray(),
                      cfg.GetSection("monitorTrunks").GetChildren().Select(x => x.Value).ToArray(),
                      cfg.GetSection("codecs").GetChildren().Select(x => x.Value).ToArray(),
                      cfg.GetValue("rtpAddress", cfg.GetSection("localIPEndPoints").GetChildren().Select(x => x.Value).ToArray()[0].Split(',')[1]),
                      cfg.GetValue("dtmfMode", "Rfc2833"),
                      cfg.GetValue("autoAcceptCalls", true),
                      cfg.GetValue("autoAnserCalls", true),
                      cfg.GetValue("acceptDelay", 0),
                      cfg.GetValue("useMultiMediaTimers", false),
                      cfg.GetValue("hangupWhenNoRtp", true),
                      cfg.GetValue("registrationInterval", 250));
            }

            ConfigSmppParams configSmppParams = null;
            if (Program.Config.GetSection("smpp") != null)
            {
                configSmppParams = new ConfigSmppParams(
                     Program.Config.GetSection("smpp")["systemId"],
                     Program.Config.GetSection("smpp")["password"],
                     Program.Config.GetSection("smpp")["host"],
                     Program.Config.GetSection("smpp").GetValue("port", 2775),
                     Program.Config.GetSection("smpp")["systemType"],
                     Program.Config.GetSection("smpp")["serviceType"],
                     Program.Config.GetSection("smpp")["sourceAddress"]);
            }

            ConfigServiceParams configServiceParams = new ConfigServiceParams(
                    Program.Config.GetValue("instance", "SIPServerService#1"),
                     Program.Config.GetValue("clientUrl", ""),
                     Program.Config.GetValue("autoStartSipServer", true));

            ConfigParams configParams = new ConfigParams(
                     configServiceParams,
                     configSipParams, 
                     configSmppParams);

            return Ok(new ResponseParams("success", configParams,
                $"configuration retreived."));
        }

        /// <summary>
        /// Set the system configuration parameters.
        /// </summary>
        /// <returns>The ConfigParams structure.</returns>
        [Route("/api/v1/admin/config")]
        [Authorize]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Config([FromBody] ConfigParams configParams)
        {
            if (SIPServerService.GetSIPServer().State == SIPServerState.NotStarted)
            {
                SIPServerService.GetInstance().AutoStartSipServer = configParams.serviceParams.autoStartSipServer;
                SIPServerService.GetInstance().ClientUrl = configParams.serviceParams.clientUrl;

                if (configParams.sipParams != null)
                {
                    SIPServerService.GetSIPServer().AcceptDelay = configParams.sipParams.acceptDelay;
                    SIPServerService.GetSIPServer().AutoAcceptCalls = configParams.sipParams.autoAcceptCalls;
                    SIPServerService.GetSIPServer().AutoAnswerCalls = configParams.sipParams.autoAnswerCalls;
                    SIPServerService.GetSIPServer().UseMultiMediaTimers = configParams.sipParams.useMultiMediaTimers;
                    SIPServerService.GetSIPServer().HangupWhenNoRtp = configParams.sipParams.hangupWhenNoRtp;
                    SIPServerService.GetSIPServer().RegistrationInterval = configParams.sipParams.registrationInterval;
                    SIPServerService.GetSIPServer().DtmfMode = (SIPCallDtmfMode)Enum.Parse(typeof(SIPCallDtmfMode), configParams.sipParams.dtmfMode);

                    if (configParams.sipParams.rtpAddress != null)
                    {
                        SIPServerService.GetSIPServer().RtpAddress = IPAddress.Parse(configParams.sipParams.rtpAddress);
                    }

                    if (configParams.sipParams.localIpEndPoints != null)
                    {
                        StringCollection col = new StringCollection();
                        col.AddRange(configParams.sipParams.localIpEndPoints);
                        SIPServerService.GetSIPServer().LocalIPEndPoints = col;
                    }

                    if (configParams.sipParams.registerAccounts != null)
                    {
                        StringCollection col = new StringCollection();
                        col.AddRange(configParams.sipParams.registerAccounts);
                        SIPServerService.GetSIPServer().RegisterAccounts = col;
                    }

                    if (configParams.sipParams.monitorTrunks != null)
                    {
                        StringCollection col = new StringCollection();
                        col.AddRange(configParams.sipParams.monitorTrunks);
                        SIPServerService.GetSIPServer().MonitorTrunks = col;
                    }

                    if (configParams.sipParams.codecs != null)
                    {
                        StringCollection col = new StringCollection();
                        col.AddRange(configParams.sipParams.codecs);
                        SIPServerService.GetSIPServer().SetCodecs(col);
                    }
                }

                if (configParams.smppParams != null)
                {
                    SmppConfiguration _smppConfiguration = new SmppConfiguration()
                    {
                        SystemID = configParams.smppParams.systemId,
                        Password = configParams.smppParams.password,
                        Host = configParams.smppParams.host,
                        Port = configParams.smppParams.port,
                        SystemType = configParams.smppParams.systemType,
                        DefaultServiceType = configParams.smppParams.serviceType,
                        SourceAddress = configParams.smppParams.sourceAddress,
                        Encoding = JamaaTech.Smpp.Net.Lib.DataCoding.UCS2,
                        AddressNpi = JamaaTech.Smpp.Net.Lib.NumberingPlanIndicator.Unknown,
                        AddressTon = JamaaTech.Smpp.Net.Lib.TypeOfNumber.Alphanumeric,
                        UserMessageReferenceType = UserMessageReferenceType.None,
                        RegisterDeliveryNotification = true,
                        UseSeparateConnections = true
                    };
                    SIPServerService.GetSIPServer().SmppConfig = _smppConfiguration;
                }

                return Ok(new ResponseParams("success", configParams,
                    $"new settings applied."));
            }
            else
            {
                return NotFound(new ResponseParams("failed", configParams,
                    $"sip server must be stopped before changing settings, current state is {SIPServerService.GetSIPServer().State}."));
            }
        }

        /// <summary>
        /// Retrieve all the identities provisioned in your account.
        /// </summary>
        /// <returns>A list of identities in a sip uri format.</returns>
        [Route("/api/v1/admin/identities")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]        
        public IActionResult GetIdentities()
        {
            Dictionary<string, SIPIdentity> identities = SIPServerService.GetSIPServer().GetIdentities();

            if (!(identities.Count > 0))
                return NotFound(new ResponseParams("error", null,
                    "No identities configured. please contact technical support."));

            return Ok(new ResponseParams("success", identities.Values.ToArray(),
                $"{identities.Count} identities found."));
        }

        /// <summary>
        /// Retrieve a list of media clips 
        /// </summary>
        /// <returns>A list of media clips.</returns>
        [Route("/api/v1/admin/files/clips")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Clips()
        {
            FilesParams[] files = SIPServerService.GetInstance().GetClips();
            if (files == null)
                return NotFound(new ResponseParams("error", null,
                    $"No media clips found."));

            return Ok(new ResponseParams("success", files,
                $"{files.Length} media clips found."));
        }

        /// <summary>
        /// Delete a media clips 
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/files/clips")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Clips([FromQuery] string filename)
        {
            if (SIPServerService.GetInstance().DelClip(filename))
                return Ok(new ResponseParams("success", filename,
                $"1 media clip deleted."));
            else
                return NotFound(new ResponseParams("error", filename,
                    $"Could not delete media clip."));
        }

        /// <summary>
        /// Retrieve a list of log files
        /// </summary>
        /// <returns>A list of log files.</returns>
        [Route("/api/v1/admin/files/logs")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Logs()
        {
            FilesParams[] files = SIPServerService.GetInstance().GetLogs();
            if (files == null)
                return NotFound(new ResponseParams("error", null,
                    $"No log files found."));

            return Ok(new ResponseParams("success", files,
                $"{files.Length} log files found."));
        }

        /// <summary>
        /// Delete a log file
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/files/logs")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Logs([FromQuery] string filename)
        {
            if (SIPServerService.GetInstance().DelLog(filename))
                return Ok(new ResponseParams("success", filename,
                $"1 log file deleted."));
            else
                return NotFound(new ResponseParams("error", filename,
                    $"Could not delete log file."));
        }

        /// <summary>
        /// Retrieve a script saved for a specific identity.
        /// </summary>
        /// <returns>A list of ScriptCommands registred as a script.</returns>
        [Route("/api/v1/admin/identity/script")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Script([FromQuery] string identity)
        {
            ScriptCommand[] script = SIPServerService.GetInstance().GetRegisteredScript(identity);
            if (script == null)
                return NotFound(new ResponseParams("error", null,
                    $"No script found for identity {identity}."));

            return Ok(new ResponseParams("success", script,
                $"Script found for identity {identity}."));
        }

        /// <summary>
        /// Save a script to be executed automatically for inbound calls to the specified identity.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/identity/script")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Script([FromQuery] string identity, [FromBody] ScriptCommand[] scriptCommands)
        {
            if (!SIPServerService.GetInstance().SetRegisteredScript(identity, scriptCommands))
                return NotFound(new ResponseParams("error", null,
                    $"Could not save script for identity {identity}, please remove a previous script before you add a new one."));
         
            return Ok(new ResponseParams("success", scriptCommands,
                $"Script saved for identity {identity}."));
        }

        /// <summary>
        /// Delete a saved script for a specified identity.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/identity/script")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult DelScript([FromQuery] string identity)
        {
            if (!SIPServerService.GetInstance().DelRegisteredScript(identity))
                return NotFound(new ResponseParams("error", null,
                    $"Could not delete script for identity {identity}, no script must be loaded."));

            return Ok(new ResponseParams("success", null,
                $"Script deleted for identity {identity}."));
        }

        /// <summary>
        /// Get the current system status.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/system/status")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Status()
        {
            return Ok(new ResponseParams("success", new { status = SIPServerService.GetSIPServer().State },
                $"System status returned."));
        }
        /// <summary>
        /// Start the system.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/system/start")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> Start()
        {
            _ = SIPServerService.GetInstance().StartSIPServer();

                return Ok(new ResponseParams("success", null,
                    $"System is starting."));
        }

        /// <summary>
        /// Restart the system.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/system/restart")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Restart([FromQuery] bool force)
        {
            if (SIPServerService.GetInstance().Restart(force))
                return Ok(new ResponseParams("success", null,
                    $"System is restarting."));
            else
                return NotFound(new ResponseParams("failed", null,
                    $"Could not restart the systems, check logs for more information."));
        }

        /// <summary>
        /// Shutdown the system.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/system/shutdown")]
        [HttpPut]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Shutdown([FromQuery] bool force)
        {
            _ = SIPServerService.GetInstance().StopSIPServer(force);

            return Ok(new ResponseParams("success", null,
                $"Shutdown initiated."));
        }

        /// <summary>
        /// Cleanup the logs and temporary files.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("/api/v1/admin/system/cleanup")]
        [HttpDelete]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Cleanup([FromQuery] string identity)
        {
            if (!SIPServerService.GetInstance().DelLog("*"))
                return NotFound(new ResponseParams("error", null,
                    $"Could not delete log files."));

            return Ok(new ResponseParams("success", null,
                $"Log directory cleaned up."));
        }
    }
}
