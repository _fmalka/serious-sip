﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace SeriousSIP
{
    /// <summary>
    /// Administration API set to connect, configure and perform management tasks.
    /// </summary>
    [Authorize]    
    public class AAuth : ControllerBase
    {
        private readonly ILogger<AAuth> _logger;
        private readonly IJwtAuthManager _jwtAuthManager;

        /// <summary>
        /// Authenicator class to support JWT token generation and distribution.
        /// </summary>
        public AAuth(ILogger<AAuth> logger, IJwtAuthManager jwtAuthManager)
        {
            _logger = logger;
            _jwtAuthManager = jwtAuthManager;
        }

        /// <summary>
        /// Connects (Authenticate) locally to the service without oAuth2 service.
        /// </summary>
        /// <param name="request">The strong secret to identify with.</param>
        /// <returns>If strong secret validated, returns a JWT token to be used in Authorization Header as Bearer.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("/api/v1/aauth/connect", Order = 0)]
        public ActionResult Connect([FromBody] LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var jwtTokenConfig = Program.Config.GetSection("usePrivateToken").Get<JwtTokenConfig>();
            if (jwtTokenConfig == null)
            {
                return Unauthorized();
            }
            else
            {
                if (jwtTokenConfig.Secret != request.secret)
                {
                    return Unauthorized();
                }
            }

            var userName = "admin@modulo.co.il";
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, userName),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim("scope", "keypad-system"),
                new Claim("scope", "keypad-bridges"),
                new Claim("scope", "keypad-calls"),
                new Claim("scope", "keypad-contacts"),
                new Claim("scope", "keypad-messages")
            };

            var jwtResult = _jwtAuthManager.GenerateTokens(userName, claims, DateTime.Now);
            _logger.LogInformation($"User [{userName}] logged in the system.");
            return Ok(new ResponseParams("success",
                                    new LoginResult { accessToken = jwtResult.AccessToken, refreshToken = jwtResult.RefreshToken.TokenString },
                                    "login successful."));
        }

        /// <summary>
        /// Disconnects from the local service and dismiss the JWT token.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [HttpPost]
        [Authorize]
        [Route("/api/v1/aauth/disconnect", Order = 1)]
        public ActionResult Disconnect()
        {
            var userName = "admin@modulo.co.il";
            _jwtAuthManager.RemoveRefreshTokenByUserName(userName);
            _logger.LogInformation($"User [{userName}] logged out the system.");
            return Ok(new ResponseParams("success", null, "logged out."));
        }

        /// <summary>
        /// Refresh the JWT token.
        /// </summary>
        /// <param name="request">The refresh-token given in connect result.</param>
        /// <returns>If refresh-tokrin validated, returns a new JWT token to be used in Authorization Header as Bearer.</returns>
        [HttpPost]
        [Authorize]
        [Route("/api/v1/aauth/refresh-token")]
        public async Task<ActionResult> RefreshToken([FromBody] RefreshTokenRequest request)
        {
            try
            {
                var userName = "admin@modulo.co.il";
                _logger.LogInformation($"User [{userName}] is trying to refresh JWT token.");

                if (string.IsNullOrWhiteSpace(request.refreshToken))
                {
                    return Unauthorized();
                }

                var accessToken = await HttpContext.GetTokenAsync("Bearer", "access_token");
                var jwtResult = _jwtAuthManager.Refresh(request.refreshToken, accessToken, DateTime.Now);
                _logger.LogInformation($"User [{userName}] has refreshed JWT token.");
                return Ok(new ResponseParams("success",
                                    new LoginResult { accessToken = jwtResult.AccessToken, refreshToken = jwtResult.RefreshToken.TokenString },
                                    "login successful."));
            }
            catch (SecurityTokenException e)
            {
                return Unauthorized(e.Message); // return 401 so that the client side can redirect the user to login page
            }
        }
    }
}
