﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Linq;

namespace SeriousSIP
{
    /// <summary>
    /// Contact management and group calling/messaging API set.
    /// </summary>
    [Authorize(Policy = "keypad-contacts")]
    [Route("api/v1/contacts/", Name = "Contacts")]
    public class Contacts : Controller
    {
        /// <summary>
        /// Subscribe to contact server events to recieve webhook notifications on your contacts statuses.
        /// </summary>
        /// <param name="contactsSubscribeParams">See the ContactsSubscribeParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("subscribe")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Subscribe([FromBody] ContactsSubscribeParams contactsSubscribeParams)
        {
            foreach (string identity in contactsSubscribeParams.identities)
            {
                if ((contactsSubscribeParams.contactsSubscriptions.OnContactStateUpdated != null) &&
                    (Uri.IsWellFormedUriString(contactsSubscribeParams.contactsSubscriptions.OnContactStateUpdated.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        contactsSubscribeParams.contactsSubscriptions.OnContactStateUpdated, EventUriType.OnContactStateUpdated))
                        return NotFound(new ResponseParams("error",
                                          contactsSubscribeParams, "Failed to subscribe to OnContactStateUpdated web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));
            }

            return Ok(new ResponseParams("success", contactsSubscribeParams, "Subscriptions saved."));
        }

        /// <summary>
        /// Unsubscribe, cancel all the webhook calls for one or more identities.
        /// </summary>
        /// <param name="contactsUnsubscribeParams">See the ContactsUnsubscribeParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("unsubscribe")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Unsubscribe([FromBody] ContactsUnsubscribeParams contactsUnsubscribeParams)
        {
            foreach (string identity in contactsUnsubscribeParams.identities)
            {
                if (!SIPServerService.GetInstance().UnsubscribeContacts(identity))
                {
                    NotFound(new ResponseParams("error", contactsUnsubscribeParams,
                        $"Could not remove subscriptions for given identity {identity}."));
                }
            }

            return Ok(new ResponseParams("success", contactsUnsubscribeParams, "Subscriptions removed."));
        }

        /// <summary>
        /// Get all the contacts stored in the contact server with high level contact information.
        /// </summary>
        /// <returns>A list of contacts. See the ContactInfoParams parameter block for details.</returns>
        [Route("api/v1/contacts")]
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult GetContacts()
        {
            
            if (SIPServerService.GetSIPServer().Contacts.Count == 0)
            {
                return NotFound(new ResponseParams("error", null,
                                             "No contacts found."));
            }

            ContactInfoParams[] contacts = new ContactInfoParams[SIPServerService.GetSIPServer().Contacts.Count];
            var i = 0;
            foreach (var item in SIPServerService.GetSIPServer().Contacts.Values)
            {
                contacts[i++] = new ContactInfoParams(item.Id, item.DisplayName, item.UserName, item.FirstName, item.LastName,
                                        item.Type, item.PresenceState, item.PhoneState, item.Tel, item.Email, item.Tags);
            }

            return Ok(new ResponseParams("success", contacts,
                                         $"{SIPServerService.GetSIPServer().Contacts.Count} contacts found."));
        }

        /// <summary>
        /// Create a new contact in the contact server.
        /// </summary>
        /// <param name="newContactParams">See the NewContactParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure. Returns a unique contact ID in case of success.</returns>
        [Route("contact")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Contact([FromBody] NewContactParams newContactParams)
        {
            SIPContact c = new SIPContact(newContactParams.display, newContactParams.username, newContactParams.tel, newContactParams.email);
            c.FirstName = newContactParams.firstname;
            c.LastName = newContactParams.lastname;
            c.Type = newContactParams.type;

            if (SIPServerService.GetSIPServer().Contacts.Any(item => item.Value.Tel == newContactParams.tel)) {
                return NotFound(new ResponseParams("error", newContactParams,
                                             "Contact already exists."));
            }

            if (SIPServerService.GetSIPServer().Contacts.TryAdd(c.Id, c))
            {
                return Ok(new ResponseParams("success", new { id = c.Id },
                                             "Add contact done."));
            }
            else
            {
                return NotFound(new ResponseParams("error", newContactParams,
                                             "Could not add contact."));
            }
        }

        /// <summary>
        /// Delete a contact from the contact server.
        /// </summary>
        /// <param name="id">The unique id of the contact to be deleted.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("contact")]
        [HttpDelete]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Contact([FromQuery] string id)
        {
            if (SIPServerService.GetSIPServer().Contacts.TryRemove(id, out var c))
            {
                return Ok(new ResponseParams("success", new { id = c.Id },
                                             "Remove contact done."));
            }
            else
            {
                return NotFound(new ResponseParams("error", new { id = id },
                                             "Contact not found."));
            }
        }

    }
}
