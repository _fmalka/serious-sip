﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Threading.Tasks;

namespace SeriousSIP
{
    /// <summary>
    /// Messaging API set to send instant messaging and SMS.
    /// </summary>
    [Authorize(Policy = "keypad-messages")]
    [Route("api/v1/messages/", Name = "Messages")]
    public class Messages : Controller
    {
        /// <summary>
        /// Subscribe to message server events to recieve webhook notifications on incoming and outgoing messages.
        /// </summary>
        /// <param name="messagesSubscribeParams">See the MessagesSubscribeParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("subscribe")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Subscribe([FromBody] MessagesSubscribeParams messagesSubscribeParams)
        {
            foreach (string identity in messagesSubscribeParams.identities)
            {
                if ((messagesSubscribeParams.inboundMessagesSubscriptions.OnMessageReceived != null) &&
                    (Uri.IsWellFormedUriString(messagesSubscribeParams.inboundMessagesSubscriptions.OnMessageReceived.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        messagesSubscribeParams.inboundMessagesSubscriptions.OnMessageReceived, EventUriType.OnMessageReceived))
                        return NotFound(new ResponseParams("error",
                                          messagesSubscribeParams, "Failed to subscribe to OnMessageReceived web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageSent != null) &&
                    (Uri.IsWellFormedUriString(messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageSent.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageSent, EventUriType.OnMessageSent))
                        return NotFound(new ResponseParams("error",
                                          messagesSubscribeParams, "Failed to subscribe to OnMessageSent web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageFailed != null) &&
                    (Uri.IsWellFormedUriString(messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageFailed.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageFailed, EventUriType.OnMessageFailed))
                        return NotFound(new ResponseParams("error",
                                          messagesSubscribeParams, "Failed to subscribe to OnMessageFailed web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageTimeout != null) &&
                    (Uri.IsWellFormedUriString(messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageTimeout.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        messagesSubscribeParams.outboundMessagesSubscriptions.OnMessageTimeout, EventUriType.OnMessageTimeout))
                        return NotFound(new ResponseParams("error",
                                          messagesSubscribeParams, "Failed to subscribe to OnMessageTimeout web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((messagesSubscribeParams.inboundMessagesSubscriptions.OnTextMessageReceived != null) &&
                    (Uri.IsWellFormedUriString(messagesSubscribeParams.inboundMessagesSubscriptions.OnTextMessageReceived.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        messagesSubscribeParams.inboundMessagesSubscriptions.OnTextMessageReceived, EventUriType.OnTextMessageReceived))
                        return NotFound(new ResponseParams("error",
                                          messagesSubscribeParams, "Failed to subscribe to OnTextMessageReceived web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((messagesSubscribeParams.outboundMessagesSubscriptions.OnTextMessageSent != null) &&
                    (Uri.IsWellFormedUriString(messagesSubscribeParams.outboundMessagesSubscriptions.OnTextMessageSent.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        messagesSubscribeParams.outboundMessagesSubscriptions.OnTextMessageSent, EventUriType.OnTextMessageSent))
                        return NotFound(new ResponseParams("error",
                                          messagesSubscribeParams, "Failed to subscribe to OnTextMessageSent web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));

                if ((messagesSubscribeParams.outboundMessagesSubscriptions.OnTextMessageDelivered != null) &&
                    (Uri.IsWellFormedUriString(messagesSubscribeParams.outboundMessagesSubscriptions.OnTextMessageDelivered.ToString(), UriKind.Absolute)))
                    if (!SIPServerService.GetInstance().Subscribe(identity,
                        messagesSubscribeParams.outboundMessagesSubscriptions.OnTextMessageDelivered, EventUriType.OnTextMessageDelivered))
                        return NotFound(new ResponseParams("error",
                                          messagesSubscribeParams, "Failed to subscribe to OnTextMessageDelivered web event, " +
                                             "make sure that the identity is correct and that " +
                                             "you removed any previous subscriptions."));
            }

            return Ok(new ResponseParams("success", messagesSubscribeParams, "Subscriptions saved."));
        }

        /// <summary>
        /// Unsubscribe, cancel all the webhook calls for one or more identities.
        /// </summary>
        /// <param name="messagesUnsubscribeParams">See the MessagesUnsubscribeParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("unsubscribe")]
        [HttpPut]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Unsubscribe([FromBody] MessagesUnsubscribeParams messagesUnsubscribeParams)
        {
            foreach (string identity in messagesUnsubscribeParams.identities)
            {
                if (!SIPServerService.GetInstance().UnsubscribeMessages(identity))
                {
                    NotFound(new ResponseParams("error", messagesUnsubscribeParams,
                        $"Could not remove subscriptions for given identity {identity}."));
                }
            }

            return Ok(new ResponseParams("success", messagesUnsubscribeParams, "Subscriptions removed."));
        }

        /// <summary>
        /// Retrieve the last 24h stats for instant messages.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("message/statistics")]
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        public IActionResult InstantMessagesStats()
        {
            return Ok(new ResponseParams("success", SIPServerService.GetInstance().GetInstantMessagesStats(),
                                         "Get statistics done."));
        }

        /// <summary>
        /// Retrieve the last 24 hours stats for text messages.
        /// </summary>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("text/statistics")]
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        public IActionResult TextMessagesStats()
        {
            return Ok(new ResponseParams("success", SIPServerService.GetInstance().GetTextMessagesStats(),
                                         "Get statistics done."));
        }

        /// <summary>
        /// Send a new instant message to a sip peer.
        /// </summary>
        /// <param name="sendMessageParams">See the SendMessageParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure. Returns a unique message ID in case of success.</returns>
        [Route("message/send")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult Send([FromBody] SendMessageParams sendMessageParams)
        {
            var id = SIPServerService.GetSIPServer().SendMessage(sendMessageParams.to, sendMessageParams.identity, sendMessageParams.message);
            if (id == null)
                return NotFound(new ResponseParams("error", sendMessageParams, "Could not send message, please check input parameters."));

            return Ok(new ResponseParams("success", new { id = id }, "Message sent."));
        }

        /// <summary>
        /// Acknowledge the reception of an instant message from a sip peer.
        /// </summary>
        /// <param name="id">The unique ID of the message received that needs to be acknowledged.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("message/ack")]
        [HttpDelete]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public async Task<IActionResult> Ack([FromQuery] string id)
        {
            var msg = SIPServerService.GetSIPServer().GetMessage(id);
            if (msg == null)
                return NotFound(new ResponseParams("error", new { id = id }, "Could not find call with given id."));

            await SIPServerService.GetSIPServer().AckMessage(msg);

            return Ok(new ResponseParams("success", new { id = id }, "Message acked."));
        }

        /// <summary>
        /// Send a new SMS to a mobile destination.
        /// </summary>
        /// <param name="sendMessageParams">See the SendMessageParams parameter block for details.</param>
        /// <returns>Success or error with posted data structure and reason of success or failure.</returns>
        [Route("text/send")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseParams), 200)]
        [ProducesResponseType(typeof(ResponseParams), 404)]
        public IActionResult SendText([FromBody] SendMessageParams sendMessageParams)
        {
            var id = SIPServerService.GetSIPServer().SendTextMessage(sendMessageParams.to, sendMessageParams.identity, sendMessageParams.message);
            if (id == null)
                return NotFound(new ResponseParams("error", sendMessageParams, "Could not send message, please check input parameters."));

            return Ok(new ResponseParams("success", new { id = id }, "Message sent."));
        }
    }
}
