﻿## Keypad, SIP server toolkit

This fully C# library can be used to add Real-time Communications, typically audio calls and messages, to applications implementing the Keypad REST API.

**How to use?**

Most typically the toolkit is deployed on a local server next to the organization PABX and is connected to it via a SIP trunk or SIP extensions. The organizational application can then gain real-time communication capabilities and becomes integrated with the PABX. Although the software can also be deployed on the cloud to provide real-time communications APIs to many applications.

**Feature highlights**

- Supports both VoIP and WebRTC with or with SSL encryption.
- Supports G711, G729 and G722 audio codecs.
- Supports inbound, outbound and bridiging of calls.
- Playback PCM and WAV files.
- Records streams into PCM files.
- Generate and detect RFC2833, SIP-INFO and inband DTMFs.
- Performs call progress analysis on outbound call to determine the type of answer.
- Support multiple authentication schemes (oAuth2.0/Apikey/Clear).
- Generates call data records and message data records.
- Implements SUBSCRIBE/NOTIFY to keep trace of extenstions status and availability.
- Implements an internal script engine to run simple applications with no API calls.
- Intuitive web-based graphical interface for monitoring.

**Some of the protocols specifications supported:**

 - Session Initiation Protocol [(SIP)](https://tools.ietf.org/html/rfc3261),
 - Real-time Transport Protocol [(RTP)](https://tools.ietf.org/html/rfc3550),
 - Web Real-time Communications [(WebRTC)](https://www.w3.org/TR/webrtc/),
 - Interactive Connectivity Establishment [(ICE)](https://tools.ietf.org/html/rfc8445)

**Operating system Support**

The library is compliant with .NET Standard 2.0, .NET Framework 4.6.1 and .NET 5. It can be compiled and run on any Windows or Linux OS supporting .NET 5. **Note: In windows, the library can leverage the underlaying rtc multimedia timers of windows operating system.**

## Installation

On debian:
````bash
wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y aspnetcore-runtime-5.0

mkdir /opt/keypad/
mkdir /var/log/keypad/
ln -s /var/log/keypad /opt/keypad/logs

tar -xzvf ./keypad-1.0.0.tgz /opt/keypad/

````

On windows:

Donwload and install aspnetcore runtime 5.0.5+
1. [runtime-aspnetcore-5.0.5](https://dotnet.microsoft.com/download/dotnet/thank-you/runtime-aspnetcore-5.0.5-windows-x64-installer)
2. Extract the keypad-1.0.0.zip file to "C:\Program Files\Keypad"

**Starting as a system service**

On debian:
````bash
sudo cp /opt/keypad/keypad.service /etc/systemd/
systemctl enable keypad.service
````

On windows: (from a command line with administration rights)
````bash
SIPServerService.exe install -username:DOMAIN\ServiceAccount -password:itsASecret -servicename:AwesomeService –autostart
````

**Configuration**

The appsettings.json file contains the parameters to properly configure one specific instance of the toolkit.

Configuring the service:

| Parameter | Description | Format | Default value | Example |
|-|-|-|-|-|
| instance | the name and unique identifier of the sip server instance | string | SIPServerService#1 | MySIPServer |
| storagePath | full path to the storage of the instance for clips, logs, scripts and cdrs | string |\\ | C:\\Program Files\\Keypad\\ |
| logConfigurationFile | the full path of the file containing the logger configuration | string | logger.json | logger.json |
| useUrls | a list of URLs that will be used for API access and GUI access | list of strings | [ "http://localhost:6050" ] | [ "http://localhost:6050" ] |
| clientUrl | optional. a default URL for a client application | string |  | http://localhost:6060 |
| autoStartSipServer | either or not to start the sip server automatically or wait for the application to start it. | boolean | true | true |
| productKey | a product key granting permisson to run the toolkit | string |  | ICzLx0...UHII2 |
| oAuthServiceUrl | the URL to use for authentication and authorization for oAuth 2.0 mode | string |  | http://localhost:8088 |
| usePrivateToken| a private token structure to use when local authentication and authorization is used instead of oAuth 2.0 (ApiKey mode) | privateToken |  |  |

Configuring the sip server:

| Parameter | Description | Format | Default value | Example |
|-|-|-|-|-|
| localIPEndPoints | a list of sip end points for the server to listen to | list of strings | [ "udp,0.0.0.0,5060" ] | [ "udp,0.0.0.0,5060", "wss,0.0.0.0,443" ] |,
| hangupWhenNoRtp | when true, disconnect the call if rtcp counters are not progressing | boolean | true | true |
| userAgentHeader | optional. sets the User-Agent header in all sip messages | string | Keypad-1.0.0 | |
| enableRtpTestMode | when true, the toolkit automatically play a message in loop for all calls | boolean | false |  |
| maxCalls | limit the number of calls to be handled by the instance of the toolkit | number | 0 | 100 |
| registrationInterval | the time in milliseconds to wait between register requests | number | 250 |  |
| autoAcceptCalls | when true, the sip server automatically accept incoming calls | boolean | true |  |
| acceptDelay | the time in milliseconds to delay the incoming call to let the ringback tone be heard | number | 0 | 250 |
| autoAnswerCalls | when true, the sip server automatically answers incoming calls | boolean | true |  |
| useMultiMediaTimers | when true, the rtp stack use windows rtc multimedia timers | boolean | false |  |
| codecs | a list of codecs that will be used for SDP and RTP | list of strings | [ "PCMA", "PCMU", "G729", "G722" ] |  |
| dtmfMode | the type of DTMF to use for digit exchange on the calls | Enumarated | Rfc2833 | Rfc2833,Inband,DtmfRelay,Dtmf |  |
| registerAccounts | a list of extensions to register with the PABX | list of accounts | [""] | [ "username,authUsername,password,domain,expires" ] |
| monitorTrunks | a list of sip trunks to monitor | list of trunks | [""] | [ "sip:10.0.0.100,60000" ] |

Configuring the smpp client:

| Parameter | Description | Format | Default value | Example |
|-|-|-|-|-|
| systemId | the unique identifier of the smpp account to connect with | string |  |  |
| password | the smpp account password | string |  |  |
| host | the hostname or ip address of the smpp server to connect to | string |  |  |
| port | the port where the smpp server is expecting client connection | number | 2775 |  |
| systemType | the type of the system to be provided by the smpp provider | string |  |  |
| serviceType | the type of the service to be provided by the smpp provider | string |  |  |
| sourceAddress | the default address/number to identify with when sending a short message | string |  |  |

Full configuration file example

````bash
{
    "instance": "SIPServerService#10",
    "storagePath": "C:\\Users\\User\\source\\repos\\serious-sip\\SIPServerService\\run\\",
    "logConfigurationFile": "logger.json",
    "useUrls": "http://localhost:6050;https://localhost:6051",
    "_clientUrl": "http://localhost:6060",
    "autoStartSipServer": true,
    "productKey": "ICzLx0XAu10JBgSvAIKWcS/euQqaf2k02PGk224tUfHTVqDONZBYcRKD/DUHII2h",
    "usePrivateToken": {
        "secret": "StrONGKAutHENTICATIONKEy",
        "issuer": "http://localhost:6060",
        "audience": "http://localhost:6060",
        "accessTokenExpiration": 20,
        "refreshTokenExpiration": 60
    },
    "oAuthServiceUrl": "http://localhost:8088",
    "sipServer": {
        "localIPEndPoints": [ "udp,sip1.cy2.omega-telecom.net,5060", "ws,10.0.0.45,8081" ],
        "hangupWhenNoRtp": true,
        "userAgentHeader": "Keypad-1.0.0",
        "enableRtpTestMode": false,
        "maxCalls": 100,
        "registrationInterval": 250,
        "autoAcceptCalls": true,
        "acceptDelay": 0,
        "autoAnswerCalls": true,
        "useMultiMediaTimers": true,
        "codecs": [ "PCMA", "PCMU", "G729", "G722" ],
        "dtmfMode": "Rfc2833",
        "registerAccounts": [
            "4006,4006,test123,10.0.0.100,3600"
        ],
        "monitorTrunks": [
            "sip:10.0.0.100,60000"
        ]
    },
    smpp": {
        "systemId": "franck",
        "password": "franck",
        "host": "192.168.1.101",
        "port": 2775,
        "systemType": "5750",
        "serviceType": "5750",
        "sourceAddress": "5750"
    }
}
````

Configuring the logger:
for full documentation on logger configuration see [Serilog](https://serilog.net/)


Log configuration file example

````bash
{
    "Serilog": {
        "Using": [  "Serilog.Sinks.Console", "Serilog.Sinks.File" ],
        "MinimumLevel": {
            "Default": "Debug",
            "Override": {
                "Default": "Debug",
                "SeriousSIP.SIPCallDataRecord": "Verbose",
                "SeriousSIP.SIPMessageDataRecord": "Verbose"
            }
        },
        "WriteTo": [
            {
                "name": "Console"
            },
            {
                "Name": "RollingFile",
                "Args": {
                    "RestrictedToMinimumLevel": "Warning",
                    "pathFormat": "logs/error-{Date}.log",
                    "outputTemplate": "{Timestamp:HH:mm:ss,fff} | {Level:u3} | {SourceContext:u30} | {CallContext:u60} | {Message:lj}{NewLine}{Exception}",
                    "retainedFileCountLimit": 7
                }
            },
            {
                "Name": "Logger",
                "Args": {
                    "configureLogger": {
                        "Filter": [
                            {
                                "Name": "ByIncludingOnly",
                                "Args": {
                                    "expression": "(Contains(SourceContext, 'SeriousSIP.SIPCallDataRecord') or Contains(SourceContext, 'SeriousSIP.SIPMessageDataRecord')) and @Level = 'Verbose'"
                                }
                            }
                        ],
                        "WriteTo": [
                            {
                                "Name": "RollingFile",
                                "Args": {
                                    "pathFormat": "logs/cdr-{Date}.log",
                                    "outputTemplate": "{Message:lj}{NewLine}",
                                    "retainedFileCountLimit": 7
                                }
                            }
                        ]
                    }
                }
            },
            {
                "Name": "Logger",
                "Args": {
                    "configureLogger": {
                        "Filter": [
                            {
                                "Name": "ByExcluding",
                                "Args": {
                                    "expression": "Contains(SourceContext, 'SeriousSIP.SIPCallDataRecord') or Contains(SourceContext, 'SeriousSIP.SIPMessageDataRecord')"
                                }
                            }
                        ],
                        "WriteTo": [
                            {
                                "Name": "RollingFile",
                                "Args": {
                                    "pathFormat": "logs/log-{Date}.log",
                                    "outputTemplate": "{Timestamp:HH:mm:ss,fff} | {Level:u3} | {SourceContext} | {CallContext} | {Message:lj}{NewLine}{Exception}",
                                    "retainedFileCountLimit": 7
                                }
                            }
                        ]
                    }
                }
            }
        ],
        "Enrich": [ "FromLogContext" ],
        "Properties": {
            "Application": "SIPServerTester",
            "CallContext": ""
        }
    }
}
````

**Documentation**

To access full API documentation run the toolkit and access [swagger](http://localhost:6050/swagger/).

To access the webRTC demonstration run the tookkit and access [webrtc](http://localhost:6050/webrtc/).