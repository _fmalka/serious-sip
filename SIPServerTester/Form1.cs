﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Windows.Forms;
using SeriousSIP;
using System.Threading.Tasks;
using System.Drawing;
using SIPSorcery.SIP;
using Microsoft.Extensions.Configuration;
using System.IO;
using SIPSorcery.Media;
using System.Diagnostics;
using SIPSorcery.Net;
using SeriousSIP.Server;
using SeriousSIP.Model;
using SIPCallDtmfMode = SeriousSIP.SIPCallDtmfMode;
using SIPCallDirection = SIPSorcery.SIP.SIPCallDirection;
using System.Net;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace SIPServerTester
{
    public partial class Form1 : Form
    {
        class InMemorySink : ILogEventSink
        {
            public ConcurrentQueue<LogEvent> Events { get; } = new ConcurrentQueue<LogEvent>();

            public void Emit(LogEvent logEvent)
            {
                if (logEvent == null) throw new ArgumentNullException(nameof(logEvent));
                Events.Enqueue(logEvent);
            }
        }
        
        private bool isUsingRestApi() {
            return ((Properties.Settings.Default.UseRestApi != null) &&
                        (!(Properties.Settings.Default.UseRestApi == "")));
        }

        // for work with REST API
        private SIPServerApplication _remoteApp;

        //
        private SIPServer _sipServer = null;
        private static Microsoft.Extensions.Logging.ILogger _logger = NullLogger.Instance;
        private static readonly InMemorySink sink = new InMemorySink();
        private static readonly LoggingLevelSwitch levelSwitch = new LoggingLevelSwitch();
        private readonly Timer tLogger = new Timer();
        private readonly Timer tCalls = new Timer();
        private readonly Timer tStats = new Timer();
        private SIPCallDtmfMode _dtmfMode;

        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();            
            _logger = AddLogger();
            AddGuiTimers();
            LoadSettings();
        }

        private void LoadSettings()
        {
            tsbStart1.DropDownItems.Clear();
            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.config*");
            foreach (string file in files)
            {
                if (file != "SIPServerTester.dll.config.xml")
                    tsbStart1.DropDownItems.Add(file);
            }

            tbsSpeak1.DropDownItems.Clear();
            tbsSpeak1.DropDownItems.Add("Input");
            tbsSpeak1.DropDownItems.Add("Ssml");

            tsbNewCall1.DropDownItems.Clear();
            tsbNewCall1.DropDownItems.Add("1 call");
            tsbNewCall1.DropDownItems.Add("10 call");

            tsbDtmfMode.DropDownItems.Clear();
            tsbDtmfMode.DropDownItems.Add(SIPCallDtmfMode.Rfc2833.ToString());
            tsbDtmfMode.DropDownItems.Add(SIPCallDtmfMode.Inband.ToString());
            tsbDtmfMode.DropDownItems.Add(SIPCallDtmfMode.DtmfRelay.ToString());
            tsbDtmfMode.DropDownItems.Add(SIPCallDtmfMode.Dtmf.ToString());

            tbsHoldCall.DropDownItems.Clear();
            //tbsHoldCall.DropDownItems.Add(MediaStreamStatusEnum.SendRecv.ToString());
            tbsHoldCall.DropDownItems.Add(MediaStreamStatusEnum.SendOnly.ToString());
            tbsHoldCall.DropDownItems.Add(MediaStreamStatusEnum.RecvOnly.ToString());
            tbsHoldCall.DropDownItems.Add(MediaStreamStatusEnum.Inactive.ToString());

            SIPCallDtmfMode dtmfMode = (SIPCallDtmfMode)Enum.Parse(typeof(SIPCallDtmfMode), SIPServerTester.Properties.Settings.Default.DefaultDtmfMode);
            tsbLogLevel.DropDownItems[(int)dtmfMode].Select();

            tbDigitsStr.Text = SIPServerTester.Properties.Settings.Default.DefaultDigitsString;
            tbIdentity.Text = SIPServerTester.Properties.Settings.Default.DefaultCallIdentity;
            tbNewCallTo.Text = SIPServerTester.Properties.Settings.Default.DefaultCallDestination;
            tbPlaybackFile.Text = SIPServerTester.Properties.Settings.Default.DefaultPlaybackFile;
            tbRecordDir.Text = SIPServerTester.Properties.Settings.Default.DefaultRecordFileLocation;
            tbTransferTo.Text = SIPServerTester.Properties.Settings.Default.DefaultTransferDestination;
        }

        private void AddGuiTimers()
        {
            tLogger.Interval = 1000;
            tLogger.Tick += TLogger_Tick;
            tCalls.Interval = 1000;
            tCalls.Tick += TCalls_Tick;
            tStats.Interval = 1000;
            tStats.Tick += TStats_Tick;
        }

        private void TStats_Tick(object sender, EventArgs e)
        {
            long offered = 0;
            long accepted = 0;
            long answered = 0;
            long hangup = 0;
            long connected = 0;
            long calling = 0;
            long cancelled = 0;
            long trying = 0;
            long timeout = 0;
            long ringing = 0;

            if (_sipServer == null) return;

            for (int t = 0; t < 59; t++)
            {
                offered += _sipServer.GetStats()[t].Offered;
                accepted += _sipServer.GetStats()[t].Accepted;
                answered += _sipServer.GetStats()[t].Answered;
                hangup += _sipServer.GetStats()[t].Hangup;
                calling += _sipServer.GetStats()[t].Calling;
                cancelled += _sipServer.GetStats()[t].Cancelled;
                trying += _sipServer.GetStats()[t].Trying;
                timeout += _sipServer.GetStats()[t].Timeout;
                ringing += _sipServer.GetStats()[t].Ringing;
                connected += _sipServer.GetStats()[t].Connected;

            }

            lblOffered.Text = "" + offered;
            lblAccepted.Text = "" + accepted;
            lblAnswered.Text = "" + answered;
            lblHangup.Text = "" + hangup;
            lblCalling.Text = "" + calling;
            lblCancelled.Text = "" + cancelled;
            lblTrying.Text = "" + trying;
            
            lblConnected.Text = "" + connected;

        }

        private void TLogger_Tick(object sender, EventArgs e)
        {
            while (sink.Events.TryDequeue(out LogEvent logEvent))
            {
                if ((tsbShowErrorsOnly.Checked) &&
                    (logEvent.Level < LogEventLevel.Warning))
                    continue;

                ListViewItem lvi = new ListViewItem();
                lvi.Text = logEvent.Timestamp.ToString("yyyy-MM-dd HH:mm:ss,fff");
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, logEvent.Level.ToString()));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, logEvent.MessageTemplate.Text));
                LogBox.Items.Add(lvi);
            }

            if ((tsbAutoScroll.Checked) && (LogBox.Items.Count > 0))
            {
                LogBox.Items[LogBox.Items.Count - 1].EnsureVisible();
                //LogBox.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                //LogBox.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        private ListViewItem GetCallsRow(String callId)
        {
            if (lvCalls.Items[callId] != null)
            {
                return lvCalls.Items[callId];
            }
            else
            {
                ListViewItem lvi = lvCalls.Items.Add(new ListViewItem(callId));
                lvi.Name = callId;
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                lvi.SubItems.Add(""); 
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                lvi.SubItems.Add("");
                return lvi;
            }
        }

        private void TCalls_Tick(object sender, EventArgs e)
        {
            if (isUsingRestApi())
            {
                if (_remoteApp == null)
                    return;

                ResponseParams resp;
                List<CallInfoParams> calls;
                Dictionary<string, CallInfoParams> callsDict;
                try
                {
                    resp = _remoteApp.Calls.Calls();
                    if (resp.Status == "error")
                    {
                        lvCalls.Items.Clear();
                        return;
                    }

                    calls = JsonConvert.DeserializeObject<List<CallInfoParams>>(resp.Data.ToString());
                    callsDict = calls.ToDictionary(x => x.Id, x => x);

                    foreach (ListViewItem it in lvCalls.Items)
                    {
                        if (!callsDict.ContainsKey(it.Name))
                        {
                            lvCalls.Items.Remove(it);
                        }
                    }
                }
                catch {
                    lvCalls.Items.Clear();
                    return;
                }

                foreach (var item in callsDict)
                {
                    ListViewItem lvi = GetCallsRow(item.Key.ToString());
                    lvi.SubItems[1].Text = item.Value.From;
                    lvi.SubItems[2].Text = item.Value.To;
                    lvi.SubItems[3].Text = item.Value.Direction.ToString();
                    lvi.SubItems[4].Text = item.Value.State.ToString();
                    lvi.SubItems[6].Text = Enum.GetName(typeof(SeriousSIP.SIPCallAction), item.Value.Action);
                    lvi.SubItems[7].Text = item.Value.RtpTx.ToString();
                    lvi.SubItems[8].Text = item.Value.RtpRx.ToString();
                    lvi.SubItems[9].Text = item.Value.Duration;
                }
            }
            else
            {
                if (_sipServer == null)
                    return;

                foreach (ListViewItem it in lvCalls.Items)
                {
                    if (!_sipServer.Calls.ContainsKey(it.Name))
                    {
                        lvCalls.Items.Remove(it);
                    }
                }

                foreach (var item in _sipServer.Calls)
                {
                    ListViewItem lvi = GetCallsRow(item.Key.ToString());
                    if (item.Value.GetLastSIPRequest() != null)
                    {
                        lvi.SubItems[1].Text = item.Value.GetLastSIPRequest().Header.From.FromURI.ToString();
                        lvi.SubItems[2].Text = item.Value.GetLastSIPRequest().Header.To.ToURI.ToString();
                        lvi.SubItems[3].Text = item.Value.GetCallDirection().ToString();
                    }
                    else if (item.Value.CallDescriptor != null)
                    {
                        lvi.SubItems[1].Text = item.Value.CallDescriptor.From;
                        lvi.SubItems[2].Text = item.Value.CallDescriptor.To;
                        lvi.SubItems[3].Text = item.Value.GetCallDirection().ToString();
                    }
                    else
                    {
                        continue;
                    }
                    lvi.SubItems[4].Text = item.Value.GetCallState().ToString();
                    lvi.SubItems[5].Text = item.Value.GetDigitsBuffer();
                    lvi.SubItems[6].Text = Enum.GetName(typeof(SeriousSIP.SIPCallAction), item.Value.GetCallAction());
                    lvi.SubItems[7].Text = (item.Value.MediaSession as VoIPMediaSession)?.AudioRtcpSession.PacketsSentCount.ToString();
                    lvi.SubItems[8].Text = (item.Value.MediaSession as VoIPMediaSession)?.AudioRtcpSession.PacketsReceivedCount.ToString();
                    lvi.SubItems[9].Text = TimeSpan.FromMilliseconds(item.Value.GetDuration()).ToString(@"hh\:mm\:ss");
                }
            }

            tssLabel2.Text = lvCalls.Items.Count + " Calls";
            lvCalls.Refresh();
        }

        private Microsoft.Extensions.Logging.ILogger AddLogger()
        {
            tsbLogLevel.DropDownItems.Add(LogEventLevel.Verbose.ToString());
            tsbLogLevel.DropDownItems.Add(LogEventLevel.Debug.ToString());
            tsbLogLevel.DropDownItems.Add(LogEventLevel.Information.ToString());
            tsbLogLevel.DropDownItems.Add(LogEventLevel.Warning.ToString());
            tsbLogLevel.DropDownItems.Add(LogEventLevel.Error.ToString());
            tsbLogLevel.DropDownItems.Add(LogEventLevel.Fatal.ToString());

            Logger serilogLogger;

            if (File.Exists(SIPServerTester.Properties.Settings.Default.DefaultLogConfigurationFile))
            {
                var configuration = new ConfigurationBuilder()
                                      .AddJsonFile($"{Directory.GetCurrentDirectory()}\\{SIPServerTester.Properties.Settings.Default.DefaultLogConfigurationFile}").Build();
                
                LogEventLevel l = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), configuration.GetSection("Serilog").GetSection("MinimumLevel").GetSection("Default").Value);
                tsbLogLevel.DropDownItems[(int)l].Select();
                levelSwitch.MinimumLevel = l;
                
                serilogLogger = new LoggerConfiguration()
                                  .ReadFrom.Configuration(configuration)
                                  .WriteTo.Sink(sink)
                                  .MinimumLevel.ControlledBy(levelSwitch)
                                  .CreateLogger();
            }
            else
            {
                LogEventLevel l = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), SIPServerTester.Properties.Settings.Default.DefaultLogLevel);
                tsbLogLevel.DropDownItems[(int)l].Select();
                levelSwitch.MinimumLevel = l;

                serilogLogger = new LoggerConfiguration()
                    .Enrich.FromLogContext()
                    .WriteTo.Sink(sink)
                    .WriteTo.File(SIPServerTester.Properties.Settings.Default.DefaultLogFile,
                                   outputTemplate: "{Timestamp:HH:mm:ss,fff} [{Level:u3}] {SourceContext} {CallContext} {Message:lj}{NewLine}{Exception}",
                                    rollingInterval: RollingInterval.Day)
                    .MinimumLevel.ControlledBy(levelSwitch)
                    .CreateLogger();
            }

            var factory = new SerilogLoggerFactory(serilogLogger);
            SIPSorcery.LogFactory.Set(factory);
            return factory.CreateLogger($"{this.GetType().FullName}");
        }

        private void tsbStart_Click(object sender, EventArgs e)
        {
            try
            {
                tLogger.Start();

                if ((Properties.Settings.Default.UseRestApi != null) &&
                    (!(Properties.Settings.Default.UseRestApi == "")))
                {
                    _logger.LogInformation($"Starting using rest api at {Properties.Settings.Default.UseRestApi}");
                    _remoteApp = new SIPServerApplication(Properties.Settings.Default.UseRestApi, "http://localhost:6060");
                    _remoteApp.Start();

                    _remoteApp.OnAplicationStart += _remoteApp_OnAplicationStart;
                    _remoteApp.OnApplicationStop += _remoteApp_OnApplicationStop;

                    _remoteApp.OnCallAccepted += _remoteApp_OnCallAccepted;
                    _remoteApp.OnCallAnswered += _remoteApp_OnCallAnswered;
                    _remoteApp.OnCallConnected += _remoteApp_OnCallConnected;
                    _remoteApp.OnCallDisconnected += _remoteApp_OnCallDisconnected;
                    _remoteApp.OnCallFailed += _remoteApp_OnCallFailed;
                    _remoteApp.OnCallOffered += _remoteApp_OnCallOffered;
                    _remoteApp.OnCallGetDtmfDone += _remoteApp_OnCallGetDtmfDone;
                    _remoteApp.OnCallSendDtmfDone += _remoteApp_OnCallSendDtmfDone;
                    _remoteApp.OnCallSpeakDone += _remoteApp_OnCallSpeakDone;
                    _remoteApp.OnCallPlayDone += _remoteApp_OnCallPlayDone;
                    _remoteApp.OnCallPlayError += _remoteApp_OnCallPlayError;
                    _remoteApp.OnCallPlayFilesDone += _remoteApp_OnCallPlayFilesDone;
                    _remoteApp.OnCallPlayRecordDone += _remoteApp_OnCallPlayRecordDone;
                    _remoteApp.OnCallRecordDone += _remoteApp_OnCallRecordDone;
                    _remoteApp.OnCallRecordError += _remoteApp_OnCallRecordError;
                    _remoteApp.OnCallRinging += _remoteApp_OnCallRinging;
                    _remoteApp.OnRegistrationSuccess += _remoteApp_OnRegistrationSuccess;
                    _remoteApp.OnSystemStartedSuccess += _remoteApp_OnSystemStartedSuccess;
                    _remoteApp.OnSystemStartedFailed += _remoteApp_OnSystemStartedFailed;
                    _remoteApp.OnSystemStoppedSuccess += _remoteApp_OnSystemStoppedSuccess;
                    _remoteApp.OnSystemStoppedFailed += _remoteApp_OnSystemStoppedFailed;

                    _logger.LogInformation("Waiting for application start event...");

                    tsbDtmfMode.Enabled = this.tsbStart.Enabled = tsbStart1.Enabled = false;
                    this.tsbStop.Enabled = true;
                    tsbNewCall.Enabled = true;
                    tsbNewCall1.Enabled = true;
                }
                else
                {
                    _logger.LogInformation("Starting using dll...");

                    _sipServer = new SIPServer();

                    _sipServer.LocalIPEndPoints = Properties.Settings.Default.LocalIPEndPoints;
                    _sipServer.RegisterAccounts = Properties.Settings.Default.RegisterAccounts;
                    _sipServer.SetCodecs(Properties.Settings.Default.DefaultCodecs);
                    _sipServer.DtmfMode = _dtmfMode;
                    _sipServer.AutoAcceptCalls = true;
                    _sipServer.AutoAnswerCalls = tbsAutoAnswer.Checked;
                    _sipServer.MaxCalls = int.Parse(Properties.Settings.Default.MaxCalls);
                    _sipServer.EnableRtpTestMode = bool.Parse(Properties.Settings.Default.EnableRtpTestMode);
                    _sipServer.UseMultiMediaTimers = bool.Parse(Properties.Settings.Default.UseMultiMediaTimers);
                    _sipServer.RtpAddress = IPAddress.Parse(Properties.Settings.Default.RtpAddress);
                    _sipServer.HangupWhenNoRtp = bool.Parse(Properties.Settings.Default.HangupWhenNoRtp);

                    //// incoming call events
                    _sipServer.OnCallOffered += OnCallOffered;
                    _sipServer.OnCallAccepted += OnCallAccepted;
                    _sipServer.OnCallAnswered += OnCallConnected;

                    //// outgoing call events
                    _sipServer.OnCallRinging += OnCallRinging;
                    _sipServer.OnCallDiverted += OnCallDiverted;
                    _sipServer.OnCallFailed += OnCallFailed;

                    //// common events
                    _sipServer.OnCallConnected += OnCallConnected;
                    _sipServer.OnCallDisconnected += OnCallDisconnected;

                    // registration events
                    _sipServer.OnRegistrationRemoved += OnRegistrationRemoved;
                    _sipServer.OnRegistrationSuccess += OnRegistrationSuccess;
                    _sipServer.OnRegistrationTemporaryFailure += OnRegistrationTemporaryFailure;
                    _sipServer.OnRegistrationFailed += OnRegistrationFailed;

                    // server start events
                    _sipServer.OnSIPServerStartFailed += OnSIPServerStartFailed;
                    _sipServer.OnSIPServerStartSuccess += OnSIPServerStartSuccess;
                    _sipServer.OnSIPServerStopFailed += OnSIPServerStopFailed;
                    _sipServer.OnSIPServerStopSuccess += OnSIPServerStopSuccess;

                    _ = _sipServer.Start();

                    tsbDtmfMode.Enabled = this.tsbStart.Enabled = tsbStart1.Enabled = false;
                    this.tsbStop.Enabled = true;
                    tsbNewCall.Enabled = true;
                    tsbNewCall1.Enabled = true;

                    tssLabel1.Text = _sipServer.State.ToString();

                    foreach (SIPChannel c in _sipServer.GetTransportChannels())
                    {
                        tssLabel1.Text += " | Listening on " + c.ListeningSIPEndPoint.ToString();
                    }

                    tssIndicator.BackColor = Color.Green;

                    tStats.Start();
                }

                tCalls.Start();

            } catch (Exception ex)
            {
                _logger.LogInformation("Exiting...");
                _logger.LogCritical(ex.Message);
                _logger.LogError(ex.StackTrace);
            }
        }

        private Task _remoteApp_OnCallSendDtmfDone(string callId, CallInfoParams p)
        {
            _logger.LogInformation($"Send dtmf done for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallSpeakDone(string callId, CallInfoParams p)
        {
            _logger.LogInformation($"Speak done for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnSystemStoppedFailed(string message)
        {
            _logger.LogError($"Failed to start instance {message}");

            return Task.CompletedTask;
        }

        private async Task _remoteApp_OnSystemStoppedSuccess()
        {
            _logger.LogInformation($"server is now down.");

            tsbDtmfMode.Enabled = this.tsbStart1.Enabled = this.tsbStart.Enabled = true;
            this.tsbStop.Enabled = false;
            tsbNewCall.Enabled = false;
            tsbNewCall1.Enabled = false;

            tssLabel1.Text = "Not running";
            tssIndicator.BackColor = Color.Red;

            await Task.Delay(1000);

            tCalls.Stop();
            tStats.Stop();
            tLogger.Stop();

            _remoteApp.OnAplicationStart -= _remoteApp_OnAplicationStart;
            _remoteApp.OnApplicationStop -= _remoteApp_OnApplicationStop;

            _remoteApp.OnCallAccepted -= _remoteApp_OnCallAccepted;
            _remoteApp.OnCallAnswered -= _remoteApp_OnCallAnswered;
            _remoteApp.OnCallConnected -= _remoteApp_OnCallConnected;
            _remoteApp.OnCallDisconnected -= _remoteApp_OnCallDisconnected;
            _remoteApp.OnCallFailed -= _remoteApp_OnCallFailed;
            _remoteApp.OnCallOffered -= _remoteApp_OnCallOffered;
            _remoteApp.OnCallGetDtmfDone -= _remoteApp_OnCallGetDtmfDone;
            _remoteApp.OnCallPlayDone -= _remoteApp_OnCallPlayDone;
            _remoteApp.OnCallPlayError -= _remoteApp_OnCallPlayError;
            _remoteApp.OnCallPlayFilesDone -= _remoteApp_OnCallPlayFilesDone;
            _remoteApp.OnCallPlayRecordDone -= _remoteApp_OnCallPlayRecordDone;
            _remoteApp.OnCallRecordDone -= _remoteApp_OnCallRecordDone;
            _remoteApp.OnCallRecordError -= _remoteApp_OnCallRecordError;
            _remoteApp.OnCallRinging -= _remoteApp_OnCallRinging;
            _remoteApp.OnRegistrationSuccess -= _remoteApp_OnRegistrationSuccess;
            _remoteApp.OnSystemStartedSuccess -= _remoteApp_OnSystemStartedSuccess;
            _remoteApp.OnSystemStartedFailed -= _remoteApp_OnSystemStartedFailed;
            _remoteApp.OnSystemStoppedSuccess -= _remoteApp_OnSystemStoppedSuccess;
            _remoteApp.OnSystemStoppedFailed -= _remoteApp_OnSystemStoppedFailed;
            _remoteApp.Stop();
            _remoteApp = null;
        }

        private Task _remoteApp_OnApplicationStop(string instance)
        {
            _logger.LogInformation($"On application stop from {instance}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnAplicationStart(string instance)
        {
            _logger.LogInformation($"On application start from {instance}");

            // configure the sip server
            _logger.LogInformation($"Configuring server instance {instance}");

            ConfigServiceParams serviceParams = new ConfigServiceParams {
                AutoStartSipServer = false,
                ClientUrl = "http://localhost:6060",
                InstanceName = instance
            };
            ConfigSipParams sipParams = new ConfigSipParams
            {
                LocalIpEndPoints = Properties.Settings.Default.LocalIPEndPoints.Cast<string>().ToList(),
                RegisterAccounts = Properties.Settings.Default.RegisterAccounts.Cast<string>().ToList(),
                Codecs = Properties.Settings.Default.DefaultCodecs.Cast<string>().ToList(),
                DtmfMode = _dtmfMode.ToString(),
                AutoAcceptCalls = tbsAutoAnswer.Checked,
                AutoAnswerCalls = tbsAutoAnswer.Checked,
                UseMultiMediaTimers = bool.Parse(Properties.Settings.Default.UseMultiMediaTimers),
            };
            ConfigParams config = new ConfigParams {
                ServiceParams = serviceParams,
                SipParams = sipParams
            };

            ResponseParams resp = _remoteApp.Admin.ApiV1AdminConfigPut(config);

            if (resp.Status != "success") {
                _logger.LogError($"Failed to configure server: {resp.Message}");
                return Task.CompletedTask;
            }

            _logger.LogInformation($"Server instance {instance} configured, now starting...");

            resp = _remoteApp.Admin.ApiV1AdminSystemStartPut();

            if (resp.Status != "success")
            {
                _logger.LogError($"Could not start server: {resp.Message}");
                return Task.CompletedTask;
            }

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnSystemStartedFailed(string instance)
        {
            _logger.LogError($"Failed to start instance {instance}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnSystemStartedSuccess()
        {
            _logger.LogInformation($"Server is now running.");

            tssLabel1.Text = "Running";
            tssIndicator.BackColor = Color.Green;

            tCalls.Start();
            tStats.Start();

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnRegistrationSuccess(string uri)
        {
            _logger.LogInformation($"Registration success for {uri}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallRinging(string callId)
        {
            _logger.LogInformation($"Call ringing for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallRecordError(string callId, string reason, CallInfoParams p)
        {
            _logger.LogError($"Record error {reason} for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallRecordDone(string callId, CallInfoParams p)
        {
            _logger.LogInformation($"Record done for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallPlayRecordDone(string callId, CallInfoParams p)
        {
            _logger.LogInformation($"Play Record done for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallPlayFilesDone(string callId, CallInfoParams p)
        {
            _logger.LogInformation($"PlayFiles done for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallPlayError(string callId, string reason, CallInfoParams p)
        {
            _logger.LogError($"Play error {reason} for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallPlayDone(string callId, CallInfoParams p)
        {
            _logger.LogInformation($"Play done for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallGetDtmfDone(string callId, string digits, CallInfoParams p)
        {
            _logger.LogInformation($"Get dtmf {digits} for {callId}");

            ListViewItem lvi = GetCallsRow(callId);
            lvi.SubItems[5].Text = digits;
            lvCalls.Refresh();

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallFailed(string callId, string reason)
        {
            _logger.LogError($"Call error {reason} for {callId}");

            return Task.CompletedTask;
        }

        private Task RunCallFlow_Scenario1(string callId)
        {
            string[] files = { @"clips\IVR-Sorry_closed.pcm", @"clips\Press1.pcm" };
            ResponseParams resp = _remoteApp.Calls.ApiV1CallsCallPlayfilesPut(new PlayFilesParams(
                files.ToList<string>(),
                100,
                true,
                true,
                "test",
                false),
                callId);

            if (resp.Status == "error")
                return Task.CompletedTask;

            resp = _remoteApp.Calls.ApiV1CallsCallDtmfGetPut(new GetDigitsParams(
                        30 * 1000,
                        1,
                        "1",
                        false,
                        "test",
                        false
                        ),
                        callId);

            if (resp.Status == "error")
                return Task.CompletedTask;

            resp = _remoteApp.Calls.ApiV1CallsCallHoldPut(callId, Properties.Settings.Default.HoldStreamStatus);

            if (resp.Status == "error")
                return Task.CompletedTask;

            resp = _remoteApp.Calls.ApiV1CallsCallTransferBlindPost(new BlindTransferParams(
                tbTransferTo.Text,
                new List<string>(),
                10 * 1000,
                true),
                callId);

            return Task.CompletedTask;
        }

        private async Task RunCallFlow_Scenario2(string callId, SIPURI toURI)
        {
            var filename = Properties.Settings.Default.DefaultPlaybackFile;
            if (File.Exists(@"clips\" + toURI.User + ".wav"))
                filename = @"clips\" + toURI.User + ".wav";

            while (true)
            {
                ListViewItem lvi = GetCallsRow(callId);
                if (lvi.SubItems[4].Text != "Active")
                    break;

                ResponseParams resp = _remoteApp.Calls.ApiV1CallsCallPlayPut(new PlayParams(
                filename,
                true,
                true,
                "test",
                true),
                callId);

                if (resp.Status == "error")
                    return;

                await Task.Delay(500);
            }
        }

        private async Task RunCallFlow(string callId)
        {
            ListViewItem lvi = GetCallsRow(callId);

            _logger.LogInformation($"User: {lvi.SubItems[2].Text}, direction: {lvi.SubItems[3].Text}");

            SIPURI toURI = SIPURI.ParseSIPURI(lvi.SubItems[2].Text);
            SIPCallDirection callDirection = (SIPCallDirection)Enum.Parse(typeof(SIPCallDirection), lvi.SubItems[3].Text);

            foreach (string s in Properties.Settings.Default.RouteTable)
            {
                var entry = s.Split(',');
                var username = entry[0];
                var direction = (SIPCallDirection)Enum.Parse(typeof(SIPCallDirection), entry[1]);
                var scenario = entry[2];

                if ((toURI.User != null) &&
                    (toURI.User.StartsWith(username)) &&
                    (callDirection == direction))
                {
                    if (scenario == "Scenario1")
                    {
                        _logger.LogInformation($"Scenario1 matched.");
                        await RunCallFlow_Scenario1(callId);
                        break;
                    }
                    else if (scenario == "Scenario2")
                    {
                        _logger.LogInformation($"Scenario2 matched.");
                        await RunCallFlow_Scenario2(callId, toURI);
                        break;
                    }
                }
                else if ((toURI.User != null) &&
                    (toURI.User.StartsWith(username)) &&
                    (callDirection == direction))
                {
                    _logger.LogInformation($"Scenario1 matched.");
                    await RunCallFlow_Scenario1(callId);
                }
                else if (scenario == "Scenario2")
                {
                    _logger.LogInformation($"Scenario2 matched.");
                    await RunCallFlow_Scenario2(callId, toURI);
                    break;
                }

            }
        }

        private Task _remoteApp_OnCallDisconnected(string callId, string reason)
        {
            _logger.LogInformation($"Call disconnected with reason {reason} for {callId}");

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallConnected(string callId, CallInfoParams callInfoParams)
        {
            _logger.LogInformation($"Call connected for {callId}, user context {callInfoParams.UserContext}");

            if (tbsAutoRun.Checked)
            {
                RunCallFlow(callId);
            }

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallAnswered(string callId, CallInfoParams callInfoParams)
        {
            _logger.LogInformation($"Call answered for {callId}");

            if (tbsAutoRun.Checked)
            {
                RunCallFlow(callId);
            }

            return Task.CompletedTask;
        }

        private Task _remoteApp_OnCallAccepted(string callId, CallInfoParams callInfoParams)
        {
            _logger.LogInformation($"Call accepted for {callId}");

            return Task.CompletedTask;
        }

        private async Task _remoteApp_OnCallOffered(string callId, CallInfoParams callInfoParams)
        {
            _logger.LogInformation($"Call offered {callInfoParams.From}, {callInfoParams.To} with id {callId}");
            
            try
            {
                _remoteApp.Calls.ApiV1CallsCallAcceptPut(new List<string>(), callId);
            }
            catch (Exception e)
            {
                _logger.LogError($"{e.Message}");
            }

        }

        private Task OnSIPServerStopSuccess(string message)
        {
            _logger.LogInformation($"Server is stopped, {message}");

            return Task.CompletedTask;
        }

        private Task OnSIPServerStopFailed(string message)
        {
            _logger.LogError(message);

            return Task.CompletedTask;
        }

        private Task OnSIPServerStartSuccess(string message)
        {
            _logger.LogInformation(message);

            return Task.CompletedTask;
        }

        private Task OnSIPServerStartFailed(string message)
        {
            _logger.LogError(message);

            return Task.CompletedTask;
        }

        private Task OnRegistrationFailed(SIPURI uri, string reason)
        {
            _logger.LogError($"registration failed for {uri}, reason: {reason}");

            return Task.CompletedTask;
        }

        private Task OnRegistrationTemporaryFailure(SIPURI uri, string reason)
        {
            _logger.LogError($"registration temporary failure for {uri}, reason: {reason}");

            return Task.CompletedTask;
        }

        private Task OnRegistrationSuccess(SIPURI uri)
        {
            _logger.LogInformation($"registration success for {uri}.");

            return Task.CompletedTask;
        }

        private Task OnRegistrationRemoved(SIPURI uri)
        {
            _logger.LogInformation($"registration removed for {uri}.");

            return Task.CompletedTask;
        }

        //private async Task OnCallPlayDone(string callId)
        //{
        //    await _remoteApp.PlayFile(callId, @"clips\Start.wav", true, true);
        //}

        private async Task OnCallDisconnected(string callId, string identity, string reason)
        {
            _logger.LogInformation($"Call disconnected; reason: {reason}");

            try
            {
                ListViewItem lvi = GetCallsRow(callId);
                lvi.Remove();

                tssLabel2.Text = lvCalls.Items.Count + " Calls";
                lvCalls.Refresh();
            }
            catch (Exception e)
            {
                _logger.LogError($"{e.Message}");
            }

            await Task.CompletedTask;
        }

        private async Task OnCallDisconnected(string callId)
        {
            ListViewItem row = GetCallsRow(callId);
            lvCalls.Items.Remove(row);

            await Task.CompletedTask;
        }

        //private async Task OnCallConnected(string callId, CallInfoParams callInfoParams)
        //{
        //    //ListViewItem row = GetCallsRow(callId);
        //    //row.SubItems[1].Text = callInfoParams.from;
        //    //row.SubItems[2].Text = callInfoParams.to;
        //    //row.SubItems[3].Text = callInfoParams.direction.ToString();
        //    //row.SubItems[4].Text = callInfoParams.state.ToString();
        //    //row.SubItems[6].Text = callInfoParams.action.ToString();
        //    //row.SubItems[7].Text = callInfoParams.rtpTx.ToString();
        //    //row.SubItems[8].Text = callInfoParams.rtpRx.ToString();
        //    //row.SubItems[9].Text = callInfoParams.duration;

        //    //await _remoteApp.PlayFile(callId, @"clips\Start.wav", true, true);
        //}

        private async Task OnCallFailed(SIPCall call, string reason)
        {
            _logger.LogError($"Call disconnected; reason: {reason}");

            await Task.CompletedTask;
        }

        private async Task OnCallDiverted(SIPCall call, string reason)
        {
            _logger.LogInformation($"Call was diverted; reason: {reason}");

            await Task.CompletedTask;
        }

        private async Task OnCallRinging(SIPCall call)
        {
            _logger.LogInformation($"Call is ringing");

            await Task.CompletedTask;
        }

        private async Task RunCallFlow_Scenario1(SIPCall call)
        {
            string[] files = { @"clips\IVR-Sorry_closed.pcm" }; //, @"clips\IVR-our_office_will_open_again_at.pcm", @"clips\time-6amto10pm.pcm" };
            await call.PlayFiles(files, 100, true, false);
            //await call.PlayRecord(@"clips\IVR-Please_leave_message_after-the_beep.pcm", @"clips\" + call.Dialogue.CallId.Substring(0, 10) + ".pcm", 30);
            await call.PlayFile(@"clips\Press1.pcm", true, true);
            var digit = await call.GetDigits(30*1000, 1, '1', false);
            //if (digit == "1")
            //{
            //    await call.PlayFile(@"clips\" + call.Dialogue.CallId.Substring(0, 10) + ".pcm", true, true);
            //}
            await call.PlayFile(@"clips\IVR-thankyou_for_calling_us_please_wait_connect_call.pcm", true, false);
            MediaStreamStatusEnum streamStatus = (MediaStreamStatusEnum)Enum.Parse(typeof(MediaStreamStatusEnum), Properties.Settings.Default.HoldStreamStatus);
            call.PutOnHold(streamStatus);
            await call.BlindTransfer(tbTransferTo.Text, 10*1000, true);
        }

        private async Task RunCallFlow_Scenario2(SIPCall call)
        {
            var filename = Properties.Settings.Default.DefaultPlaybackFile;
            if (File.Exists(@"clips\" + call.GetToURI().User + ".wav"))
                filename = @"clips\" + call.GetToURI().User + ".wav";

            call.OnPlayDone += () => call.PlayFile(filename, true, true); 
            _ = call.PlayFile(filename, true, true);
        }

        private async Task RunCallFlow(SIPCall call)
        {

            _logger.LogInformation($"User: {call.GetToURI().User}, direction: {call.GetCallDirection()}");

            foreach (string s in Properties.Settings.Default.RouteTable)
            {
                var entry = s.Split(',');
                var username = entry[0];
                var direction = (SIPCallDirection)Enum.Parse(typeof(SIPCallDirection), entry[1]);
                var scenario = entry[2];

                if ((call.GetToURI().User != null) &&
                    (call.GetToURI().User.StartsWith(username)) &&
                    (call.GetCallDirection() == direction))
                {
                    if (scenario == "Scenario1")
                    {
                        _logger.LogInformation($"Scenario1 matched.");
                        await RunCallFlow_Scenario1(call);
                        break;
                    }
                    else if (scenario == "Scenario2")
                    {
                        _logger.LogInformation($"Scenario2 matched.");
                        await RunCallFlow_Scenario2(call);
                       break;
                    } 
                }
                else if ((call.GetToURI().User != null) && 
                    (call.GetToURI().User.StartsWith(username)) &&
                    (call.GetCallDirection() == direction))
                {
                    _logger.LogInformation($"Scenario1 matched.");
                    await RunCallFlow_Scenario1(call);
                }
                else if (scenario == "Scenario2")
                {
                    _logger.LogInformation($"Scenario2 matched.");
                    await RunCallFlow_Scenario2(call);
                    break;
                }

            }
        }

        private async Task OnCallConnected(SIPCall call)
        {
            try
            {
                if (tbsAutoRun.Checked)
                {
                    if ((call.Dialogue.Direction == SIPCallDirection.Out) && (tbsUsePVD.Checked))
                    {
                        // call progress analysis feature set
                        call.OnPositiveAnswerMachineDetection += () =>
                        {
                            _logger.LogInformation("Not running call flow, because reached an answering machine.");
                            return Task.CompletedTask;
                        };
                        call.OnPositiveVoiceDetection += () =>
                        {
                            return RunCallFlow(call);
                        };
                        call.OnPositiveFaxDetection += () =>
                        {
                            _logger.LogInformation("Not running call flow, because reached a fax machine.");
                            return Task.CompletedTask;
                        };
                        call.OnRingbackToneDetection += () =>
                        {
                            _logger.LogInformation("Not running call flow, because call is still ringing.");
                            return Task.CompletedTask;
                        };
                    } 
                    else
                    {
                        await RunCallFlow(call);
                    }
                }
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message, e);
            }
        }

        private async Task OnCallAccepted(SIPCall call)
        {
            if (tbsAutoAnswer.Checked)
            {
                await call.Answer();
            }
        }

        private async Task OnCallOffered(SIPCall call)
        {
            await call.Accept(3);
        }

        private async void tsbStop_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("Stopping...");

            try
            {
                if (_sipServer != null)
                {
                    await _sipServer.Shutdown();
                    tsbDtmfMode.Enabled = this.tsbStart1.Enabled = this.tsbStart.Enabled = true;
                    this.tsbStop.Enabled = false;
                    tsbNewCall.Enabled = false;
                    tsbNewCall1.Enabled = false;

                    tssLabel1.Text = "Not running";
                    tssIndicator.BackColor = Color.Red;

                    await Task.Delay(1000);

                    tCalls.Stop();
                    tStats.Stop();
                    tLogger.Stop();
                }

                if (isUsingRestApi())
                {
                    _remoteApp.Admin.ApiV1AdminSystemShutdownPut(true);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}");
            }
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            LogBox.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            LogBox.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void LogBox_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            tbOneLog.Text = e.Item.SubItems[2].Text;
        }

        private void tsbNewCall_Click(object sender, EventArgs e)
        {
            if (isUsingRestApi())
            {
                if (tbsUsePVD.Checked)
                    _remoteApp.Calls.ApiV1CallsCallMakePost(new MakeCallParams(tbNewCallTo.Text, tbIdentity.Text,
                        new List<string>(), 10 * 1000, "test", false));
                else
                    _remoteApp.Calls.ApiV1CallsCallMakePost(new MakeCallParams(tbNewCallTo.Text, tbIdentity.Text,
                        new List<string>(), 0, "test", false));
            }
            else
            {
                if (tbsUsePVD.Checked)
                    _ = _sipServer.MakeCall(tbNewCallTo.Text, tbIdentity.Text, 10 * 1000);
                else
                    _ = _sipServer.MakeCall(tbNewCallTo.Text, tbIdentity.Text, 0);
            }
        }

        private async void tsbNewCall1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Text == "1 call")
            {
                if (isUsingRestApi())
                {
                    if (tbsUsePVD.Checked)
                        _remoteApp.Calls.ApiV1CallsCallMakePost(new MakeCallParams(tbNewCallTo.Text, tbIdentity.Text,
                            new List<string>(), 10 * 1000, "test", false));
                    else
                        _remoteApp.Calls.ApiV1CallsCallMakePost(new MakeCallParams(tbNewCallTo.Text, tbIdentity.Text,
                            new List<string>(), 0, "test", false));
                }
                else
                {
                    if (tbsUsePVD.Checked)
                        _ = _sipServer.MakeCall(tbNewCallTo.Text, tbIdentity.Text, 10 * 1000);
                    else
                        _ = _sipServer.MakeCall(tbNewCallTo.Text, tbIdentity.Text, 0);
                }
            }
            else if (e.ClickedItem.Text == "10 call")
            {
                var dest = tbNewCallTo.Text;

                for (int i = 0; i < 10; i++)
                {
                    //var uri = SIPURI.ParseSIPURI(dest);
                    //uri.User = $"{int.Parse(uri.User) + 1}";
                    //dest = uri.ToString();

                    _logger.LogWarning($"making call {i} to {dest}");

                    if (isUsingRestApi())
                    {
                        if (tbsUsePVD.Checked)
                            _remoteApp.Calls.ApiV1CallsCallMakePost(new MakeCallParams(tbNewCallTo.Text, tbIdentity.Text,
                                new List<string>(), 10 * 1000, "test", false));
                        else
                            _remoteApp.Calls.ApiV1CallsCallMakePost(new MakeCallParams(tbNewCallTo.Text, tbIdentity.Text,
                                new List<string>(), 0, "test", false));
                    }
                    else
                    {
                        if (tbsUsePVD.Checked)
                            _ = _sipServer.MakeCall(tbNewCallTo.Text, tbIdentity.Text, 10*1000);
                        else
                            _ = _sipServer.MakeCall(tbNewCallTo.Text, tbIdentity.Text, 0);
                    }
                    await Task.Delay(500);
                }
            }
        }

        private void tsbDropCall_Click(object sender, EventArgs e)
        {
            if (isUsingRestApi())
            {
                foreach (ListViewItem item in lvCalls.SelectedItems)
                {
                    _remoteApp.Calls.ApiV1CallsCallDropDelete(new List<string>(), item.Name);
                }
            }
            else
            {
                foreach (ListViewItem item in lvCalls.SelectedItems)
                {
                    if (_sipServer.GetCall(item.Name).IsCallActive)
                        _sipServer.GetCall(item.Name).Hangup();
                    else if (_sipServer.GetCall(item.Name).IsRinging)
                        _sipServer.GetCall(item.Name).Cancel();
                }
            }
        }

        private void lvCalls_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvCalls.SelectedItems.Count > 0)
            {
                tsbDropCall.Enabled = tbsHoldCall.Enabled = tsbRetrieveCall.Enabled = tsbBlindTransfer.Enabled = tsbAttendedTransfer.Enabled = true;
                tbsPlay.Enabled = tbsRecord.Enabled = tbsSendDigits.Enabled = tbsGetDigits.Enabled = tbsSpeak1.Enabled = tbsMusic.Enabled = tbsStop.Enabled = tbsBridge.Enabled = true;
                btnDig0.Enabled = btnDig1.Enabled = btnDig2.Enabled = btnDig3.Enabled = btnDig4.Enabled = btnDig5.Enabled = true;
                btnDig6.Enabled = btnDig7.Enabled = btnDig8.Enabled = btnDig9.Enabled = btnDigS.Enabled = btnDigP.Enabled = true;
                if (tbsAutoAnswer.Checked)
                    tbsAnswerCall.Enabled = false;
                else
                    tbsAnswerCall.Enabled = true;

            } else
            {
                tsbDropCall.Enabled = tbsHoldCall.Enabled = tsbRetrieveCall.Enabled = tsbBlindTransfer.Enabled = tsbAttendedTransfer.Enabled = false;
                tbsPlay.Enabled = tbsRecord.Enabled = tbsSendDigits.Enabled = tbsGetDigits.Enabled = tbsSpeak1.Enabled = tbsMusic.Enabled = tbsStop.Enabled = tbsBridge.Enabled = false;
                btnDig0.Enabled = btnDig1.Enabled = btnDig2.Enabled = btnDig3.Enabled = btnDig4.Enabled = btnDig5.Enabled = false;
                btnDig6.Enabled = btnDig7.Enabled = btnDig8.Enabled = btnDig9.Enabled = btnDigS.Enabled = btnDigP.Enabled = false;
                tbsAcceptCall.Enabled = tbsAnswerCall.Enabled = false;
            }
        }

        private void tbsSendDigits_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            tbDigitsStr.Text, "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigits(tbDigitsStr.Text);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig1_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "1", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('1');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig2_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "2", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('2');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig3_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "3", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('3');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig4_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "4", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('4');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig5_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "5", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('5');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig6_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "6", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('6');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig7_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "7", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('7');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig8_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "8", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('8');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig9_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "9", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('9');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDig0_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "0", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('0');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDigS_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "*", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('*');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void btnDigP_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfSendPut(new SendDigitsParams(
                            "#", "test", false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).SendDigit('#');
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tsbRetrieveCall_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallRetrievePut(item.Name);
                    }
                    else
                    {
                        _sipServer.GetCall(item.Name).TakeOffHold();
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tsbBlindTransfer_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallTransferBlindPost(new BlindTransferParams(
                            tbTransferTo.Text, new List<string>(), 10*1000, true
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).BlindTransfer(tbTransferTo.Text, 10 * 1000, true);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tsbAttendedTransfer_Click(object sender, EventArgs e)
        {
            if (lvCalls.SelectedItems.Count >= 2)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallTransferAttendedPost(new AttendedTransferParams(
                            lvCalls.SelectedItems[0].Name,
                            lvCalls.SelectedItems[1].Name,
                            new List<string>(),
                            10 * 1000));
                    }
                    else
                    {
                        _ = _sipServer.GetCall(lvCalls.SelectedItems[0].Name).AttendedTransfer(_sipServer.GetCall(lvCalls.SelectedItems[1].Name), 10 * 1000);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            } else
            {
                MessageBox.Show("2 calls needs to be selected to perform an attended transfer!");
            }
        }

        private void tbsPlay_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        if (item.SubItems[4].Text == "Bridged")
                        {
                            ResponseParams resp = _remoteApp.Bridges.Bridges();
                            if (resp.Status == "success")
                            {
                                var bridges = JsonConvert.DeserializeObject<List<BridgeInfoParams>>(resp.Data.ToString());
                                
                                foreach (BridgeInfoParams bridge in bridges)
                                {
                                    if (bridge.ALegCallId == item.Name ||
                                        bridge.BLegCallId == item.Name)
                                    {
                                        _remoteApp.Bridges.ApiV1BridgesBridgePlayPut(new BridgePlayParams(
                                            SIPLeg.Any,
                                            tbPlaybackFile.Text,
                                            true,
                                            true,
                                            false
                                            ), bridge.Id);
                                    }
                                }
                            }
                        } 
                        else
                        {
                            _remoteApp.Calls.ApiV1CallsCallPlayPut(new PlayParams(
                                tbPlaybackFile.Text, 
                                true,
                                true,
                                "test", 
                                false
                                ), item.Name);
                        }
                    }
                    else
                    {
                        if (_sipServer.GetCall(item.Name).GetCallAction() == SeriousSIP.SIPCallAction.Bridged)
                        {
                            if (_sipServer.Bridges.TryGetValue(_sipServer.GetCall(item.Name).BridgeId, out var bridge))
                            {
                                _ = bridge.PlayFile(SIPBridge.SIPLeg.Any, tbPlaybackFile.Text, true, true);
                                break;
                            }
                        }
                        else
                        {
                            _ = _sipServer.GetCall(item.Name).PlayFile(tbPlaybackFile.Text, true, true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tbsRecord_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        if (item.SubItems[4].Text == "Bridged")
                        {
                            ResponseParams resp = _remoteApp.Bridges.Bridges();
                            if (resp.Status == "success")
                            {
                                var bridges = JsonConvert.DeserializeObject<List<BridgeInfoParams>>(resp.Data.ToString());

                                foreach (BridgeInfoParams bridge in bridges)
                                {
                                    if (bridge.ALegCallId == item.Name ||
                                        bridge.BLegCallId == item.Name)
                                    {
                                        _remoteApp.Bridges.ApiV1BridgesBridgeRecordPut(new BridgeRecordParams(
                                            SIPLeg.Any,
                                            tbRecordDir.Text + "output-" + item.Index + ".pcm",
                                            false
                                            ), bridge.Id);
                                    }
                                }
                            }
                        }
                        else
                        {
                            _remoteApp.Calls.ApiV1CallsCallRecordPut(new RecordParams(
                                tbRecordDir.Text + "output-" + item.Index + ".pcm", 
                                30*1000,
                                "#",
                                true,
                                10*1000,
                                "test", 
                                false
                                ), item.Name);
                        }
                    }
                    else
                    {
                        if (_sipServer.GetCall(item.Name).GetCallAction() == SeriousSIP.SIPCallAction.Bridged)
                        {
                            if (_sipServer.Bridges.TryGetValue(_sipServer.GetCall(item.Name).BridgeId, out var bridge))
                            {
                                bridge.RecordFile(SIPBridge.SIPLeg.Any, tbRecordDir.Text + "output-" + item.Index + ".pcm");
                                break;
                            }
                        }
                        else
                        {
                            _ = _sipServer.GetCall(item.Name).RecordFile(tbRecordDir.Text + "output-" + item.Index + ".pcm", 30);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tbsGetDigits_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallDtmfGetPut(new GetDigitsParams(
                            20 * 1000,
                            12,
                            "#",
                            true,
                            "test", 
                            false
                            ), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).GetDigits(20 * 1000, 12, '#', true);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tbsAuto_Click(object sender, EventArgs e)
        {
            tbsAutoAnswer.Checked = !tbsAutoAnswer.Checked;
            lvCalls_SelectedIndexChanged(sender, e);

            if (_sipServer != null)
            {
                _sipServer.AutoAnswerCalls = tbsAutoAnswer.Checked;
            }
        }

        private void tbsAcceptCall_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                if (isUsingRestApi())
                {
                    _remoteApp.Calls.ApiV1CallsCallAcceptPut(new List<string>(), item.Name);
                }
                else
                {
                    _ = _sipServer.GetCall(item.Name).Accept(0);
                }
            }
        }

        private void tbsAnswerCall_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallAnswerPut(new List<string>(), item.Name);
                    }
                    else
                    {
                        _ = _sipServer.GetCall(item.Name).Answer();
                    }
                }
                catch(Exception ex )
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tbsAutoRun_Click(object sender, EventArgs e)
        {
            tbsAutoRun.Checked = !tbsAutoRun.Checked;
        }

        private void tsbAutoScroll_Click(object sender, EventArgs e)
        {
            tsbAutoScroll.Checked = !tsbAutoScroll.Checked;
        }

        private void tsbFilter_Enter(object sender, EventArgs e)
        {
            if (tsbSearch.Text == "Search")
                tsbSearch.Text = "";
        }

        private void tsbFilter_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                for (int i = 0; i < LogBox.Items.Count; i++)
                {
                    if (LogBox.Items[i].SubItems[2].Text.Contains(tsbSearch.Text))
                    {
                        LogBox.Items[i].BackColor = Color.Yellow;
                        LogBox.Items[i].EnsureVisible();
                        tsbClear.Enabled = true;
                    }
                    else
                    {
                        LogBox.Items[i].BackColor = Color.White;
                    }
                }
            }
        }

        private void tsbLogLevel_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            levelSwitch.MinimumLevel = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), ((ToolStripItemClickedEventArgs)e).ClickedItem.Text);
        }

        private void tsbClear_Click(object sender, EventArgs e)
        {
            tsbSearch.Text = "";

            for (int i = 0; i < LogBox.Items.Count; i++)
            {
               LogBox.Items[i].BackColor = Color.White;
            }

            tsbSearch.Text = "Search";
            tsbClear.Enabled = false;
        }

        private void tsbClearLog_Click(object sender, EventArgs e)
        {
            tbOneLog.Clear();
            LogBox.Items.Clear();
        }

        private void tsbDtmfMode_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            _dtmfMode = (SIPCallDtmfMode)Enum.Parse(typeof(SIPCallDtmfMode), e.ClickedItem.Text);
        }

        private void tsbShowErrorsOnly_Click(object sender, EventArgs e)
        {
            tsbShowErrorsOnly.Checked = !tsbShowErrorsOnly.Checked;
        }
        
        private void tbsUsePVD_Click(object sender, EventArgs e)
        {
            tbsUsePVD.Checked = !tbsUsePVD.Checked;
        }

        private void tbsStop_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        if (item.SubItems[4].Text == "Bridged")
                        {
                            ResponseParams resp = _remoteApp.Bridges.Bridges();
                            if (resp.Status == "success")
                            {
                                var bridges = JsonConvert.DeserializeObject<List<BridgeInfoParams>>(resp.Data.ToString());

                                foreach (BridgeInfoParams bridge in bridges)
                                {
                                    if (bridge.ALegCallId == item.Name ||
                                        bridge.BLegCallId == item.Name)
                                    {
                                        _remoteApp.Bridges.ApiV1BridgesBridgePlayStopPut(bridge.Id);
                                    }
                                }
                            }
                        }
                        else
                        {
                            _remoteApp.Calls.ApiV1CallsCallPlayStopPut(item.Name);
                        }
                    }
                    else
                    {
                        if (_sipServer.GetCall(item.Name).GetCallAction() == SeriousSIP.SIPCallAction.Bridged)
                        {
                            if (_sipServer.Bridges.TryGetValue(_sipServer.GetCall(item.Name).BridgeId, out var bridge))
                            {
                                bridge.Terminate(false);
                            }
                        }
                        else
                        {
                            _sipServer.GetCall(item.Name).PlayStop();
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tbsSpeak_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallSpeakssmlPut(new SpeakSsmlParams(
                            File.ReadAllText(@"demo.ssml"),
                            true,
                            true,
                            "test", 
                            false
                            ), item.Name);
                    }
                    else
                    {
                        // can speak text or ssml
                        // _= sipCallServer.GetCall(item.Name).Speak("Welcome to serious SIP.", true, true);
                        _ = _sipServer.GetCall(item.Name).SpeakSsml(File.ReadAllText(@"demo.ssml"), true, true);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tbsMusic_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallPlaymusicPut(item.Name);
                    }
                    else
                    {
                        _sipServer.GetCall(item.Name).PlayMusic();
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tsbLogFile_Click(object sender, EventArgs e)
        {
            Process fileopener = new Process();
            fileopener.StartInfo.FileName = "explorer";
            fileopener.StartInfo.Arguments = $@"logs\log-{DateTime.Now.ToString("yyyyMMdd")}.log";
            fileopener.Start();
        }

        private void tbsHoldCall_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            foreach (ListViewItem item in lvCalls.SelectedItems)
            {
                try
                {
                    if (isUsingRestApi())
                    {
                        _remoteApp.Calls.ApiV1CallsCallHoldPut(item.Name, e.ClickedItem.Text);
                    }
                    else
                    {
                        var streamStatus = (MediaStreamStatusEnum)Enum.Parse(typeof(MediaStreamStatusEnum), e.ClickedItem.Text);
                        _sipServer.GetCall(item.Name).PutOnHold(streamStatus);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}");
                }
            }
        }

        private void tsbStart1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            // make sure the link to bin is existing for this to work
            // from run/, do mklink bin ..\bin\Debug\net5-windows
            File.Copy(e.ClickedItem.Text, Directory.GetCurrentDirectory()+"\\bin\\SIPServerTester.dll.config", true);
            ConfigurationManager.RefreshSection("applicationSettings");
            Properties.Settings.Default.Reload();
            LoadSettings();
            tsbStart_Click(sender, null);
        }

        private void tbsSpeak1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Text == "Input")
            {
                foreach (ListViewItem item in lvCalls.SelectedItems)
                {
                    try
                    {
                        if (isUsingRestApi())
                        {
                            if (tbSpeak.Text != "")
                                _remoteApp.Calls.ApiV1CallsCallSpeakPut(new SpeakParams(
                                    tbSpeak.Text,
                                    true,
                                    true,
                                    "test", 
                                    false),
                                    item.Name);
                        }
                        else
                        {
                            if (tbSpeak.Text != "")
                                _ = _sipServer.GetCall(item.Name).Speak(tbSpeak.Text, true, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"{ex.Message}");
                    }
                }
            }
            else
            {
                foreach (ListViewItem item in lvCalls.SelectedItems)
                {
                    try
                    {
                        if (isUsingRestApi())
                        {
                            _remoteApp.Calls.ApiV1CallsCallSpeakssmlPut(new SpeakSsmlParams(
                              File.ReadAllText(@"demo.ssml"),
                              true,
                              true,
                              "test", 
                              false
                              ), item.Name);
                        }
                        else
                        {
                            _ = _sipServer.GetCall(item.Name).SpeakSsml(File.ReadAllText(@"demo.ssml"), true, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"{ex.Message}");
                    }
                }
            }
        }

        private void tbsBridge_Click(object sender, EventArgs e)
        {
            if (lvCalls.SelectedItems.Count != 2)
            {
                MessageBox.Show("Select 2 calls to create a bridge.");
                return;
            }

            try
            {
                if (isUsingRestApi())
                {
                    _remoteApp.Bridges.ApiV1BridgesBridgePost(new BridgeCreateParams(
                      lvCalls.SelectedItems[0].Name,
                      lvCalls.SelectedItems[1].Name,
                      false
                      ));
                }
                else
                {
                    List<SIPCall> calls = new List<SIPCall>();
                    foreach (ListViewItem item in lvCalls.SelectedItems)
                    {
                        calls.Add(_sipServer.GetCall(item.Name));
                    }
                    _sipServer.CreateBridge(calls[0], calls[1]);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}");
            }
            
        }
    }
}
